package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;



public class RequestPrivateStoreQuitBuy
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (!player.isInStoreMode() || player.getPrivateStoreType() != 3) {
      
      player.sendActionFailed();
      
      return;
    } 
    player.setPrivateStoreType(0);
  }
}
