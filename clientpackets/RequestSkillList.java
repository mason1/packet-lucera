package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.SkillList;





public final class RequestSkillList
  extends L2GameClientPacket
{
  private static final String ZZ = "[C] 50 RequestSkillList";
  
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    
    if (player != null) {
      player.sendPacket(new SkillList(player));
    }
  }


  
  public String getType() { return "[C] 50 RequestSkillList"; }
}
