package l2.gameserver.network.l2.c2s;

import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExShowCastleInfo;




public class RequestAllCastleInfo
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() { ((GameClient)getClient()).getActiveChar().sendPacket(new ExShowCastleInfo()); }
}
