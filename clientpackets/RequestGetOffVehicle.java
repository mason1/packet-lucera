package l2.gameserver.network.l2.c2s;

import l2.gameserver.data.BoatHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.boat.Boat;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.utils.Location;

public class RequestGetOffVehicle
  extends L2GameClientPacket {
  private int Bq;
  private Location Bd = new Location();


  
  protected void readImpl() {
    this.Bq = readD();
    this.Bd.x = readD();
    this.Bd.y = readD();
    this.Bd.z = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Boat boat = BoatHolder.getInstance().getBoat(this.Bq);
    if (boat == null || boat.isMoving()) {
      
      player.sendActionFailed();
      
      return;
    } 
    boat.oustPlayer(player, this.Bd, false);
  }
}
