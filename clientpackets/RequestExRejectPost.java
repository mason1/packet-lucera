package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.dao.MailDAO;
import l2.gameserver.model.Player;
import l2.gameserver.model.World;
import l2.gameserver.model.mail.Mail;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExNoticePostArrived;
import l2.gameserver.network.l2.s2c.ExShowReceivedPostList;









public class RequestExRejectPost
  extends L2GameClientPacket
{
  private int YI;
  
  protected void readImpl() { this.YI = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.YOU_CANNOT_CANCEL_BECAUSE_THE_PRIVATE_SHOP_OR_WORKSHOP_IS_IN_PROGRESS);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendPacket(Msg.YOU_CANNOT_CANCEL_DURING_AN_EXCHANGE);
      
      return;
    } 
    if (player.getEnchantScroll() != null) {
      
      player.sendPacket(Msg.YOU_CANNOT_CANCEL_DURING_AN_ITEM_ENHANCEMENT_OR_ATTRIBUTE_ENHANCEMENT);
      
      return;
    } 
    if (!player.isInPeaceZone()) {
      
      player.sendPacket(Msg.YOU_CANNOT_CANCEL_IN_A_NON_PEACE_ZONE_LOCATION);
      
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
      
      return;
    } 
    Mail mail = MailDAO.getInstance().getReceivedMailByMailId(player.getObjectId(), this.YI);
    if (mail != null) {
      
      if (mail.getType() != Mail.SenderType.NORMAL || mail.getAttachments().isEmpty()) {
        
        player.sendActionFailed();
        
        return;
      } 
      int i = 1296000 + (int)(System.currentTimeMillis() / 1000L);
      
      Mail mail1 = mail.reject();
      MailDAO.getInstance().deleteReceivedMailByMailId(mail.getReceiverId(), mail.getMessageId());
      MailDAO.getInstance().deleteSentMailByMailId(mail.getReceiverId(), mail.getMessageId());
      mail1.setExpireTime(i);
      mail1.save();
      
      Player player1 = World.getPlayer(mail1.getReceiverId());
      if (player1 != null) {
        player1.sendPacket(ExNoticePostArrived.STATIC_TRUE);
      }
    } 
    player.sendPacket(new ExShowReceivedPostList(player));
  }
}
