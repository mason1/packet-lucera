package l2.gameserver.network.l2.c2s;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import l2.commons.lang.ArrayUtils;
import l2.gameserver.Config;
import l2.gameserver.cache.ItemInfoCache;
import l2.gameserver.cache.Msg;
import l2.gameserver.handler.voicecommands.IVoicedCommandHandler;
import l2.gameserver.handler.voicecommands.VoicedCommandHandler;
import l2.gameserver.instancemanager.PetitionManager;
import l2.gameserver.model.GameObjectsStorage;
import l2.gameserver.model.Player;
import l2.gameserver.model.World;
import l2.gameserver.model.chat.ChatFilters;
import l2.gameserver.model.chat.chatfilter.ChatFilter;
import l2.gameserver.model.chat.chatfilter.ChatMsg;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.matching.MatchingRoom;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.ChatType;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.network.l2.s2c.Say2;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.utils.Log;
import l2.gameserver.utils.MapUtils;
import l2.gameserver.utils.Strings;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Say2C
  extends L2GameClientPacket {
  private static final Logger _log = LoggerFactory.getLogger(Say2C.class);

  
  private static final Pattern aai = Pattern.compile("[\b]\tType=[0-9]+[\\s]+\tID=([0-9]+)[\\s]+\tColor=[0-9]+[\\s]+\tUnderline=[0-9]+[\\s]+\tClassID=[0-9]+[\\s]+\tTitle=\033(.[^\033]*)[^\b]");
  private static final Pattern aaj = Pattern.compile("[\b]\tType=[0-9]+(.[^\b]*)[\b]");
  
  private String aak;
  
  private ChatType aal;
  
  private String Zi;
  
  protected void readImpl() {
    this.aak = readS(Config.CHAT_MESSAGE_MAX_LEN);
    this.aal = (ChatType)ArrayUtils.valid(ChatType.VALUES, readD());
    this.Zi = (this.aal == ChatType.TELL) ? readS(Config.CNAME_MAXLEN) : null;
  }
  
  protected void runImpl() {
    MatchingRoom matchingRoom2, matchingRoom1;
    List list;
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    if (this.aal == null || this.aak == null || this.aak.length() == 0) {
      
      player1.sendActionFailed();
      
      return;
    } 
    this.aak = this.aak.replaceAll("\\\\n", "\n");
    
    if (this.aak.contains("\n")) {
      
      String[] arrayOfString = this.aak.split("\n");
      this.aak = "";
      for (byte b = 0; b < arrayOfString.length; b++) {
        
        arrayOfString[b] = arrayOfString[b].trim();
        if (arrayOfString[b].length() != 0) {
          
          if (this.aak.length() > 0)
            this.aak += "\n  >"; 
          this.aak += arrayOfString[b];
        } 
      } 
    } 
    if (this.aak.length() == 0) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (this.aak.startsWith(".")) {
      
      String str1 = this.aak.substring(1).trim();
      String str2 = str1.split("\\s+")[0];
      String str3 = str1.substring(str2.length()).trim();
      
      if (str2.length() > 0) {

        
        IVoicedCommandHandler iVoicedCommandHandler = VoicedCommandHandler.getInstance().getVoicedCommandHandler(str2);
        if (iVoicedCommandHandler != null) {
          
          iVoicedCommandHandler.useVoicedCommand(str2, player1, str3);
          return;
        } 
      } 
      player1.sendMessage(new CustomMessage("common.command404", player1, new Object[0]));
      
      return;
    } 
    Player player2 = (this.Zi == null) ? null : World.getPlayer(this.Zi);
    
    long l = System.currentTimeMillis();

    
    if (!(player1.getPlayerAccess()).CanAnnounce)
    {
      for (ChatFilter chatFilter : ChatFilters.getinstance().getFilters()) {
        
        if (chatFilter.isMatch(player1, this.aal, this.aak, player2))
        {
          switch (chatFilter.getAction()) {
            
            case 1:
              player1.updateNoChannel(Integer.parseInt(chatFilter.getValue()) * 1000L);
              break;
            case 2:
              player1.sendMessage(new CustomMessage(chatFilter.getValue(), player1, new Object[0]));
              return;
            case 3:
              this.aak = chatFilter.getValue();
              break;
            case 4:
              this.aal = ChatType.valueOf(chatFilter.getValue());
              break;
          } 

        
        }
      } 
    }
    if (player1.getNoChannel() > 0L)
    {
      if (ArrayUtils.contains(Config.BAN_CHANNEL_LIST, this.aal)) {
        
        if (player1.getNoChannelRemained() > 0L) {
          
          long l1 = player1.getNoChannelRemained() / 60000L;
          player1.sendMessage((new CustomMessage("common.ChatBanned", player1, new Object[0])).addNumber(l1));
          return;
        } 
        player1.updateNoChannel(0L);
      } 
    }
    
    if (this.aak.isEmpty()) {
      return;
    }
    
    Matcher matcher = aai.matcher(this.aak);


    
    while (matcher.find()) {
      
      int i = Integer.parseInt(matcher.group(1));
      ItemInstance itemInstance = player1.getInventory().getItemByObjectId(i);
      
      if (itemInstance == null) {
        
        player1.sendActionFailed();
        
        break;
      } 
      ItemInfoCache.getInstance().put(itemInstance);
    } 
    
    String str = player1.getVar("translit");
    if (str != null) {

      
      matcher = aaj.matcher(this.aak);
      StringBuilder stringBuilder = new StringBuilder();
      int i = 0;
      while (matcher.find()) {
        
        stringBuilder.append(Strings.fromTranslit(this.aak.substring(i, i = matcher.start()), str.equals("tl") ? 1 : 2));
        stringBuilder.append(this.aak.substring(i, i = matcher.end()));
      } 
      
      this.aak = stringBuilder.append(Strings.fromTranslit(this.aak.substring(i, this.aak.length()), str.equals("tl") ? 1 : 2)).toString();
    } 
    
    Say2 say2 = new Say2(player1.getObjectId(), this.aal, player1.getName(), this.aak);
    
    switch (this.aal) {
      
      case TELL:
        if (player2 == null) {
          
          player1.sendPacket((new SystemMessage(3)).addString(this.Zi));
          return;
        } 
        if (player2.isInOfflineMode()) {
          
          player1.sendMessage(new CustomMessage("common.PlayerInOfflineTrade", player1, new Object[0]));
          return;
        } 
        if (player2.isInBlockList(player1) || player2.isBlockAll()) {
          
          player1.sendPacket(Msg.YOU_HAVE_BEEN_BLOCKED_FROM_THE_CONTACT_YOU_SELECTED);
          return;
        } 
        if (player2.getMessageRefusal()) {
          
          player1.sendPacket(Msg.THE_PERSON_IS_IN_A_MESSAGE_REFUSAL_MODE);
          
          return;
        } 
        if (player1.canTalkWith(player2)) {
          player2.sendPacket(say2);
        }
        say2 = new Say2(player1.getObjectId(), this.aal, "->" + player2.getName(), this.aak);
        player1.sendPacket(say2);
        break;
      case SHOUT:
        if (player1.isCursedWeaponEquipped()) {
          
          player1.sendMessage(new CustomMessage("SHOUT_AND_TRADE_CHATING_CANNOT_BE_USED_SHILE_POSSESSING_A_CURSED_WEAPON", player1, new Object[0]));
          return;
        } 
        if (player1.isInObserverMode()) {
          
          player1.sendPacket(Msg.YOU_CANNOT_CHAT_LOCALLY_WHILE_OBSERVING);
          
          return;
        } 
        if (Config.GLOBAL_SHOUT && player1.getLevel() > Config.GLOBAL_SHOUT_MIN_LEVEL && player1.getPvpKills() >= Config.GLOBAL_SHOUT_MIN_PVP_COUNT) {
          b(player1, say2);
        } else {
          a(player1, say2);
        } 
        player1.sendPacket(say2);
        break;
      case TRADE:
        if (player1.isCursedWeaponEquipped()) {
          
          player1.sendMessage(new CustomMessage("SHOUT_AND_TRADE_CHATING_CANNOT_BE_USED_SHILE_POSSESSING_A_CURSED_WEAPON", player1, new Object[0]));
          return;
        } 
        if (player1.isInObserverMode()) {
          
          player1.sendPacket(Msg.YOU_CANNOT_CHAT_LOCALLY_WHILE_OBSERVING);
          
          return;
        } 
        if (Config.GLOBAL_TRADE_CHAT && player1.getLevel() > Config.GLOBAL_TRADE_CHAT_MIN_LEVEL && player1.getPvpKills() >= Config.GLOBAL_TRADE_MIN_PVP_COUNT) {
          b(player1, say2);
        } else {
          a(player1, say2);
        } 
        player1.sendPacket(say2);
        break;
      case ALL:
        if (player1.isCursedWeaponEquipped()) {
          say2 = new Say2(player1.getObjectId(), this.aal, player1.getTransformationName(), this.aak);
        }
        list = null;
        
        list = World.getAroundPlayers(player1);
        
        if (list != null) {
          for (Player player : list) {
            
            if (player == player1 || player.getReflection() != player1.getReflection() || player.isBlockAll() || player.isInBlockList(player1))
              continue; 
            player.sendPacket(say2);
          } 
        }
        player1.sendPacket(say2);
        break;
      case CLAN:
        if (player1.getClan() != null)
          player1.getClan().broadcastToOnlineMembers(new L2GameServerPacket[] { say2 }); 
        break;
      case ALLIANCE:
        if (player1.getClan() != null && player1.getClan().getAlliance() != null)
          player1.getClan().getAlliance().broadcastToOnlineMembers(say2); 
        break;
      case PARTY:
        if (player1.isInParty())
          player1.getParty().broadCast(new IStaticPacket[] { say2 }); 
        break;
      case PARTY_ROOM:
        matchingRoom1 = player1.getMatchingRoom();
        if (matchingRoom1 != null && matchingRoom1.getType() == MatchingRoom.PARTY_MATCHING)
          matchingRoom1.broadCast(new IStaticPacket[] { say2 }); 
        break;
      case COMMANDCHANNEL_ALL:
        if (!player1.isInParty() || !player1.getParty().isInCommandChannel()) {
          
          player1.sendPacket(Msg.YOU_DO_NOT_HAVE_AUTHORITY_TO_USE_THE_COMMAND_CHANNEL);
          return;
        } 
        if (player1.getParty().getCommandChannel().getChannelLeader() == player1) {
          player1.getParty().getCommandChannel().broadCast(new IStaticPacket[] { say2 }); break;
        } 
        player1.sendPacket(Msg.ONLY_CHANNEL_OPENER_CAN_GIVE_ALL_COMMAND);
        break;
      case COMMANDCHANNEL_COMMANDER:
        if (!player1.isInParty() || !player1.getParty().isInCommandChannel()) {
          
          player1.sendPacket(Msg.YOU_DO_NOT_HAVE_AUTHORITY_TO_USE_THE_COMMAND_CHANNEL);
          return;
        } 
        if (player1.getParty().isLeader(player1)) {
          player1.getParty().getCommandChannel().broadcastToChannelPartyLeaders(say2); break;
        } 
        player1.sendPacket(Msg.ONLY_A_PARTY_LEADER_CAN_ACCESS_THE_COMMAND_CHANNEL);
        break;
      case HERO_VOICE:
        if (player1.isHero() || (player1.getPlayerAccess()).CanAnnounce || (player1.getPvpKills() >= Config.PVP_COUNT_TO_CHAT && Config.PVP_HERO_CHAT_SYSTEM))
        {
          for (Player player : GameObjectsStorage.getAllPlayersForIterate()) {
            if (!player.isInBlockList(player1) && !player.isBlockAll())
              player.sendPacket(say2); 
          }  } 
        break;
      case PETITION_PLAYER:
      case PETITION_GM:
        if (!PetitionManager.getInstance().isPlayerInConsultation(player1)) {
          
          player1.sendPacket(new SystemMessage(745));
          
          return;
        } 
        PetitionManager.getInstance().sendActivePetitionMessage(player1, this.aak);
        break;
      case BATTLEFIELD:
        if (player1.getBattlefieldChatId() == 0) {
          return;
        }
        for (Player player : GameObjectsStorage.getAllPlayersForIterate()) {
          if (!player.isInBlockList(player1) && !player.isBlockAll() && player.getBattlefieldChatId() == player1.getBattlefieldChatId())
            player.sendPacket(say2); 
        }  break;
      case MPCC_ROOM:
        matchingRoom2 = player1.getMatchingRoom();
        if (matchingRoom2 != null && matchingRoom2.getType() == MatchingRoom.CC_MATCHING)
          matchingRoom2.broadCast(new IStaticPacket[] { say2 }); 
        break;
      default:
        _log.warn("Character " + player1.getName() + " used unknown chat type: " + this.aal.ordinal() + ".");
        break;
    } 
    Log.LogChat(this.aal.name(), player1.getName(), this.Zi, this.aak, 0);
    player1.getMessageBucket().addLast(new ChatMsg(this.aal, (player2 == null) ? 0 : player2.getObjectId(), this.aak.hashCode(), (int)(l / 1000L)));
    
    player1.getListeners().onSay(this.aal.ordinal(), this.Zi, this.aak);
  }

  
  private static void a(Player paramPlayer, Say2 paramSay2) {
    int i = MapUtils.regionX(paramPlayer);
    int j = MapUtils.regionY(paramPlayer);
    int k = Config.SHOUT_OFFSET;
    
    for (Player player : GameObjectsStorage.getAllPlayersForIterate()) {
      
      if (player == paramPlayer || paramPlayer.getReflection() != player.getReflection() || player.isBlockAll() || player.isInBlockList(paramPlayer)) {
        continue;
      }
      int m = MapUtils.regionX(player);
      int n = MapUtils.regionY(player);
      
      if ((m >= i - k && m <= i + k && n >= j - k && n <= j + k) || paramPlayer.isInRangeZ(player, Config.CHAT_RANGE)) {
        player.sendPacket(paramSay2);
      }
    } 
  }
  
  private static void b(Player paramPlayer, Say2 paramSay2) {
    for (Player player : GameObjectsStorage.getAllPlayersForIterate()) {
      
      if (player == paramPlayer || paramPlayer.getReflection() != player.getReflection() || player.isBlockAll() || player.isInBlockList(paramPlayer)) {
        continue;
      }
      player.sendPacket(paramSay2);
    } 
  }
}
