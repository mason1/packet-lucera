package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.handler.items.IRefineryHandler;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.s2c.ExPutCommissionResultForVariationMake;


public class RequestConfirmGemStone
  extends L2GameClientPacket
{
  private static final int Yo = 2130;
  private static final int Yp = 2131;
  private static final int Yq = 2132;
  private int Yr;
  private int Ys;
  private int Yt;
  private long Yu;
  
  protected void readImpl() {
    this.Yr = readD();
    this.Ys = readD();
    this.Yt = readD();
    this.Yu = readD();
  }


  
  protected void runImpl() {
    if (this.Yu <= 0L) {
      return;
    }
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    ItemInstance itemInstance1 = player.getInventory().getItemByObjectId(this.Yr);
    ItemInstance itemInstance2 = player.getInventory().getItemByObjectId(this.Ys);
    ItemInstance itemInstance3 = player.getInventory().getItemByObjectId(this.Yt);
    IRefineryHandler iRefineryHandler = player.getRefineryHandler();
    
    if (itemInstance1 == null || itemInstance2 == null || itemInstance3 == null || iRefineryHandler == null) {
      
      player.sendPacket(new IStaticPacket[] { Msg.THIS_IS_NOT_A_SUITABLE_ITEM, ExPutCommissionResultForVariationMake.FAIL_PACKET });
      return;
    } 
    iRefineryHandler.onPutGemstoneItem(player, itemInstance1, itemInstance2, itemInstance3, this.Yu);
  }
}
