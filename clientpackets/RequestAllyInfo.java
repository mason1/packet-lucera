package l2.gameserver.network.l2.c2s;

import java.util.ArrayList;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.tables.ClanTable;






public class RequestAllyInfo
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Alliance alliance = player.getAlliance();
    if (alliance == null) {
      return;
    }
    int i = 0;
    Clan clan = player.getAlliance().getLeader();
    i = ClanTable.getInstance().getAlliance(clan.getAllyId()).getMembers().length;
    int[] arrayOfInt1 = new int[i + 1];
    int[] arrayOfInt2 = new int[i + 1];
    Clan[] arrayOfClan = player.getAlliance().getMembers();
    for (byte b1 = 0; b1 < i; b1++) {
      
      arrayOfInt1[b1 + true] = arrayOfClan[b1].getOnlineMembers(0).size();
      arrayOfInt2[b1 + true] = arrayOfClan[b1].getAllSize();
      arrayOfInt1[0] = arrayOfInt1[0] + arrayOfInt1[b1 + true];
      arrayOfInt2[0] = arrayOfInt2[0] + arrayOfInt2[b1 + true];
    } 
    
    ArrayList arrayList = new ArrayList(7 + 5 * i);
    arrayList.add(Msg._ALLIANCE_INFORMATION_);
    arrayList.add((new SystemMessage(492)).addString(player.getClan().getAlliance().getAllyName()));
    arrayList.add((new SystemMessage(493)).addNumber(arrayOfInt1[0]).addNumber(arrayOfInt2[0]));
    arrayList.add((new SystemMessage(494)).addString(clan.getName()).addString(clan.getLeaderName()));
    arrayList.add((new SystemMessage(495)).addNumber(i));
    arrayList.add(Msg._CLAN_INFORMATION_);
    for (byte b2 = 0; b2 < i; b2++) {
      
      arrayList.add((new SystemMessage(497)).addString(arrayOfClan[b2].getName()));
      arrayList.add((new SystemMessage(498)).addString(arrayOfClan[b2].getLeaderName()));
      arrayList.add((new SystemMessage(499)).addNumber(arrayOfClan[b2].getLevel()));
      arrayList.add((new SystemMessage(493)).addNumber(arrayOfInt1[b2 + true]).addNumber(arrayOfInt2[b2 + true]));
      arrayList.add(Msg.__DASHES__);
    } 
    arrayList.add(Msg.__EQUALS__);
    
    player.sendPacket(arrayList);
  }
}
