package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.handler.items.IRefineryHandler;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.s2c.ExPutIntensiveResultForVariationMake;

public class RequestConfirmRefinerItem
  extends L2GameClientPacket
{
  private int Yr;
  private int Ys;
  
  protected void readImpl() {
    this.Yr = readD();
    this.Ys = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    ItemInstance itemInstance1 = player.getInventory().getItemByObjectId(this.Yr);
    ItemInstance itemInstance2 = player.getInventory().getItemByObjectId(this.Ys);
    IRefineryHandler iRefineryHandler = player.getRefineryHandler();
    
    if (itemInstance1 == null || itemInstance2 == null || iRefineryHandler == null) {
      
      player.sendPacket(new IStaticPacket[] { Msg.THIS_IS_NOT_A_SUITABLE_ITEM, ExPutIntensiveResultForVariationMake.FAIL_PACKET });
      return;
    } 
    iRefineryHandler.onPutMineralItem(player, itemInstance1, itemInstance2);
  }
}
