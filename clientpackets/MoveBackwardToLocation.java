package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.ActionFail;
import l2.gameserver.network.l2.s2c.CharMoveToLocation;
import l2.gameserver.utils.Location;

public class MoveBackwardToLocation
  extends L2GameClientPacket
{
  private Location XB = new Location();
  private Location XC = new Location();


  
  private int XD;


  
  protected void readImpl() {
    this.XB.x = readD();
    this.XB.y = readD();
    this.XB.z = readD();
    this.XC.x = readD();
    this.XC.y = readD();
    this.XC.z = readD();
    if (this._buf.hasRemaining()) {
      this.XD = readD();
    }
  }

  
  protected void runImpl() {
    GameClient gameClient = (GameClient)getClient();
    if (gameClient == null)
      return; 
    Player player = gameClient.getActiveChar();
    if (player == null) {
      return;
    }
    player.setActive();
    
    if (player.isTeleporting()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isFrozen()) {
      
      player.sendPacket(new IStaticPacket[] { SystemMsg.YOU_CANNOT_MOVE_WHILE_FROZEN, ActionFail.STATIC });
      
      return;
    } 
    if (player.isOlyObserver()) {
      
      if (player.getOlyObservingStadium().getObservingLoc().distance(this.XB) < 8192.0D) {
        player.sendPacket(new CharMoveToLocation(player.getObjectId(), this.XC, this.XB));
      } else {
        player.sendActionFailed();
      } 
      return;
    } 
    if (player.isOutOfControl()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (Config.ALT_ALLOW_DELAY_NPC_TALK)
    {
      if (!player.canMoveAfterInteraction()) {
        
        player.sendMessage(new CustomMessage("YOU_CANNOT_MOVE_WHILE_SPEAKING_TO_AN_NPC__ONE_MOMENT_PLEASE", player, new Object[0]));
        player.sendActionFailed();
        
        return;
      } 
    }
    if (player.getTeleMode() > 0) {
      
      if (player.getTeleMode() == 1)
        player.setTeleMode(0); 
      player.sendActionFailed();
      player.teleToLocation(this.XB);
      
      return;
    } 
    if (player.isInFlyingTransform()) {
      this.XB.z = Math.min(5950, Math.max(50, this.XB.z));
    }
    
    player.moveBackwardToLocationForPacket(this.XB, (this.XD != 0 && !player.getVarB("no_pf")));
  }
}
