package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.utils.ItemFunctions;
import l2.gameserver.utils.Log;






public class RequestCrystallizeItem
  extends L2GameClientPacket
{
  private int Bq;
  private long Yw;
  
  protected void readImpl() {
    this.Bq = readD();
    this.Yw = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    
    if (player == null) {
      return;
    }
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    ItemInstance itemInstance = player.getInventory().getItemByObjectId(this.Bq);
    if (itemInstance == null) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (itemInstance.isHeroWeapon()) {
      
      player.sendPacket(Msg.HERO_WEAPONS_CANNOT_BE_DESTROYED);
      
      return;
    } 
    if (!itemInstance.canBeCrystallized(player)) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    int i = itemInstance.getTemplate().getCrystalCount();
    int j = (itemInstance.getTemplate().getCrystalType()).cry;

    
    int k = player.getSkillLevel(Integer.valueOf(248));
    if (k < 1 || j - 1458 + 1 > k) {
      
      player.sendPacket(Msg.CANNOT_CRYSTALLIZE_CRYSTALLIZATION_SKILL_LEVEL_TOO_LOW);
      player.sendActionFailed();
      
      return;
    } 
    Log.LogItem(player, Log.ItemLog.Crystalize, itemInstance);
    
    if (!player.getInventory().destroyItemByObjectId(this.Bq, 1L)) {
      
      player.sendActionFailed();
      
      return;
    } 
    player.sendPacket(Msg.THE_ITEM_HAS_BEEN_SUCCESSFULLY_CRYSTALLIZED);
    ItemFunctions.addItem(player, j, i, true);
    player.sendChanges();
  }
}
