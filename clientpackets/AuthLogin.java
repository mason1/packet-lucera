package l2.gameserver.network.l2.c2s;

import l2.gameserver.Shutdown;
import l2.gameserver.network.authcomm.AuthServerCommunication;
import l2.gameserver.network.authcomm.SessionKey;
import l2.gameserver.network.authcomm.gs2as.PlayerAuthRequest;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.LoginFail;
import l2.gameserver.network.l2.s2c.ServerClose;








public class AuthLogin
  extends L2GameClientPacket
{
  private String Xl;
  private int Xm;
  private int Xn;
  private int Xo;
  private int Xp;
  private int Xq;
  private long Xr;
  private int Xs;
  
  protected void readImpl() {
    this.Xl = readS(32).toLowerCase();
    this.Xn = readD();
    this.Xm = readD();
    this.Xo = readD();
    this.Xp = readD();
    this.Xq = readD();
    this.Xr = readQ();
    this.Xs = readD();
  }


  
  protected void runImpl() {
    GameClient gameClient = (GameClient)getClient();
    
    SessionKey sessionKey = new SessionKey(this.Xo, this.Xp, this.Xm, this.Xn);
    gameClient.setSessionId(sessionKey);
    gameClient.setLoginName(this.Xl);
    
    if (gameClient.getRevision() == 0 || (Shutdown.getInstance().getMode() != -1 && Shutdown.getInstance().getSeconds() <= 15)) {
      gameClient.closeNow(false);
    } else {
      
      if (AuthServerCommunication.getInstance().isShutdown()) {
        
        gameClient.close(new LoginFail(LoginFail.SYSTEM_ERROR_LOGIN_LATER));
        
        return;
      } 
      GameClient gameClient1 = AuthServerCommunication.getInstance().addWaitingClient(gameClient);
      if (gameClient1 != null) {
        gameClient1.close(ServerClose.STATIC);
      }
      AuthServerCommunication.getInstance().sendPacket(new PlayerAuthRequest(gameClient));
    } 
  }
}
