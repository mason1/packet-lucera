package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.model.World;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.FriendAddRequest;
import l2.gameserver.network.l2.s2c.SystemMessage2;
import org.apache.commons.lang3.StringUtils;


public class RequestFriendInvite
  extends L2GameClientPacket
{
  private String _name;
  
  protected void readImpl() { this._name = readS(16); }



  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null || StringUtils.isEmpty(this._name)) {
      return;
    }
    if (player1.isOutOfControl()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isProcessingRequest()) {
      
      player1.sendPacket(SystemMsg.WAITING_FOR_ANOTHER_REPLY);
      
      return;
    } 
    Player player2 = World.getPlayer(this._name);
    if (player2 == null) {
      
      player1.sendPacket(SystemMsg.THE_USER_WHO_REQUESTED_TO_BECOME_FRIENDS_IS_NOT_FOUND_IN_THE_GAME);
      
      return;
    } 
    if (player2 == player1) {
      
      player1.sendPacket(SystemMsg.YOU_CANNOT_ADD_YOURSELF_TO_YOUR_OWN_FRIEND_LIST);
      
      return;
    } 
    if (player2.isBlockAll() || player2.isInBlockList(player1) || player2.getMessageRefusal()) {
      
      player1.sendPacket(SystemMsg.THAT_PERSON_IS_IN_MESSAGE_REFUSAL_MODE);
      
      return;
    } 
    if (player1.getFriendList().getList().containsKey(Integer.valueOf(player2.getObjectId()))) {
      
      player1.sendPacket((new SystemMessage2(SystemMsg.C1_IS_ALREADY_ON_YOUR_FRIEND_LIST)).addName(player2));
      
      return;
    } 
    if (player1.getFriendList().getList().size() >= 128) {
      
      player1.sendPacket(SystemMsg.YOU_CAN_ONLY_ENTER_UP_128_NAMES_IN_YOUR_FRIENDS_LIST);
      
      return;
    } 
    if (player2.getFriendList().getList().size() >= 128) {
      
      player1.sendPacket(SystemMsg.THE_FRIENDS_LIST_OF_THE_PERSON_YOU_ARE_TRYING_TO_ADD_IS_FULL_SO_REGISTRATION_IS_NOT_POSSIBLE);
      
      return;
    } 
    if (player2.isOlyParticipant()) {
      
      player1.sendPacket(SystemMsg.A_USER_CURRENTLY_PARTICIPATING_IN_THE_OLYMPIAD_CANNOT_SEND_PARTY_AND_FRIEND_INVITATIONS);
      
      return;
    } 
    (new Request(Request.L2RequestType.FRIEND, player1, player2)).setTimeout(10000L);
    
    player1.sendPacket((new SystemMessage2(SystemMsg.C1_HAS_SENT_A_FRIEND_REQUEST)).addName(player2));
    player2.sendPacket(new IStaticPacket[] { (new SystemMessage2(SystemMsg.C1_HAS_SENT_A_FRIEND_REQUEST)).addName(player1), new FriendAddRequest(player1.getName()) });
  }
}
