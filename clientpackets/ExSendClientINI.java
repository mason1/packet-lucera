package l2.gameserver.network.l2.c2s;


public class ExSendClientINI
  extends L2GameClientPacket
{
  private int Xx;
  private byte[] Xy;
  
  protected void readImpl() {
    this.Xx = readC();
    this.Xy = new byte[readH()];
    readB(this.Xy);
  }
  
  protected void runImpl() {}
}
