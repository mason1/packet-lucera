package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.model.World;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.AskJoinParty;
import l2.gameserver.network.l2.s2c.SystemMessage2;



public class RequestJoinParty
  extends L2GameClientPacket
{
  private String _name;
  private int ES;
  
  protected void readImpl() {
    this._name = readS(Config.CNAME_MAXLEN);
    this.ES = readD();
  }


  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    if (player1.isOutOfControl()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isProcessingRequest()) {
      
      player1.sendPacket(SystemMsg.WAITING_FOR_ANOTHER_REPLY);
      
      return;
    } 
    Player player2 = World.getPlayer(this._name);
    if (player2 == null) {
      
      player1.sendPacket(SystemMsg.THAT_PLAYER_IS_NOT_ONLINE);
      
      return;
    } 
    if (player2 == player1) {
      
      player1.sendPacket(SystemMsg.THAT_IS_AN_INCORRECT_TARGET);
      player1.sendActionFailed();
      
      return;
    } 
    if (player2.isBusy()) {
      
      player1.sendPacket((new SystemMessage2(SystemMsg.C1_IS_ON_ANOTHER_TASK)).addName(player2));
      
      return;
    } 
    IStaticPacket iStaticPacket = player2.canJoinParty(player1);
    if (iStaticPacket != null) {
      
      player1.sendPacket(iStaticPacket);
      
      return;
    } 
    if (player1.isInParty()) {
      
      if (player1.getParty().getMemberCount() >= Config.ALT_MAX_PARTY_SIZE) {
        
        player1.sendPacket(SystemMsg.THE_PARTY_IS_FULL);
        
        return;
      } 
      
      if (Config.PARTY_LEADER_ONLY_CAN_INVITE && !player1.getParty().isLeader(player1)) {
        
        player1.sendPacket(SystemMsg.ONLY_THE_LEADER_CAN_GIVE_OUT_INVITATIONS);
        
        return;
      } 
      if (player1.getParty().isInDimensionalRift()) {
        
        player1.sendMessage(new CustomMessage("l2p.gameserver.clientpackets.RequestJoinParty.InDimensionalRift", player1, new Object[0]));
        player1.sendActionFailed();
        
        return;
      } 
    } 
    (new Request(Request.L2RequestType.PARTY, player1, player2)).setTimeout(10000L).set("itemDistribution", this.ES);
    
    player2.sendPacket(new AskJoinParty(player1.getName(), this.ES));
    player1.sendPacket((new SystemMessage2(SystemMsg.C1_HAS_BEEN_INVITED_TO_THE_PARTY)).addName(player2));
  }
}
