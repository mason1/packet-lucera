package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.EnchantResult;



public class RequestExCancelEnchantItem
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player != null) {
      
      player.setEnchantScroll(null);
      player.sendPacket(EnchantResult.CANCEL);
    } 
  }
}
