package l2.gameserver.network.l2.c2s;

import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.residence.Residence;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.CastleSiegeAttackerList;


public class RequestCastleSiegeAttackerList
  extends L2GameClientPacket
{
  private int Yg;
  
  protected void readImpl() { this.Yg = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Residence residence = ResidenceHolder.getInstance().getResidence(this.Yg);
    if (residence != null)
      sendPacket(new CastleSiegeAttackerList(residence)); 
  }
}
