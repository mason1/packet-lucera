package l2.gameserver.network.l2.c2s;

import l2.gameserver.network.l2.GameClient;














public class NetPing
  extends L2GameClientPacket
{
  public static final int MIN_CLIP_RANGE = 1433;
  public static final int MAX_CLIP_RANGE = 6144;
  private int XH;
  private int XI;
  private int WT;
  
  protected void runImpl() {
    GameClient gameClient = (GameClient)getClient();
    if (gameClient.getRevision() == 0) {
      gameClient.closeNow(false);
    } else {
      
      gameClient.onPing(this.XH, this.WT, Math.max(1433, Math.min(this.XI, 6144)));
    } 
  }



  
  protected void readImpl() {
    this.XH = readD();
    this.WT = readD();
    this.XI = readD();
  }
}
