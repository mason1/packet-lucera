package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.data.xml.holder.SkillAcquireHolder;
import l2.gameserver.instancemanager.QuestManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.SkillLearn;
import l2.gameserver.model.actor.instances.player.ShortCut;
import l2.gameserver.model.base.AcquireType;
import l2.gameserver.model.base.ClassId;
import l2.gameserver.model.base.Experience;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.quest.Quest;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.CharacterCreateSuccess;
import l2.gameserver.network.l2.s2c.CharacterSelectionInfo;
import l2.gameserver.network.l2.s2c.ExIsCharNameCreatable;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.tables.CharTemplateTable;
import l2.gameserver.tables.SkillTable;
import l2.gameserver.templates.PlayerTemplate;
import l2.gameserver.templates.item.ItemTemplate;
import l2.gameserver.utils.ItemFunctions;




public class CharacterCreate
  extends L2GameClientPacket
{
  private String _name;
  private int BB;
  private int Bu;
  private int Bz;
  private int BA;
  private int By;
  
  protected void readImpl() {
    this._name = readS();
    readD();
    this.BB = readD();
    this.Bu = readD();
    readD();
    readD();
    readD();
    readD();
    readD();
    readD();
    this.Bz = readD();
    this.BA = readD();
    this.By = readD();
  }


  
  protected void runImpl() {
    for (ClassId classId : ClassId.VALUES) {
      if (classId.getId() == this.Bu && classId.getLevel() != 1)
        return; 
    }  GameClient gameClient = (GameClient)getClient();
    if (gameClient == null) {
      return;
    }

    
    if (this.Bz < 0 || (this.BB == 0 && this.Bz > 4) || (this.BB != 0 && this.Bz > 6)) {
      
      sendPacket(ExIsCharNameCreatable.UNABLE_TO_CREATE_A_CHARACTER);
      return;
    } 
    if (this.By > 2 || this.By < 0) {
      
      sendPacket(ExIsCharNameCreatable.UNABLE_TO_CREATE_A_CHARACTER);
      return;
    } 
    if (this.BA > 3 || this.BA < 0) {
      
      sendPacket(ExIsCharNameCreatable.UNABLE_TO_CREATE_A_CHARACTER);
      
      return;
    } 
    
    Player player = Player.create(this.Bu, this.BB, ((GameClient)getClient()).getLogin(), this._name, this.Bz, this.BA, this.By);
    if (player == null) {
      return;
    }
    CharacterSelectionInfo characterSelectionInfo = new CharacterSelectionInfo(gameClient.getLogin(), (gameClient.getSessionKey()).playOkID1);
    sendPacket(new L2GameServerPacket[] { CharacterCreateSuccess.STATIC, characterSelectionInfo });
    
    a((GameClient)getClient(), player);
  }

  
  private void a(GameClient paramGameClient, Player paramPlayer) {
    PlayerTemplate playerTemplate = paramPlayer.getTemplate();
    
    Player.restoreCharSubClasses(paramPlayer);
    
    if (Config.STARTING_ADENA > 0) {
      paramPlayer.addAdena(Config.STARTING_ADENA);
    }
    paramPlayer.setLoc(playerTemplate.spawnLoc);
    
    if (Config.ALT_NEW_CHARACTER_LEVEL > 0) {
      paramPlayer.getActiveClass().setExp(Experience.getExpForLevel(Config.ALT_NEW_CHARACTER_LEVEL));
    }
    if (Config.CHAR_TITLE) {
      paramPlayer.setTitle(Config.ADD_CHAR_TITLE);
    } else {
      paramPlayer.setTitle("");
    } 
    for (ItemTemplate itemTemplate : playerTemplate.getItems()) {
      
      ItemInstance itemInstance = ItemFunctions.createItem(itemTemplate.getItemId());
      paramPlayer.getInventory().addItem(itemInstance);



      
      if (itemInstance.isEquipable() && (paramPlayer.getActiveWeaponItem() == null || itemInstance.getTemplate().getType2() != 0)) {
        paramPlayer.getInventory().equipItem(itemInstance);
      }
    } 
    if (!paramPlayer.isMageClass()) {
      
      for (boolean bool = false; bool < Config.STARTING_ITEMS_FIGHTER.length; bool += true)
      {
        int i = Config.STARTING_ITEMS_FIGHTER[bool];
        long l = Config.STARTING_ITEMS_FIGHTER[bool + true];
        ItemFunctions.addItem(paramPlayer, i, l, false);
      }
    
    } else {
      
      for (boolean bool = false; bool < Config.STARTING_ITEMS_MAGE.length; bool += true) {
        
        int i = Config.STARTING_ITEMS_MAGE[bool];
        long l = Config.STARTING_ITEMS_MAGE[bool + true];
        ItemFunctions.addItem(paramPlayer, i, l, false);
      } 
    } 
    
    for (SkillLearn skillLearn : SkillAcquireHolder.getInstance().getAvailableSkills(paramPlayer, AcquireType.NORMAL)) {
      paramPlayer.addSkill(SkillTable.getInstance().getInfo(skillLearn.getId(), skillLearn.getLevel()), true);
    }
    for (ShortCut shortCut : CharTemplateTable.getInstance().getShortCuts(paramPlayer.getClassId())) {
      int i; ItemInstance itemInstance; switch (shortCut.getType()) {

        
        case 2:
          i = paramPlayer.getSkillLevel(Integer.valueOf(shortCut.getId()));
          if (i > 0) {
            
            ShortCut shortCut1 = new ShortCut(shortCut.getSlot(), shortCut.getPage(), shortCut.getType(), shortCut.getId(), i, shortCut.getCharacterType());
            paramPlayer.registerShortCut(shortCut1);
          } 
          continue;

        
        case 1:
          itemInstance = paramPlayer.getInventory().getItemByItemId(shortCut.getId());
          if (itemInstance != null) {
            
            ShortCut shortCut1 = new ShortCut(shortCut.getSlot(), shortCut.getPage(), shortCut.getType(), itemInstance.getObjectId(), shortCut.getLevel(), shortCut.getCharacterType());
            paramPlayer.registerShortCut(shortCut1);
          } 
          continue;
      } 
      
      paramPlayer.registerShortCut(shortCut);
    } 


    
    startInitialQuests(paramPlayer);
    
    paramPlayer.setCurrentHpMp(paramPlayer.getMaxHp(), paramPlayer.getMaxMp());
    paramPlayer.setCurrentCp(0.0D);
    paramPlayer.setOnlineStatus(false);
    
    paramPlayer.store(false);
    paramPlayer.getInventory().store();
    paramPlayer.deleteMe();
    
    paramGameClient.setCharSelection(CharacterSelectionInfo.loadCharacterSelectInfo(paramGameClient.getLogin()));
  }

  
  public static void startInitialQuests(Player paramPlayer) {
    for (byte b = 0; b < Config.ALT_INITIAL_QUESTS.length; b++) {
      
      int i = Config.ALT_INITIAL_QUESTS[b];
      Quest quest = QuestManager.getQuest(i);
      if (quest != null)
      {
        quest.newQuestState(paramPlayer, 1);
      }
    } 
  }
}
