package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.dao.CharacterVariablesDAO;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ActionFail;
import l2.gameserver.network.l2.s2c.CharSelected;
import l2.gameserver.network.l2.s2c.Ex2ndPasswordCheck;
import l2.gameserver.network.l2.s2c.ExShowScreenMessage;
import l2.gameserver.utils.AutoBan;









public class CharacterSelected
  extends L2GameClientPacket
{
  private int Xv;
  
  protected void readImpl() { this.Xv = readD(); }



  
  protected void runImpl() {
    GameClient gameClient = (GameClient)getClient();
    
    if (gameClient.getActiveChar() != null) {
      return;
    }

    
    int i = gameClient.getObjectIdForSlot(this.Xv);
    if (AutoBan.isBanned(i)) {
      
      sendPacket(ActionFail.STATIC);
      
      return;
    } 
    String str1 = CharacterVariablesDAO.getInstance().getVar(i, "hwidlock@");
    if (str1 != null && !str1.isEmpty() && gameClient.getHwid() != null && !gameClient.getHwid().isEmpty() && !str1.equalsIgnoreCase(gameClient.getHwid())) {
      
      sendPacket(new ExShowScreenMessage("HWID is locked.", 10000, ExShowScreenMessage.ScreenMessageAlign.TOP_CENTER, true));
      sendPacket(ActionFail.STATIC);
      
      return;
    } 
    String str2 = CharacterVariablesDAO.getInstance().getVar(i, "iplock@");
    if (str2 != null && !str2.isEmpty() && gameClient.getIpAddr() != null && !gameClient.getIpAddr().isEmpty() && !str2.equalsIgnoreCase(gameClient.getIpAddr())) {
      
      sendPacket(new ExShowScreenMessage("IP address is locked.", 10000, ExShowScreenMessage.ScreenMessageAlign.TOP_CENTER, true));
      sendPacket(ActionFail.STATIC);
      
      return;
    } 
    if (Config.USE_SECOND_PASSWORD_AUTH && !gameClient.isSecondPasswordAuthed()) {
      
      if (gameClient.getSecondPasswordAuth().isSecondPasswordSet()) {
        
        gameClient.sendPacket(new Ex2ndPasswordCheck(Ex2ndPasswordCheck.Ex2ndPasswordCheckResult.CHECK));
      }
      else {
        
        gameClient.sendPacket(new Ex2ndPasswordCheck(Ex2ndPasswordCheck.Ex2ndPasswordCheckResult.CREATE));
      } 
      
      return;
    } 
    Player player = gameClient.loadCharFromDisk(this.Xv);
    if (player == null) {
      
      sendPacket(ActionFail.STATIC);
      return;
    } 
    if (player.getAccessLevel() < 0)
    {
      player.setAccessLevel(0);
    }
    gameClient.setState(GameClient.GameClientState.IN_GAME);
    gameClient.sendPacket(new CharSelected(player, (gameClient.getSessionKey()).playOkID1));
  }
}
