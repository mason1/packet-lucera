package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.NpcHtmlMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;





public class RequestLinkHtml
  extends L2GameClientPacket
{
  private static final Logger _log = LoggerFactory.getLogger(RequestLinkHtml.class);


  
  private String Zl;


  
  protected void readImpl() { this.Zl = readS(); }




  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (this.Zl.contains("..") || !this.Zl.endsWith(".htm")) {
      
      _log.warn("[RequestLinkHtml] hack? link contains prohibited characters: '" + this.Zl + "', skipped");
      
      return;
    } 
    try {
      NpcHtmlMessage npcHtmlMessage = new NpcHtmlMessage(0);
      npcHtmlMessage.setFile("" + this.Zl);
      sendPacket(npcHtmlMessage);
    }
    catch (Exception exception) {
      
      _log.warn("Bad RequestLinkHtml: ", exception);
    } 
  }
}
