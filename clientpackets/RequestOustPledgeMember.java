package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.model.pledge.SubUnit;
import l2.gameserver.model.pledge.UnitMember;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.network.l2.s2c.PledgeShowMemberListDelete;
import l2.gameserver.network.l2.s2c.PledgeShowMemberListDeleteAll;
import l2.gameserver.network.l2.s2c.SystemMessage;


public class RequestOustPledgeMember
  extends L2GameClientPacket
{
  private String Zi;
  
  protected void readImpl() { this.Zi = readS(Config.CNAME_MAXLEN); }



  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    
    if (player1 == null || (player1.getClanPrivileges() & 0x40) != 64) {
      return;
    }
    Clan clan = player1.getClan();
    UnitMember unitMember = clan.getAnyMember(this.Zi);
    if (unitMember == null) {
      
      player1.sendPacket(SystemMsg.THE_TARGET_MUST_BE_A_CLAN_MEMBER);
      return;
    } 
    SubUnit subUnit = clan.getSubUnit(0);
    
    Player player2 = unitMember.getPlayer();
    
    if (unitMember.isOnline() && unitMember.getPlayer().isInCombat()) {
      
      player1.sendPacket(SystemMsg.A_CLAN_MEMBER_MAY_NOT_BE_DISMISSED_DURING_COMBAT);
      
      return;
    } 
    if (unitMember.isClanLeader() || subUnit.getNextLeaderObjectId() == unitMember.getObjectId()) {
      
      player1.sendPacket(SystemMsg.A_CLAN_LEADER_CANNOT_WITHDRAW_FROM_THEIR_OWN_CLAN);
      
      return;
    } 
    int i = unitMember.getPledgeType();
    clan.removeClanMember(i, unitMember.getObjectId());
    clan.broadcastToOnlineMembers(new L2GameServerPacket[] { (new SystemMessage(191)).addString(this.Zi), new PledgeShowMemberListDelete(this.Zi) });
    
    if (i != -1) {
      clan.setExpelledMember();
    }
    if (player2 == null) {
      return;
    }
    player2.removeEventsByClass(l2.gameserver.model.entity.events.impl.SiegeEvent.class);
    
    if (i == -1)
      player2.setLvlJoinedAcademy(0); 
    player2.setClan(null);
    
    if (!player2.isNoble()) {
      player2.setTitle("");
    }
    player2.setLeaveClanCurTime();
    
    player2.broadcastCharInfo();
    player2.broadcastRelationChanged();
    player2.store(true);
    
    player2.sendPacket(new IStaticPacket[] { Msg.YOU_HAVE_RECENTLY_BEEN_DISMISSED_FROM_A_CLAN_YOU_ARE_NOT_ALLOWED_TO_JOIN_ANOTHER_CLAN_FOR_24_HOURS, PledgeShowMemberListDeleteAll.STATIC });
  }
}
