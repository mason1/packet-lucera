package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.CrestCache;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.network.l2.GameClient;




public class RequestSetPledgeCrestLarge
  extends L2GameClientPacket
{
  private int ZU;
  private byte[] _data;
  
  protected void readImpl() {
    this.ZU = readD();
    if (this.ZU == 2176 && this.ZU == this._buf.remaining()) {
      
      this._data = new byte[this.ZU];
      readB(this._data);
    } 
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Clan clan = player.getClan();
    if (clan == null) {
      return;
    }
    if ((player.getClanPrivileges() & 0x80) == 128) {
      
      if (clan.isPlacedForDisband()) {
        
        player.sendPacket(Msg.DISPERSION_HAS_ALREADY_BEEN_REQUESTED);
        return;
      } 
      if (clan.getCastle() == 0 && clan.getHasHideout() == 0) {
        
        player.sendPacket(Msg.THE_CLANS_EMBLEM_WAS_SUCCESSFULLY_REGISTERED__ONLY_A_CLAN_THAT_OWNS_A_CLAN_HALL_OR_A_CASTLE_CAN_GET_THEIR_EMBLEM_DISPLAYED_ON_CLAN_RELATED_ITEMS);
        
        return;
      } 
      int i = 0;
      
      if (this._data != null && CrestCache.isValidCrestData(this._data)) {
        
        i = CrestCache.getInstance().savePledgeCrestLarge(clan.getClanId(), this._data);
        player.sendPacket(Msg.THE_CLANS_EMBLEM_WAS_SUCCESSFULLY_REGISTERED__ONLY_A_CLAN_THAT_OWNS_A_CLAN_HALL_OR_A_CASTLE_CAN_GET_THEIR_EMBLEM_DISPLAYED_ON_CLAN_RELATED_ITEMS);
      }
      else if (clan.hasCrestLarge()) {
        CrestCache.getInstance().removePledgeCrestLarge(clan.getClanId());
      } 
      clan.setCrestLargeId(i);
      clan.broadcastClanStatus(false, true, false);
    } 
  }
}
