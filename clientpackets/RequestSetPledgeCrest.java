package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.CrestCache;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.network.l2.GameClient;

public class RequestSetPledgeCrest
  extends L2GameClientPacket
{
  private int ZU;
  private byte[] _data;
  
  protected void readImpl() {
    this.ZU = readD();
    if (this.ZU == 256 && this.ZU == this._buf.remaining()) {
      
      this._data = new byte[this.ZU];
      readB(this._data);
    } 
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Clan clan = player.getClan();
    if ((player.getClanPrivileges() & 0x80) == 128) {
      
      if (clan.isPlacedForDisband()) {
        
        player.sendPacket(Msg.DISPERSION_HAS_ALREADY_BEEN_REQUESTED);
        return;
      } 
      if (clan.getLevel() < 3) {
        
        player.sendPacket(Msg.CLAN_CREST_REGISTRATION_IS_ONLY_POSSIBLE_WHEN_CLANS_SKILL_LEVELS_ARE_ABOVE_3);
        
        return;
      } 
      int i = 0;
      
      if (this._data != null && CrestCache.isValidCrestData(this._data)) {
        i = CrestCache.getInstance().savePledgeCrest(clan.getClanId(), this._data);
      } else if (clan.hasCrest()) {
        CrestCache.getInstance().removePledgeCrest(clan.getClanId());
      } 
      clan.setCrestId(i);
      clan.broadcastClanStatus(false, true, false);
    } 
  }
}
