package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.network.l2.GameClient;







public class RequestTargetCanceld
  extends L2GameClientPacket
{
  private int aab;
  
  protected void readImpl() { this.aab = readH(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (this.aab == 0) {
      
      if (player.isCastingNow()) {
        
        Skill skill = player.getCastingSkill();
        player.abortCast((skill != null && (skill.isHandler() || skill.getHitTime() > 1000)), false);
      }
      else if (player.getTarget() != null) {
        player.setTarget(null);
      } 
    } else if (player.getTarget() != null) {
      player.setTarget(null);
    } 
  }
}
