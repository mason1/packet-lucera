package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.model.matching.MatchingRoom;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;





public class AnswerJoinPartyRoom
  extends L2GameClientPacket
{
  private int pI;
  
  protected void readImpl() {
    if (this._buf.hasRemaining()) {
      this.pI = readD();
    } else {
      this.pI = 0;
    } 
  }

  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    request = player1.getRequest();
    if (request == null || !request.isTypeOf(Request.L2RequestType.PARTY_ROOM)) {
      return;
    }
    if (!request.isInProgress()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isOutOfControl()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    Player player2 = request.getRequestor();
    if (player2 == null) {
      
      request.cancel();
      player1.sendPacket(SystemMsg.THAT_PLAYER_IS_NOT_ONLINE);
      player1.sendActionFailed();
      
      return;
    } 
    if (player2.getRequest() != request) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    
    if (this.pI == 0) {
      
      request.cancel();
      player2.sendPacket(SystemMsg.THE_PLAYER_DECLINED_TO_JOIN_YOUR_PARTY);
      
      return;
    } 
    if (player1.getMatchingRoom() != null) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    
    try {
      MatchingRoom matchingRoom = player2.getMatchingRoom();
      if (matchingRoom == null || matchingRoom.getType() != MatchingRoom.PARTY_MATCHING) {
        return;
      }
      matchingRoom.addMember(player1);
    }
    finally {
      
      request.done();
    } 
  }
}
