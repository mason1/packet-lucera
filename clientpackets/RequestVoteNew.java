package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.SystemMessage;


public class RequestVoteNew
  extends L2GameClientPacket
{
  private int aae;
  
  protected void readImpl() { this.aae = readD(); }




  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    if (player1.getTarget() == null) {
      
      player1.sendPacket(Msg.SELECT_TARGET);
      
      return;
    } 
    if (!player1.getTarget().isPlayer() || player1.getTarget().getObjectId() != this.aae) {
      
      player1.sendPacket(SystemMsg.THAT_IS_AN_INCORRECT_TARGET);
      
      return;
    } 
    Player player2 = player1.getTarget().getPlayer();
    
    if (player2 == player1) {
      
      player1.sendPacket(Msg.SELECT_TARGET);
      
      return;
    } 
    if (player1.getLevel() < 10) {
      
      player1.sendPacket(Msg.ONLY_LEVEL_SUP_10_CAN_RECOMMEND);
      
      return;
    } 
    if (player1.getGivableRec() <= 0) {
      
      player1.sendPacket(Msg.NO_MORE_RECOMMENDATIONS_TO_HAVE);
      
      return;
    } 
    if (player1.isRecommended(player2)) {
      
      player1.sendPacket(Msg.THAT_CHARACTER_HAS_ALREADY_BEEN_RECOMMENDED);
      
      return;
    } 
    if (player2.getReceivedRec() >= 255) {
      
      player1.sendPacket(Msg.YOU_NO_LONGER_RECIVE_A_RECOMMENDATION);
      
      return;
    } 
    player1.giveRecommendation(player2);
    player1.sendPacket((new SystemMessage(830)).addName(player2).addNumber(player1.getGivableRec()));
    
    player2.sendPacket((new SystemMessage(831)).addName(player1));
    
    player1.sendUserInfo(false);
    player2.broadcastUserInfo(true);
  }
}
