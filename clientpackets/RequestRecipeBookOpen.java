package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.RecipeBookItemList;

public class RequestRecipeBookOpen
  extends L2GameClientPacket
{
  private boolean ZL;
  
  protected void readImpl() {
    if (this._buf.hasRemaining()) {
      this.ZL = (readD() == 0);
    }
  }

  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    sendPacket(new RecipeBookItemList(player, this.ZL));
  }
}
