package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.handler.bbs.CommunityBoardManager;
import l2.gameserver.handler.bbs.ICommunityBoardHandler;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.SystemMessage;




public class RequestBBSwrite
  extends L2GameClientPacket
{
  private String XP;
  private String XQ;
  private String XR;
  private String XS;
  private String XT;
  private String XU;
  
  public void readImpl() {
    this.XP = readS();
    this.XQ = readS();
    this.XR = readS();
    this.XS = readS();
    this.XT = readS();
    this.XU = readS();
  }


  
  public void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    ICommunityBoardHandler iCommunityBoardHandler = CommunityBoardManager.getInstance().getCommunityHandler(this.XP, player);
    if (iCommunityBoardHandler != null)
    {
      if (!Config.COMMUNITYBOARD_ENABLED) {
        player.sendPacket(new SystemMessage(938));
      } else {
        iCommunityBoardHandler.onWriteCommand(player, this.XP, this.XQ, this.XR, this.XS, this.XT, this.XU);
      } 
    }
  }
}
