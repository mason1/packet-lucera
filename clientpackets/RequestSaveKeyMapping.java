package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExUISetting;




public class RequestSaveKeyMapping
  extends L2GameClientPacket
{
  private byte[] _data;
  
  protected void readImpl() {
    int i = readD();
    if (i > this._buf.remaining() || i > 32767 || i < 0) {
      
      this._data = null;
      return;
    } 
    this._data = new byte[i];
    readB(this._data);
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._data == null)
      return; 
    player.setKeyBindings(this._data);
    player.sendPacket(new ExUISetting(player));
  }
}
