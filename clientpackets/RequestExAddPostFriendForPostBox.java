package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.dao.CharacterDAO;
import l2.gameserver.dao.CharacterPostFriendDAO;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.ExConfirmAddingPostFriend;
import l2.gameserver.network.l2.s2c.SystemMessage2;
import org.napile.primitive.maps.IntObjectMap;





public class RequestExAddPostFriendForPostBox
  extends L2GameClientPacket
{
  private String _name;
  
  protected void readImpl() { this._name = readS(Config.CNAME_MAXLEN); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    int i = CharacterDAO.getInstance().getObjectIdByName(this._name);
    if (i == 0) {
      
      player.sendPacket(new ExConfirmAddingPostFriend(this._name, ExConfirmAddingPostFriend.NAME_IS_NOT_EXISTS));
      
      return;
    } 
    if (this._name.equalsIgnoreCase(player.getName())) {
      
      player.sendPacket(new ExConfirmAddingPostFriend(this._name, ExConfirmAddingPostFriend.NAME_IS_NOT_REGISTERED));
      
      return;
    } 
    IntObjectMap intObjectMap = player.getPostFriends();
    if (intObjectMap.size() >= 100) {
      
      player.sendPacket(new ExConfirmAddingPostFriend(this._name, ExConfirmAddingPostFriend.LIST_IS_FULL));
      
      return;
    } 
    if (intObjectMap.containsKey(i)) {
      
      player.sendPacket(new ExConfirmAddingPostFriend(this._name, ExConfirmAddingPostFriend.ALREADY_ADDED));
      
      return;
    } 
    CharacterPostFriendDAO.getInstance().insert(player, i);
    intObjectMap.put(i, CharacterDAO.getInstance().getNameByObjectId(i));
    
    player.sendPacket(new IStaticPacket[] { (new SystemMessage2(SystemMsg.S1_WAS_SUCCESSFULLY_ADDED_TO_YOUR_CONTACT_LIST)).addString(this._name), new ExConfirmAddingPostFriend(this._name, ExConfirmAddingPostFriend.SUCCESS) });
  }
}
