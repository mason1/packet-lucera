package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.data.xml.holder.EventHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.World;
import l2.gameserver.model.entity.events.EventType;
import l2.gameserver.model.entity.events.impl.DuelEvent;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.SystemMessage2;

public class RequestDuelStart
  extends L2GameClientPacket
{
  private String _name;
  private int YA;
  
  protected void readImpl() {
    this._name = readS(Config.CNAME_MAXLEN);
    this.YA = readD();
  }


  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    if (player1.isActionsDisabled()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isProcessingRequest()) {
      
      player1.sendPacket(SystemMsg.WAITING_FOR_ANOTHER_REPLY);
      
      return;
    } 
    Player player2 = World.getPlayer(this._name);
    if (player2 == null || player2 == player1) {
      
      player1.sendPacket(SystemMsg.THERE_IS_NO_OPPONENT_TO_RECEIVE_YOUR_CHALLENGE_FOR_A_DUEL);
      
      return;
    } 
    DuelEvent duelEvent = (DuelEvent)EventHolder.getInstance().getEvent(EventType.PVP_EVENT, this.YA);
    if (duelEvent == null) {
      return;
    }
    if (!duelEvent.canDuel(player1, player2, true)) {
      return;
    }
    if (player2.isBusy()) {
      
      player1.sendPacket((new SystemMessage2(SystemMsg.C1_IS_ON_ANOTHER_TASK)).addName(player2));
      
      return;
    } 
    duelEvent.askDuel(player1, player2);
  }
}
