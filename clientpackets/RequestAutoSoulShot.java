package l2.gameserver.network.l2.c2s;

import l2.gameserver.handler.items.IItemHandler;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExAutoSoulShot;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.utils.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class RequestAutoSoulShot
  extends L2GameClientPacket
{
  private static final Logger LOG = LoggerFactory.getLogger(RequestAutoSoulShot.class);
  
  private int _itemId;
  
  private boolean enabled;
  private int type;
  
  protected void readImpl() {
    this._itemId = readD();
    this.enabled = (readD() == 1);
    this.type = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    
    if (player == null) {
      return;
    }
    if (player.getPrivateStoreType() != 0 || player.isDead()) {
      return;
    }
    ItemInstance itemInstance = player.getInventory().getItemByItemId(this._itemId);
    
    if (itemInstance == null) {
      return;
    }
    if (!itemInstance.getTemplate().isShotItem()) {
      return;
    }
    if (!itemInstance.getTemplate().testCondition(player, itemInstance, false)) {
      
      String str = "Player " + player.getName() + " trying illegal item use, ban this player!";
      Log.add(str, "illegal-actions");
      LOG.warn(str);
      
      return;
    } 
    if (this.enabled) {
      
      player.addAutoSoulShot(Integer.valueOf(this._itemId));
      player.sendPacket(new ExAutoSoulShot(this._itemId, true, this.type));
      player.sendPacket((new SystemMessage(1433)).addString(itemInstance.getName()));
      IItemHandler iItemHandler = itemInstance.getTemplate().getHandler();
      iItemHandler.useItem(player, itemInstance, false);
    }
    else {
      
      player.removeAutoSoulShot(Integer.valueOf(this._itemId));
      player.sendPacket(new ExAutoSoulShot(this._itemId, false, this.type));
      player.sendPacket((new SystemMessage(1434)).addItemName(this._itemId));
    } 
  }
}
