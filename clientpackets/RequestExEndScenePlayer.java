package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;


public class RequestExEndScenePlayer
  extends L2GameClientPacket
{
  private int IG;
  
  protected void readImpl() { this.IG = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    if (!player.isInMovie() || player.getMovieId() != this.IG) {
      
      player.sendActionFailed();
      return;
    } 
    player.setIsInMovie(false);
    player.setMovieId(0);
    player.decayMe();
    player.spawnMe();
  }
}
