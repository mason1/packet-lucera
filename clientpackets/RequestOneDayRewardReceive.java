package l2.gameserver.network.l2.c2s;

import java.util.List;
import l2.gameserver.Config;
import l2.gameserver.data.xml.holder.OneDayRewardHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.oneDayReward.OneDayReward;
import l2.gameserver.model.entity.oneDayReward.OneDayRewardRequirement;
import l2.gameserver.model.entity.oneDayReward.OneDayRewardStatus;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExConnectedTimeAndGettableReward;
import l2.gameserver.network.l2.s2c.ExOneDayReceiveRewardList;
import l2.gameserver.stats.conditions.Condition;
import l2.gameserver.utils.ItemFunctions;
import org.apache.commons.lang3.tuple.Pair;







public class RequestOneDayRewardReceive
  extends L2GameClientPacket
{
  private int Zp;
  
  protected void readImpl() { this.Zp = readH(); }



  
  protected void runImpl() {
    if (!Config.EX_ONE_DAY_REWARD) {
      return;
    }

    
    player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }

    
    List list = OneDayRewardHolder.getInstance().getOneDayReward(this.Zp, player);
    if (list == null) {
      return;
    }

    
    OneDayReward oneDayReward = null;
    
    if (list.size() == 1) {
      
      oneDayReward = (OneDayReward)list.get(0);
    }
    else if (list.size() > 1) {
      
      for (OneDayReward oneDayReward1 : list)
      {
        Condition condition = oneDayReward1.getCondition();
      }
    } 
    
    if (oneDayReward == null) {
      return;
    }

    
    OneDayRewardRequirement oneDayRewardRequirement = oneDayReward.getRequirement();
    if (oneDayRewardRequirement == null) {
      return;
    }

    
    OneDayRewardStatus oneDayRewardStatus = player.getOneDayRewardStore().getStatus(oneDayReward);
    if (oneDayRewardStatus.isReceived() || !oneDayRewardRequirement.isDone(oneDayRewardStatus)) {
      return;
    }


    
    try {
      player.getOneDayRewardStore().updateStatus(oneDayReward, oneDayRewardStatus.getCurrentProgress(), true);
      
      for (Pair pair : oneDayReward.getRewards())
      {
        ItemFunctions.addItem(player, ((Integer)pair.getKey()).intValue(), ((Long)pair.getValue()).longValue(), true);
      
      }
    }
    finally {
      
      player.sendPacket(new ExOneDayReceiveRewardList(player));
      
      player.sendPacket(new ExConnectedTimeAndGettableReward(player));
    } 
  }
}
