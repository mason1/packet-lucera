package l2.gameserver.network.l2.c2s;

import java.util.Set;
import l2.commons.math.SafeMath;
import l2.gameserver.cache.Msg;
import l2.gameserver.dao.MailDAO;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.mail.Mail;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExShowSentPostList;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.utils.Log;











public class RequestExCancelSentPost
  extends L2GameClientPacket
{
  private int YI;
  
  protected void readImpl() { this.YI = readD(); }



  
  protected void runImpl() {
    player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.YOU_CANNOT_CANCEL_BECAUSE_THE_PRIVATE_SHOP_OR_WORKSHOP_IS_IN_PROGRESS);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendPacket(Msg.YOU_CANNOT_CANCEL_DURING_AN_EXCHANGE);
      
      return;
    } 
    if (player.getEnchantScroll() != null) {
      
      player.sendPacket(Msg.YOU_CANNOT_CANCEL_DURING_AN_ITEM_ENHANCEMENT_OR_ATTRIBUTE_ENHANCEMENT);
      
      return;
    } 
    if (!player.isInPeaceZone()) {
      
      player.sendPacket(Msg.YOU_CANNOT_CANCEL_IN_A_NON_PEACE_ZONE_LOCATION);
      
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
      
      return;
    } 
    Mail mail = MailDAO.getInstance().getSentMailByMailId(player.getObjectId(), this.YI);
    if (mail != null) {
      
      if (mail.getAttachments().isEmpty()) {
        
        player.sendActionFailed();
        return;
      } 
      player.getInventory().writeLock();
      try {
        ItemInstance[] arrayOfItemInstance;
        byte b = 0;
        long l = 0L;
        for (ItemInstance itemInstance : mail.getAttachments()) {
          
          l = SafeMath.addAndCheck(l, SafeMath.mulAndCheck(itemInstance.getCount(), itemInstance.getTemplate().getWeight()));
          if (!itemInstance.getTemplate().isStackable() || player.getInventory().getItemByItemId(itemInstance.getItemId()) == null) {
            b++;
          }
        } 
        if (!player.getInventory().validateWeight(l)) {
          
          sendPacket(Msg.YOU_COULD_NOT_CANCEL_RECEIPT_BECAUSE_YOUR_INVENTORY_IS_FULL);
          
          return;
        } 
        if (!player.getInventory().validateCapacity(b)) {
          
          sendPacket(Msg.YOU_COULD_NOT_CANCEL_RECEIPT_BECAUSE_YOUR_INVENTORY_IS_FULL);
          
          return;
        } 
        
        Set set = mail.getAttachments();
        
        synchronized (set) {
          
          arrayOfItemInstance = (ItemInstance[])mail.getAttachments().toArray(new ItemInstance[set.size()]);
          set.clear();
        } 
        
        for (ItemInstance itemInstance : arrayOfItemInstance) {
          
          player.sendPacket((new SystemMessage(3073)).addItemName(itemInstance.getItemId()).addNumber(itemInstance.getCount()));
          Log.LogItem(player, Log.ItemLog.PostCancel, itemInstance);
          player.getInventory().addItem(itemInstance);
        } 
        
        mail.update();
        MailDAO.getInstance().deleteSentMailByMailId(player.getObserverMode(), this.YI);
        MailDAO.getInstance().deleteReceivedMailByMailId(player.getObserverMode(), this.YI);
        mail.delete();
        
        player.sendPacket(Msg.MAIL_SUCCESSFULLY_CANCELLED);
      }
      catch (ArithmeticException arithmeticException) {

      
      }
      finally {
        
        player.getInventory().writeUnlock();
      } 
    } 
    player.sendPacket(new ExShowSentPostList(player));
  }
}
