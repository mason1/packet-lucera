package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.matching.MatchingRoom;
import l2.gameserver.network.l2.GameClient;





public class RequestWithdrawPartyRoom
  extends L2GameClientPacket
{
  private int Yy;
  
  protected void readImpl() { this.Yy = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    MatchingRoom matchingRoom = player.getMatchingRoom();
    if (matchingRoom.getId() != this.Yy || matchingRoom.getType() != MatchingRoom.PARTY_MATCHING) {
      return;
    }
    if (matchingRoom.getLeader() == player) {
      return;
    }
    matchingRoom.removeMember(player, false);
  }
}
