package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Creature;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.model.items.attachment.FlagItemAttachment;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.tables.SkillTable;





public class RequestMagicSkillUse
  extends L2GameClientPacket
{
  private Integer Zn;
  private boolean XL;
  private boolean XM;
  
  protected void readImpl() {
    this.Zn = Integer.valueOf(readD());
    this.XL = (readD() != 0);
    this.XM = (readC() != 0);
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    
    if (player == null) {
      return;
    }
    player.setActive();
    
    if (player.isOutOfControl()) {
      
      player.sendActionFailed();
      
      return;
    } 
    Skill skill = SkillTable.getInstance().getInfo(this.Zn.intValue(), player.getSkillLevel(this.Zn));
    if (skill != null) {
      
      if (!skill.isActive() && !skill.isToggle()) {
        return;
      }
      FlagItemAttachment flagItemAttachment = player.getActiveWeaponFlagAttachment();
      if (flagItemAttachment != null && !flagItemAttachment.canCast(player, skill)) {
        
        player.sendActionFailed();
        
        return;
      } 
      
      if ((player.getTransformation() != 0 || player.isCursedWeaponEquipped()) && 
        !player.getAllSkills().contains(skill)) {
        return;
      }
      if (skill.isToggle() && 
        player.getEffectList().getEffectsBySkill(skill) != null) {
        
        player.getEffectList().stopEffect(skill.getId());
        player.sendPacket((new SystemMessage(335)).addSkillName(skill.getId(), skill.getLevel()));
        player.sendActionFailed();
        
        return;
      } 
      Creature creature = skill.getAimingTarget(player, player.getTarget());
      
      player.setGroundSkillLoc(null);
      player.getAI().Cast(skill, creature, this.XL, this.XM);
    } else {
      
      player.sendActionFailed();
    } 
  }
}
