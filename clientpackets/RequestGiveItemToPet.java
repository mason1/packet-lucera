package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.instances.PetInstance;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.PcInventory;
import l2.gameserver.model.items.PetInventory;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.utils.Log;

public class RequestGiveItemToPet
  extends L2GameClientPacket
{
  private int Bq;
  private long Xf;
  
  protected void readImpl() {
    this.Bq = readD();
    this.Xf = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this.Xf < 1L) {
      return;
    }
    PetInstance petInstance = (PetInstance)player.getPet();
    if (petInstance == null) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isOutOfControl()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(SystemMsg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade() || player.isProcessingRequest()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(SystemMsg.YOU_CANNOT_DO_THAT_WHILE_FISHING_);
      
      return;
    } 
    if (petInstance.isDead()) {
      
      player.sendPacket(SystemMsg.YOUR_PET_IS_DEAD_AND_ANY_ATTEMPT_YOU_MAKE_TO_GIVE_IT_SOMETHING_GOES_UNRECOGNIZED);
      
      return;
    } 
    if (this.Bq == petInstance.getControlItemObjId()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendActionFailed();
      
      return;
    } 
    petInventory = petInstance.getInventory();
    pcInventory = player.getInventory();
    
    petInventory.writeLock();
    pcInventory.writeLock();
    
    try {
      ItemInstance itemInstance = pcInventory.getItemByObjectId(this.Bq);
      if (itemInstance == null || itemInstance.getCount() < this.Xf || !itemInstance.canBeDropped(player, false)) {
        
        player.sendActionFailed();
        
        return;
      } 
      boolean bool = false;
      long l = itemInstance.getTemplate().getWeight() * this.Xf;
      if (!itemInstance.getTemplate().isStackable() || petInstance.getInventory().getItemByItemId(itemInstance.getItemId()) == null) {
        bool = true;
      }
      if (!petInstance.getInventory().validateWeight(l)) {
        
        player.sendPacket(Msg.EXCEEDED_PET_INVENTORYS_WEIGHT_LIMIT);
        
        return;
      } 
      if (!petInstance.getInventory().validateCapacity(bool)) {
        
        player.sendPacket(Msg.EXCEEDED_PET_INVENTORYS_WEIGHT_LIMIT);
        
        return;
      } 
      itemInstance = pcInventory.removeItemByObjectId(this.Bq, this.Xf);
      Log.LogItem(player, Log.ItemLog.ToPet, itemInstance);
      petInventory.addItem(itemInstance);
      
      petInstance.sendChanges();
      player.sendChanges();
    }
    finally {
      
      pcInventory.writeUnlock();
      petInventory.writeUnlock();
    } 
  }
}
