package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.s2c.FriendAddRequestResult;
import l2.gameserver.network.l2.s2c.L2Friend;
import l2.gameserver.network.l2.s2c.SystemMessage;


public class RequestFriendAddReply
  extends L2GameClientPacket
{
  private int pI;
  
  protected void readImpl() { this.pI = this._buf.hasRemaining() ? readD() : 0; }



  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    Request request = player1.getRequest();
    if (request == null || !request.isTypeOf(Request.L2RequestType.FRIEND)) {
      return;
    }
    if (player1.isOutOfControl()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    if (!request.isInProgress()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isOutOfControl()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    Player player2 = request.getRequestor();
    if (player2 == null) {
      
      request.cancel();
      player1.sendPacket(Msg.THE_USER_WHO_REQUESTED_TO_BECOME_FRIENDS_IS_NOT_FOUND_IN_THE_GAME);
      player1.sendActionFailed();
      
      return;
    } 
    if (player2.getRequest() != request) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    if (this.pI == 0) {
      
      request.cancel();
      player2.sendPacket(Msg.YOU_HAVE_FAILED_TO_INVITE_A_FRIEND);
      player1.sendActionFailed();
      
      return;
    } 
    player2.getFriendList().addFriend(player1);
    player1.getFriendList().addFriend(player2);
    
    player2.sendPacket(new IStaticPacket[] { Msg.YOU_HAVE_SUCCEEDED_IN_INVITING_A_FRIEND, (new SystemMessage(132)).addString(player1.getName()), new L2Friend(player1, true) });
    player1.sendPacket(new IStaticPacket[] { (new SystemMessage(479)).addString(player2.getName()), new L2Friend(player2, true) });
    player1.sendPacket(new FriendAddRequestResult(player2, 1));
    player2.sendPacket(new FriendAddRequestResult(player1, 1));
  }
}
