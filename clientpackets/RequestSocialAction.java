package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.network.l2.s2c.SocialAction;


public class RequestSocialAction
  extends L2GameClientPacket
{
  private int Xd;
  
  protected void readImpl() { this.Xd = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    if (player.isOutOfControl() || player.getTransformation() != 0 || player.isCursedWeaponEquipped() || player
      .isActionsDisabled() || player.isSitting() || player
      .getPrivateStoreType() != 0 || player.isProcessingRequest()) {
      
      player.sendActionFailed();
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(SystemMsg.YOU_CANNOT_DO_THAT_WHILE_FISHING_2);
      return;
    } 
    if (this.Xd > 1 && this.Xd < 14)
    {
      player.broadcastPacket(new L2GameServerPacket[] { new SocialAction(player.getObjectId(), this.Xd) });
    }
  }
}
