package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.model.pledge.RankPrivs;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.PledgePowerGradeList;



public class RequestPledgePowerGradeList
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    Clan clan = player.getClan();
    if (clan != null) {
      
      RankPrivs[] arrayOfRankPrivs = clan.getAllRankPrivs();
      player.sendPacket(new PledgePowerGradeList(arrayOfRankPrivs));
    } 
  }
}
