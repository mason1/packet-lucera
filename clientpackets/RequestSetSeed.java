package l2.gameserver.network.l2.c2s;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.instancemanager.CastleManorManager;
import l2.gameserver.model.Manor;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.templates.manor.SeedProduction;














public class RequestSetSeed
  extends L2GameClientPacket
{
  private int _count;
  private int EJ;
  private long[] ZX;
  
  protected void readImpl() {
    this.EJ = readD();
    this._count = readD();
    if (this._count * 20 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      return;
    } 
    this.ZX = new long[this._count * 3];
    for (byte b = 0; b < this.ZX.length; ) {
      
      int i = readD();
      long l1 = readQ();
      long l2 = readQ();
      if (i < 1 || l1 < 0L || l2 < 0L) {
        
        this._count = 0;
        return;
      } 
      this.ZX[b++] = i;
      this.ZX[b++] = l1;
      this.ZX[b++] = l2;
    } 
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0) {
      return;
    }
    if (player.getClan() == null) {
      
      player.sendActionFailed();
      
      return;
    } 
    Castle castle = (Castle)ResidenceHolder.getInstance().getResidence(Castle.class, this.EJ);
    if (castle == null || castle.getOwnerId() != player.getClanId() || (player
      .getClanPrivileges() & 0x10000) != 65536) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (castle.isNextPeriodApproved()) {
      
      player.sendPacket(SystemMsg.A_MANOR_CANNOT_BE_SET_UP_BETWEEN_430_AM_AND_8_PM);
      player.sendActionFailed();
      
      return;
    } 
    ArrayList arrayList = new ArrayList(this._count);
    List list = Manor.getInstance().getSeedsForCastle(this.EJ);
    for (byte b = 0; b < this._count; b++) {
      
      int i = (int)this.ZX[b * 3 + 0];
      long l1 = this.ZX[b * 3 + 1];
      long l2 = this.ZX[b * 3 + 2];
      if (i > 0)
      {
        for (Manor.SeedData seedData : list) {
          if (seedData.getId() == i) {
            
            if (l1 > seedData.getSeedLimit()) {
              break;
            }
            long l = Manor.getInstance().getSeedBasicPrice(i);
            if (l2 < l * 60L / 100L || l2 > l * 10L) {
              break;
            }
            SeedProduction seedProduction = CastleManorManager.getInstance().getNewSeedProduction(i, l1, l2, l1);
            arrayList.add(seedProduction);
            break;
          } 
        } 
      }
    } 
    castle.setSeedProduction(arrayList, 1);
    castle.saveSeedData(1);
  }
}
