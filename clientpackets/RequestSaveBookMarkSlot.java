package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;



public class RequestSaveBookMarkSlot
  extends L2GameClientPacket
{
  private String name;
  private String acronym;
  private int icon;
  
  protected void readImpl() {
    this.name = readS(32);
    this.icon = readD();
    this.acronym = readS(4);
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
  }
}
