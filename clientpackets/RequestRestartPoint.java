package l2.gameserver.network.l2.c2s;

import l2.commons.lang.ArrayUtils;
import l2.gameserver.Config;
import l2.gameserver.instancemanager.ReflectionManager;
import l2.gameserver.listener.actor.player.impl.ReviveAnswerListener;
import l2.gameserver.model.Player;
import l2.gameserver.model.Zone;
import l2.gameserver.model.base.RestartType;
import l2.gameserver.model.entity.Reflection;
import l2.gameserver.model.entity.events.GlobalEvent;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.model.entity.residence.ClanHall;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.s2c.ActionFail;
import l2.gameserver.network.l2.s2c.Die;
import l2.gameserver.utils.ItemFunctions;
import l2.gameserver.utils.Location;
import l2.gameserver.utils.TeleportUtils;
import org.apache.commons.lang3.tuple.Pair;



public class RequestRestartPoint
  extends L2GameClientPacket
{
  private RestartType ZO;
  
  protected void readImpl() { this.ZO = (RestartType)ArrayUtils.valid(RestartType.VALUES, readD()); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    
    if (this.ZO == null || player == null) {
      return;
    }
    if (player.isFakeDeath()) {
      
      player.breakFakeDeath();
      
      return;
    } 
    if (!player.isDead() && !player.isGM()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isFestivalParticipant()) {
      
      player.doRevive();
      
      return;
    } 
    if (player.isResurectProhibited()) {
      
      player.sendActionFailed();
      
      return;
    } 
    switch (this.ZO) {
      
      case FIXED:
        if ((player.getPlayerAccess()).ResurectFixed) {
          player.doRevive(100.0D);
        } else if (Config.SERVICE_FEATHER_REVIVE_ENABLE && (!Config.SERVICE_DISABLE_FEATHER_ON_SIEGES_AND_EPIC || (!player.isOnSiegeField() && !player.isInZone(Zone.ZoneType.epic))) && 
          !player.getInventory().isLockedItem(Config.SERVICE_FEATHER_ITEM_ID) && 
          ItemFunctions.removeItem(player, Config.SERVICE_FEATHER_ITEM_ID, 1L, true) == 1L) {
          
          player.sendMessage(new CustomMessage("YOU_HAVE_USED_THE_FEATHER_OF_BLESSING_TO_RESURRECT", player, new Object[0]));
          player.doRevive(100.0D);
        }
        else if (Config.ALT_REVIVE_WINDOW_TO_TOWN) {
          
          player.setPendingRevive(true);
          player.teleToLocation(Config.ALT_REVIVE_WINDOW_TO_TOWN_LOCATION, ReflectionManager.DEFAULT);
        } else {
          
          player.sendPacket(new IStaticPacket[] { ActionFail.STATIC, new Die(player) });
        }  return;
    } 
    Location location = null;
    Reflection reflection = player.getReflection();
    
    if (reflection == ReflectionManager.DEFAULT)
      for (GlobalEvent globalEvent : player.getEvents()) {
        location = globalEvent.getRestartLoc(player, this.ZO);
      } 
    if (location == null) {
      location = defaultLoc(this.ZO, player);
    }
    if (location != null) {
      
      Pair pair = player.getAskListener(false);
      if (pair != null && pair.getValue() instanceof ReviveAnswerListener && !((ReviveAnswerListener)pair.getValue()).isForPet()) {
        player.getAskListener(true);
      }
      player.setPendingRevive(true);
      player.teleToLocation(location, ReflectionManager.DEFAULT);
    } else {
      
      player.sendPacket(new IStaticPacket[] { ActionFail.STATIC, new Die(player) });
    } 
  }




  
  public static Location defaultLoc(RestartType paramRestartType, Player paramPlayer) {
    null = null;
    Clan clan = paramPlayer.getClan();
    
    switch (paramRestartType) {
      
      case TO_CLANHALL:
        if (clan != null && clan.getHasHideout() != 0) {
          
          ClanHall clanHall = paramPlayer.getClanHall();
          null = TeleportUtils.getRestartLocation(paramPlayer, RestartType.TO_CLANHALL);
          if (clanHall.getFunction(5) != null) {
            paramPlayer.restoreExp(clanHall.getFunction(5).getLevel());
          }
        } 












        
        return null;case TO_CASTLE: if (clan != null && clan.getCastle() != 0) { Castle castle = paramPlayer.getCastle(); null = TeleportUtils.getRestartLocation(paramPlayer, RestartType.TO_CASTLE); if (castle.getFunction(5) != null) paramPlayer.restoreExp(castle.getFunction(5).getLevel());  }  return null;
    } 
    return TeleportUtils.getRestartLocation(paramPlayer, RestartType.TO_VILLAGE);
  }
}
