package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.model.pledge.UnitMember;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.network.l2.s2c.PledgeReceiveMemberInfo;
import l2.gameserver.network.l2.s2c.PledgeShowMemberListUpdate;
import l2.gameserver.network.l2.s2c.SystemMessage;

public class RequestPledgeSetAcademyMaster
  extends L2GameClientPacket {
  private int _mode;
  private String ZB;
  private String ZC;
  
  protected void readImpl() {
    this._mode = readD();
    this.ZB = readS(16);
    this.ZC = readS(16);
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Clan clan = player.getClan();
    if (clan == null) {
      return;
    }
    if ((player.getClanPrivileges() & 0x100) == 256) {
      
      UnitMember unitMember1 = player.getClan().getAnyMember(this.ZB);
      UnitMember unitMember2 = player.getClan().getAnyMember(this.ZC);
      if (unitMember1 != null && unitMember2 != null) {
        
        if (unitMember2.getPledgeType() != -1 || unitMember1.getPledgeType() == -1) {
          return;
        }
        if (this._mode == 1) {
          
          if (unitMember1.hasApprentice()) {
            
            player.sendMessage(new CustomMessage("l2p.gameserver.clientpackets.RequestOustAlly.MemberAlreadyHasApprentice", player, new Object[0]));
            return;
          } 
          if (unitMember2.hasSponsor()) {
            
            player.sendMessage(new CustomMessage("l2p.gameserver.clientpackets.RequestOustAlly.ApprenticeAlreadyHasSponsor", player, new Object[0]));
            return;
          } 
          unitMember1.setApprentice(unitMember2.getObjectId());
          clan.broadcastToOnlineMembers(new L2GameServerPacket[] { new PledgeShowMemberListUpdate(unitMember2) });
          clan.broadcastToOnlineMembers(new L2GameServerPacket[] { (new SystemMessage(1755)).addString(unitMember1.getName()).addString(unitMember2.getName()) });
        }
        else {
          
          if (!unitMember1.hasApprentice()) {
            
            player.sendMessage(new CustomMessage("l2p.gameserver.clientpackets.RequestOustAlly.MemberHasNoApprentice", player, new Object[0]));
            return;
          } 
          unitMember1.setApprentice(0);
          clan.broadcastToOnlineMembers(new L2GameServerPacket[] { new PledgeShowMemberListUpdate(unitMember2) });
          clan.broadcastToOnlineMembers(new L2GameServerPacket[] { (new SystemMessage(1763)).addString(unitMember1.getName()).addString(unitMember2.getName()) });
        } 
        if (unitMember2.isOnline())
          unitMember2.getPlayer().broadcastCharInfo(); 
        player.sendPacket(new PledgeReceiveMemberInfo(unitMember1));
      } 
    } else {
      
      player.sendMessage(new CustomMessage("l2p.gameserver.clientpackets.RequestOustAlly.NoMasterRights", player, new Object[0]));
    } 
  }
}
