package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.entity.events.impl.DuelEvent;
import l2.gameserver.network.l2.GameClient;



public class RequestDuelSurrender
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    DuelEvent duelEvent = (DuelEvent)player.getEvent(DuelEvent.class);
    if (duelEvent == null) {
      return;
    }
    duelEvent.packetSurrender(player);
  }
}
