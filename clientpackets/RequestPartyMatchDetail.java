package l2.gameserver.network.l2.c2s;

import l2.gameserver.instancemanager.MatchingRoomManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.matching.MatchingRoom;
import l2.gameserver.network.l2.GameClient;




public class RequestPartyMatchDetail
  extends L2GameClientPacket
{
  private int Yy;
  private int Zw;
  private int _level;
  
  protected void readImpl() {
    this.Yy = readD();
    this.Zw = readD();
    this._level = readD();
  }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.getMatchingRoom() != null) {
      return;
    }
    if (this.Yy > 0) {
      
      MatchingRoom matchingRoom = MatchingRoomManager.getInstance().getMatchingRoom(MatchingRoom.PARTY_MATCHING, this.Yy);
      if (matchingRoom == null) {
        return;
      }
      matchingRoom.addMember(player);
    }
    else {
      
      for (MatchingRoom matchingRoom : MatchingRoomManager.getInstance().getMatchingRooms(MatchingRoom.PARTY_MATCHING, this.Zw, (this._level == 1), player)) {
        if (matchingRoom.addMember(player))
          break; 
      } 
    } 
  }
}
