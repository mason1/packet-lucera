package l2.gameserver.network.l2.c2s;







public class MoveWithDelta
  extends L2GameClientPacket
{
  private int XE;
  private int XF;
  private int XG;
  
  protected void readImpl() {
    this.XE = readD();
    this.XF = readD();
    this.XG = readD();
  }
  
  protected void runImpl() {}
}
