package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.ExGetCrystalizingEstimation;






public class RequestCrystallizeEstimate
  extends L2GameClientPacket
{
  private int Bq;
  
  protected void readImpl() {
    this.Bq = readD();
    readQ();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    
    if (player == null) {
      return;
    }

    
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(SystemMsg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    ItemInstance itemInstance = player.getInventory().getItemByObjectId(this.Bq);
    if (itemInstance == null) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (itemInstance.isHeroWeapon()) {
      
      player.sendPacket(Msg.HERO_WEAPONS_CANNOT_BE_DESTROYED);
      
      return;
    } 
    if (!itemInstance.canBeCrystallized(player)) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(SystemMsg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(SystemMsg.YOU_CANNOT_DO_THAT_WHILE_FISHING_);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    
    int i = player.getSkillLevel(Integer.valueOf(248));
    if ((itemInstance.getTemplate().getItemGrade()).externalOrdinal > i) {
      
      player.sendPacket(Msg.CANNOT_CRYSTALLIZE_CRYSTALLIZATION_SKILL_LEVEL_TOO_LOW);
      player.sendActionFailed();
      
      return;
    } 
    sendPacket(new ExGetCrystalizingEstimation(itemInstance));
  }
}
