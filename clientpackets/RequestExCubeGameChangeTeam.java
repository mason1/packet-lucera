package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public final class RequestExCubeGameChangeTeam
  extends L2GameClientPacket
{
  private static final Logger _log = LoggerFactory.getLogger(RequestExCubeGameChangeTeam.class);
  
  int _team;
  
  int _arena;
  
  protected void readImpl() {
    this._arena = readD() + 1;
    this._team = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
  }
}
