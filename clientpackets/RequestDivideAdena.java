package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.ExDivideAdenaDone;

public class RequestDivideAdena
  extends L2GameClientPacket
{
  private long Be;
  
  protected void readImpl() {
    readD();
    this.Be = readQ();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }

    
    long l1 = player.getAdena();
    if (this.Be > l1) {
      
      player.sendPacket(SystemMsg.YOU_CANNOT_PROCEED_AS_THERE_IS_INSUFFICIENT_ADENA);
      return;
    } 
    int i = player.getParty().getMemberCount();
    long l2 = (long)Math.floor((this.Be / i));
    player.reduceAdena(i * l2, false);
    for (Player player1 : player.getParty().getPartyMembers())
    {
      player1.addAdena(l2, (player1.getObjectId() != player.getObjectId()));
    }
    player.sendPacket(new ExDivideAdenaDone(i, this.Be, l2, player.getName()));
  }
}
