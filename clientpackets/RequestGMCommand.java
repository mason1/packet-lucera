package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.World;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.pledge.SubUnit;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.s2c.ExGMViewQuestItemList;
import l2.gameserver.network.l2.s2c.GMHennaInfo;
import l2.gameserver.network.l2.s2c.GMViewCharacterInfo;
import l2.gameserver.network.l2.s2c.GMViewItemList;
import l2.gameserver.network.l2.s2c.GMViewPledgeInfo;
import l2.gameserver.network.l2.s2c.GMViewQuestInfo;
import l2.gameserver.network.l2.s2c.GMViewSkillInfo;
import l2.gameserver.network.l2.s2c.GMViewWarehouseWithdrawList;

public class RequestGMCommand
  extends L2GameClientPacket {
  private String Zg;
  private int Xt;
  
  protected void readImpl() {
    this.Zg = readS();
    this.Xt = readD();
  }

  
  protected void runImpl() {
    int i;
    ItemInstance[] arrayOfItemInstance;
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null)
      return; 
    Player player2 = World.getPlayer(this.Zg);
    if (player2 == null) {
      return;
    }
    if (!(player1.getPlayerAccess()).CanViewChar) {
      return;
    }
    switch (this.Xt) {
      
      case 1:
        player1.sendPacket(new GMViewCharacterInfo(player2));
        player1.sendPacket(new GMHennaInfo(player2));
        break;
      case 2:
        if (player2.getClan() != null)
        {
          for (SubUnit subUnit : player2.getClan().getAllSubUnits())
            player1.sendPacket(new GMViewPledgeInfo(player2.getName(), player2.getClan(), subUnit)); 
        }
        break;
      case 3:
        player1.sendPacket(new GMViewSkillInfo(player2));
        break;
      case 4:
        player1.sendPacket(new GMViewQuestInfo(player2));
        break;
      case 5:
        arrayOfItemInstance = player2.getInventory().getItems();
        i = 0;
        for (ItemInstance itemInstance : arrayOfItemInstance) {
          if (itemInstance.getTemplate().isQuest())
            i++; 
        }  player1.sendPacket(new IStaticPacket[] { new GMViewItemList(true, player2, arrayOfItemInstance, arrayOfItemInstance.length - i), new GMViewItemList(false, player2, arrayOfItemInstance, arrayOfItemInstance.length - i) });
        player1.sendPacket(new IStaticPacket[] { new ExGMViewQuestItemList(true, player2, arrayOfItemInstance, i), new ExGMViewQuestItemList(false, player2, arrayOfItemInstance, i) });
        
        player1.sendPacket(new GMHennaInfo(player2));
        break;
      case 6:
        player1.sendPacket(new GMViewWarehouseWithdrawList(player2));
        break;
    } 
  }
}
