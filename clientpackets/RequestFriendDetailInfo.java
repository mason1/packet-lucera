package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.dao.CharacterDAO;
import l2.gameserver.model.Player;
import l2.gameserver.model.actor.instances.player.Friend;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExFriendDetailInfo;






public class RequestFriendDetailInfo
  extends L2GameClientPacket
{
  private String Zf;
  
  protected void readImpl() { this.Zf = readS(Config.CNAME_MAXLEN); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }

    
    int i = CharacterDAO.getInstance().getObjectIdByName(this.Zf);
    if (i == 0) {
      return;
    }

    
    Friend friend = (Friend)player.getFriendList().getList().get(Integer.valueOf(i));
    if (friend == null) {
      return;
    }

    
    sendPacket(new ExFriendDetailInfo(player.getObjectId(), friend));
  }
}
