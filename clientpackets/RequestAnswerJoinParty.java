package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.model.Party;
import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.ActionFail;
import l2.gameserver.network.l2.s2c.JoinParty;


public class RequestAnswerJoinParty
  extends L2GameClientPacket
{
  private int pI;
  
  protected void readImpl() {
    if (this._buf.hasRemaining()) {
      
      this.pI = readD();
    }
    else {
      
      this.pI = 0;
    } 
  }


  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }

    
    request = player1.getRequest();
    if (request == null || !request.isTypeOf(Request.L2RequestType.PARTY)) {
      return;
    }

    
    if (!request.isInProgress()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isOutOfControl()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    Player player2 = request.getRequestor();
    if (player2 == null) {
      
      request.cancel();
      player1.sendPacket(SystemMsg.THAT_PLAYER_IS_NOT_ONLINE);
      player1.sendActionFailed();
      
      return;
    } 
    if (player2.getRequest() != request) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    
    if (this.pI <= 0) {
      
      request.cancel();
      player2.sendPacket(JoinParty.FAIL);
      
      return;
    } 
    if (player1.isOlyParticipant()) {
      
      request.cancel();
      player1.sendPacket(SystemMsg.A_PARTY_CANNOT_BE_FORMED_IN_THIS_AREA);
      player2.sendPacket(JoinParty.FAIL);
      
      return;
    } 
    if (player2.isOlyParticipant()) {
      
      request.cancel();
      player2.sendPacket(JoinParty.FAIL);
      
      return;
    } 
    Party party = player2.getParty();
    
    if (party != null && party.getMemberCount() >= Config.ALT_MAX_PARTY_SIZE) {
      
      request.cancel();
      player1.sendPacket(SystemMsg.THE_PARTY_IS_FULL);
      player2.sendPacket(SystemMsg.THE_PARTY_IS_FULL);
      player2.sendPacket(JoinParty.FAIL);
      
      return;
    } 
    if (!Config.ALT_PARTY_CLASS_LIMIT.isEmpty() && Config.ALT_PARTY_CLASS_LIMIT.containsKey(Integer.valueOf(player1.getActiveClass().getClassId()))) {
      
      byte b = 0;
      if (party != null) {
        
        for (Player player : party.getPartyMembers())
        {
          if (player.getActiveClass().getClassId() == player1.getActiveClass().getClassId())
          {
            b++;
          
          }
        }
      
      }
      else if (player2.getActiveClass().getClassId() == player1.getActiveClass().getClassId()) {
        
        b++;
      } 
      
      if (b >= ((Integer)Config.ALT_PARTY_CLASS_LIMIT.get(Integer.valueOf(player1.getActiveClass().getClassId()))).intValue()) {
        
        request.cancel();
        player1.sendPacket(SystemMsg.PARTY_PARTICIPATION_HAS_FAILED_BECAUSE_REQUIREMENTS_ARE_NOT_MET);
        player2.sendPacket(new IStaticPacket[] { SystemMsg.PARTY_PARTICIPATION_HAS_FAILED_BECAUSE_REQUIREMENTS_ARE_NOT_MET, JoinParty.FAIL });
        
        return;
      } 
    } 
    IStaticPacket iStaticPacket = player1.canJoinParty(player2);
    if (iStaticPacket != null) {
      
      request.cancel();
      player1.sendPacket(new IStaticPacket[] { iStaticPacket, ActionFail.STATIC });
      player2.sendPacket(JoinParty.FAIL);
      
      return;
    } 
    if (party == null) {
      
      int i = request.getInteger("itemDistribution");
      player2.setParty(party = new Party(player2, i));
    } 

    
    try {
      player1.joinParty(party);
      player2.sendPacket(JoinParty.SUCCESS);
    }
    finally {
      
      request.done();
    } 
  }
}
