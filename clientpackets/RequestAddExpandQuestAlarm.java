package l2.gameserver.network.l2.c2s;

import l2.gameserver.instancemanager.QuestManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.quest.Quest;
import l2.gameserver.model.quest.QuestState;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExQuestNpcLogList;






public class RequestAddExpandQuestAlarm
  extends L2GameClientPacket
{
  private int _questId;
  
  protected void readImpl() { this._questId = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Quest quest = QuestManager.getQuest(this._questId);
    if (quest == null) {
      return;
    }
    QuestState questState = player.getQuestState(quest.getClass());
    if (questState == null) {
      return;
    }
    player.sendPacket(new ExQuestNpcLogList(questState));
  }
}
