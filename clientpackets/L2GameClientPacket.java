package l2.gameserver.network.l2.c2s;

import java.nio.BufferUnderflowException;
import java.util.List;
import l2.commons.net.nio.impl.ReceivablePacket;
import l2.gameserver.Config;
import l2.gameserver.GameServer;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




public abstract class L2GameClientPacket
  extends ReceivablePacket<GameClient>
{
  private static final Logger _log = LoggerFactory.getLogger(L2GameClientPacket.class);


  
  public final boolean read() {
    if (Config.DEBUG)
    {
      System.out.println("RECV: " + getClass().getSimpleName());
    }
    
    try {
      readImpl();
      return true;
    }
    catch (BufferUnderflowException bufferUnderflowException) {
      
      ((GameClient)this._client).onPacketReadFail();
      _log.error("Client: " + this._client + " - Failed reading: " + getType() + " - Server Version: " + GameServer.getInstance().getVersion().getRevisionNumber(), bufferUnderflowException);
    }
    catch (Exception exception) {
      
      _log.error("Client: " + this._client + " - Failed reading: " + getType() + " - Server Version: " + GameServer.getInstance().getVersion().getRevisionNumber(), exception);
    } 
    
    return false;
  }

  
  protected abstract void readImpl();

  
  public final void run() {
    GameClient gameClient = (GameClient)getClient();
    
    try {
      runImpl();
    }
    catch (Exception exception) {
      
      _log.error("Client: " + gameClient + " - Failed running: " + getType() + " - Server Version: " + GameServer.getInstance().getVersion().getRevisionNumber(), exception);
    } 
  }

  
  protected abstract void runImpl();
  
  protected String readS(int paramInt) {
    String str = readS();
    return (str.length() > paramInt) ? str.substring(0, paramInt) : str;
  }


  
  protected void sendPacket(L2GameServerPacket paramL2GameServerPacket) { ((GameClient)getClient()).sendPacket(paramL2GameServerPacket); }



  
  protected void sendPacket(L2GameServerPacket... paramVarArgs) { ((GameClient)getClient()).sendPacket(paramVarArgs); }



  
  protected void sendPackets(List<L2GameServerPacket> paramList) { ((GameClient)getClient()).sendPackets(paramList); }



  
  public String getType() { return "[C] " + getClass().getSimpleName(); }
}
