package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Party;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.Reflection;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;




public class RequestOustPartyMember
  extends L2GameClientPacket
{
  private String _name;
  
  protected void readImpl() { this._name = readS(16); }



  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    Party party = player1.getParty();
    if (party == null || !player1.getParty().isLeader(player1)) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isOlyParticipant()) {
      
      player1.sendMessage(new CustomMessage("l2p.gameserver.clientpackets.RequestOustPartyMember.CantOustNow", player1, new Object[0]));
      
      return;
    } 
    Player player2 = party.getPlayerByName(this._name);
    
    if (player2 == player1) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player2 == null) {
      
      player1.sendActionFailed();
      
      return;
    } 
    Reflection reflection = party.getReflection();
    
    if (reflection != null && reflection instanceof l2.gameserver.model.entity.DimensionalRift && player2.getReflection().equals(reflection)) {
      player1.sendMessage(new CustomMessage("l2p.gameserver.clientpackets.RequestOustPartyMember.CantOustInRift", player1, new Object[0]));
    } else if (reflection != null && !(reflection instanceof l2.gameserver.model.entity.DimensionalRift)) {
      player1.sendMessage(new CustomMessage("l2p.gameserver.clientpackets.RequestOustPartyMember.CantOustInDungeon", player1, new Object[0]));
    } else {
      party.removePartyMember(player2, true);
    } 
  }
}
