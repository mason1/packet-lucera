package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Party;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;


public class AnswerPartyLootModification
  extends L2GameClientPacket
{
  public int _answer;
  
  protected void readImpl() { this._answer = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Party party = player.getParty();
    if (party != null)
      party.answerLootChangeRequest(player, (this._answer == 1)); 
  }
}
