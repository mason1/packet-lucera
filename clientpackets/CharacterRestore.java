package l2.gameserver.network.l2.c2s;

import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.CharacterSelectionInfo;




public class CharacterRestore
  extends L2GameClientPacket
{
  private int Xv;
  
  protected void readImpl() { this.Xv = readD(); }



  
  protected void runImpl() {
    GameClient gameClient = (GameClient)getClient();
    
    try {
      gameClient.markRestoredChar(this.Xv);
    }
    catch (Exception exception) {}
    
    CharacterSelectionInfo characterSelectionInfo = new CharacterSelectionInfo(gameClient.getLogin(), (gameClient.getSessionKey()).playOkID1);
    sendPacket(characterSelectionInfo);
    gameClient.setCharSelection(characterSelectionInfo.getCharInfo());
  }
}
