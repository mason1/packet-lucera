package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.database.mysql;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.CharacterDeleteFail;
import l2.gameserver.network.l2.s2c.CharacterDeleteSuccess;
import l2.gameserver.network.l2.s2c.CharacterSelectionInfo;
import l2.gameserver.network.l2.s2c.Ex2ndPasswordCheck;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CharacterDelete
  extends L2GameClientPacket
{
  private static final Logger _log = LoggerFactory.getLogger(CharacterDelete.class);


  
  private int Xv;


  
  protected void readImpl() { this.Xv = readD(); }



  
  protected void runImpl() {
    int i = dg();
    int j = dh();
    if (i > 0 || j > 0) {
      
      if (i == 2) {
        sendPacket(new CharacterDeleteFail(CharacterDeleteFail.REASON_CLAN_LEADERS_MAY_NOT_BE_DELETED));
      } else if (i == 1) {
        sendPacket(new CharacterDeleteFail(CharacterDeleteFail.REASON_YOU_MAY_NOT_DELETE_CLAN_MEMBER));
      } else if (j > 0) {
        sendPacket(new CharacterDeleteFail(CharacterDeleteFail.REASON_DELETION_FAILED));
      } 
      return;
    } 
    GameClient gameClient = (GameClient)getClient();
    if (Config.USE_SECOND_PASSWORD_AUTH && !gameClient.isSecondPasswordAuthed()) {
      
      if (gameClient.getSecondPasswordAuth().isSecondPasswordSet()) {
        
        gameClient.sendPacket(new Ex2ndPasswordCheck(Ex2ndPasswordCheck.Ex2ndPasswordCheckResult.CHECK));
      }
      else {
        
        gameClient.sendPacket(new Ex2ndPasswordCheck(Ex2ndPasswordCheck.Ex2ndPasswordCheckResult.CREATE));
      } 
      
      return;
    } 
    
    try {
      if (Config.DELETE_DAYS == 0) {
        gameClient.deleteCharacterInSlot(this.Xv);
      } else {
        gameClient.markToDeleteChar(this.Xv);
      }  gameClient.sendPacket(new CharacterDeleteSuccess());
    }
    catch (Exception exception) {
      
      _log.error(exception.getMessage(), exception);
    } 
    
    CharacterSelectionInfo characterSelectionInfo = new CharacterSelectionInfo(gameClient.getLogin(), (gameClient.getSessionKey()).playOkID1);
    gameClient.sendPacket(characterSelectionInfo);
    gameClient.setCharSelection(characterSelectionInfo.getCharInfo());
  }

  
  private int dg() {
    int i = ((GameClient)getClient()).getObjectIdForSlot(this.Xv);
    if (i == -1)
      return 0; 
    if (mysql.simple_get_int("clanid", "characters", "obj_Id=" + i) > 0) {
      
      if (mysql.simple_get_int("leader_id", "clan_subpledges", "leader_id=" + i + " AND type = " + Character.MIN_VALUE) > 0)
        return 2; 
      return 1;
    } 
    return 0;
  }

  
  private int dh() {
    int i = ((GameClient)getClient()).getObjectIdForSlot(this.Xv);
    if (i == -1)
      return 0; 
    if (mysql.simple_get_int("online", "characters", "obj_Id=" + i) > 0)
      return 1; 
    return 0;
  }
}
