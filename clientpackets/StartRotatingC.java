package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.network.l2.s2c.StartRotating;




public class StartRotatingC
  extends L2GameClientPacket
{
  private int Xz;
  private int Pz;
  
  protected void readImpl() {
    this.Xz = readD();
    this.Pz = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    player.setHeading(this.Xz);
    player.broadcastPacket(new L2GameServerPacket[] { new StartRotating(player, this.Xz, this.Pz, 0) });
  }
}
