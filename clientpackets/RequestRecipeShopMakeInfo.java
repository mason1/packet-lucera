package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.items.ManufactureItem;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.RecipeShopItemInfo;


public class RequestRecipeShopMakeInfo
  extends L2GameClientPacket
{
  private int _manufacturerId;
  private int UE;
  
  protected void readImpl() {
    this._manufacturerId = readD();
    this.UE = readD();
  }


  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    if (player1.isActionsDisabled()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    Player player2 = (Player)player1.getVisibleObject(this._manufacturerId);
    if (player2 == null || player2.getPrivateStoreType() != 5 || !player2.isInActingRange(player1)) {
      
      player1.sendActionFailed();
      
      return;
    } 
    long l = -1L;
    for (ManufactureItem manufactureItem : player2.getCreateList()) {
      if (manufactureItem.getRecipeId() == this.UE) {
        
        l = manufactureItem.getCost();
        break;
      } 
    } 
    if (l == -1L) {
      
      player1.sendActionFailed();
      
      return;
    } 
    player1.sendPacket(new RecipeShopItemInfo(player1, player2, this.UE, l, -1));
  }
}
