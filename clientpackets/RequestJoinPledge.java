package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.GameObject;
import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.AskJoinPledge;
import l2.gameserver.network.l2.s2c.SystemMessage;


public class RequestJoinPledge
  extends L2GameClientPacket
{
  public static final String PLEDGE_TYPE_VAR_NAME = "pledgeType";
  private int Bq;
  private int GA;
  
  protected void readImpl() {
    this.Bq = readD();
    this.GA = readD();
  }


  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null || player1.getClan() == null) {
      return;
    }
    if (player1.isOutOfControl()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isProcessingRequest()) {
      
      player1.sendPacket(Msg.WAITING_FOR_ANOTHER_REPLY);
      
      return;
    } 
    Clan clan = player1.getClan();
    
    if (clan.isPlacedForDisband()) {
      
      player1.sendPacket(SystemMsg.DISPERSION_HAS_ALREADY_BEEN_REQUESTED);
      
      return;
    } 
    if (!clan.canInvite()) {
      
      player1.sendPacket(Msg.AFTER_A_CLAN_MEMBER_IS_DISMISSED_FROM_A_CLAN_THE_CLAN_MUST_WAIT_AT_LEAST_A_DAY_BEFORE_ACCEPTING_A_NEW_MEMBER);
      
      return;
    } 
    if (this.Bq == player1.getObjectId()) {
      
      player1.sendPacket(Msg.YOU_CANNOT_ASK_YOURSELF_TO_APPLY_TO_A_CLAN);
      
      return;
    } 
    if ((player1.getClanPrivileges() & 0x2) != 2) {
      
      player1.sendPacket(Msg.ONLY_THE_LEADER_CAN_GIVE_OUT_INVITATIONS);
      
      return;
    } 
    GameObject gameObject = player1.getVisibleObject(this.Bq);
    if (gameObject == null || !gameObject.isPlayer()) {
      
      player1.sendPacket(SystemMsg.THAT_IS_AN_INCORRECT_TARGET);
      
      return;
    } 
    Player player2 = (Player)gameObject;
    if (player2.getClan() == player1.getClan()) {
      
      player1.sendPacket(SystemMsg.THAT_IS_AN_INCORRECT_TARGET);
      
      return;
    } 
    if (!(player2.getPlayerAccess()).CanJoinClan) {
      
      player1.sendPacket((new SystemMessage(760)).addName(player2));
      
      return;
    } 
    if (player2.getClan() != null) {
      
      player1.sendPacket((new SystemMessage(10)).addName(player2));
      
      return;
    } 
    if (player2.isBusy()) {
      
      player1.sendPacket((new SystemMessage(153)).addName(player2));
      
      return;
    } 
    if (this.GA == -1 && (player2.getLevel() > 40 || player2.getClassId().getLevel() > 2)) {
      
      player1.sendPacket(Msg.TO_JOIN_A_CLAN_ACADEMY_CHARACTERS_MUST_BE_LEVEL_40_OR_BELOW_NOT_BELONG_ANOTHER_CLAN_AND_NOT_YET_COMPLETED_THEIR_2ND_CLASS_TRANSFER);
      
      return;
    } 
    if (clan.getUnitMembersSize(this.GA) >= clan.getSubPledgeLimit(this.GA)) {
      
      if (this.GA == 0) {
        player1.sendPacket((new SystemMessage(1835)).addString(clan.getName()));
      } else {
        player1.sendPacket(Msg.THE_ACADEMY_ROYAL_GUARD_ORDER_OF_KNIGHTS_IS_FULL_AND_CANNOT_ACCEPT_NEW_MEMBERS_AT_THIS_TIME);
      } 
      return;
    } 
    Request request = (new Request(Request.L2RequestType.CLAN, player1, player2)).setTimeout(10000L);
    request.set("pledgeType", this.GA);
    player2.sendPacket(new AskJoinPledge(player1.getObjectId(), player1.getClan().getName()));
  }
}
