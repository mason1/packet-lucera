package l2.gameserver.network.l2.c2s;

import l2.commons.lang.ArrayUtils;
import l2.gameserver.Config;
import l2.gameserver.data.xml.holder.SkillAcquireHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.SkillLearn;
import l2.gameserver.model.base.AcquireType;
import l2.gameserver.model.base.ClassId;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.AcquireSkillInfo;
import l2.gameserver.tables.SkillTable;





public class RequestAquireSkillInfo
  extends L2GameClientPacket
{
  private int _id;
  private int _level;
  private AcquireType XO;
  
  protected void readImpl() {
    this._id = readD();
    this._level = readD();
    this.XO = (AcquireType)ArrayUtils.valid(AcquireType.VALUES, readD());
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || player.getTransformation() != 0 || player.isCursedWeaponEquipped() || 
      SkillTable.getInstance().getInfo(this._id, this._level) == null || this.XO == null) {
      return;
    }
    NpcInstance npcInstance = player.getLastNpc();
    if (npcInstance == null || !npcInstance.isInActingRange(player)) {
      return;
    }
    int i = player.getVarInt("AcquireSkillClassId", player.getClassId().getId());
    ClassId classId = (i >= 0 && i < ClassId.VALUES.length) ? ClassId.VALUES[i] : null;
    
    SkillLearn skillLearn = SkillAcquireHolder.getInstance().getSkillLearn(player, classId, this._id, this._level, this.XO);
    if (skillLearn == null) {
      return;
    }
    if (Config.ALT_DISABLE_SPELLBOOKS && this.XO == AcquireType.NORMAL) {
      
      sendPacket(new AcquireSkillInfo(this.XO, skillLearn, 0, 0));
    }
    else {
      
      sendPacket(new AcquireSkillInfo(this.XO, skillLearn, skillLearn.getItemId(), (int)skillLearn.getItemCount()));
    } 
  }
}
