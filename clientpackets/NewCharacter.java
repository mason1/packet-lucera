package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.base.ClassId;
import l2.gameserver.network.l2.s2c.NewCharacterSuccess;
import l2.gameserver.tables.CharTemplateTable;




public class NewCharacter
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    NewCharacterSuccess newCharacterSuccess = new NewCharacterSuccess();
    
    newCharacterSuccess.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.fighter, false));
    newCharacterSuccess.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.mage, false));
    newCharacterSuccess.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.elvenFighter, false));
    newCharacterSuccess.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.elvenMage, false));
    newCharacterSuccess.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.darkFighter, false));
    newCharacterSuccess.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.darkMage, false));
    newCharacterSuccess.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.orcFighter, false));
    newCharacterSuccess.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.orcMage, false));
    newCharacterSuccess.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.dwarvenFighter, false));
    
    sendPacket(newCharacterSuccess);
  }
}
