package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;



public class RequestExMoveToLocationAirShip
  extends L2GameClientPacket
{
  private int YS;
  private int YT;
  private int YU;
  
  protected void readImpl() {
    this.YS = readD();
    switch (this.YS) {
      
      case 4:
        this.YT = readD() + 1;
        break;
      case 0:
        this.YT = readD();
        this.YU = readD();
        break;
      case 2:
        readD();
        readD();
        break;
      case 3:
        readD();
        readD();
        break;
    } 
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || player.getBoat() == null)
      return; 
  }
}
