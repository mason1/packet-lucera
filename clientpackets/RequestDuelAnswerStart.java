package l2.gameserver.network.l2.c2s;

import l2.gameserver.data.xml.holder.EventHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.model.entity.events.EventType;
import l2.gameserver.model.entity.events.impl.DuelEvent;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.SystemMessage2;


public class RequestDuelAnswerStart
  extends L2GameClientPacket
{
  private int pI;
  private int YA;
  
  protected void readImpl() {
    this.YA = readD();
    readD();
    this.pI = readD();
  }

  
  protected void runImpl() {
    SystemMessage2 systemMessage22, systemMessage21;
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    request = player1.getRequest();
    if (request == null || !request.isTypeOf(Request.L2RequestType.DUEL)) {
      return;
    }
    if (!request.isInProgress()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isActionsDisabled()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    Player player2 = request.getRequestor();
    if (player2 == null) {
      
      request.cancel();
      player1.sendPacket(SystemMsg.THAT_PLAYER_IS_NOT_ONLINE);
      player1.sendActionFailed();
      
      return;
    } 
    if (player2.getRequest() != request) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    if (this.YA != request.getInteger("duelType")) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    DuelEvent duelEvent = (DuelEvent)EventHolder.getInstance().getEvent(EventType.PVP_EVENT, this.YA);
    
    switch (this.pI) {
      
      case 0:
        request.cancel();
        if (this.YA == 1) {
          player2.sendPacket(SystemMsg.THE_OPPOSING_PARTY_HAS_DECLINED_YOUR_CHALLENGE_TO_A_DUEL); break;
        } 
        player2.sendPacket((new SystemMessage2(SystemMsg.C1_HAS_DECLINED_YOUR_CHALLENGE_TO_A_PARTY_DUEL)).addName(player1));
        break;
      case -1:
        request.cancel();
        player2.sendPacket((new SystemMessage2(SystemMsg.C1_IS_SET_TO_REFUSE_DUEL_REQUESTS_AND_CANNOT_RECEIVE_A_DUEL_REQUEST)).addName(player1));
        break;
      case 1:
        if (!duelEvent.canDuel(player2, player1, false)) {
          
          request.cancel();

          
          return;
        } 
        
        if (this.YA == 1) {
          
          systemMessage21 = new SystemMessage2(SystemMsg.YOU_HAVE_ACCEPTED_C1S_CHALLENGE_TO_A_PARTY_DUEL);
          systemMessage22 = new SystemMessage2(SystemMsg.S1_HAS_ACCEPTED_YOUR_CHALLENGE_TO_DUEL_AGAINST_THEIR_PARTY);
        }
        else {
          
          systemMessage21 = new SystemMessage2(SystemMsg.YOU_HAVE_ACCEPTED_C1S_CHALLENGE_A_DUEL);
          systemMessage22 = new SystemMessage2(SystemMsg.C1_HAS_ACCEPTED_YOUR_CHALLENGE_TO_A_DUEL);
        } 
        
        player1.sendPacket(systemMessage21.addName(player2));
        player2.sendPacket(systemMessage22.addName(player1));

        
        try {
          duelEvent.createDuel(player2, player1);
        }
        finally {
          
          request.done();
        } 
        break;
    } 
  }
}
