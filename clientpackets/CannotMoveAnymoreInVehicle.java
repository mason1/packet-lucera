package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.entity.boat.Boat;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.utils.Location;

public class CannotMoveAnymoreInVehicle extends L2GameClientPacket {
  private Location _loc = new Location();
  
  private int Xu;

  
  protected void readImpl() {
    this.Xu = readD();
    this._loc.x = readD();
    this._loc.y = readD();
    this._loc.z = readD();
    this._loc.h = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Boat boat = player.getBoat();
    if (boat != null && boat.getObjectId() == this.Xu) {
      
      player.setInBoatPosition(this._loc);
      player.setHeading(this._loc.h);
      player.broadcastPacket(new L2GameServerPacket[] { boat.inStopMovePacket(player) });
    } 
  }
}
