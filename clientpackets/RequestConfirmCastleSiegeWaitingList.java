package l2.gameserver.network.l2.c2s;

import l2.gameserver.dao.SiegeClanDAO;
import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.events.impl.CastleSiegeEvent;
import l2.gameserver.model.entity.events.objects.SiegeClanObject;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.CastleSiegeDefenderList;





public class RequestConfirmCastleSiegeWaitingList
  extends L2GameClientPacket
{
  private boolean Yn;
  private int Yg;
  private int Bs;
  
  protected void readImpl() {
    this.Yg = readD();
    this.Bs = readD();
    this.Yn = (readD() == 1);
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.getClan() == null) {
      return;
    }
    Castle castle = (Castle)ResidenceHolder.getInstance().getResidence(Castle.class, this.Yg);
    
    if (castle == null || player.getClan().getCastle() != castle.getId()) {
      
      player.sendActionFailed();
      
      return;
    } 
    CastleSiegeEvent castleSiegeEvent = (CastleSiegeEvent)castle.getSiegeEvent();
    
    SiegeClanObject siegeClanObject = castleSiegeEvent.getSiegeClan("defenders_waiting", this.Bs);
    if (siegeClanObject == null) {
      siegeClanObject = castleSiegeEvent.getSiegeClan("defenders", this.Bs);
    }
    if (siegeClanObject == null) {
      return;
    }
    if ((player.getClanPrivileges() & 0x20000) != 131072) {
      
      player.sendPacket(SystemMsg.YOU_DO_NOT_HAVE_THE_AUTHORITY_TO_MODIFY_THE_CASTLE_DEFENDER_LIST);
      
      return;
    } 
    if (castleSiegeEvent.isRegistrationOver()) {
      
      player.sendPacket(SystemMsg.THIS_IS_NOT_THE_TIME_FOR_SIEGE_REGISTRATION_AND_SO_REGISTRATIONS_CANNOT_BE_ACCEPTED_OR_REJECTED);
      
      return;
    } 
    int i = castleSiegeEvent.getObjects("defenders").size();
    if (i >= 20) {
      
      player.sendPacket(SystemMsg.NO_MORE_REGISTRATIONS_MAY_BE_ACCEPTED_FOR_THE_DEFENDER_SIDE);
      
      return;
    } 
    castleSiegeEvent.removeObject(siegeClanObject.getType(), siegeClanObject);
    
    if (this.Yn) {
      siegeClanObject.setType("defenders");
    } else {
      siegeClanObject.setType("defenders_refused");
    } 
    castleSiegeEvent.addObject(siegeClanObject.getType(), siegeClanObject);
    
    SiegeClanDAO.getInstance().update(castle, siegeClanObject);
    
    player.sendPacket(new CastleSiegeDefenderList(castle));
  }
}
