package l2.gameserver.network.l2.c2s;

import l2.gameserver.handler.admincommands.AdminCommandHandler;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;

public class SendBypassBuildCmd
  extends L2GameClientPacket
{
  private String aam;
  
  protected void readImpl() {
    this.aam = readS();
    
    if (this.aam != null) {
      this.aam = this.aam.trim();
    }
  }

  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    
    if (player == null) {
      return;
    }
    String str = this.aam;
    
    if (!str.contains("admin_")) {
      str = "admin_" + str;
    }
    AdminCommandHandler.getInstance().useAdminCommandHandler(player, str);
  }
}
