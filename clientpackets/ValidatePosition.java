package l2.gameserver.network.l2.c2s;
import l2.gameserver.data.BoatHolder;
import l2.gameserver.geodata.GeoEngine;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.boat.Boat;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExServerPrimitive;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.utils.Location;

public class ValidatePosition extends L2GameClientPacket {
  private final Location _loc = new Location();

  
  private int YW;

  
  private Location Iv;

  
  private Location Iw;

  
  protected void readImpl() {
    this._loc.x = readD();
    this._loc.y = readD();
    this._loc.z = readD();
    this._loc.h = readD();
    this.YW = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.isTeleporting() || player.isInObserverMode() || player.isOlyObserver()) {
      return;
    }
    this.Iv = player.getLastClientPosition();
    this.Iw = player.getLastServerPosition();
    
    if (this.Iv == null)
      this.Iv = player.getLoc(); 
    if (this.Iw == null) {
      this.Iw = player.getLoc();
    }
    if (player.getX() == 0 && player.getY() == 0 && player.getZ() == 0) {
      
      Z(player);
      
      return;
    } 
    if (player.isInFlyingTransform()) {

      
      if (this._loc.x > -166168) {
        
        player.setTransformation(0);
        
        return;
      } 
      
      if (this._loc.z <= 0 || this._loc.z >= 6000) {
        
        player.teleToLocation(player.getLoc().setZ(Math.min(5950, Math.max(50, this._loc.z))));
        
        return;
      } 
    } 
    double d = player.getDistance(this._loc.x, this._loc.y);
    int i = Math.abs(this._loc.z - player.getZ());
    int j = this.Iw.z - player.getZ();
    
    if (this.YW > 0) {
      
      Boat boat = BoatHolder.getInstance().getBoat(this.YW);
      if (boat != null && player.getBoat() == boat) {
        
        player.setHeading(this._loc.h);
        boat.validateLocationPacket(player);
      } 
      player.setLastClientPosition(this._loc.setH(player.getHeading()));
      player.setLastServerPosition(player.getLoc());
      
      return;
    } 
    
    if (player.isFalling()) {
      
      d = 0.0D;
      i = 0;
      j = 0;
    } 
    
    int k = 256 + ((GameClient)getClient()).getPing() * player.getMoveSpeed() / 1000;
    int m = player.maxZDiff() + 64;
    int n = player.getVarInt("debugMove", 0);
    if (n > 0) {
      
      ExServerPrimitive exServerPrimitive = new ExServerPrimitive("", player.getLoc().clone().setZ(player.getZ() + 64));
      exServerPrimitive.addLine("Diff: " + d + " max: " + k, 16777215, true, player
          .getX(), player.getY(), player.getZ() + 80, this._loc
          .getX(), this._loc.getY(), this._loc.getZ() + 64);
      player.broadcastPacket(new L2GameServerPacket[] { exServerPrimitive });
    } 
    
    boolean bool = (!player.isInWater() && !player.isFlying()) ? 1 : 0;
    
    if (bool && j >= 256) {
      
      player.falling(j);
    }
    else if (bool && i >= m) {
      
      if (player.getIncorrectValidateCount() >= 6) {
        player.teleToClosestTown();
      } else if (player.getIncorrectValidateCount() > 3) {
        
        player.teleToLocation(Location.findPointToStay(this.Iw, player.getIncorrectValidateCount() * 32, player.getGeoIndex()));
        player.setIncorrectValidateCount(player.getIncorrectValidateCount() + 1);
      }
      else {
        
        player.teleToLocation(this.Iw);
        player.setIncorrectValidateCount(player.getIncorrectValidateCount() + 1);
      }
    
    } else if (bool && i >= m / 2) {
      
      player.validateLocation(0);
    }
    else if (this._loc.z < -30000 || this._loc.z > 30000) {
      
      if (player.getIncorrectValidateCount() >= 3) {
        player.teleToClosestTown();
      } else {
        
        Z(player);
        player.setIncorrectValidateCount(player.getIncorrectValidateCount() + 1);
      }
    
    } else if (d > 1024.0D) {
      
      if (player.getIncorrectValidateCount() >= 6) {
        player.teleToClosestTown();
      } else if (player.getIncorrectValidateCount() > 3) {
        
        player.teleToLocation(Location.findPointToStay(this.Iw, player.getIncorrectValidateCount() * 32, player.getGeoIndex()));
        player.setIncorrectValidateCount(player.getIncorrectValidateCount() + 1);
      }
      else {
        
        player.teleToLocation(player.getLoc());
        player.setIncorrectValidateCount(player.getIncorrectValidateCount() + 1);
      }
    
    } else if (d > k) {
      
      player.validateLocation(1);
    } else {
      
      player.setIncorrectValidateCount(0);
    } 
    player.setLastClientPosition(this._loc.setH(player.getHeading()));
    player.setLastServerPosition(player.getLoc());
  }

  
  private void Z(Player paramPlayer) {
    if (paramPlayer.isGM()) {
      
      paramPlayer.sendMessage("Server loc: " + paramPlayer.getLoc());
      paramPlayer.sendMessage("Correcting position...");
    } 
    if (this.Iw.x != 0 && this.Iw.y != 0 && this.Iw.z != 0) {
      
      if (GeoEngine.getNSWE(this.Iw.x, this.Iw.y, this.Iw.z, paramPlayer.getGeoIndex()) == 15) {
        paramPlayer.teleToLocation(this.Iw);
      } else {
        paramPlayer.teleToClosestTown();
      } 
    } else if (this.Iv.x != 0 && this.Iv.y != 0 && this.Iv.z != 0) {
      
      if (GeoEngine.getNSWE(this.Iv.x, this.Iv.y, this.Iv.z, paramPlayer.getGeoIndex()) == 15) {
        paramPlayer.teleToLocation(this.Iv);
      } else {
        paramPlayer.teleToClosestTown();
      } 
    } else {
      paramPlayer.teleToClosestTown();
    } 
  }
}
