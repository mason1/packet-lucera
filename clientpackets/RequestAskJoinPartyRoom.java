package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.model.World;
import l2.gameserver.model.matching.MatchingRoom;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.ExAskJoinPartyRoom;
import l2.gameserver.network.l2.s2c.SystemMessage2;






public class RequestAskJoinPartyRoom
  extends L2GameClientPacket
{
  private String _name;
  
  protected void readImpl() { this._name = readS(16); }



  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null)
      return; 
    Player player2 = World.getPlayer(this._name);
    
    if (player2 == null || player2 == player1) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isProcessingRequest()) {
      
      player1.sendPacket(SystemMsg.WAITING_FOR_ANOTHER_REPLY);
      
      return;
    } 
    if (player2.isProcessingRequest()) {
      
      player1.sendPacket((new SystemMessage2(SystemMsg.C1_IS_ON_ANOTHER_TASK)).addName(player2));
      
      return;
    } 
    if (player2.getMatchingRoom() != null) {
      return;
    }
    MatchingRoom matchingRoom = player1.getMatchingRoom();
    if (matchingRoom == null || matchingRoom.getType() != MatchingRoom.PARTY_MATCHING) {
      return;
    }
    if (matchingRoom.getLeader() != player1) {
      
      player1.sendPacket(SystemMsg.ONLY_A_ROOM_LEADER_MAY_INVITE_OTHERS_TO_A_PARTY_ROOM);
      
      return;
    } 
    if (matchingRoom.getPlayers().size() >= matchingRoom.getMaxMembersSize()) {
      
      player1.sendPacket(SystemMsg.THE_PARTY_ROOM_IS_FULL);
      
      return;
    } 
    (new Request(Request.L2RequestType.PARTY_ROOM, player1, player2)).setTimeout(10000L);
    
    player2.sendPacket(new ExAskJoinPartyRoom(player1.getName(), matchingRoom.getTopic()));
    
    player1.sendPacket(((SystemMessage2)(new SystemMessage2(SystemMsg.S1_HAS_SENT_AN_INVITATION_TO_ROOM_S2)).addName(player1)).addString(matchingRoom.getTopic()));
    player2.sendPacket(((SystemMessage2)(new SystemMessage2(SystemMsg.S1_HAS_SENT_AN_INVITATION_TO_ROOM_S2)).addName(player1)).addString(matchingRoom.getTopic()));
  }
}
