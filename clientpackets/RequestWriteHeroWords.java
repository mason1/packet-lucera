package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.entity.oly.HeroController;
import l2.gameserver.network.l2.GameClient;









public class RequestWriteHeroWords
  extends L2GameClientPacket
{
  private String aah;
  
  protected void readImpl() { this.aah = readS(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || !player.isHero()) {
      return;
    }
    if (this.aah == null || this.aah.length() > 300) {
      return;
    }
    HeroController.getInstance().setHeroMessage(player.getObjectId(), this.aah);
  }
}
