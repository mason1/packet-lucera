package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.data.xml.holder.ItemHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.PremiumItem;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExGetPremiumItemList;
import l2.gameserver.network.l2.s2c.SystemMessage2;


public final class RequestWithDrawPremiumItem
  extends L2GameClientPacket
{
  private int aaf;
  private int Br;
  private long aag;
  
  protected void readImpl() {
    this.aaf = readD();
    this.Br = readD();
    this.aag = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    
    if (player == null)
      return; 
    if (this.aag <= 0L) {
      return;
    }
    if (player.getObjectId() != this.Br) {
      return;
    }
    if (player.getPremiumItemList().isEmpty()) {
      return;
    }
    if (player.getWeightPenalty() >= 3 || player.getInventoryLimit() * 0.8D <= player.getInventory().getSize()) {
      
      player.sendPacket(Msg.YOU_CANNOT_RECEIVE_THE_VITAMIN_ITEM_BECAUSE_YOU_HAVE_EXCEED_YOUR_INVENTORY_WEIGHT_QUANTITY_LIMIT);
      return;
    } 
    if (player.isProcessingRequest()) {
      
      player.sendPacket(Msg.YOU_CANNOT_RECEIVE_A_VITAMIN_ITEM_DURING_AN_EXCHANGE);
      
      return;
    } 
    PremiumItem premiumItem = (PremiumItem)player.getPremiumItemList().get(Integer.valueOf(this.aaf));
    if (premiumItem == null)
      return; 
    boolean bool = ItemHolder.getInstance().getTemplate(premiumItem.getItemId()).isStackable();
    if (premiumItem.getCount() < this.aag)
      return; 
    if (!bool) {
      for (byte b = 0; b < this.aag; b++)
        b(player, premiumItem.getItemId(), 1L); 
    } else {
      b(player, premiumItem.getItemId(), this.aag);
    }  if (this.aag < premiumItem.getCount()) {
      
      ((PremiumItem)player.getPremiumItemList().get(Integer.valueOf(this.aaf))).updateCount(premiumItem.getCount() - this.aag);
      player.updatePremiumItem(this.aaf, premiumItem.getCount() - this.aag);
    }
    else {
      
      player.getPremiumItemList().remove(Integer.valueOf(this.aaf));
      player.deletePremiumItem(this.aaf);
    } 
    
    if (player.getPremiumItemList().isEmpty()) {
      player.sendPacket(Msg.THERE_ARE_NO_MORE_VITAMIN_ITEMS_TO_BE_FOUND);
    } else {
      player.sendPacket(new ExGetPremiumItemList(player));
    } 
  }
  
  private void b(Player paramPlayer, int paramInt, long paramLong) {
    paramPlayer.getInventory().addItem(paramInt, paramLong);
    paramPlayer.sendPacket(SystemMessage2.obtainItems(paramInt, paramLong, 0));
  }
}
