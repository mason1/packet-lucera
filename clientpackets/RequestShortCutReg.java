package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.actor.instances.player.ShortCut;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ShortCutRegister;

public class RequestShortCutReg extends L2GameClientPacket {
  private int _type;
  private int _id;
  private int _slot;
  
  protected void readImpl() {
    this._type = readD();
    int i = readD();
    this._id = readD();
    this.ZY = readD();
    this._characterType = readD();
    
    this._slot = i % 12;
    this._page = i / 12;
  }
  
  private int _page;
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (this._page < 0 || this._page > 11) {
      
      player.sendActionFailed();
      
      return;
    } 
    switch (this._type) {
      
      case 2:
        this.ZY = player.getSkillLevel(Integer.valueOf(this._id));
        break;
      default:
        this.ZY = 0;
        break;
    } 

    
    ShortCut shortCut = new ShortCut(this._slot, this._page, this._type, this._id, this.ZY, this._characterType);
    player.sendPacket(new ShortCutRegister(player, shortCut));
    player.registerShortCut(shortCut);
  }
  
  private int ZY;
  private int _characterType;
}
