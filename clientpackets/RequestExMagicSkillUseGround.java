package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.Creature;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.tables.SkillTable;
import l2.gameserver.utils.Location;














public class RequestExMagicSkillUseGround
  extends L2GameClientPacket
{
  private Location _loc = new Location();

  
  private int _skillId;
  
  private boolean XL;
  
  private boolean XM;

  
  protected void readImpl() {
    this._loc.x = readD();
    this._loc.y = readD();
    this._loc.z = readD();
    this._skillId = readD();
    this.XL = (readD() != 0);
    this.XM = (readC() != 0);
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.isOutOfControl()) {
      
      player.sendActionFailed();
      
      return;
    } 
    Skill skill = SkillTable.getInstance().getInfo(this._skillId, player.getSkillLevel(Integer.valueOf(this._skillId)));
    if (skill != null) {
      
      if (skill.getAddedSkills().length == 0) {
        return;
      }
      
      if ((player.getTransformation() != 0 || player.isCursedWeaponEquipped()) && 
        !player.getAllSkills().contains(skill)) {
        return;
      }
      if (!player.isInRange(this._loc, skill.getCastRange())) {
        
        player.sendPacket(Msg.YOUR_TARGET_IS_OUT_OF_RANGE);
        player.sendActionFailed();
        
        return;
      } 
      Creature creature = skill.getAimingTarget(player, player.getTarget());
      
      if (skill.checkCondition(player, creature, this.XL, this.XM, true)) {
        
        player.setGroundSkillLoc(this._loc);
        player.getAI().Cast(skill, creature, this.XL, this.XM);
      } else {
        
        player.sendActionFailed();
      } 
    } else {
      player.sendActionFailed();
    } 
  }
}
