package l2.gameserver.network.l2.c2s;

import l2.gameserver.instancemanager.MatchingRoomManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.matching.MatchingRoom;
import l2.gameserver.network.l2.GameClient;





public class RequestExJoinMpccRoom
  extends L2GameClientPacket
{
  private int Yy;
  
  protected void readImpl() { this.Yy = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.getMatchingRoom() != null) {
      return;
    }
    MatchingRoom matchingRoom = MatchingRoomManager.getInstance().getMatchingRoom(MatchingRoom.CC_MATCHING, this.Yy);
    if (matchingRoom == null) {
      return;
    }
    matchingRoom.addMember(player);
  }
}
