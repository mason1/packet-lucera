package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.SystemMessage2;
import l2.gameserver.tables.PetDataTable;
import l2.gameserver.utils.ItemFunctions;
import l2.gameserver.utils.Log;







public class RequestDestroyItem
  extends L2GameClientPacket
{
  private int Bq;
  private long Be;
  
  protected void readImpl() {
    this.Bq = readD();
    this.Be = readQ();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
      
      return;
    } 
    long l = this.Be;
    
    ItemInstance itemInstance = player.getInventory().getItemByObjectId(this.Bq);
    if (itemInstance == null) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (l < 1L) {
      
      player.sendPacket(Msg.YOU_CANNOT_DESTROY_IT_BECAUSE_THE_NUMBER_IS_INCORRECT);
      
      return;
    } 
    if (!player.isGM() && itemInstance.isHeroWeapon()) {
      
      player.sendPacket(Msg.HERO_WEAPONS_CANNOT_BE_DESTROYED);
      
      return;
    } 
    if (player.getPet() != null && player.getPet().getControlItemObjId() == itemInstance.getObjectId()) {
      
      player.sendPacket(Msg.THE_PET_HAS_BEEN_SUMMONED_AND_CANNOT_BE_DELETED);
      
      return;
    } 
    if (!player.isGM() && !itemInstance.canBeDestroyed(player)) {
      
      player.sendPacket(Msg.THIS_ITEM_CANNOT_BE_DISCARDED);
      
      return;
    } 
    if (this.Be > itemInstance.getCount()) {
      l = itemInstance.getCount();
    }
    boolean bool = itemInstance.canBeCrystallized(player);
    
    int i = (itemInstance.getTemplate().getCrystalType()).cry;
    int j = itemInstance.getTemplate().getCrystalCount();
    if (bool)
    {
      if (Config.DWARF_AUTOMATICALLY_CRYSTALLIZE_ON_ITEM_DELETE) {
        
        int k = player.getSkillLevel(Integer.valueOf(248));
        if (k < 1 || i - 1458 + 1 > k) {
          bool = false;
        }
      } else {
        bool = false;
      } 
    }
    Log.LogItem(player, Log.ItemLog.Delete, itemInstance, l);
    
    if (!player.getInventory().destroyItemByObjectId(this.Bq, l)) {
      
      player.sendActionFailed();
      
      return;
    } 
    
    if (PetDataTable.isPetControlItem(itemInstance)) {
      PetDataTable.deletePet(itemInstance, player);
    }
    if (bool) {
      
      player.sendPacket(Msg.THE_ITEM_HAS_BEEN_SUCCESSFULLY_CRYSTALLIZED);
      ItemFunctions.addItem(player, i, j, true);
    } else {
      
      player.sendPacket(SystemMessage2.removeItems(itemInstance.getItemId(), l));
    } 
    player.sendChanges();
  }
}
