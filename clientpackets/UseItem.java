package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.ExUseSharedGroupItem;
import l2.gameserver.network.l2.s2c.SystemMessage2;
import l2.gameserver.skills.TimeStamp;
import l2.gameserver.tables.PetDataTable;

public class UseItem
  extends L2GameClientPacket
{
  private int Bq;
  private boolean XL;
  
  protected void readImpl() {
    this.Bq = readD();
    this.XL = (readD() == 1);
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    player.setActive();
    
    ItemInstance itemInstance = player.getInventory().getItemByObjectId(this.Bq);
    if (itemInstance == null) {
      
      player.sendActionFailed();
      
      return;
    } 
    int i = itemInstance.getItemId();
    
    if (player.isInStoreMode()) {
      
      if (PetDataTable.isPetControlItem(itemInstance)) {
        player.sendPacket(SystemMsg.YOU_CANNOT_SUMMON_DURING_A_TRADE_OR_WHILE_USING_A_PRIVATE_STORE);
      } else {
        player.sendPacket(SystemMsg.YOU_MAY_NOT_USE_ITEMS_IN_A_PRIVATE_STORE_OR_PRIVATE_WORK_SHOP);
      } 
      return;
    } 
    if (player.isFishing() && (i < 6535 || i > 6540)) {
      
      player.sendPacket(SystemMsg.YOU_CANNOT_DO_THAT_WHILE_FISHING_2);
      
      return;
    } 
    if (player.isSharedGroupDisabled(itemInstance.getTemplate().getReuseGroup())) {
      
      player.sendReuseMessage(itemInstance);
      
      return;
    } 
    if (!itemInstance.getTemplate().testCondition(player, itemInstance, true)) {
      return;
    }
    if (player.getInventory().isLockedItem(itemInstance)) {
      return;
    }
    if (itemInstance.getTemplate().isForPet()) {
      
      player.sendPacket(SystemMsg.YOU_MAY_NOT_EQUIP_A_PET_ITEM);
      
      return;
    } 
    
    if (Config.ALT_IMPROVED_PETS_LIMITED_USE && player.isMageClass() && itemInstance.getItemId() == 10311) {
      
      player.sendPacket((new SystemMessage2(SystemMsg.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS)).addItemName(i));
      
      return;
    } 
    
    if (Config.ALT_IMPROVED_PETS_LIMITED_USE && !player.isMageClass() && itemInstance.getItemId() == 10313) {
      
      player.sendPacket((new SystemMessage2(SystemMsg.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS)).addItemName(i));
      
      return;
    } 
    if (player.isOutOfControl()) {
      
      player.sendActionFailed();
      
      return;
    } 
    boolean bool = itemInstance.getTemplate().getHandler().useItem(player, itemInstance, this.XL);
    if (bool) {
      
      long l = itemInstance.getTemplate().getReuseType().next(itemInstance);
      if (l > System.currentTimeMillis()) {
        
        TimeStamp timeStamp = new TimeStamp(itemInstance.getItemId(), l, itemInstance.getTemplate().getReuseDelay());
        player.addSharedGroupReuse(itemInstance.getTemplate().getReuseGroup(), timeStamp);
        
        if (itemInstance.getTemplate().getReuseDelay() > 0) {
          player.sendPacket(new ExUseSharedGroupItem(itemInstance.getTemplate().getDisplayReuseGroup(), timeStamp));
        }
      } 
    } else {
      
      player.sendActionFailed();
    } 
  }
}
