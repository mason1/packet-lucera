package l2.gameserver.network.l2.c2s;

import java.util.Set;
import l2.commons.dao.JdbcEntityState;
import l2.commons.math.SafeMath;
import l2.gameserver.cache.Msg;
import l2.gameserver.dao.MailDAO;
import l2.gameserver.model.Player;
import l2.gameserver.model.World;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.mail.Mail;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExShowReceivedPostList;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.utils.ItemFunctions;
import l2.gameserver.utils.Log;













public class RequestExReceivePost
  extends L2GameClientPacket
{
  private int YI;
  
  protected void readImpl() { this.YI = readD(); }



  
  protected void runImpl() {
    player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.YOU_CANNOT_RECEIVE_BECAUSE_THE_PRIVATE_SHOP_OR_WORKSHOP_IS_IN_PROGRESS);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendPacket(Msg.YOU_CANNOT_RECEIVE_DURING_AN_EXCHANGE);
      
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
      
      return;
    } 
    if (player.getEnchantScroll() != null) {
      
      player.sendPacket(Msg.YOU_CANNOT_RECEIVE_DURING_AN_ITEM_ENHANCEMENT_OR_ATTRIBUTE_ENHANCEMENT);
      
      return;
    } 
    Mail mail = MailDAO.getInstance().getReceivedMailByMailId(player.getObjectId(), this.YI);
    if (mail != null) {
      
      player.getInventory().writeLock();
      try {
        ItemInstance[] arrayOfItemInstance;
        Set set = mail.getAttachments();

        
        if (set.size() > 0 && !player.isInPeaceZone()) {
          
          player.sendPacket(Msg.YOU_CANNOT_RECEIVE_IN_A_NON_PEACE_ZONE_LOCATION);
          return;
        } 
        synchronized (set) {
          
          if (mail.getAttachments().isEmpty()) {
            return;
          }
          arrayOfItemInstance = (ItemInstance[])mail.getAttachments().toArray(new ItemInstance[set.size()]);
          
          byte b = 0;
          long l = 0L;
          for (ItemInstance itemInstance : arrayOfItemInstance) {
            
            l = SafeMath.addAndCheck(l, SafeMath.mulAndCheck(itemInstance.getCount(), itemInstance.getTemplate().getWeight()));
            if (!itemInstance.getTemplate().isStackable() || player.getInventory().getItemByItemId(itemInstance.getItemId()) == null) {
              b++;
            }
          } 
          if (!player.getInventory().validateWeight(l)) {
            
            sendPacket(Msg.YOU_COULD_NOT_RECEIVE_BECAUSE_YOUR_INVENTORY_IS_FULL);
            
            return;
          } 
          if (!player.getInventory().validateCapacity(b)) {
            
            sendPacket(Msg.YOU_COULD_NOT_RECEIVE_BECAUSE_YOUR_INVENTORY_IS_FULL);
            
            return;
          } 
          if (mail.getPrice() > 0L) {
            
            if (!player.reduceAdena(mail.getPrice(), true)) {
              
              player.sendPacket(Msg.YOU_CANNOT_RECEIVE_BECAUSE_YOU_DON_T_HAVE_ENOUGH_ADENA);
              
              return;
            } 
            Player player1 = World.getPlayer(mail.getSenderId());
            if (player1 != null) {
              
              player1.addAdena(mail.getPrice(), true);
              player1.sendPacket((new SystemMessage(3072)).addName(player));
            }
            else {
              
              int i = 1296000 + (int)(System.currentTimeMillis() / 1000L);
              Mail mail1 = mail.reply();
              mail1.setExpireTime(i);
              
              ItemInstance itemInstance = ItemFunctions.createItem(57);
              itemInstance.setOwnerId(mail1.getReceiverId());
              itemInstance.setCount(mail.getPrice());
              itemInstance.setLocation(ItemInstance.ItemLocation.MAIL);
              itemInstance.save();
              
              Log.LogItem(player, Log.ItemLog.PostSend, itemInstance);
              
              mail1.addAttachment(itemInstance);
              mail1.save();
            } 
          } 
          
          set.clear();
        } 
        
        mail.setJdbcState(JdbcEntityState.UPDATED);
        mail.update();
        
        for (ItemInstance itemInstance : arrayOfItemInstance) {
          
          player.sendPacket((new SystemMessage(3073)).addItemName(itemInstance.getItemId()).addNumber(itemInstance.getCount()));
          Log.LogItem(player, Log.ItemLog.PostRecieve, itemInstance);
          player.getInventory().addItem(itemInstance);
        } 
        
        player.sendPacket(Msg.MAIL_SUCCESSFULLY_RECEIVED);
      }
      catch (ArithmeticException arithmeticException) {

      
      }
      finally {
        
        player.getInventory().writeUnlock();
      } 
    } 
    
    player.sendPacket(new ExShowReceivedPostList(player));
  }
}
