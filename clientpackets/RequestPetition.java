package l2.gameserver.network.l2.c2s;

import l2.gameserver.instancemanager.PetitionManager;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;

public final class RequestPetition
  extends L2GameClientPacket
{
  private String Af;
  private int _type;
  
  protected void readImpl() {
    this.Af = readS();
    this._type = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    PetitionManager.getInstance().handle(player, this._type, this.Af);
  }
}
