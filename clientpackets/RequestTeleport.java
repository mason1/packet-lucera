package l2.gameserver.network.l2.c2s;

public class RequestTeleport
  extends L2GameClientPacket
{
  private int unk;
  private int _type;
  
  protected void readImpl() {
    this.unk = readD();
    this._type = readD();
    if (this._type == 2) {
      
      this.YE = readD();
      this.YF = readD();
    }
    else if (this._type == 3) {
      
      this.YE = readD();
      this.YF = readD();
      this.aac = readD();
    } 
  }
  
  private int YE;
  private int YF;
  private int aac;
  
  protected void runImpl() {}
}
