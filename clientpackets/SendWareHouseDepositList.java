package l2.gameserver.network.l2.c2s;

import l2.commons.math.SafeMath;
import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.model.items.ClanWarehouse;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.PcInventory;
import l2.gameserver.model.items.Warehouse;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.utils.Log;
import org.apache.commons.lang3.ArrayUtils;







public class SendWareHouseDepositList
  extends L2GameClientPacket
{
  private static final long aan = 30L;
  private int _count;
  private int[] Yd;
  private long[] Ye;
  
  protected void readImpl() {
    this._count = readD();
    if (this._count * 12 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      
      return;
    } 
    this.Yd = new int[this._count];
    this.Ye = new long[this._count];
    
    for (byte b = 0; b < this._count; b++) {
      
      this.Yd[b] = readD();
      this.Ye[b] = readQ();
      if (this.Ye[b] < 1L || ArrayUtils.indexOf(this.Yd, this.Yd[b]) < b) {
        
        this._count = 0;
        return;
      } 
    } 
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0) {
      return;
    }
    if (!(player.getPlayerAccess()).UseWarehouse) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    
    NpcInstance npcInstance = player.getLastNpc();
    if (npcInstance == null || !npcInstance.isInActingRange(player)) {
      
      player.sendPacket(Msg.WAREHOUSE_IS_TOO_FAR);
      
      return;
    } 
    pcInventory = player.getInventory();
    boolean bool = (player.getUsingWarehouseType() != Warehouse.WarehouseType.CLAN);
    
    if (bool) {
      clanWarehouse = player.getWarehouse();
    } else {
      clanWarehouse = player.getClan().getWarehouse();
    } 
    pcInventory.writeLock();
    clanWarehouse.writeLock();
    
    try {
      int i = 0;
      long l1 = 0L;
      
      if (bool) {
        i = player.getWarehouseLimit() - clanWarehouse.getSize();
      } else {
        i = player.getClan().getWhBonus() + Config.WAREHOUSE_SLOTS_CLAN - clanWarehouse.getSize();
      } 
      boolean bool1 = false;

      
      byte b1 = 0; while (true) { ItemInstance itemInstance; if (b1 < this._count)
        
        { itemInstance = pcInventory.getItemByObjectId(this.Yd[b1]);
          if (itemInstance == null || itemInstance.getCount() < this.Ye[b1] || !itemInstance.canBeStored(player, bool)) {
            
            this.Yd[b1] = 0;
            this.Ye[b1] = 0L;
            
            continue;
          } 
          if (!itemInstance.isStackable() || clanWarehouse.getItemByItemId(itemInstance.getItemId()) == null)
          
          { if (i <= 0)
            
            { this.Yd[b1] = 0;
              this.Ye[b1] = 0L; }
            else
            
            { i--;

              
              if (itemInstance.getItemId() == 57)
                l1 = this.Ye[b1];  }  continue; }  } else { break; }  if (itemInstance.getItemId() == 57) l1 = this.Ye[b1];

        
        b1++; }

      
      if (i <= 0) {
        player.sendPacket(Msg.YOUR_WAREHOUSE_IS_FULL);
      }
      if (!bool1) {
        
        player.sendPacket(SystemMsg.INCORRECT_ITEM_COUNT);
        
        return;
      } 
      
      long l2 = SafeMath.mulAndCheck(bool1, 30L);
      
      if (l2 + l1 > player.getAdena()) {
        
        player.sendPacket(Msg.YOU_LACK_THE_FUNDS_NEEDED_TO_PAY_FOR_THIS_TRANSACTION);
        
        return;
      } 
      if (!player.reduceAdena(l2, true)) {
        
        sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
        
        return;
      } 
      for (byte b2 = 0; b2 < this._count; b2++) {
        
        if (this.Yd[b2] != 0) {
          
          ItemInstance itemInstance = pcInventory.removeItemByObjectId(this.Yd[b2], this.Ye[b2]);
          Log.LogItem(player, bool ? Log.ItemLog.WarehouseDeposit : Log.ItemLog.ClanWarehouseDeposit, itemInstance);
          clanWarehouse.addItem(itemInstance);
        } 
      } 
    } catch (ArithmeticException arithmeticException) {

      
      sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);

      
      return;
    } finally {
      clanWarehouse.writeUnlock();
      pcInventory.writeUnlock();
    } 

    
    player.sendChanges();
    player.sendPacket(Msg.THE_TRANSACTION_IS_COMPLETE);
  }
}
