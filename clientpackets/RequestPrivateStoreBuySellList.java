package l2.gameserver.network.l2.c2s;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import l2.commons.math.SafeMath;
import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.TradeItem;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.s2c.ExPrivateStoreBuyingResult;
import l2.gameserver.utils.Log;
import l2.gameserver.utils.TradeHelper;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




public class RequestPrivateStoreBuySellList
  extends L2GameClientPacket
{
  private static final Logger _log = LoggerFactory.getLogger(RequestPrivateStoreBuySellList.class);
  
  private int ZG;
  
  private int _count;
  private int[] Yd;
  private long[] Ye;
  private long[] ZF;
  
  protected void readImpl() {
    this.ZG = readD();
    this._count = readD();
    
    if (this._count * 28 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      
      return;
    } 
    this.Yd = new int[this._count];
    this.Ye = new long[this._count];
    this.ZF = new long[this._count];
    
    for (byte b = 0; b < this._count; b++) {
      
      this.Yd[b] = readD();
      readD();
      readH();
      readH();
      this.Ye[b] = readQ();
      this.ZF[b] = readQ();
      
      if (this.Ye[b] < 1L || this.ZF[b] < 1L || ArrayUtils.indexOf(this.Yd, this.Yd[b]) < b) {
        
        this._count = 0;
        break;
      } 
    } 
  }


  
  protected void runImpl() {
    player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null || this._count == 0) {
      return;
    }
    if (player1.isActionsDisabled()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isInStoreMode()) {
      
      player1.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player1.isInTrade()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isFishing()) {
      
      player1.sendPacket(Msg.YOU_CANNOT_DO_ANYTHING_ELSE_WHILE_FISHING);
      
      return;
    } 
    if (!(player1.getPlayerAccess()).UseTrade) {
      
      player1.sendPacket(Msg.THIS_ACCOUNT_CANOT_USE_PRIVATE_STORES);
      
      return;
    } 
    player2 = (Player)player1.getVisibleObject(this.ZG);
    if (player2 == null || player2.getPrivateStoreType() != 3 || !player1.isInActingRange(player2)) {
      
      player1.sendPacket(Msg.THE_ATTEMPT_TO_SELL_HAS_FAILED);
      player1.sendActionFailed();
      
      return;
    } 
    list = player2.getBuyList();
    if (list.isEmpty()) {
      
      player1.sendPacket(Msg.THE_ATTEMPT_TO_SELL_HAS_FAILED);
      player1.sendActionFailed();
      
      return;
    } 
    arrayList = new ArrayList();
    
    l1 = 0L;
    b = 0;
    l2 = 0L;
    
    player2.getInventory().writeLock();
    player1.getInventory().writeLock();
    
    try {
      ArrayList arrayList1 = new ArrayList(list); byte b1;
      label191: for (b1 = 0; b1 < this._count; b1++) {
        
        int i = this.Yd[b1];
        long l3 = this.Ye[b1];
        long l4 = this.ZF[b1];
        
        ItemInstance itemInstance = player1.getInventory().getItemByObjectId(i);
        if (itemInstance == null || itemInstance.getCount() < l3 || !itemInstance.canBeTraded(player1)) {
          break;
        }
        TradeItem tradeItem = null;
        
        Iterator iterator = arrayList1.iterator();
        while (iterator.hasNext()) {
          
          TradeItem tradeItem1 = (TradeItem)iterator.next();
          if (tradeItem1.getItemId() == itemInstance.getItemId() && 
            tradeItem1.getOwnersPrice() == l4) {
            
            if ((Config.PRIVATE_BUY_MATCH_ENCHANT && itemInstance.getEnchantLevel() != tradeItem1.getEnchantLevel()) || (!Config.PRIVATE_BUY_MATCH_ENCHANT && itemInstance
              .getEnchantLevel() < tradeItem1.getEnchantLevel()))
              continue; 
            if (l3 > tradeItem1.getCount()) {
              break label191;
            }
            l1 = SafeMath.addAndCheck(l1, SafeMath.mulAndCheck(l3, l4));
            l2 = SafeMath.addAndCheck(l2, SafeMath.mulAndCheck(l3, itemInstance.getTemplate().getWeight()));
            if (!itemInstance.isStackable() || player2.getInventory().getItemByItemId(itemInstance.getItemId()) == null) {
              b++;
            }
            tradeItem = new TradeItem();
            tradeItem.setObjectId(i);
            tradeItem.setItemId(itemInstance.getItemId());
            tradeItem.setEnchantLevel(itemInstance.getEnchantLevel());
            tradeItem.setCount(l3);
            tradeItem.setOwnersPrice(l4);
            
            arrayList.add(tradeItem);
            iterator.remove();
            
            break;
          } 
        } 
      } 
    } catch (ArithmeticException arithmeticException) {

      
      arrayList.clear();
      sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);


      
      return;
    } finally {
      try {
        if (arrayList.size() != this._count) {
          
          player1.sendPacket(Msg.THE_ATTEMPT_TO_SELL_HAS_FAILED);
          player1.sendActionFailed();
          
          return;
        } 
        if (!player2.getInventory().validateWeight(l2)) {
          
          player2.sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
          player1.sendPacket(Msg.THE_ATTEMPT_TO_SELL_HAS_FAILED);
          player1.sendActionFailed();
          
          return;
        } 
        if (!player2.getInventory().validateCapacity(b)) {
          
          player2.sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
          player1.sendPacket(Msg.THE_ATTEMPT_TO_SELL_HAS_FAILED);
          player1.sendActionFailed();
          
          return;
        } 
        if (!player2.reduceAdena(l1)) {
          
          player2.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
          player1.sendPacket(Msg.THE_ATTEMPT_TO_SELL_HAS_FAILED);
          player1.sendActionFailed();
          
          return;
        } 
        
        for (TradeItem tradeItem : arrayList) {
          
          ItemInstance itemInstance1 = player1.getInventory().removeItemByObjectId(tradeItem.getObjectId(), tradeItem.getCount());
          for (TradeItem tradeItem1 : list) {
            
            if (tradeItem1.getItemId() == tradeItem.getItemId() && 
              tradeItem1.getOwnersPrice() == tradeItem.getOwnersPrice()) {
              
              if ((Config.PRIVATE_BUY_MATCH_ENCHANT && itemInstance1.getEnchantLevel() != tradeItem1.getEnchantLevel()) || (!Config.PRIVATE_BUY_MATCH_ENCHANT && itemInstance1
                .getEnchantLevel() < tradeItem1.getEnchantLevel()))
                continue; 
              tradeItem1.setCount(tradeItem1.getCount() - tradeItem.getCount());
              if (tradeItem1.getCount() < 1L)
                list.remove(tradeItem1); 
              break;
            } 
          } 
          Log.LogItem(player1, Log.ItemLog.PrivateStoreSell, itemInstance1);
          Log.LogItem(player2, Log.ItemLog.PrivateStoreBuy, itemInstance1);
          long l3 = itemInstance1.getCount();
          ItemInstance itemInstance2 = player2.getInventory().addItem(itemInstance1);
          player2.sendPacket(new ExPrivateStoreBuyingResult(itemInstance2.getObjectId(), l3, player1.getName()));
          TradeHelper.purchaseItem(player2, player1, tradeItem);
        } 
        
        long l = TradeHelper.getTax(player1, l1);
        if (l > 0L) {
          
          l1 -= l;
          player1.sendMessage((new CustomMessage("trade.HavePaidTax", player1, new Object[0])).addNumber(l));
        } 
        
        player1.addAdena(l1);
        player2.saveTradeList();
      }
      finally {
        
        player1.getInventory().writeUnlock();
        player2.getInventory().writeUnlock();
      } 
    } 
    
    if (list.isEmpty()) {
      TradeHelper.cancelStore(player2);
    }
    player1.sendChanges();
    player2.sendChanges();
    
    player1.sendActionFailed();
  }
}
