package l2.gameserver.network.l2.c2s;

import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.QuestList;




public class RequestQuestList
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() { sendPacket(new QuestList(((GameClient)getClient()).getActiveChar())); }
}
