package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;


public class SetPrivateStoreMsgSell
  extends L2GameClientPacket
{
  private String aap;
  
  protected void readImpl() { this.aap = readS(32); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    player.setSellStoreName(this.aap);
  }
}
