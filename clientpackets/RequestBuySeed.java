package l2.gameserver.network.l2.c2s;

import l2.commons.math.SafeMath;
import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.data.xml.holder.ItemHolder;
import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.model.GameObject;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.model.instances.ManorManagerInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.SystemMessage2;
import l2.gameserver.templates.item.ItemTemplate;
import l2.gameserver.templates.manor.SeedProduction;
import l2.gameserver.utils.Log;












public class RequestBuySeed
  extends L2GameClientPacket
{
  private int _count;
  private int EJ;
  private int[] Yd;
  private long[] Ye;
  
  protected void readImpl() {
    this.EJ = readD();
    this._count = readD();
    
    if (this._count * 12 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      
      return;
    } 
    this.Yd = new int[this._count];
    this.Ye = new long[this._count];
    
    for (byte b = 0; b < this._count; b++) {
      
      this.Yd[b] = readD();
      this.Ye[b] = readQ();
      if (this.Ye[b] < 1L) {
        
        this._count = 0;
        return;
      } 
    } 
  }


  
  protected void runImpl() {
    player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0) {
      return;
    }
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
      
      return;
    } 
    if (!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && player.getKarma() > 0 && !player.isGM()) {
      
      player.sendActionFailed();
      
      return;
    } 
    GameObject gameObject = player.getTarget();
    
    ManorManagerInstance manorManagerInstance = (gameObject != null && gameObject instanceof ManorManagerInstance) ? (ManorManagerInstance)gameObject : null;
    if (!player.isGM() && (manorManagerInstance == null || !manorManagerInstance.isInActingRange(player))) {
      
      player.sendActionFailed();
      
      return;
    } 
    Castle castle = (Castle)ResidenceHolder.getInstance().getResidence(Castle.class, this.EJ);
    if (castle == null) {
      return;
    }
    long l1 = 0L;
    byte b = 0;
    long l2 = 0L;

    
    try {
      for (byte b1 = 0; b1 < this._count; b1++)
      {
        int i = this.Yd[b1];
        long l3 = this.Ye[b1];
        long l4 = 0L;
        long l5 = 0L;
        
        SeedProduction seedProduction = castle.getSeed(i, 0);
        l4 = seedProduction.getPrice();
        l5 = seedProduction.getCanProduce();
        
        if (l4 < 1L) {
          return;
        }
        if (l5 < l3) {
          return;
        }
        l1 = SafeMath.addAndCheck(l1, SafeMath.mulAndCheck(l3, l4));
        
        ItemTemplate itemTemplate = ItemHolder.getInstance().getTemplate(i);
        if (itemTemplate == null) {
          return;
        }
        l2 = SafeMath.addAndCheck(l2, SafeMath.mulAndCheck(l3, itemTemplate.getWeight()));
        if (!itemTemplate.isStackable() || player.getInventory().getItemByItemId(i) == null) {
          b++;
        }
      }
    
    } catch (ArithmeticException arithmeticException) {
      
      sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);
      
      return;
    } 
    player.getInventory().writeLock();
    
    try {
      if (!player.getInventory().validateWeight(l2)) {
        
        sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
        
        return;
      } 
      if (!player.getInventory().validateCapacity(b)) {
        
        sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
        
        return;
      } 
      if (!player.reduceAdena(l1, true)) {
        
        sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
        
        return;
      } 
      
      castle.addToTreasuryNoTax(l1, false, true);

      
      for (byte b1 = 0; b1 < this._count; b1++)
      {
        int i = this.Yd[b1];
        long l = this.Ye[b1];

        
        SeedProduction seedProduction = castle.getSeed(i, 0);
        seedProduction.setCanProduce(seedProduction.getCanProduce() - l);
        castle.updateSeed(seedProduction.getId(), seedProduction.getCanProduce(), 0);

        
        player.getInventory().addItem(i, l);
        player.sendPacket(SystemMessage2.obtainItems(i, l, 0));
        Log.LogItem(player, Log.ItemLog.CastleManorSeedBuy, i, l);
      }
    
    } finally {
      
      player.getInventory().writeUnlock();
    } 
    
    player.sendChanges();
  }
}
