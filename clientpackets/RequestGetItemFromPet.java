package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.instances.PetInstance;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.PcInventory;
import l2.gameserver.model.items.PetInventory;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.utils.Log;


public class RequestGetItemFromPet
  extends L2GameClientPacket
{
  private int Bq;
  private long Xf;
  private int XA;
  
  protected void readImpl() {
    this.Bq = readD();
    this.Xf = readD();
    this.XA = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this.Xf < 1L) {
      return;
    }
    PetInstance petInstance = (PetInstance)player.getPet();
    if (petInstance == null) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isOutOfControl()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(SystemMsg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade() || player.isProcessingRequest()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(SystemMsg.YOU_CANNOT_DO_THAT_WHILE_FISHING_);
      
      return;
    } 
    petInventory = petInstance.getInventory();
    pcInventory = player.getInventory();
    
    petInventory.writeLock();
    pcInventory.writeLock();
    
    try {
      ItemInstance itemInstance = petInventory.getItemByObjectId(this.Bq);
      if (itemInstance == null || itemInstance.getCount() < this.Xf || itemInstance.isEquipped()) {
        
        player.sendActionFailed();
        
        return;
      } 
      boolean bool = false;
      long l = itemInstance.getTemplate().getWeight() * this.Xf;
      if (!itemInstance.getTemplate().isStackable() || player.getInventory().getItemByItemId(itemInstance.getItemId()) == null) {
        bool = true;
      }
      if (!player.getInventory().validateWeight(l)) {
        
        player.sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
        
        return;
      } 
      if (!player.getInventory().validateCapacity(bool)) {
        
        player.sendPacket(SystemMsg.YOUR_INVENTORY_IS_FULL);
        
        return;
      } 
      itemInstance = petInventory.removeItemByObjectId(this.Bq, this.Xf);
      Log.LogItem(player, Log.ItemLog.FromPet, itemInstance);
      pcInventory.addItem(itemInstance);
      
      petInstance.sendChanges();
      player.sendChanges();
    }
    finally {
      
      pcInventory.writeUnlock();
      petInventory.writeUnlock();
    } 
  }
}
