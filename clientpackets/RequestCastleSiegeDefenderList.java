package l2.gameserver.network.l2.c2s;

import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.CastleSiegeDefenderList;


public class RequestCastleSiegeDefenderList
  extends L2GameClientPacket
{
  private int Yg;
  
  protected void readImpl() { this.Yg = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Castle castle = (Castle)ResidenceHolder.getInstance().getResidence(Castle.class, this.Yg);
    if (castle == null || castle.getOwner() == null) {
      return;
    }
    player.sendPacket(new CastleSiegeDefenderList(castle));
  }
}
