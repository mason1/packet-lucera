package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.CrestCache;
import l2.gameserver.network.l2.s2c.AllianceCrest;





public class RequestAllyCrest
  extends L2GameClientPacket
{
  private int Vv;
  
  protected void readImpl() { this.Vv = readD(); }



  
  protected void runImpl() {
    if (this.Vv == 0)
      return; 
    byte[] arrayOfByte = CrestCache.getInstance().getAllyCrest(this.Vv);
    if (arrayOfByte != null) {
      
      AllianceCrest allianceCrest = new AllianceCrest(this.Vv, arrayOfByte);
      sendPacket(allianceCrest);
    } 
  }
}
