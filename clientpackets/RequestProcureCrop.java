package l2.gameserver.network.l2.c2s;

import java.util.Collections;
import java.util.List;
import l2.commons.math.SafeMath;
import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.data.xml.holder.ItemHolder;
import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.model.GameObject;
import l2.gameserver.model.Manor;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.model.instances.ManorManagerInstance;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.SystemMessage2;
import l2.gameserver.templates.item.ItemTemplate;
import l2.gameserver.templates.manor.CropProcure;





public class RequestProcureCrop
  extends L2GameClientPacket
{
  private int EJ;
  private int _count;
  private int[] Yd;
  private long[] Ye;
  private List<CropProcure> ZH = Collections.emptyList();


  
  protected void readImpl() {
    this.EJ = readD();
    this._count = readD();
    if (this._count * 16 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      return;
    } 
    this.Yd = new int[this._count];
    this.Ye = new long[this._count];
    for (byte b = 0; b < this._count; b++) {
      
      readD();
      this.Yd[b] = readD();
      this.Ye[b] = readD();
      if (this.Ye[b] < 1L) {
        
        this._count = 0;
        return;
      } 
    } 
  }


  
  protected void runImpl() {
    player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0) {
      return;
    }
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && player.getKarma() > 0 && !player.isGM()) {
      
      player.sendActionFailed();
      
      return;
    } 
    GameObject gameObject = player.getTarget();
    
    ManorManagerInstance manorManagerInstance = (gameObject != null && gameObject instanceof ManorManagerInstance) ? (ManorManagerInstance)gameObject : null;
    if (!player.isGM() && (manorManagerInstance == null || !player.isInActingRange(manorManagerInstance))) {
      
      player.sendActionFailed();
      
      return;
    } 
    Castle castle = (Castle)ResidenceHolder.getInstance().getResidence(Castle.class, this.EJ);
    if (castle == null) {
      return;
    }
    byte b = 0;
    long l = 0L;

    
    try {
      for (byte b1 = 0; b1 < this._count; b1++) {
        
        int i = this.Yd[b1];
        long l1 = this.Ye[b1];
        
        CropProcure cropProcure = castle.getCrop(i, 0);
        if (cropProcure == null) {
          return;
        }
        int j = Manor.getInstance().getRewardItem(i, castle.getCrop(i, 0).getReward());
        long l2 = Manor.getInstance().getRewardAmountPerCrop(castle.getId(), i, castle.getCropRewardType(i));
        
        l2 = SafeMath.mulAndCheck(l1, l2);
        
        ItemTemplate itemTemplate = ItemHolder.getInstance().getTemplate(j);
        if (itemTemplate == null) {
          return;
        }
        l = SafeMath.addAndCheck(l, SafeMath.mulAndCheck(l1, itemTemplate.getWeight()));
        if (!itemTemplate.isStackable() || player.getInventory().getItemByItemId(i) == null) {
          b++;
        }
      } 
    } catch (ArithmeticException arithmeticException) {

      
      sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);
      
      return;
    } 
    player.getInventory().writeLock();
    
    try {
      if (!player.getInventory().validateWeight(l)) {
        
        sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
        
        return;
      } 
      if (!player.getInventory().validateCapacity(b)) {
        
        sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
        
        return;
      } 
      this.ZH = castle.getCropProcure(0);
      
      for (byte b1 = 0; b1 < this._count; b1++) {
        
        int i = this.Yd[b1];
        long l1 = this.Ye[b1];
        
        int j = Manor.getInstance().getRewardItem(i, castle.getCrop(i, 0).getReward());
        long l2 = Manor.getInstance().getRewardAmountPerCrop(castle.getId(), i, castle.getCropRewardType(i));
        
        l2 = SafeMath.mulAndCheck(l1, l2);
        
        if (player.getInventory().destroyItemByItemId(i, l1)) {

          
          ItemInstance itemInstance = player.getInventory().addItem(j, l2);
          if (itemInstance != null)
          {

            
            player.sendPacket(SystemMessage2.obtainItems(j, l2, 0)); } 
        } 
      } 
    } catch (ArithmeticException arithmeticException) {

      
      this._count = 0;
    }
    finally {
      
      player.getInventory().writeUnlock();
    } 
    
    player.sendChanges();
  }
}
