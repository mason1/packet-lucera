package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.network.l2.GameClient;






public class RequestAnswerJoinAlly
  extends L2GameClientPacket
{
  private int pI;
  
  protected void readImpl() { this.pI = (this._buf.remaining() >= 4) ? readD() : 0; }



  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    request = player1.getRequest();
    if (request == null || !request.isTypeOf(Request.L2RequestType.ALLY)) {
      return;
    }
    if (!request.isInProgress()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isOutOfControl()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    Player player2 = request.getRequestor();
    if (player2 == null) {
      
      request.cancel();
      player1.sendPacket(Msg.THAT_PLAYER_IS_NOT_ONLINE);
      player1.sendActionFailed();
      
      return;
    } 
    if (player2.getRequest() != request) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    if (player2.getAlliance() == null) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    if (this.pI == 0) {
      
      request.cancel();
      player2.sendPacket(Msg.YOU_HAVE_FAILED_TO_INVITE_A_CLAN_INTO_THE_ALLIANCE);
      
      return;
    } 
    
    try {
      Alliance alliance = player2.getAlliance();
      player1.sendPacket(Msg.YOU_HAVE_ACCEPTED_THE_ALLIANCE);
      player1.getClan().setAllyId(player2.getAllyId());
      player1.getClan().updateClanInDB();
      alliance.addAllyMember(player1.getClan(), true);
      alliance.broadcastAllyStatus();
    }
    finally {
      
      request.done();
    } 
  }
}
