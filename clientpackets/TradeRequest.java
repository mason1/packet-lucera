package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.GameObject;
import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.SendTradeRequest;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.utils.Util;





public class TradeRequest
  extends L2GameClientPacket
{
  private int Bq;
  
  protected void readImpl() { this.Bq = readD(); }



  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    if (player1.isOutOfControl()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (!(player1.getPlayerAccess()).UseTrade) {
      
      player1.sendPacket(Msg.THIS_ACCOUNT_CANOT_TRADE_ITEMS);
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isInStoreMode()) {
      
      player1.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player1.isFishing()) {
      
      player1.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
      
      return;
    } 
    if (player1.isInTrade()) {
      
      player1.sendPacket(Msg.YOU_ARE_ALREADY_TRADING_WITH_SOMEONE);
      
      return;
    } 
    if (player1.isProcessingRequest()) {
      
      player1.sendPacket(Msg.WAITING_FOR_ANOTHER_REPLY);
      
      return;
    } 
    String str = player1.getVar("tradeBan");
    if (str != null && (str.equals("-1") || Long.parseLong(str) >= System.currentTimeMillis())) {
      
      if (str.equals("-1")) {
        player1.sendMessage(new CustomMessage("common.TradeBannedPermanently", player1, new Object[0]));
      } else {
        player1.sendMessage((new CustomMessage("common.TradeBanned", player1, new Object[0])).addString(Util.formatTime((int)(Long.parseLong(str) / 1000L - System.currentTimeMillis() / 1000L))));
      } 
      return;
    } 
    GameObject gameObject = player1.getVisibleObject(this.Bq);
    if (gameObject == null || !gameObject.isPlayer() || gameObject == player1) {
      
      player1.sendPacket(SystemMsg.THAT_IS_AN_INCORRECT_TARGET);
      
      return;
    } 
    if (!gameObject.isInActingRange(player1)) {
      
      player1.sendPacket(Msg.YOUR_TARGET_IS_OUT_OF_RANGE);
      
      return;
    } 
    Player player2 = (Player)gameObject;
    if (!(player2.getPlayerAccess()).UseTrade) {
      
      player1.sendPacket(SystemMsg.THAT_IS_AN_INCORRECT_TARGET);
      
      return;
    } 
    str = player2.getVar("tradeBan");
    if (str != null && (str.equals("-1") || Long.parseLong(str) >= System.currentTimeMillis())) {
      
      player1.sendPacket(SystemMsg.THAT_IS_AN_INCORRECT_TARGET);
      
      return;
    } 
    if (player2.isInBlockList(player1)) {
      
      player1.sendPacket(Msg.YOU_HAVE_BEEN_BLOCKED_FROM_THE_CONTACT_YOU_SELECTED);
      
      return;
    } 
    if (player2.getTradeRefusal() || player2.isBusy()) {
      
      player1.sendPacket((new SystemMessage(153)).addString(player2.getName()));
      
      return;
    } 
    (new Request(Request.L2RequestType.TRADE_REQUEST, player1, player2)).setTimeout(10000L);
    player2.sendPacket(new SendTradeRequest(player1.getObjectId()));
    player1.sendPacket((new SystemMessage(118)).addString(player2.getName()));
  }
}
