package l2.gameserver.network.l2.c2s;

import l2.commons.math.SafeMath;
import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExBuySellList;
import l2.gameserver.utils.Log;
import org.apache.commons.lang3.ArrayUtils;





public class RequestSellItem
  extends L2GameClientPacket
{
  private int vU;
  private int _count;
  private int[] Yd;
  private long[] Ye;
  
  protected void readImpl() {
    this.vU = readD();
    this._count = readD();
    if (this._count * 16 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      return;
    } 
    this.Yd = new int[this._count];
    this.Ye = new long[this._count];
    
    for (byte b = 0; b < this._count; b++) {
      
      this.Yd[b] = readD();
      readD();
      this.Ye[b] = readQ();
      if (this.Ye[b] < 1L || ArrayUtils.indexOf(this.Yd, this.Yd[b]) < b) {
        
        this._count = 0;
        break;
      } 
    } 
  }


  
  protected void runImpl() {
    player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0) {
      return;
    }
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
      
      return;
    } 
    if (!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && player.getKarma() > 0 && !player.isGM()) {
      
      player.sendActionFailed();
      
      return;
    } 
    NpcInstance npcInstance = player.getLastNpc();
    boolean bool = (npcInstance != null && npcInstance.isMerchantNpc()) ? 1 : 0;
    
    if (!Config.ALT_ALLOW_REMOTE_SELL_ITEMS_TO_SHOP)
    {
      if (!player.isGM() && (npcInstance == null || !bool || !player.isInActingRange(npcInstance))) {
        
        player.sendActionFailed();
        
        return;
      } 
    }
    player.getInventory().writeLock();
    
    try {
      for (byte b = 0; b < this._count; b++) {
        
        int i = this.Yd[b];
        long l = this.Ye[b];
        
        ItemInstance itemInstance = player.getInventory().getItemByObjectId(i);
        if (itemInstance != null && itemInstance.getCount() >= l && itemInstance.canBeSold(player)) {

          
          long l1 = SafeMath.mulAndCheck(Math.max(1L, itemInstance.getReferencePrice() / Config.ALT_SHOP_REFUND_SELL_DIVISOR), l);
          
          ItemInstance itemInstance1 = player.getInventory().removeItemByObjectId(i, l);
          
          Log.LogItem(player, Log.ItemLog.RefundSell, itemInstance1);
          
          player.addAdena(l1);
          player.getRefund().addItem(itemInstance1);
        } 
      } 
    } catch (ArithmeticException arithmeticException) {
      
      sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);

      
      return;
    } finally {
      player.getInventory().writeUnlock();
    } 
    
    player.sendPacket(new ExBuySellList.SellRefundList(player, true));
    player.sendChanges();
  }
}
