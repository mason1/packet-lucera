package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.utils.Location;


public class RequestDropItem
  extends L2GameClientPacket
{
  private int Bq;
  private long Be;
  private Location _loc;
  
  protected void readImpl() {
    this.Bq = readD();
    this.Be = readQ();
    this._loc = new Location(readD(), readD(), readD());
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (this.Be < 1L || this._loc.isNull()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (!Config.ALLOW_DISCARDITEM || (player.getPlayerAccess()).BlockInventory) {
      
      player.sendMessage(new CustomMessage("l2p.gameserver.clientpackets.RequestDropItem.Disallowed", player, new Object[0]));
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isSitting() || player.isDropDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
      
      return;
    } 
    if (player.isActionBlocked("drop_item")) {
      
      player.sendPacket(Msg.YOU_CANNOT_DISCARD_THOSE_ITEMS_HERE);
      
      return;
    } 
    if (!player.isInRangeSq(this._loc, 22500L) || Math.abs(this._loc.z - player.getZ()) > 50) {
      
      player.sendPacket(Msg.THAT_IS_TOO_FAR_FROM_YOU_TO_DISCARD);
      
      return;
    } 
    ItemInstance itemInstance = player.getInventory().getItemByObjectId(this.Bq);
    if (itemInstance == null) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (!itemInstance.canBeDropped(player, false)) {
      
      player.sendPacket(Msg.THAT_ITEM_CANNOT_BE_DISCARDED);
      
      return;
    } 
    itemInstance.getTemplate().getHandler().dropItem(player, itemInstance, this.Be, this._loc);
  }
}
