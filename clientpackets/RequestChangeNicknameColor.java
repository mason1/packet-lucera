package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.utils.Util;

public class RequestChangeNicknameColor extends L2GameClientPacket {
  public static final int NAME_COLOR_ID = 13021;
  public static final int TITLE_COLOR_ID = 13307;
  private static final int[] Yj = { 9671679, 8145404, 9959676, 16423662, 16735635, 64672, 10528257, 7903407, 4743829, 10066329 };



  
  private int Yk;


  
  private int Yl;


  
  private String _title;



  
  protected void readImpl() {
    this.Yk = readD();
    this._title = readS(16);
    this.Yl = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (this.Yk < 0 || this.Yk >= Yj.length) {
      return;
    }
    if (!this._title.isEmpty() && !Util.isMatchingRegexp(this._title, Config.CLAN_TITLE_TEMPLATE)) {
      
      player.sendMessage("Incorrect title.");
      
      return;
    } 
    ItemInstance itemInstance = player.getInventory().getItemByObjectId(this.Yl);
    if (itemInstance == null) {
      return;
    }
    int i = itemInstance.getItemId();
    if (player.consumeItem(i, 1L)) {
      
      switch (i) {
        
        case 13307:
          player.setTitleColor(Yj[this.Yk]);
          break;
        case 13021:
          player.setNameColor(Yj[this.Yk]);
          break;
      } 
      player.setTitle(this._title);
      player.broadcastUserInfo(true);
    } 
  }
}
