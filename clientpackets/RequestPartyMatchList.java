package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.matching.MatchingRoom;
import l2.gameserver.model.matching.PartyMatchingRoom;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;



public class RequestPartyMatchList
  extends L2GameClientPacket
{
  private int Zx;
  private int Zy;
  private int p;
  private int q;
  private int Yy;
  private String Zz;
  
  protected void readImpl() {
    this.Yy = readD();
    this.Zy = readD();
    this.p = readD();
    this.q = readD();
    this.Zx = readD();
    this.Zz = readS(64);
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    MatchingRoom matchingRoom = player.getMatchingRoom();
    if (matchingRoom == null) {
      new PartyMatchingRoom(player, this.p, this.q, this.Zy, this.Zx, this.Zz);
    } else if (matchingRoom.getId() == this.Yy && matchingRoom.getType() == MatchingRoom.PARTY_MATCHING && matchingRoom.getLeader() == player) {
      
      matchingRoom.setMinLevel(this.p);
      matchingRoom.setMaxLevel(this.q);
      matchingRoom.setMaxMemberSize(this.Zy);
      matchingRoom.setTopic(this.Zz);
      matchingRoom.setLootType(this.Zx);
      matchingRoom.broadCast(new IStaticPacket[] { matchingRoom.infoRoomPacket() });
    } 
  }
}
