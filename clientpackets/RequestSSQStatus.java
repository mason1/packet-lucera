package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.entity.SevenSigns;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.SSQStatus;







public class RequestSSQStatus
  extends L2GameClientPacket
{
  private int _page;
  
  protected void readImpl() { this._page = readC(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if ((SevenSigns.getInstance().isSealValidationPeriod() || SevenSigns.getInstance().isCompResultsPeriod()) && this._page == 4) {
      return;
    }
    player.sendPacket(new SSQStatus(player, this._page));
  }
}
