package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;




public final class RequestBidItemAuction
  extends L2GameClientPacket
{
  private int XV;
  private long NU;
  
  protected final void readImpl() {
    this.XV = readD();
    this.NU = readQ();
  }


  
  protected final void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
  }
}
