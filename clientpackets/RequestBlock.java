package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.BlockList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestBlock
  extends L2GameClientPacket {
  private static final Logger _log = LoggerFactory.getLogger(RequestBlock.class);
  
  private static final int BLOCK = 0;
  
  private static final int XW = 1;
  private static final int XX = 2;
  private static final int XY = 3;
  private static final int XZ = 4;
  private Integer Ya;
  private String Yb = null;


  
  protected void readImpl() {
    this.Ya = Integer.valueOf(readD());
    
    if (this.Ya.intValue() == 0 || this.Ya.intValue() == 1) {
      this.Yb = readS(16);
    }
  }

  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    switch (this.Ya.intValue()) {
      
      case 0:
        player.addToBlockList(this.Yb);
        return;
      case 1:
        player.removeFromBlockList(this.Yb);
        return;
      case 2:
        player.sendPacket(new BlockList(player));
        return;
      case 3:
        player.setBlockAll(true);
        player.sendPacket(Msg.YOU_ARE_NOW_BLOCKING_EVERYTHING);
        player.sendEtcStatusUpdate();
        return;
      case 4:
        player.setBlockAll(false);
        player.sendPacket(Msg.YOU_ARE_NO_LONGER_BLOCKING_EVERYTHING);
        player.sendEtcStatusUpdate();
        return;
    } 
    _log.info("Unknown 0x0a block type: " + this.Ya);
  }
}
