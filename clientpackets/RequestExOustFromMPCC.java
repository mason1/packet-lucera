package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.World;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.s2c.SystemMessage;




public class RequestExOustFromMPCC
  extends L2GameClientPacket
{
  private String _name;
  
  protected void readImpl() { this._name = readS(16); }



  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null || !player1.isInParty() || !player1.getParty().isInCommandChannel()) {
      return;
    }
    Player player2 = World.getPlayer(this._name);

    
    if (player2 == null) {
      
      player1.sendPacket(Msg.THAT_PLAYER_IS_NOT_CURRENTLY_ONLINE);
      
      return;
    } 
    
    if (player1 == player2) {
      return;
    }
    
    if (!player2.isInParty() || !player2.getParty().isInCommandChannel() || player1.getParty().getCommandChannel() != player2.getParty().getCommandChannel()) {
      
      player1.sendPacket(Msg.INVALID_TARGET);
      
      return;
    } 
    
    if (player1.getParty().getCommandChannel().getChannelLeader() != player1) {
      
      player1.sendPacket(Msg.ONLY_THE_CREATOR_OF_A_CHANNEL_CAN_ISSUE_A_GLOBAL_COMMAND);
      
      return;
    } 
    player2.getParty().getCommandChannel().getChannelLeader().sendPacket((new SystemMessage(1584)).addString(player2.getName()));
    player2.getParty().getCommandChannel().removeParty(player2.getParty());
    player2.getParty().broadCast(new IStaticPacket[] { Msg.YOU_WERE_DISMISSED_FROM_THE_COMMAND_CHANNEL });
  }
}
