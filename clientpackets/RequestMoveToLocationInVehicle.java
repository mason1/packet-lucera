package l2.gameserver.network.l2.c2s;

import l2.gameserver.data.BoatHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.boat.Boat;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.utils.Location;

public class RequestMoveToLocationInVehicle extends L2GameClientPacket {
  private Location _pos = new Location();
  private Location YV = new Location();
  
  private int YW;

  
  protected void readImpl() {
    this.YW = readD();
    this._pos.x = readD();
    this._pos.y = readD();
    this._pos.z = readD();
    this.YV.x = readD();
    this.YV.y = readD();
    this.YV.z = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Boat boat = BoatHolder.getInstance().getBoat(this.YW);
    if (boat == null) {
      
      player.sendActionFailed();
      
      return;
    } 
    boat.moveInBoat(player, this.YV, this._pos);
  }
}
