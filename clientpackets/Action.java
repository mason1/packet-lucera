package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.GameObject;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.ActionFail;

public class Action
  extends L2GameClientPacket {
  private int Bq;
  private int Xd;
  
  protected void readImpl() {
    this.Bq = readD();
    readD();
    readD();
    readD();
    this.Xd = readC();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }

    
    onAction(player, this.Bq, (this.Xd == 1));
  }

  
  public static void onAction(Player paramPlayer, int paramInt, boolean paramBoolean) {
    if (paramPlayer.isOutOfControl()) {
      
      paramPlayer.sendActionFailed();
      
      return;
    } 
    if (paramPlayer.isInStoreMode()) {
      
      paramPlayer.sendActionFailed();
      
      return;
    } 
    GameObject gameObject = paramPlayer.getVisibleObject(paramInt);
    if (gameObject == null) {
      
      paramPlayer.sendActionFailed();
      
      return;
    } 
    paramPlayer.setActive();
    
    if (paramPlayer.getAggressionTarget() != null && paramPlayer.getAggressionTarget() != gameObject) {
      
      paramPlayer.sendActionFailed();
      
      return;
    } 
    if (paramPlayer.isLockedTarget()) {
      
      paramPlayer.sendActionFailed();
      
      return;
    } 
    if (paramPlayer.isFrozen()) {
      
      paramPlayer.sendPacket(new IStaticPacket[] { SystemMsg.YOU_CANNOT_MOVE_WHILE_FROZEN, ActionFail.STATIC });
      
      return;
    } 
    gameObject.onAction(paramPlayer, paramBoolean);
  }
}
