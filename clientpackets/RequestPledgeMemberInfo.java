package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.model.pledge.UnitMember;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.PledgeReceiveMemberInfo;



public class RequestPledgeMemberInfo
  extends L2GameClientPacket
{
  private int GA;
  private String Zi;
  
  protected void readImpl() {
    this.GA = readD();
    this.Zi = readS(16);
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    Clan clan = player.getClan();
    if (clan != null) {
      
      UnitMember unitMember = clan.getAnyMember(this.Zi);
      if (unitMember != null)
        player.sendPacket(new PledgeReceiveMemberInfo(unitMember)); 
    } 
  }
}
