package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.model.Effect;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.model.Summon;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.SystemMessage;

public class RequestDispel extends L2GameClientPacket {
  private int Bq;
  private int _id;
  private int _level;
  
  protected void readImpl() {
    this.Bq = readD();
    this._id = readD();
    this._level = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || (player.getObjectId() != this.Bq && player.getPet() == null)) {
      return;
    }

    
    Summon summon = player;
    if (player.getObjectId() != this.Bq)
    {
      summon = player.getPet();
    }
    
    for (Effect effect : summon.getEffectList().getAllEffects()) {
      
      if (effect.getDisplayId() == this._id && (effect.getDisplayLevel() == this._level || (effect.getDisplayLevel() >= 100 && effect.getSkill() != null && effect.getSkill().getBaseLevel() == this._level))) {
        
        if (!effect.isOffensive() && (!effect.getSkill().isMusic() || Config.SONGDANCE_CAN_BE_DISPELL) && effect.getSkill().isSelfDispellable() && effect.getSkill().getSkillType() != Skill.SkillType.TRANSFORMATION) {
          
          effect.exit();
          
          continue;
        } 
        
        return;
      } 
    } 
    player.sendPacket((new SystemMessage(92)).addSkillName(this._id, this._level));
  }
}
