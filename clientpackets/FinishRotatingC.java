package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.FinishRotating;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;




public class FinishRotatingC
  extends L2GameClientPacket
{
  private int Xz;
  private int XA;
  
  protected void readImpl() {
    this.Xz = readD();
    this.XA = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    player.broadcastPacket(new L2GameServerPacket[] { new FinishRotating(player, this.Xz, 0) });
  }
}
