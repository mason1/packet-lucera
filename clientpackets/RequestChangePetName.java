package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.instances.PetInstance;
import l2.gameserver.network.l2.GameClient;


public class RequestChangePetName
  extends L2GameClientPacket
{
  private String _name;
  
  protected void readImpl() { this._name = readS(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    PetInstance petInstance = (player.getPet() != null && player.getPet().isPet()) ? (PetInstance)player.getPet() : null;
    if (petInstance == null) {
      return;
    }
    if (petInstance.isDefaultName()) {
      
      if (this._name.length() < 1 || this._name.length() > 8) {
        
        sendPacket(Msg.YOUR_PETS_NAME_CAN_BE_UP_TO_8_CHARACTERS);
        return;
      } 
      petInstance.setName(this._name);
      petInstance.broadcastCharInfo();
      petInstance.updateControlItem();
    } 
  }
}
