package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.CrestCache;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExPledgeCrestLarge;


public class RequestPledgeCrestLarge
  extends L2GameClientPacket
{
  private int Vv;
  
  protected void readImpl() {
    this.Vv = readD();
    readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    if (this.Vv == 0)
      return; 
    byte[] arrayOfByte = CrestCache.getInstance().getPledgeCrestLarge(this.Vv);
    if (arrayOfByte != null) {
      
      ExPledgeCrestLarge exPledgeCrestLarge = new ExPledgeCrestLarge(this.Vv, arrayOfByte);
      sendPacket(exPledgeCrestLarge);
    } 
  }
}
