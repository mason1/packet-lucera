package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExListPartyMatchingWaitingRoom;

public class RequestListPartyMatchingWaitingRoom
  extends L2GameClientPacket
{
  private int p;
  private int q;
  private int _page;
  private int[] Zm;
  
  protected void readImpl() {
    this._page = readD();
    this.p = readD();
    this.q = readD();
    int i = readD();
    if (i > 127 || i < 0)
      i = 0; 
    this.Zm = new int[i];
    for (byte b = 0; b < i; b++) {
      this.Zm[b] = readD();
    }
  }

  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    player.sendPacket(new ExListPartyMatchingWaitingRoom(player, this.p, this.q, this._page, this.Zm));
  }
}
