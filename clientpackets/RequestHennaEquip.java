package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.data.xml.holder.HennaHolder;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.templates.Henna;






public class RequestHennaEquip
  extends L2GameClientPacket
{
  private int _symbolId;
  
  protected void readImpl() { this._symbolId = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Henna henna = HennaHolder.getInstance().getHenna(this._symbolId);
    if (henna == null || !henna.isForThisClass(player)) {
      
      player.sendPacket(Msg.THE_SYMBOL_CANNOT_BE_DRAWN);
      
      return;
    } 
    long l1 = player.getAdena();
    long l2 = player.getInventory().getCountOf(henna.getDyeId());
    
    if (l2 >= henna.getDrawCount() && l1 >= henna.getPrice()) {
      
      if (player.consumeItem(henna.getDyeId(), henna.getDrawCount()) && player.reduceAdena(henna.getPrice())) {
        
        player.sendPacket(SystemMsg.THE_SYMBOL_HAS_BEEN_ADDED);
        player.addHenna(henna);
      } 
    } else {
      
      player.sendPacket(SystemMsg.THE_SYMBOL_CANNOT_BE_DRAWN);
    } 
  }
}
