package l2.gameserver.network.l2.c2s;

import java.util.Map;
import l2.gameserver.data.xml.holder.EnchantSkillHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.model.base.Experience;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.templates.SkillEnchant;






public class RequestExEnchantSkillInfo
  extends L2GameClientPacket
{
  private int _skillId;
  private int YK;
  
  protected void readImpl() {
    this._skillId = readD();
    this.YK = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }

    
    if (player.getClassId().getLevel() < 4 || player.getLevel() < 76) {
      
      player.sendPacket(new SystemMessage(1438));
      
      return;
    } 
    Skill skill = player.getKnownSkill(this._skillId);
    if (skill == null) {
      
      player.sendPacket(new SystemMessage(1438));
      return;
    } 
    int i = skill.getLevel();
    Map map = EnchantSkillHolder.getInstance().getLevelsOf(this._skillId);
    
    if (map == null || map.isEmpty()) {
      
      player.sendPacket(new SystemMessage(1438));
      
      return;
    } 
    SkillEnchant skillEnchant1 = (SkillEnchant)map.get(Integer.valueOf(i));
    SkillEnchant skillEnchant2 = (SkillEnchant)map.get(Integer.valueOf(this.YK));
    
    if (skillEnchant2 == null) {
      
      player.sendPacket(new SystemMessage(1438));
      
      return;
    } 
    if (skillEnchant1 != null) {
      
      if (skillEnchant1.getRouteId() != skillEnchant2.getRouteId() || skillEnchant2.getEnchantLevel() != skillEnchant1.getEnchantLevel() + 1) {
        
        player.sendPacket(new SystemMessage(1438));
        
        return;
      } 
    } else if (skillEnchant2.getEnchantLevel() != 1) {
      
      player.sendPacket(new SystemMessage(1438));
      
      return;
    } 
    int[] arrayOfInt = skillEnchant2.getChances();
    int j = Experience.LEVEL.length - arrayOfInt.length - 1;
    
    if (player.getLevel() < j) {
      
      sendPacket((new SystemMessage(607))
          .addNumber(j));
      
      return;
    } 
    int k = Math.max(0, Math.min(player.getLevel() - j, arrayOfInt.length - 1));
    int m = arrayOfInt[k];
  }
}
