package l2.gameserver.network.l2.c2s;

import l2.gameserver.instancemanager.QuestManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.quest.Quest;
import l2.gameserver.network.l2.GameClient;



public class RequestTutorialLinkHtml
  extends L2GameClientPacket
{
  String _bypass;
  int _type;
  
  protected void readImpl() {
    this._type = readD();
    this._bypass = readS();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Quest quest = QuestManager.getQuest(255);
    if (quest != null)
      player.processQuestEvent(quest.getName(), this._bypass, null); 
  }
}
