package l2.gameserver.network.l2.c2s;

import l2.gameserver.network.l2.s2c.ExRemoveEnchantSupportItemResult;











public class RequestExRemoveEnchantSupportItem
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() { sendPacket(ExRemoveEnchantSupportItemResult.STATIC); }
}
