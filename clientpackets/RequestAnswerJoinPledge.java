package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.data.xml.holder.EventHolder;
import l2.gameserver.data.xml.holder.OneDayRewardHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.model.base.TeamType;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.model.pledge.SubUnit;
import l2.gameserver.model.pledge.UnitMember;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.JoinPledge;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.network.l2.s2c.PledgeShowInfoUpdate;
import l2.gameserver.network.l2.s2c.PledgeShowMemberListAdd;
import l2.gameserver.network.l2.s2c.PledgeSkillList;
import l2.gameserver.network.l2.s2c.SkillList;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.network.l2.s2c.SystemMessage2;



public class RequestAnswerJoinPledge
  extends L2GameClientPacket
{
  private int pI;
  
  protected void readImpl() { this.pI = this._buf.hasRemaining() ? readD() : 0; }


  
  protected void runImpl() {
    Integer integer;
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    request = player1.getRequest();
    if (request == null || !request.isTypeOf(Request.L2RequestType.CLAN)) {
      return;
    }
    if (!request.isInProgress()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isOutOfControl()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    Player player2 = request.getRequestor();
    if (player2 == null) {
      
      request.cancel();
      player1.sendPacket(SystemMsg.THAT_PLAYER_IS_NOT_ONLINE);
      player1.sendActionFailed();
      
      return;
    } 
    if (player2.getRequest() != request) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    Clan clan = player2.getClan();
    if (clan == null) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    if (this.pI == 0 || player1.isOlyParticipant() || player1.getTeam() != TeamType.NONE) {
      
      request.cancel();
      player2.sendPacket((new SystemMessage2(SystemMsg.S1_DECLINED_YOUR_CLAN_INVITATION)).addName(player1));
      
      return;
    } 
    if (!player1.canJoinClan()) {
      
      request.cancel();
      player1.sendPacket(SystemMsg.AFTER_LEAVING_OR_HAVING_BEEN_DISMISSED_FROM_A_CLAN_YOU_MUST_WAIT_AT_LEAST_A_DAY_BEFORE_JOINING_ANOTHER_CLAN);

      
      return;
    } 
    
    try {
      integer = Integer.valueOf(request.getInteger("pledgeType"));
    }
    catch (Exception exception) {
      
      request.done();
      
      return;
    } 
    if (integer == null || clan.getUnitMembersSize(integer.intValue()) >= clan.getSubPledgeLimit(integer.intValue())) {
      
      request.cancel();
      if (integer.intValue() == 0) {
        player2.sendPacket((new SystemMessage(1835)).addString(clan.getName()));
      } else {
        player2.sendPacket(Msg.THE_ACADEMY_ROYAL_GUARD_ORDER_OF_KNIGHTS_IS_FULL_AND_CANNOT_ACCEPT_NEW_MEMBERS_AT_THIS_TIME);
      } 
      
      return;
    } 
    try {
      player1.sendPacket(new JoinPledge(player2.getClanId()));

      
      SubUnit subUnit = clan.getSubUnit(integer.intValue());
      if (subUnit == null) {
        return;
      }
      UnitMember unitMember = new UnitMember(clan, player1.getName(), player1.getTitle(), player1.getLevel(), player1.getClassId().getId(), player1.getObjectId(), integer.intValue(), player1.getPowerGrade(), player1.getApprentice(), player1.getSex(), -128);
      subUnit.addUnitMember(unitMember);
      
      player1.setPledgeType(integer.intValue());
      player1.setClan(clan);
      
      unitMember.setPlayerInstance(player1, false);
      
      if (integer.intValue() == -1) {
        player1.setLvlJoinedAcademy(player1.getLevel());
      }
      unitMember.setPowerGrade(clan.getAffiliationRank(player1.getPledgeType()));
      
      clan.broadcastToOtherOnlineMembers(new PledgeShowMemberListAdd(unitMember), player1);
      clan.broadcastToOnlineMembers(new L2GameServerPacket[] { (new SystemMessage2(SystemMsg.S1_HAS_JOINED_THE_CLAN)).addString(player1.getName()), new PledgeShowInfoUpdate(clan) });

      
      player1.sendPacket(SystemMsg.ENTERED_THE_CLAN);
      player1.sendPacket(player1.getClan().listAll());
      player1.setLeaveClanTime(0L);
      player1.updatePledgeClass();

      
      clan.addSkillsQuietly(player1);
      
      player1.sendPacket(new PledgeSkillList(clan));
      player1.sendPacket(new SkillList(player1));
      
      OneDayRewardHolder.getInstance().fireRequirements(player1, null, l2.gameserver.model.entity.oneDayReward.requirement.JoinClanRequirement.class);
      
      EventHolder.getInstance().findEvent(player1);
      player1.broadcastCharInfo();
      
      player1.store(false);
    }
    finally {
      
      request.done();
    } 
  }
}
