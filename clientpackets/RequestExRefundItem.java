package l2.gameserver.network.l2.c2s;

import java.util.ArrayList;
import l2.commons.math.SafeMath;
import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.ExBuySellList;
import l2.gameserver.utils.Log;
import org.apache.commons.lang3.ArrayUtils;




public class RequestExRefundItem
  extends L2GameClientPacket
{
  private int vU;
  private int _count;
  private int[] Yd;
  
  protected void readImpl() {
    this.vU = readD();
    this._count = readD();
    if (this._count * 4 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      return;
    } 
    this.Yd = new int[this._count];
    for (byte b = 0; b < this._count; b++) {
      
      this.Yd[b] = readD();
      if (ArrayUtils.indexOf(this.Yd, this.Yd[b]) < b) {
        
        this._count = 0;
        break;
      } 
    } 
  }


  
  protected void runImpl() {
    player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0) {
      return;
    }
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
      
      return;
    } 
    if (!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && player.getKarma() > 0 && !player.isGM()) {
      
      player.sendActionFailed();
      
      return;
    } 
    NpcInstance npcInstance = player.getLastNpc();
    
    boolean bool = (npcInstance != null && npcInstance.isMerchantNpc()) ? 1 : 0;
    if (!player.isGM() && (npcInstance == null || !bool || !player.isInRange(npcInstance, 400L))) {
      
      player.sendActionFailed();
      
      return;
    } 
    player.getInventory().writeLock();
    
    try {
      byte b = 0;
      long l1 = 0L;
      long l2 = 0L;
      
      ArrayList arrayList = new ArrayList();
      for (int i : this.Yd) {
        
        ItemInstance itemInstance = player.getRefund().getItemByObjectId(i);
        if (itemInstance != null) {

          
          l2 = SafeMath.addAndCheck(l2, SafeMath.mulAndCheck(itemInstance.getCount(), itemInstance.getReferencePrice()) / 2L);
          l1 = SafeMath.addAndCheck(l1, SafeMath.mulAndCheck(itemInstance.getCount(), itemInstance.getTemplate().getWeight()));
          
          if (!itemInstance.isStackable() || player.getInventory().getItemByItemId(itemInstance.getItemId()) == null) {
            b++;
          }
          arrayList.add(itemInstance);
        } 
      } 
      if (arrayList.isEmpty()) {
        
        player.sendPacket(SystemMsg.INCORRECT_ITEM_COUNT);
        player.sendActionFailed();
        
        return;
      } 
      if (!player.getInventory().validateWeight(l1)) {
        
        sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
        player.sendActionFailed();
        
        return;
      } 
      if (!player.getInventory().validateCapacity(b)) {
        
        sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
        player.sendActionFailed();
        
        return;
      } 
      if (!player.reduceAdena(l2)) {
        
        player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
        player.sendActionFailed();
        
        return;
      } 
      for (ItemInstance itemInstance1 : arrayList)
      {
        ItemInstance itemInstance2 = player.getRefund().removeItem(itemInstance1);
        Log.LogItem(player, Log.ItemLog.RefundReturn, itemInstance2);
        player.getInventory().addItem(itemInstance2);
      }
    
    } catch (ArithmeticException arithmeticException) {
      
      sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);

      
      return;
    } finally {
      player.getInventory().writeUnlock();
    } 
    
    player.sendPacket(new ExBuySellList.SellRefundList(player, true));
    player.sendChanges();
  }
}
