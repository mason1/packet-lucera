package l2.gameserver.network.l2.c2s;

import java.util.concurrent.CopyOnWriteArrayList;
import l2.commons.math.SafeMath;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.TradeItem;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.network.l2.s2c.PrivateStoreManageListSell;
import l2.gameserver.network.l2.s2c.PrivateStoreMsgSell;
import l2.gameserver.utils.TradeHelper;
import org.apache.commons.lang3.ArrayUtils;






public class SetPrivateStoreSellList
  extends L2GameClientPacket
{
  private int _count;
  private boolean aaq;
  private int[] Yd;
  private long[] Ye;
  private long[] ZF;
  
  protected void readImpl() {
    this.aaq = (readD() == 1);
    this._count = readD();
    
    if (this._count * 20 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      
      return;
    } 
    this.Yd = new int[this._count];
    this.Ye = new long[this._count];
    this.ZF = new long[this._count];
    
    for (byte b = 0; b < this._count; b++) {
      
      this.Yd[b] = readD();
      this.Ye[b] = readQ();
      this.ZF[b] = readQ();
      if (this.Ye[b] < 1L || this.ZF[b] < 0L || ArrayUtils.indexOf(this.Yd, this.Yd[b]) < b) {
        
        this._count = 0;
        break;
      } 
    } 
  }


  
  protected void runImpl() {
    player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0) {
      return;
    }
    if (!TradeHelper.checksIfCanOpenStore(player, this.aaq ? 8 : 1)) {
      
      player.sendActionFailed();
      
      return;
    } 
    
    CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
    
    long l = 0L;
    player.getInventory().writeLock();
    
    try {
      for (byte b = 0; b < this._count; b++) {
        
        int i = this.Yd[b];
        long l1 = this.Ye[b];
        long l2 = this.ZF[b];
        
        ItemInstance itemInstance = player.getInventory().getItemByObjectId(i);
        
        if (itemInstance != null && itemInstance.getCount() >= l1 && itemInstance.canBeTraded(player) && itemInstance.getItemId() != 57) {

          
          TradeItem tradeItem = new TradeItem(itemInstance);
          tradeItem.setCount(l1);
          tradeItem.setOwnersPrice(l2);
          
          l = SafeMath.addAndCheck(l, SafeMath.mulAndCheck(l1, l2));
          
          copyOnWriteArrayList.add(tradeItem);
        } 
      } 
    } catch (ArithmeticException arithmeticException) {

      
      copyOnWriteArrayList.clear();
      sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);

      
      return;
    } finally {
      player.getInventory().writeUnlock();
    } 
    
    if (copyOnWriteArrayList.size() > player.getTradeLimit() || SafeMath.addAndCheck(l, player.getAdena()) >= 2147483647L) {
      
      player.sendPacket(new IStaticPacket[] { new PrivateStoreManageListSell(true, player, this.aaq), new PrivateStoreManageListSell(false, player, this.aaq) });
      
      return;
    } 
    if (!copyOnWriteArrayList.isEmpty()) {
      
      player.setSellList(this.aaq, copyOnWriteArrayList);
      player.saveTradeList();
      player.setPrivateStoreType(this.aaq ? 8 : 1);
      player.broadcastPacket(new L2GameServerPacket[] { new PrivateStoreMsgSell(player) });
    } 
    
    player.sendActionFailed();
  }
}
