package l2.gameserver.network.l2.c2s;

public class RequestChangeBookMarkSlot
  extends L2GameClientPacket
{
  private int Yh;
  private int Yi;
  
  protected void readImpl() {
    this.Yh = readD();
    this.Yi = readD();
  }
  
  protected void runImpl() {}
}
