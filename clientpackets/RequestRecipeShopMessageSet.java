package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;



public class RequestRecipeShopMessageSet
  extends L2GameClientPacket
{
  private String _name;
  
  protected void readImpl() { this._name = readS(16); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    player.setManufactureName(this._name);
  }
}
