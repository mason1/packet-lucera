package l2.gameserver.network.l2.c2s;

import l2.gameserver.instancemanager.QuestManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.quest.Quest;
import l2.gameserver.network.l2.GameClient;

public class RequestTutorialClientEvent
  extends L2GameClientPacket {
  int event = 0;






  
  protected void readImpl() { this.event = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Quest quest = QuestManager.getQuest(255);
    if (quest != null)
      player.processQuestEvent(quest.getName(), "CE" + this.event, null); 
  }
}
