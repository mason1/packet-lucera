package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.CommandChannel;
import l2.gameserver.model.Party;
import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.model.World;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.ExAskJoinMPCC;
import l2.gameserver.network.l2.s2c.SystemMessage;






public class RequestExMPCCAskJoin
  extends L2GameClientPacket
{
  private String _name;
  
  protected void readImpl() { this._name = readS(16); }



  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    if (player1.isOutOfControl()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isProcessingRequest()) {
      
      player1.sendPacket(Msg.WAITING_FOR_ANOTHER_REPLY);
      
      return;
    } 
    if (!player1.isInParty()) {
      
      player1.sendPacket(Msg.YOU_DO_NOT_HAVE_AUTHORITY_TO_INVITE_SOMEONE_TO_THE_COMMAND_CHANNEL);
      
      return;
    } 
    Player player2 = World.getPlayer(this._name);

    
    if (player2 == null) {
      
      player1.sendPacket(Msg.THAT_PLAYER_IS_NOT_CURRENTLY_ONLINE);
      
      return;
    } 
    
    if (player1 == player2 || !player2.isInParty() || player1.getParty() == player2.getParty()) {
      
      player1.sendPacket(Msg.YOU_HAVE_INVITED_WRONG_TARGET);
      
      return;
    } 
    
    if (player2.isInParty() && !player2.getParty().isLeader(player2)) {
      player2 = player2.getParty().getPartyLeader();
    }
    if (player2 == null) {
      
      player1.sendPacket(Msg.THAT_PLAYER_IS_NOT_CURRENTLY_ONLINE);
      
      return;
    } 
    if (player2.getParty().isInCommandChannel()) {
      
      player1.sendPacket((new SystemMessage(1594)).addString(player2.getName()));
      
      return;
    } 
    if (player2.isBusy()) {
      
      player1.sendPacket((new SystemMessage(153)).addString(player2.getName()));
      
      return;
    } 
    Party party = player1.getParty();
    
    if (party.isInCommandChannel()) {


      
      CommandChannel commandChannel = party.getCommandChannel();
      if (commandChannel.getChannelLeader() != player1) {
        
        player1.sendPacket(Msg.YOU_DO_NOT_HAVE_AUTHORITY_TO_INVITE_SOMEONE_TO_THE_COMMAND_CHANNEL);
        
        return;
      } 
      if (commandChannel.getParties().size() >= Config.COMMAND_CHANNEL_PARY_COUNT_LIMIT) {
        
        player1.sendPacket(SystemMsg.THE_COMMAND_CHANNEL_IS_FULL);
        
        return;
      } 
      d(player1, player2);
    
    }
    else if (CommandChannel.checkAuthority(player1)) {
      d(player1, player2);
    } 
  }
  
  private void d(Player paramPlayer1, Player paramPlayer2) {
    (new Request(Request.L2RequestType.CHANNEL, paramPlayer1, paramPlayer2)).setTimeout(10000L);
    paramPlayer2.sendPacket(new ExAskJoinMPCC(paramPlayer1.getName()));
    paramPlayer1.sendMessage((new CustomMessage("l2p.gameserver.clientpackets.RequestExMPCCAskJoin.InviteToCommandChannel", paramPlayer1, new Object[0])).addString(paramPlayer2.getName()));
  }
}
