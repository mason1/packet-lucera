package l2.gameserver.network.l2.c2s;

import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.SecondPasswordAuth;
import l2.gameserver.network.l2.s2c.Ex2ndPasswordAck;


public class RequestEx2ndPasswordReq
  extends L2GameClientPacket
{
  private boolean YB;
  private String YC;
  private String YD;
  
  protected void readImpl() {
    this.YB = (readC() == 2);
    this.YC = readS(8);
    if (this.YB) {
      this.YD = readS(8);
    }
  }

  
  protected void runImpl() {
    GameClient gameClient = (GameClient)getClient();
    SecondPasswordAuth secondPasswordAuth = gameClient.getSecondPasswordAuth();
    if (secondPasswordAuth == null) {
      
      gameClient.sendPacket(new Ex2ndPasswordAck(Ex2ndPasswordAck.Ex2ndPasswordAckResult.ERROR));
      return;
    } 
    if (this.YB) {
      
      if (secondPasswordAuth.changePassword(this.YC, this.YD)) {
        
        gameClient.sendPacket(new Ex2ndPasswordAck(Ex2ndPasswordAck.Ex2ndPasswordAckResult.SUCCESS_CREATE));
        
        return;
      } 
      
      if (secondPasswordAuth.isBlocked()) {
        gameClient.sendPacket(new Ex2ndPasswordAck(Ex2ndPasswordAck.Ex2ndPasswordAckResult.BLOCK_HOMEPAGE));
      } else {
        gameClient.sendPacket(new Ex2ndPasswordAck(Ex2ndPasswordAck.Ex2ndPasswordAckResult.FAIL_VERIFY, secondPasswordAuth.getTrysCount()));
      }
    
    } else if (!secondPasswordAuth.isSecondPasswordSet()) {
      
      secondPasswordAuth.changePassword(null, this.YC);
      gameClient.sendPacket(new Ex2ndPasswordAck(Ex2ndPasswordAck.Ex2ndPasswordAckResult.SUCCESS_CREATE));
    }
    else {
      
      if (secondPasswordAuth.isValidSecondPassword(this.YC)) {
        
        gameClient.sendPacket(new Ex2ndPasswordAck(Ex2ndPasswordAck.Ex2ndPasswordAckResult.SUCCESS_VERIFY));
        gameClient.setSecondPasswordAuthed(true);
        
        return;
      } 
      
      if (secondPasswordAuth.isBlocked()) {
        gameClient.sendPacket(new Ex2ndPasswordAck(Ex2ndPasswordAck.Ex2ndPasswordAckResult.BLOCK_HOMEPAGE));
      } else {
        gameClient.sendPacket(new Ex2ndPasswordAck(Ex2ndPasswordAck.Ex2ndPasswordAckResult.FAIL_VERIFY, secondPasswordAuth.getTrysCount()));
      } 
    } 
  }
}
