package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.pledge.UnitMember;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.network.l2.s2c.NickNameChanged;
import l2.gameserver.utils.Util;

public class RequestGiveNickName
  extends L2GameClientPacket
{
  private String Zi;
  private String _title;
  
  protected void readImpl() {
    this.Zi = readS(Config.CNAME_MAXLEN);
    this._title = readS();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (!this._title.isEmpty() && !Util.isMatchingRegexp(this._title, Config.CLAN_TITLE_TEMPLATE)) {
      
      player.sendMessage(new CustomMessage("l2p.gameserver.clientpackets.RequestGiveNickName.IncorrectTittle", player, new Object[0]));
      
      return;
    } 
    
    if (player.isNoble() && this.Zi.equals(player.getName())) {
      
      player.setTitle(this._title);
      player.sendPacket(Msg.TITLE_HAS_CHANGED);
      player.broadcastPacket(new L2GameServerPacket[] { new NickNameChanged(player) });
      
      return;
    } 
    if ((player.getClanPrivileges() & 0x4) != 4) {
      return;
    }
    if (player.getClan().getLevel() < 3) {
      
      player.sendPacket(Msg.TITLE_ENDOWMENT_IS_ONLY_POSSIBLE_WHEN_CLANS_SKILL_LEVELS_ARE_ABOVE_3);
      
      return;
    } 
    UnitMember unitMember = player.getClan().getAnyMember(this.Zi);
    if (unitMember != null) {
      
      unitMember.setTitle(this._title);
      if (unitMember.isOnline()) {
        
        unitMember.getPlayer().sendPacket(Msg.TITLE_HAS_CHANGED);
        unitMember.getPlayer().sendChanges();
      } 
    } else {
      
      player.sendMessage(new CustomMessage("l2p.gameserver.clientpackets.RequestGiveNickName.NotInClan", player, new Object[0]));
    } 
  }
}
