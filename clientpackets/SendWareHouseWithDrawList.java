package l2.gameserver.network.l2.c2s;

import l2.commons.math.SafeMath;
import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.model.items.ClanWarehouse;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.PcFreight;
import l2.gameserver.model.items.PcInventory;
import l2.gameserver.model.items.Warehouse;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.utils.Log;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendWareHouseWithDrawList
  extends L2GameClientPacket {
  private static final Logger _log = LoggerFactory.getLogger(SendWareHouseWithDrawList.class);
  
  private int _count;
  
  private int[] Yd;
  
  private long[] Ye;
  
  protected void readImpl() {
    this._count = readD();
    if (this._count * 12 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      return;
    } 
    this.Yd = new int[this._count];
    this.Ye = new long[this._count];
    for (byte b = 0; b < this._count; b++) {
      
      this.Yd[b] = readD();
      this.Ye[b] = readQ();
      if (this.Ye[b] < 1L || ArrayUtils.indexOf(this.Yd, this.Yd[b]) < b) {
        
        this._count = 0;
        break;
      } 
    } 
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0) {
      return;
    }
    if (!(player.getPlayerAccess()).UseWarehouse) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    NpcInstance npcInstance = player.getLastNpc();
    if ((npcInstance == null || !npcInstance.isInActingRange(player)) && !Config.ALT_ALLOW_REMOTE_USE_CARGO_BOX) {
      
      player.sendPacket(Msg.WAREHOUSE_IS_TOO_FAR);
      
      return;
    } 
    pcFreight = null;
    Log.ItemLog itemLog = null;
    
    if (player.getUsingWarehouseType() == Warehouse.WarehouseType.PRIVATE) {
      
      pcFreight = player.getWarehouse();
      itemLog = Log.ItemLog.WarehouseWithdraw;
    }
    else if (player.getUsingWarehouseType() == Warehouse.WarehouseType.CLAN) {
      
      itemLog = Log.ItemLog.ClanWarehouseWithdraw;
      if (player.getClan() == null || player.getClan().getLevel() == 0) {
        
        player.sendPacket(Msg.ONLY_CLANS_OF_CLAN_LEVEL_1_OR_HIGHER_CAN_USE_A_CLAN_WAREHOUSE);
        return;
      } 
      boolean bool = false;
      if (player.getClan() != null && (
        player.getClanPrivileges() & 0x8) == 8 && (Config.ALT_ALLOW_OTHERS_WITHDRAW_FROM_CLAN_WAREHOUSE || player.isClanLeader() || player.getVarB("canWhWithdraw")))
        bool = true; 
      if (!bool) {
        
        player.sendPacket(Msg.YOU_DO_NOT_HAVE_THE_RIGHT_TO_USE_THE_CLAN_WAREHOUSE);
        
        return;
      } 
      ClanWarehouse clanWarehouse = player.getClan().getWarehouse();
    }
    else if (player.getUsingWarehouseType() == Warehouse.WarehouseType.FREIGHT) {
      
      pcFreight = player.getFreight();
      itemLog = Log.ItemLog.FreightWithdraw;
    }
    else {
      
      _log.warn("Error retrieving a warehouse object for char " + player.getName() + " - using warehouse type: " + player.getUsingWarehouseType());
      
      return;
    } 
    pcInventory = player.getInventory();
    
    pcInventory.writeLock();
    pcFreight.writeLock();
    
    try {
      long l = 0L;
      byte b1 = 0;
      byte b2;
      for (b2 = 0; b2 < this._count; b2++) {
        
        ItemInstance itemInstance = pcFreight.getItemByObjectId(this.Yd[b2]);
        if (itemInstance == null || itemInstance.getCount() < this.Ye[b2]) {
          
          player.sendPacket(SystemMsg.INCORRECT_ITEM_COUNT);
          
          return;
        } 
        l = SafeMath.addAndCheck(l, SafeMath.mulAndCheck(itemInstance.getTemplate().getWeight(), this.Ye[b2]));
        if (!itemInstance.isStackable() || pcInventory.getItemByItemId(itemInstance.getItemId()) == null) {
          b1++;
        }
      } 
      if (!player.getInventory().validateCapacity(b1)) {
        
        player.sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
        
        return;
      } 
      if (!player.getInventory().validateWeight(l)) {
        
        player.sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
        
        return;
      } 
      for (b2 = 0; b2 < this._count; b2++)
      {
        ItemInstance itemInstance = pcFreight.removeItemByObjectId(this.Yd[b2], this.Ye[b2]);
        Log.LogItem(player, itemLog, itemInstance);
        player.getInventory().addItem(itemInstance);
      }
    
    } catch (ArithmeticException arithmeticException) {

      
      sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);

      
      return;
    } finally {
      pcFreight.writeUnlock();
      pcInventory.writeUnlock();
    } 
    
    player.sendChanges();
    player.sendPacket(Msg.THE_TRANSACTION_IS_COMPLETE);
  }
}
