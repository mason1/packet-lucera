package l2.gameserver.network.l2.c2s;

import l2.gameserver.listener.actor.player.OnAnswerListener;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import org.apache.commons.lang3.tuple.Pair;

public class ConfirmDlg
  extends L2GameClientPacket {
  private int _answer;
  private int Xw;
  
  protected void readImpl() {
    readD();
    this._answer = readD();
    this.Xw = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Pair pair = player.getAskListener(true);
    if (pair == null || ((Integer)pair.getKey()).intValue() != this.Xw) {
      return;
    }
    OnAnswerListener onAnswerListener = (OnAnswerListener)pair.getValue();
    if (this._answer == 1) {
      onAnswerListener.sayYes();
    } else {
      onAnswerListener.sayNo();
    } 
  }
}
