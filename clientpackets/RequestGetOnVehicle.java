package l2.gameserver.network.l2.c2s;

import l2.gameserver.data.BoatHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.boat.Boat;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.utils.Location;

public class RequestGetOnVehicle extends L2GameClientPacket {
  private int Bq;
  private Location _loc = new Location();






  
  protected void readImpl() {
    this.Bq = readD();
    this._loc.x = readD();
    this._loc.y = readD();
    this._loc.z = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Boat boat = BoatHolder.getInstance().getBoat(this.Bq);
    if (boat == null) {
      return;
    }
    player._stablePoint = boat.getCurrentWay().getReturnLoc();
    boat.addPlayer(player, this._loc);
  }
}
