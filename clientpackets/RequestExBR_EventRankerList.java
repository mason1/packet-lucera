package l2.gameserver.network.l2.c2s;



public class RequestExBR_EventRankerList
  extends L2GameClientPacket
{
  private int unk;
  private int YE;
  private int YF;
  
  protected void readImpl() {
    this.unk = readD();
    this.YE = readD();
    this.YF = readD();
  }
  
  protected void runImpl() {}
}
