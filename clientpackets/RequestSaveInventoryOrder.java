package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;


public class RequestSaveInventoryOrder
  extends L2GameClientPacket
{
  int[][] _items;
  
  protected void readImpl() {
    int i = readD();
    if (i > 125)
      i = 125; 
    if (i * 8 > this._buf.remaining() || i < 1) {
      
      this._items = (int[][])null;
      return;
    } 
    this._items = new int[i][2];
    for (byte b = 0; b < i; b++) {
      
      this._items[b][0] = readD();
      this._items[b][1] = readD();
    } 
  }


  
  protected void runImpl() {
    if (this._items == null)
      return; 
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    player.getInventory().sort(this._items);
  }
}
