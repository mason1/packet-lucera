package l2.gameserver.network.l2.c2s;

import java.util.List;
import l2.gameserver.dao.MailDAO;
import l2.gameserver.model.Player;
import l2.gameserver.model.mail.Mail;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExShowReceivedPostList;
import org.apache.commons.lang3.ArrayUtils;










public class RequestExDeleteReceivedPost
  extends L2GameClientPacket
{
  private int _count;
  private int[] YJ;
  
  protected void readImpl() {
    this._count = readD();
    if (this._count * 4 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      return;
    } 
    this.YJ = new int[this._count];
    for (byte b = 0; b < this._count; b++) {
      this.YJ[b] = readD();
    }
  }

  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0) {
      return;
    }
    List list = MailDAO.getInstance().getReceivedMailByOwnerId(player.getObjectId());
    if (!list.isEmpty())
    {
      for (Mail mail : list) {
        if (ArrayUtils.contains(this.YJ, mail.getMessageId()) && 
          mail.getAttachments().isEmpty())
        {
          MailDAO.getInstance().deleteReceivedMailByMailId(player.getObjectId(), mail.getMessageId());
        }
      } 
    }
    player.sendPacket(new ExShowReceivedPostList(player));
  }
}
