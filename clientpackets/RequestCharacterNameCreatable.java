package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.dao.CharacterDAO;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExIsCharNameCreatable;
import l2.gameserver.utils.Util;


public class RequestCharacterNameCreatable
  extends L2GameClientPacket
{
  private String Ym;
  
  protected void readImpl() { this.Ym = readS(); }



  
  protected void runImpl() {
    if (CharacterDAO.getInstance().accountCharNumber(((GameClient)getClient()).getLogin()) >= 8) {
      
      sendPacket(ExIsCharNameCreatable.TOO_MANY_CHARACTERS);
      return;
    } 
    if (!Util.isMatchingRegexp(this.Ym, Config.CNAME_TEMPLATE)) {
      
      sendPacket(ExIsCharNameCreatable.ENTER_CHAR_NAME__MAX_16_CHARS);
      return;
    } 
    if (Util.isMatchingRegexp(this.Ym.toLowerCase(), Config.CNAME_FORBIDDEN_PATTERN) || CharacterDAO.getInstance().getObjectIdByName(this.Ym) > 0) {
      
      sendPacket(ExIsCharNameCreatable.NAME_ALREADY_EXISTS);
      
      return;
    } 
    
    for (String str : Config.CNAME_FORBIDDEN_NAMES) {
      
      if (str.equalsIgnoreCase(this.Ym)) {
        
        sendPacket(ExIsCharNameCreatable.NAME_ALREADY_EXISTS);
        
        return;
      } 
    } 
    sendPacket(ExIsCharNameCreatable.SUCCESS);
  }
}
