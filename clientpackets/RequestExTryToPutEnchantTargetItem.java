package l2.gameserver.network.l2.c2s;

import l2.gameserver.data.xml.holder.EnchantItemHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.PcInventory;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.ExPutEnchantTargetItemResult;
import l2.gameserver.templates.item.support.EnchantScroll;
import l2.gameserver.utils.Log;

public class RequestExTryToPutEnchantTargetItem
  extends L2GameClientPacket
{
  private int Bq;
  
  protected void readImpl() { this.Bq = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.isActionsDisabled() || player.isInStoreMode() || player.isInTrade()) {
      
      player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
      player.setEnchantScroll(null);
      
      return;
    } 
    PcInventory pcInventory = player.getInventory();
    ItemInstance itemInstance1 = pcInventory.getItemByObjectId(this.Bq);
    ItemInstance itemInstance2 = player.getEnchantScroll();
    
    if (itemInstance1 == null || itemInstance2 == null) {
      
      player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
      player.setEnchantScroll(null);
      
      return;
    } 
    Log.add(player.getName() + "|Trying to put enchant|" + itemInstance1.getItemId() + "|+" + itemInstance1.getEnchantLevel() + "|" + itemInstance1.getObjectId(), "enchants");
    
    int i = itemInstance2.getItemId();
    
    if (!itemInstance1.canBeEnchanted(false) || itemInstance1.isStackable()) {
      
      player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
      player.sendPacket(SystemMsg.DOES_NOT_FIT_STRENGTHENING_CONDITIONS_OF_THE_SCROLL);
      player.setEnchantScroll(null);
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
      player.sendPacket(SystemMsg.YOU_CANNOT_ENCHANT_WHILE_OPERATING_A_PRIVATE_STORE_OR_PRIVATE_WORKSHOP);
      player.setEnchantScroll(null);
      
      return;
    } 
    if (itemInstance1.getLocation() != ItemInstance.ItemLocation.INVENTORY && itemInstance1.getLocation() != ItemInstance.ItemLocation.PAPERDOLL) {
      
      player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
      player.sendPacket(SystemMsg.INAPPROPRIATE_ENCHANT_CONDITIONS);
      player.setEnchantScroll(null);
      
      return;
    } 
    if ((itemInstance2 = pcInventory.getItemByObjectId(itemInstance2.getObjectId())) == null) {
      
      player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
      player.setEnchantScroll(null);
      
      return;
    } 
    
    if (itemInstance1.getOwnerId() != player.getObjectId()) {
      
      player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
      player.setEnchantScroll(null);
      
      return;
    } 
    EnchantScroll enchantScroll = EnchantItemHolder.getInstance().getEnchantScroll(i);
    
    if (enchantScroll == null) {
      
      player.sendPacket(new IStaticPacket[] { SystemMsg.INAPPROPRIATE_ENCHANT_CONDITIONS, ExPutEnchantTargetItemResult.FAIL });
      player.setEnchantScroll(null);
      
      return;
    } 
    if (!enchantScroll.isUsableWith(itemInstance1, null)) {
      
      player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
      player.sendPacket(SystemMsg.DOES_NOT_FIT_STRENGTHENING_CONDITIONS_OF_THE_SCROLL);
      player.setEnchantScroll(null);
      
      return;
    } 
    player.sendPacket(ExPutEnchantTargetItemResult.SUCCESS);
  }
}
