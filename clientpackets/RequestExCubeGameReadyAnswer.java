package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;





public final class RequestExCubeGameReadyAnswer
  extends L2GameClientPacket
{
  private static final Logger _log = LoggerFactory.getLogger(RequestExCubeGameReadyAnswer.class);
  
  int _arena;
  
  int _answer;

  
  protected void readImpl() {
    this._arena = readD() + 1;
    this._answer = readD();
  }


  
  public void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
  }
}
