package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.PackageSendableList;






public class RequestPackageSendableItemList
  extends L2GameClientPacket
{
  private int Bq;
  
  protected void readImpl() { this.Bq = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    player.sendPacket(new PackageSendableList(this.Bq, player));
  }
}
