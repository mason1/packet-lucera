package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;



public class Appearing
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.isLogoutStarted()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.getObserverMode() == 1) {
      
      player.appearObserverMode();
      
      return;
    } 
    if (player.getObserverMode() == 2) {
      
      player.returnFromObserverMode();
      
      return;
    } 
    if (!player.isTeleporting()) {
      
      player.sendActionFailed();
      
      return;
    } 
    player.onTeleported();
  }
}
