package l2.gameserver.network.l2.c2s;

import java.util.HashSet;
import l2.gameserver.model.Player;
import l2.gameserver.model.matching.MatchingRoom;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExMpccPartymasterList;










public class RequestExMpccPartymasterList
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    MatchingRoom matchingRoom = player.getMatchingRoom();
    if (matchingRoom == null || matchingRoom.getType() != MatchingRoom.CC_MATCHING) {
      return;
    }
    HashSet hashSet = new HashSet();
    for (Player player1 : matchingRoom.getPlayers()) {
      if (player1.getParty() != null)
        hashSet.add(player1.getParty().getPartyLeader().getName()); 
    } 
    player.sendPacket(new ExMpccPartymasterList(hashSet));
  }
}
