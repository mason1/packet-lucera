package l2.gameserver.network.l2.c2s;

import l2.gameserver.instancemanager.QuestManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.quest.Quest;
import l2.gameserver.model.quest.QuestState;
import l2.gameserver.network.l2.GameClient;


public class RequestQuestAbort
  extends L2GameClientPacket
{
  private int ZK;
  
  protected void readImpl() { this.ZK = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    Quest quest = QuestManager.getQuest(this.ZK);
    if (player == null || quest == null) {
      return;
    }
    if (!quest.canAbortByPacket()) {
      return;
    }
    QuestState questState = player.getQuestState(quest.getClass());
    if (questState != null && !questState.isCompleted())
      questState.abortQuest(); 
  }
}
