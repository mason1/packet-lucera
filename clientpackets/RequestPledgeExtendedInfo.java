package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;







public class RequestPledgeExtendedInfo
  extends L2GameClientPacket
{
  private String _name;
  
  protected void readImpl() { this._name = readS(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    if (player.isGM())
      player.sendMessage("RequestPledgeExtendedInfo"); 
  }
}
