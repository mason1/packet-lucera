package l2.gameserver.network.l2.c2s;

import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.CharacterSelectionInfo;



public class GotoLobby
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    CharacterSelectionInfo characterSelectionInfo = new CharacterSelectionInfo(((GameClient)getClient()).getLogin(), (((GameClient)getClient()).getSessionKey()).playOkID1);
    sendPacket(characterSelectionInfo);
  }
}
