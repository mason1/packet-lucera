package l2.gameserver.network.l2.c2s;

import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExShowAgitInfo;




public class RequestAllAgitInfo
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() { ((GameClient)getClient()).getActiveChar().sendPacket(new ExShowAgitInfo()); }
}
