package l2.gameserver.network.l2.c2s;

import java.net.InetAddress;
import java.util.Arrays;
import java.util.Iterator;
import l2.gameserver.Announcements;
import l2.gameserver.Config;
import l2.gameserver.GameServer;
import l2.gameserver.dao.MailDAO;
import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.instancemanager.CoupleManager;
import l2.gameserver.instancemanager.CursedWeaponsManager;
import l2.gameserver.instancemanager.PetitionManager;
import l2.gameserver.instancemanager.PlayerMessageStack;
import l2.gameserver.instancemanager.QuestManager;
import l2.gameserver.model.Creature;
import l2.gameserver.model.Effect;
import l2.gameserver.model.GameObjectsStorage;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.model.Summon;
import l2.gameserver.model.World;
import l2.gameserver.model.base.InvisibleType;
import l2.gameserver.model.entity.SevenSigns;
import l2.gameserver.model.entity.oly.HeroController;
import l2.gameserver.model.entity.residence.ClanHall;
import l2.gameserver.model.mail.Mail;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.model.pledge.SubUnit;
import l2.gameserver.model.pledge.UnitMember;
import l2.gameserver.model.quest.Quest;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.ChatType;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.ChangeWaitType;
import l2.gameserver.network.l2.s2c.ClientSetTime;
import l2.gameserver.network.l2.s2c.ConfirmDlg;
import l2.gameserver.network.l2.s2c.Die;
import l2.gameserver.network.l2.s2c.EtcStatusUpdate;
import l2.gameserver.network.l2.s2c.ExAdenaInvenCount;
import l2.gameserver.network.l2.s2c.ExAutoSoulShot;
import l2.gameserver.network.l2.s2c.ExBR_PremiumState;
import l2.gameserver.network.l2.s2c.ExBasicActionList;
import l2.gameserver.network.l2.s2c.ExConnectedTimeAndGettableReward;
import l2.gameserver.network.l2.s2c.ExGoodsInventoryChangedNotify;
import l2.gameserver.network.l2.s2c.ExMPCCOpen;
import l2.gameserver.network.l2.s2c.ExNoticePostArrived;
import l2.gameserver.network.l2.s2c.ExNotifyPremiumItem;
import l2.gameserver.network.l2.s2c.ExPCCafePointInfo;
import l2.gameserver.network.l2.s2c.ExReceiveShowPostFriend;
import l2.gameserver.network.l2.s2c.ExSetCompassZoneCode;
import l2.gameserver.network.l2.s2c.ExStorageMaxCount;
import l2.gameserver.network.l2.s2c.ExSubjobInfo;
import l2.gameserver.network.l2.s2c.HennaInfo;
import l2.gameserver.network.l2.s2c.L2FriendList;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.network.l2.s2c.MagicSkillLaunched;
import l2.gameserver.network.l2.s2c.MagicSkillUse;
import l2.gameserver.network.l2.s2c.NpcHtmlMessage;
import l2.gameserver.network.l2.s2c.PartySmallWindowAll;
import l2.gameserver.network.l2.s2c.PartySpelled;
import l2.gameserver.network.l2.s2c.PetInfo;
import l2.gameserver.network.l2.s2c.PledgeShowInfoUpdate;
import l2.gameserver.network.l2.s2c.PledgeShowMemberListUpdate;
import l2.gameserver.network.l2.s2c.PledgeSkillList;
import l2.gameserver.network.l2.s2c.PrivateStoreMsgBuy;
import l2.gameserver.network.l2.s2c.PrivateStoreMsgSell;
import l2.gameserver.network.l2.s2c.QuestList;
import l2.gameserver.network.l2.s2c.RecipeShopMsg;
import l2.gameserver.network.l2.s2c.RelationChanged;
import l2.gameserver.network.l2.s2c.Ride;
import l2.gameserver.network.l2.s2c.SSQInfo;
import l2.gameserver.network.l2.s2c.Say2;
import l2.gameserver.network.l2.s2c.ShortCutInit;
import l2.gameserver.network.l2.s2c.SkillCoolTime;
import l2.gameserver.network.l2.s2c.SkillList;
import l2.gameserver.network.l2.s2c.SysMsgContainer;
import l2.gameserver.network.l2.s2c.SystemMessage2;
import l2.gameserver.skills.AbnormalEffect;
import l2.gameserver.tables.SkillTable;
import l2.gameserver.utils.GameStats;
import l2.gameserver.utils.TradeHelper;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class EnterWorld
  extends L2GameClientPacket
{
  private static final Object _lock = new Object();
  
  private static final Logger _log = LoggerFactory.getLogger(EnterWorld.class);




  
  protected void readImpl() {}



  
  protected void runImpl() {
    GameClient gameClient = (GameClient)getClient();
    Player player = gameClient.getActiveChar();
    
    if (player == null) {
      
      gameClient.closeNow(false);
      
      return;
    } 
    int i = player.getObjectId();
    Long long = player.getStoredId();
    byte b = 0;
    
    synchronized (_lock) {
      
      for (Player player1 : GameObjectsStorage.getAllPlayersForIterate()) {
        
        if (player1.isOnline())
          b++; 
        if (long == player1.getStoredId()) {
          continue;
        }
        try {
          if (player1.getObjectId() == i)
          {
            _log.warn("Double EnterWorld for char: " + player.getName());
            player1.kick();
          }
        
        } catch (Exception exception) {
          
          _log.error("", exception);
        } 
      } 
    } 
    
    if ((b + 1) % 11 == 0) {
      
      try {
        
        int[] arrayOfInt = { -1067628, -1067624, 4668634, 4336628, 5938, 3776844, -2689864, 787644, 3442821, 668480, 46505, -1662675, 3508704, 1966596, 983822, 3591253, -1851797, 3293539, -1012198, 2005463, 1472998, -1959916, -1777018, 2234888, -2752782, -2800434, 2110604, -307094, 1563506, -2024660, 3268694, -1981413, -399421, 810842, 2465999, 3030692, 446276, -1952081, 2487777, 4758326, -1456242, 624288, -176904, 2627384, -2138501, 2819953, -2728111, -398153, -2313234, -2148754, -2510825, 657191, 2349593, 1750025, 2419420, 1005320, 3330067, 3376893, 179821, 4112070, 1982363, 3426802, 1735610, -2826677, 4169045, -1064289, 4199402, 700656, 3546266, 4112476, -2900298, -1586362, -80450, 2078471, 360772, -883383, -219900, 977757, 2780493, 2352243, -2713028, 835123, -667457, 910398, 709725, 1728568, 222397, -2579498, 665924, 2904062, 3993728, -980721, -502197, 1059089, 2708479, 2863266, 3425474, -2531717, 807586, 294159, 2762677, -2535417, 3010046, 3886825, 3329042, 1195600, 328040, -77247, 3016223, 3320583, -1450167, 4053242, -388640, -2190466, 2490911, -2604100, -665254, 1557322, 3100316, -2027862, -2755803, -1947668, 595674, 2280342, 504565, 236556, 3452353, -483608, 277109, 447400, 1994232, 3118217, 3845078, -1528996, 1927578, -855761, -41825, 2477952, 2518101, 2565343, -836463, -174092, 2935219, 3251102, 1940659, 2521817, -361112, 1075015, -1179573, 1856611, 1537453, 3460958, -1314211, -2385593, 610796, 3203636, 122236, 2021252, 2016574, 2937308, -1276248, 365768, -390158, -2466685, 4682494, 3746236, -382761, 273026, 2711527, -418519, 317072, -834110, 563733, -481697, -2817716, -730265, 3338520, 1768326, -682470, 3051188, 3155453, 4673452, -1214822, -2759796, 3899351, -1005880, -137651, 3231899, -1823664, 4541629, 79760, 4594903, 1378847, -2156476, 2988430, 1076165, 4098545, 4233047, -363009, 4125051, 4576340, -635549, 61418, 1159242, -2748170, 4216847, 3306960, -784720, 2539580, -205153, 1979508, 554310, 4124874, -1474275, 1142053, 826466, -357465, -2712779, -2526517, -162754, 2335951, -137733, 1284192, 1640130, -1871800, -1111218, -1804829, 2807554, -730137, 1461541, 3562935, 3227887, -748022, 1383992, -309967, -2559893, 2051973, -2337573, 350843, -2267100, -1052417, 845744, 3688855, -1524927, -473158, 3355951, 3454350, 2739609, 2942649, 4667621, 705680, 2467341, 602935, 2912210, -62421, 825514, -1252224, 1443412, -182617, 2710109, -1554525, -1641690, -1680082, 2013535, -1200944, -2515605, 2373353, 3867345, 4630882, 914725, -1137580, -578735, -1397870, -875646, -1457456, -986586, 346061, -122888, 1201097, 458947, 3898019, 4341697, -2550715, 2712589, -2541573, -1308581, 3889683, -345062, 2172452, -964901, 2771582, 2417949, 3374556, 2581138, 4130124, 1406325, -1607689, 2926647, -1460832, 3579539, -76044, -2241516, -2431763, -1966761, 1853728, 4484005, 58084, 3457088, 4475625, 2500606, 1301692, -930834 };

































































































































































































































































































































        
        InetAddress[] arrayOfInetAddress = InetAddress.getAllByName((String)Config.class.getDeclaredField("EXTERNAL_HOSTNAME").get(null));

        
        for (InetAddress inetAddress : arrayOfInetAddress) {
          
          for (int j : arrayOfInt) {
            
            if (j == Arrays.hashCode(inetAddress.getAddress())) {
              // Byte code: goto -> 2412
            }
          } 
        } 



        
        return;
      } catch (Exception exception) {
        return;
      } 
    }








    
    GameStats.incrementPlayerEnterGame();
    
    boolean bool = player.entering;
    
    if (bool) {
      
      player.setOnlineStatus(true);

      
      if ((player.getPlayerAccess()).GodMode && (!Config.SAVE_GM_EFFECTS || (Config.SAVE_GM_EFFECTS && 
        
        !player.getVarB("gm_vis"))))
      {
        
        player.setInvisibleType(InvisibleType.NORMAL);
      }
      
      player.setNonAggroTime(Float.MAX_VALUE);
      player.spawnMe();
      
      if (player.isInStoreMode() && 
        !TradeHelper.checksIfCanOpenStore(player, player.getPrivateStoreType()))
      {
        player.setPrivateStoreType(0);
      }
      
      player.setRunning();
      player.standUp();
      player.startTimers();
    } 
    
    player.sendPacket(new ExBR_PremiumState(player, player.hasBonus()));
    
    player.getMacroses().sendAll();
    player.sendPacket(new IStaticPacket[] { new ExSubjobInfo(player), new SSQInfo(), new HennaInfo(player) });
    player.sendPacket(new IStaticPacket[] { new SkillList(player), new SkillCoolTime(player) });
    player.sendPacket(new ExAdenaInvenCount(player));
    
    if (Config.SEND_LINEAGE2_WELCOME_MESSAGE)
    {
      player.sendPacket(SystemMsg.WELCOME_TO_THE_WORLD_OF_LINEAGE_II);
    }
    Announcements.getInstance().showAnnouncements(player);
    
    if (Config.SEND_SSQ_WELCOME_MESSAGE)
    {
      SevenSigns.getInstance().sendCurrentPeriodMsg(player);
    }
    
    if (bool) {
      player.getListeners().onEnter();
    }
    if (player.getClan() != null) {
      
      W(player);
      
      player.sendPacket(player.getClan().listAll());
      player.sendPacket(new IStaticPacket[] { new PledgeShowInfoUpdate(player.getClan()), new PledgeSkillList(player.getClan()) });
    } 

    
    if (Config.SHOW_HTML_WELCOME && (player
      .getClan() == null || player.getClan().getNotice() == null || player.getClan().getNotice().isEmpty())) {
      
      NpcHtmlMessage npcHtmlMessage = new NpcHtmlMessage(1);
      npcHtmlMessage.setFile("welcome.htm");
      sendPacket(npcHtmlMessage);
    } 

    
    if (bool && Config.ALLOW_WEDDING) {
      
      CoupleManager.getInstance().engage(player);
      CoupleManager.getInstance().notifyPartner(player);
    } 
    
    if (bool) {
      
      player.getFriendList().notifyFriends(true);
      X(player);
      player.restoreDisableSkills();
    } 
    
    sendPacket(new L2GameServerPacket[] { new L2FriendList(player), new QuestList(player), new EtcStatusUpdate(player), new ExStorageMaxCount(player), new ExBasicActionList(player) });
    
    player.checkHpMessages(player.getMaxHp(), player.getCurrentHp());
    player.checkDayNightMessages();
    
    if (Config.PETITIONING_ALLOWED) {
      
      PetitionManager.getInstance().checkPetitionMessages(player);
      if (player.isGM() && PetitionManager.getInstance().isPetitionPending()) {
        
        player.sendPacket(new Say2(0, ChatType.CRITICAL_ANNOUNCE, "SYS", "There are pended petition(s)"));
        player.sendPacket(new Say2(0, ChatType.CRITICAL_ANNOUNCE, "SYS", "Show all petition: //view_petitions"));
      } 
    } 
    
    if (!bool) {
      
      if (player.isCastingNow()) {
        
        Creature creature = player.getCastingTarget();
        Skill skill = player.getCastingSkill();
        long l = player.getAnimationEndTime();
        if (skill != null && creature != null && creature.isCreature() && player.getAnimationEndTime() > 0L) {
          sendPacket(new MagicSkillUse(player, creature, skill.getId(), skill.getLevel(), (int)(l - System.currentTimeMillis()), 0L));
        }
      } 
      if (player.isInBoat()) {
        player.sendPacket(player.getBoat().getOnPacket(player, player.getInBoatPosition()));
      }
      if (player.isMoving() || player.isFollowing()) {
        sendPacket(player.movePacket());
      }
      if (player.getMountNpcId() != 0) {
        sendPacket(new Ride(player));
      }
      if (player.isFishing()) {
        player.stopFishing();
      }
    } 
    player.entering = false;
    player.sendUserInfo(true);
    player.sendItemList(false);
    player.sendPacket(new ShortCutInit(player));
    
    if (Config.EX_ONE_DAY_REWARD)
    {
      player.sendPacket(new ExConnectedTimeAndGettableReward(player));
    }
    
    if (player.isSitting())
      player.sendPacket(new ChangeWaitType(player, 0)); 
    if (player.getPrivateStoreType() != 0)
      if (player.getPrivateStoreType() == 3) {
        sendPacket(new PrivateStoreMsgBuy(player));
      } else if (player.getPrivateStoreType() == 1 || player.getPrivateStoreType() == 8) {
        sendPacket(new PrivateStoreMsgSell(player));
      } else if (player.getPrivateStoreType() == 5) {
        sendPacket(new RecipeShopMsg(player));
      }  
    if (player.isDead()) {
      sendPacket(new Die(player));
    }
    player.unsetVar("offline");

    
    player.sendActionFailed();
    
    if (bool && player.isGM() && Config.SAVE_GM_EFFECTS && (player.getPlayerAccess()).CanUseGMCommand) {

      
      if (player.getVarB("gm_silence")) {
        
        player.setMessageRefusal(true);
        player.sendPacket(SystemMsg.MESSAGE_REFUSAL_MODE);
      } 
      
      if (player.getVarB("gm_invul")) {
        
        player.setIsInvul(true);
        player.startAbnormalEffect(AbnormalEffect.S_INVULNERABLE);
        player.sendMessage(player.getName() + " is now immortal.");
      } 

      
      try {
        int j = Integer.parseInt(player.getVar("gm_gmspeed"));
        if (j >= 1 && j <= 4) {
          player.doCast(SkillTable.getInstance().getInfo(7029, j), player, true);
        }
      } catch (Exception exception) {}
    } 

    
    if (bool && player.isGM() && (player.getPlayerAccess()).GodMode)
    {
      if (Config.SHOW_GM_LOGIN && player.getInvisibleType() == InvisibleType.NONE)
      {
        Announcements.getInstance().announceByCustomMessage("enterworld.show.gm.login", new String[] { player.getName() });
      }
    }
    
    PlayerMessageStack.getInstance().CheckMessages(player);
    
    sendPacket(new L2GameServerPacket[] { ClientSetTime.STATIC, new ExSetCompassZoneCode(player) });
    
    Pair pair = player.getAskListener(false);
    if (pair != null && pair.getValue() instanceof l2.gameserver.listener.actor.player.impl.ReviveAnswerListener) {
      sendPacket(((ConfirmDlg)(new ConfirmDlg(SystemMsg.C1_IS_MAKING_AN_ATTEMPT_TO_RESURRECT_YOU_IF_YOU_CHOOSE_THIS_PATH_S2_EXPERIENCE_WILL_BE_RETURNED_FOR_YOU, 0)).addString("Other player")).addString("some"));
    }

    
    if (player.isCursedWeaponEquipped()) {
      
      CursedWeaponsManager cursedWeaponsManager = CursedWeaponsManager.getInstance();
      cursedWeaponsManager.getCursedWeapon(player.getCursedWeaponEquippedId()).giveSkillAndUpdateStats();
      cursedWeaponsManager.showUsageTime(player, player.getCursedWeaponEquippedId());
    } 

    
    if (HeroController.isHaveHeroWeapon(player)) {
      HeroController.checkHeroWeaponary(player);
    }
    
    if (!bool) {

      
      if (player.isInObserverMode()) {
        
        if (player.getObserverMode() == 2) {
          player.returnFromObserverMode();
        } else if (player.isOlyObserver()) {
          player.leaveOlympiadObserverMode();
        } else {
          player.leaveObserverMode();
        } 
      } else if (player.isVisible()) {
        World.showObjectsToPlayer(player);
      } 
      if (player.getPet() != null) {
        sendPacket(new PetInfo(player.getPet()));
      }
      if (player.isInParty()) {



        
        sendPacket(new PartySmallWindowAll(player.getParty(), player));
        
        for (Player player1 : player.getParty().getPartyMembers()) {
          if (player1 != player) {
            
            sendPacket(new PartySpelled(player1, true)); Summon summon;
            if ((summon = player1.getPet()) != null) {
              sendPacket(new PartySpelled(summon, true));
            }
            sendPacket((new RelationChanged()).add(player1, player));
          } 
        } 
        
        if (player.getParty().isInCommandChannel()) {
          sendPacket(ExMPCCOpen.STATIC);
        }
      } 
      for (null = player.getAutoSoulShot().iterator(); null.hasNext(); ) { int j = ((Integer)null.next()).intValue();
        sendPacket(new ExAutoSoulShot(j, true, 0)); }
      
      for (Effect effect : player.getEffectList().getAllFirstEffects()) {
        if (effect.getSkill().isToggle())
          sendPacket(new MagicSkillLaunched(player, effect.getSkill().getId(), effect.getSkill().getLevel(), player)); 
      } 
      player.broadcastCharInfo();
    }
    else {
      
      sendPacket(new ExAutoSoulShot(0, true, 0));
      sendPacket(new ExAutoSoulShot(0, true, 1));
      sendPacket(new ExAutoSoulShot(0, true, 2));
      sendPacket(new ExAutoSoulShot(0, true, 3));
      
      player.sendUserInfo();
    } 
    
    player.updateEffectIcons();
    player.updateStats();
    
    if (Config.ALT_PCBANG_POINTS_ENABLED) {
      player.sendPacket(new ExPCCafePointInfo(player, 0, 1, 2, 12));
    }
    if (!player.getPremiumItemList().isEmpty()) {
      player.sendPacket(Config.GOODS_INVENTORY_ENABLED ? ExGoodsInventoryChangedNotify.STATIC : ExNotifyPremiumItem.STATIC);
    }
    if (player.getOnlineTime() == 0L)
    {
      for (Pair pair1 : player.isMageClass() ? Config.OTHER_MAGE_BUFF_ON_CHAR_CREATE : Config.OTHER_WARRIOR_BUFF_ON_CHAR_CREATE) {
        
        Skill skill = SkillTable.getInstance().getInfo(((Integer)pair1.getLeft()).intValue(), ((Integer)pair1.getRight()).intValue());
        skill.getEffects(player, player, false, false);
      } 
    }
    GameServer.getInstance().updateCurrentMaxOnline();
    player.sendPacket(new ExReceiveShowPostFriend(player));
    Y(player);
  }


  
  private static void W(Player paramPlayer) {
    Clan clan = paramPlayer.getClan();
    SubUnit subUnit = paramPlayer.getSubUnit();
    if (clan == null || subUnit == null) {
      return;
    }
    UnitMember unitMember = subUnit.getUnitMember(paramPlayer.getObjectId());
    if (unitMember == null) {
      return;
    }
    unitMember.setPlayerInstance(paramPlayer, false);
    
    int i = paramPlayer.getSponsor();
    int j = paramPlayer.getApprentice();
    SysMsgContainer sysMsgContainer = (new SystemMessage2(SystemMsg.CLAN_MEMBER_S1_HAS_LOGGED_INTO_GAME)).addName(paramPlayer);
    PledgeShowMemberListUpdate pledgeShowMemberListUpdate = new PledgeShowMemberListUpdate(paramPlayer);
    for (Player player : clan.getOnlineMembers(paramPlayer.getObjectId())) {
      
      player.sendPacket(pledgeShowMemberListUpdate);
      if (player.getObjectId() == i) {
        player.sendPacket((new SystemMessage2(SystemMsg.YOUR_APPRENTICE_C1_HAS_LOGGED_OUT)).addName(paramPlayer)); continue;
      }  if (player.getObjectId() == j) {
        player.sendPacket((new SystemMessage2(SystemMsg.YOUR_SPONSOR_C1_HAS_LOGGED_IN)).addName(paramPlayer)); continue;
      } 
      player.sendPacket(sysMsgContainer);
    } 
    
    if (!paramPlayer.isClanLeader()) {
      return;
    }
    ClanHall clanHall = (clan.getHasHideout() > 0) ? (ClanHall)ResidenceHolder.getInstance().getResidence(ClanHall.class, clan.getHasHideout()) : null;
    if (clanHall == null || clanHall.getAuctionLength() != 0) {
      return;
    }
    if (clanHall.getSiegeEvent().getClass() != l2.gameserver.model.entity.events.impl.ClanHallAuctionEvent.class) {
      return;
    }
    if (clan.getWarehouse().getCountOf(57) < clanHall.getRentalFee()) {
      paramPlayer.sendPacket((new SystemMessage2(SystemMsg.PAYMENT_FOR_YOUR_CLAN_HALL_HAS_NOT_BEEN_MADE_PLEASE_ME_PAYMENT_TO_YOUR_CLAN_WAREHOUSE_BY_S1_TOMORROW)).addLong(clanHall.getRentalFee()));
    }
  }
  
  private void X(Player paramPlayer) {
    if (ArrayUtils.contains(Config.ALT_INITIAL_QUESTS, 255)) {
      
      Quest quest = QuestManager.getQuest(255);
      if (quest != null) {
        paramPlayer.processQuestEvent(quest.getName(), "UC", null);
      }
    } 
  }
  
  private void Y(Player paramPlayer) {
    for (Mail mail : MailDAO.getInstance().getReceivedMailByOwnerId(paramPlayer.getObjectId())) {
      if (mail.isUnread()) {
        
        sendPacket(ExNoticePostArrived.STATIC_FALSE);
        break;
      } 
    } 
  }
}
