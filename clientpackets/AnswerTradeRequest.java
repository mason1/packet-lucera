package l2.gameserver.network.l2.c2s;

import java.util.concurrent.CopyOnWriteArrayList;
import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.network.l2.s2c.SystemMessage2;
import l2.gameserver.network.l2.s2c.TradeStart;




public class AnswerTradeRequest
  extends L2GameClientPacket
{
  private int pI;
  
  protected void readImpl() { this.pI = readD(); }



  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    request = player1.getRequest();
    if (request == null || !request.isTypeOf(Request.L2RequestType.TRADE_REQUEST)) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (!request.isInProgress()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isOutOfControl()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    Player player2 = request.getRequestor();
    if (player2 == null) {
      
      request.cancel();
      player1.sendPacket(SystemMsg.THAT_PLAYER_IS_NOT_ONLINE);
      player1.sendActionFailed();
      
      return;
    } 
    if (player2.getRequest() != request) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    
    if (this.pI == 0) {
      
      request.cancel();
      player2.sendPacket((new SystemMessage2(SystemMsg.C1_HAS_DENIED_YOUR_REQUEST_TO_TRADE)).addString(player1.getName()));
      
      return;
    } 
    if (!player2.isInActingRange(player1)) {
      
      request.cancel();
      player1.sendPacket(SystemMsg.YOUR_TARGET_IS_OUT_OF_RANGE);
      
      return;
    } 
    if (player2.isActionsDisabled()) {
      
      request.cancel();
      player1.sendPacket((new SystemMessage2(SystemMsg.C1_IS_ON_ANOTHER_TASK)).addString(player2.getName()));
      player1.sendActionFailed();
      
      return;
    } 
    
    try {
      new Request(Request.L2RequestType.TRADE, player1, player2);
      player2.setTradeList(new CopyOnWriteArrayList());
      player2.sendPacket(new IStaticPacket[] { (new SystemMessage(SystemMsg.YOU_BEGIN_TRADING_WITH_C1)).addName(player1), new TradeStart(true, player2, player1), new TradeStart(false, player2, player1) });
      player1.setTradeList(new CopyOnWriteArrayList());
      player1.sendPacket(new IStaticPacket[] { (new SystemMessage(SystemMsg.YOU_BEGIN_TRADING_WITH_C1)).addName(player2), new TradeStart(true, player1, player2), new TradeStart(false, player1, player2) });
    }
    finally {
      
      request.done();
    } 
  }
}
