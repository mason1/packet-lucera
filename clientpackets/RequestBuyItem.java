package l2.gameserver.network.l2.c2s;

import java.util.ArrayList;
import java.util.List;
import l2.commons.math.SafeMath;
import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.data.xml.holder.BuyListHolder;
import l2.gameserver.instancemanager.ReflectionManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.model.items.TradeItem;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExBuySellList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




public class RequestBuyItem
  extends L2GameClientPacket
{
  private static final Logger _log = LoggerFactory.getLogger(RequestBuyItem.class);
  
  private int vU;
  
  private int _count;
  
  private int[] Yd;
  private long[] Ye;
  
  protected void readImpl() {
    this.vU = readD();
    this._count = readD();
    if (this._count * 12 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      
      return;
    } 
    this.Yd = new int[this._count];
    this.Ye = new long[this._count];
    
    for (byte b = 0; b < this._count; b++) {
      
      this.Yd[b] = readD();
      this.Ye[b] = readQ();
      if (this.Ye[b] < 1L) {
        
        this._count = 0;
        break;
      } 
    } 
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0) {
      return;
    }
    
    if (player.getBuyListId() != this.vU) {
      
      player.sendActionFailed();
      
      return;
    } 
    
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
      
      return;
    } 
    if (!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && player.getKarma() > 0 && !player.isGM()) {
      
      player.sendActionFailed();
      
      return;
    } 
    NpcInstance npcInstance = player.getLastNpc();
    boolean bool = (npcInstance != null && npcInstance.isMerchantNpc()) ? 1 : 0;
    if (!player.isGM() && (npcInstance == null || !bool || !npcInstance.isInActingRange(player))) {
      
      player.sendActionFailed();
      
      return;
    } 
    BuyListHolder.NpcTradeList npcTradeList = BuyListHolder.getInstance().getBuyList(this.vU);
    if (npcTradeList == null) {

      
      player.sendActionFailed();
      
      return;
    } 
    byte b = 0;
    long l1 = 0L;
    long l2 = 0L;
    long l3 = 0L;
    double d = 0.0D;
    
    Castle castle = null;
    if (npcInstance != null) {
      
      castle = npcInstance.getCastle(player);
      if (castle != null) {
        d = castle.getTaxRate();
      }
    } 
    ArrayList arrayList = new ArrayList(this._count);
    List list = npcTradeList.getItems();
    try {
      byte b1;
      label99: for (b1 = 0; b1 < this._count; b1++) {
        
        int i = this.Yd[b1];
        long l4 = this.Ye[b1];
        long l5 = 0L;
        
        for (TradeItem tradeItem1 : list) {
          if (tradeItem1.getItemId() == i) {
            
            if (tradeItem1.isCountLimited() && tradeItem1.getCurrentValue() < l4)
              continue label99; 
            l5 = tradeItem1.getOwnersPrice();
          } 
        } 
        if (l5 == 0L && (!player.isGM() || !(player.getPlayerAccess()).UseGMShop)) {

          
          player.sendActionFailed();
          
          return;
        } 
        l2 = SafeMath.addAndCheck(l2, SafeMath.mulAndCheck(l4, l5));
        
        TradeItem tradeItem = new TradeItem();
        tradeItem.setItemId(i);
        tradeItem.setCount(l4);
        tradeItem.setOwnersPrice(l5);
        
        l1 = SafeMath.addAndCheck(l1, SafeMath.mulAndCheck(l4, tradeItem.getItem().getWeight()));
        if (!tradeItem.getItem().isStackable() || player.getInventory().getItemByItemId(i) == null) {
          b++;
        }
        arrayList.add(tradeItem);
      } 
      
      l3 = (long)(l2 * d);
      
      l2 = SafeMath.addAndCheck(l2, l3);
      
      if (!player.getInventory().validateWeight(l1)) {
        
        sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
        
        return;
      } 
      if (!player.getInventory().validateCapacity(b)) {
        
        sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
        
        return;
      } 
      if (!player.reduceAdena(l2)) {
        
        player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
        
        return;
      } 
      for (TradeItem tradeItem : arrayList) {
        player.getInventory().addItem(tradeItem.getItemId(), tradeItem.getCount());
      }
      
      npcTradeList.updateItems(arrayList);

      
      if (castle != null && 
        l3 > 0L && castle.getOwnerId() > 0 && player.getReflection() == ReflectionManager.DEFAULT) {
        castle.addToTreasury(l3, true, false);
      }
    } catch (ArithmeticException arithmeticException) {

      
      sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);
      
      return;
    } 
    sendPacket(new ExBuySellList.SellRefundList(player, true));
    player.sendChanges();
  }
}
