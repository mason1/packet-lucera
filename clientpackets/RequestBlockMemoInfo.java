package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExBlockDetailInfo;






public class RequestBlockMemoInfo
  extends L2GameClientPacket
{
  private String Yc;
  
  protected void readImpl() { this.Yc = readS(Config.CNAME_MAXLEN); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }

    
    if (player.getBlockList().contains(this.Yc))
    {
      
      player.sendPacket(new ExBlockDetailInfo(this.Yc, "test"));
    }
  }
}
