package l2.gameserver.network.l2.c2s;

import l2.commons.lang.ArrayUtils;
import l2.gameserver.Config;
import l2.gameserver.model.Player;
import l2.gameserver.model.World;
import l2.gameserver.model.chat.ChatFilters;
import l2.gameserver.model.chat.chatfilter.ChatFilter;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.ChatType;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.L2FriendSay;
import l2.gameserver.utils.Log;







public class RequestSendL2FriendSay
  extends L2GameClientPacket
{
  private String _message;
  private String ZR;
  
  protected void readImpl() {
    this._message = readS(2048);
    this.ZR = readS(16);
  }


  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    
    Player player2 = World.getPlayer(this.ZR);
    if (player2 == null) {
      
      player1.sendPacket(SystemMsg.THAT_PLAYER_IS_NOT_ONLINE);
      
      return;
    } 
    if (player2.isBlockAll()) {
      
      player1.sendPacket(SystemMsg.THAT_PERSON_IS_IN_MESSAGE_REFUSAL_MODE);
      
      return;
    } 
    
    if (!(player1.getPlayerAccess()).CanAnnounce)
    {
      for (ChatFilter chatFilter : ChatFilters.getinstance().getFilters()) {
        
        if (chatFilter.isMatch(player1, ChatType.L2FRIEND, this._message, player2))
        {
          switch (chatFilter.getAction()) {
            
            case 1:
              player1.updateNoChannel(Integer.parseInt(chatFilter.getValue()) * 1000L);
              break;
            case 2:
              player1.sendMessage(new CustomMessage(chatFilter.getValue(), player1, new Object[0]));
              return;
            case 3:
              this._message = chatFilter.getValue();
              break;
          } 
        
        }
      } 
    }
    if (player1.getNoChannel() > 0L)
    {
      if (ArrayUtils.contains(Config.BAN_CHANNEL_LIST, ChatType.L2FRIEND)) {
        
        if (player1.getNoChannelRemained() > 0L) {
          
          long l = player1.getNoChannelRemained() / 60000L + 1L;
          player1.sendMessage((new CustomMessage("common.ChatBanned", player1, new Object[0])).addNumber(l));
          return;
        } 
        player1.updateNoChannel(0L);
      } 
    }
    
    if (!player1.getFriendList().getList().containsKey(Integer.valueOf(player2.getObjectId()))) {
      return;
    }
    if (player1.canTalkWith(player2)) {
      
      L2FriendSay l2FriendSay = new L2FriendSay(player1.getName(), this.ZR, this._message);
      player2.sendPacket(l2FriendSay);
      
      Log.LogChat("FRIENDTELL", player1.getName(), this.ZR, this._message, 0);
    } 
  }
}
