package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.GameObject;
import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.AskJoinAlliance;
import l2.gameserver.network.l2.s2c.SystemMessage;



public class RequestJoinAlly
  extends L2GameClientPacket
{
  private int Bq;
  
  protected void readImpl() { this.Bq = readD(); }



  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null || player1.getClan() == null || player1.getAlliance() == null) {
      return;
    }
    if (player1.isOutOfControl()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isProcessingRequest()) {
      
      player1.sendPacket(Msg.WAITING_FOR_ANOTHER_REPLY);
      
      return;
    } 
    if (player1.getAlliance().getMembersCount() >= Config.MAX_ALLY_SIZE) {
      
      player1.sendPacket(Msg.YOU_HAVE_FAILED_TO_INVITE_A_CLAN_INTO_THE_ALLIANCE);
      
      return;
    } 
    GameObject gameObject = player1.getVisibleObject(this.Bq);
    if (gameObject == null || !gameObject.isPlayer() || gameObject == player1) {
      
      player1.sendPacket(SystemMsg.THAT_IS_AN_INCORRECT_TARGET);
      
      return;
    } 
    Player player2 = (Player)gameObject;
    
    if (!player1.isAllyLeader()) {
      
      player1.sendPacket(Msg.FEATURE_AVAILABLE_TO_ALLIANCE_LEADERS_ONLY);
      
      return;
    } 
    if (player2.getAlliance() != null || player1.getAlliance().isMember(player2.getClan().getClanId())) {

      
      SystemMessage systemMessage = new SystemMessage(691);
      systemMessage.addString(player2.getClan().getName());
      systemMessage.addString(player2.getAlliance().getAllyName());
      player1.sendPacket(systemMessage);
      
      return;
    } 
    if (!player2.isClanLeader()) {
      
      player1.sendPacket((new SystemMessage(9)).addString(player2.getName()));
      
      return;
    } 
    if (player1.isAtWarWith(Integer.valueOf(player2.getClanId())) > 0) {
      
      player1.sendPacket(Msg.YOU_MAY_NOT_ALLY_WITH_A_CLAN_YOU_ARE_AT_BATTLE_WITH);
      
      return;
    } 
    if (!player2.getClan().canJoinAlly()) {
      
      SystemMessage systemMessage = new SystemMessage(761);
      systemMessage.addString(player2.getClan().getName());
      player1.sendPacket(systemMessage);
      
      return;
    } 
    if (!player1.getAlliance().canInvite()) {
      
      player1.sendMessage(new CustomMessage("l2p.gameserver.clientpackets.RequestJoinAlly.InvitePenalty", player1, new Object[0]));
      
      return;
    } 
    if (player2.isBusy()) {
      
      player1.sendPacket((new SystemMessage(153)).addString(player2.getName()));
      
      return;
    } 
    (new Request(Request.L2RequestType.ALLY, player1, player2)).setTimeout(10000L);
    
    player2.sendPacket((new SystemMessage(527)).addString(player1.getAlliance().getAllyName()).addName(player1));
    player2.sendPacket(new AskJoinAlliance(player1.getObjectId(), player1.getName(), player1.getAlliance().getAllyName()));
  }
}
