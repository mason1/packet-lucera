package l2.gameserver.network.l2.c2s;

import l2.commons.math.SafeMath;
import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.PcFreight;
import l2.gameserver.model.items.PcInventory;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.utils.Log;
import org.apache.commons.lang3.ArrayUtils;









public class RequestPackageSend
  extends L2GameClientPacket
{
  private static final long Zs = 1000L;
  private int Bq;
  private int _count;
  private int[] Yd;
  private long[] Ye;
  
  protected void readImpl() {
    this.Bq = readD();
    this._count = readD();
    if (this._count * 12 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      
      return;
    } 
    this.Yd = new int[this._count];
    this.Ye = new long[this._count];
    
    for (byte b = 0; b < this._count; b++) {
      
      this.Yd[b] = readD();
      this.Ye[b] = readQ();
      if (this.Ye[b] < 1L || ArrayUtils.indexOf(this.Yd, this.Yd[b]) < b) {
        
        this._count = 0;
        return;
      } 
    } 
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0) {
      return;
    }
    if (!(player.getPlayerAccess()).UseWarehouse) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(SystemMsg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    
    NpcInstance npcInstance = player.getLastNpc();
    if ((npcInstance == null || !npcInstance.isInActingRange(npcInstance)) && !Config.ALT_ALLOW_REMOTE_USE_CARGO_BOX) {
      
      player.sendPacket(Msg.WAREHOUSE_IS_TOO_FAR);
      
      return;
    } 
    if (!player.getAccountChars().containsKey(Integer.valueOf(this.Bq))) {
      return;
    }
    pcInventory = player.getInventory();
    pcFreight = new PcFreight(this.Bq);
    pcFreight.restore();
    
    pcInventory.writeLock();
    pcFreight.writeLock();
    
    try {
      int i = 0;
      long l1 = 0L;
      
      i = Config.FREIGHT_SLOTS - pcFreight.getSize();
      
      boolean bool = false;

      
      byte b1 = 0; while (true) { ItemInstance itemInstance; if (b1 < this._count)
        
        { itemInstance = pcInventory.getItemByObjectId(this.Yd[b1]);
          if (itemInstance == null || itemInstance.getCount() < this.Ye[b1] || !itemInstance.getTemplate().isTradeable() || !CanSendItem(itemInstance)) {
            
            this.Yd[b1] = 0;
            this.Ye[b1] = 0L;
            
            continue;
          } 
          if (!itemInstance.isStackable() || pcFreight.getItemByItemId(itemInstance.getItemId()) == null)
          
          { if (i <= 0)
            
            { this.Yd[b1] = 0;
              this.Ye[b1] = 0L; }
            else
            
            { i--;

              
              if (itemInstance.getItemId() == 57)
                l1 = this.Ye[b1];  }  continue; }  } else { break; }  if (itemInstance.getItemId() == 57) l1 = this.Ye[b1];

        
        b1++; }

      
      if (i <= 0) {
        player.sendPacket(SystemMsg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);
      }
      if (!bool) {
        
        player.sendPacket(SystemMsg.INCORRECT_ITEM_COUNT);
        
        return;
      } 
      
      long l2 = SafeMath.mulAndCheck(bool, 1000L);
      
      if (l2 + l1 > player.getAdena()) {
        
        player.sendPacket(SystemMsg.YOU_LACK_THE_FUNDS_NEEDED_TO_PAY_FOR_THIS_TRANSACTION);
        
        return;
      } 
      if (!player.reduceAdena(l2, true)) {
        
        player.sendPacket(SystemMsg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
        
        return;
      } 
      for (byte b2 = 0; b2 < this._count; b2++) {
        
        if (this.Yd[b2] != 0) {
          
          ItemInstance itemInstance = pcInventory.removeItemByObjectId(this.Yd[b2], this.Ye[b2]);
          Log.LogItem(player, Log.ItemLog.FreightDeposit, itemInstance);
          pcFreight.addItem(itemInstance);
        } 
      } 
    } catch (ArithmeticException arithmeticException) {

      
      player.sendPacket(SystemMsg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);

      
      return;
    } finally {
      pcFreight.writeUnlock();
      pcInventory.writeUnlock();
    } 

    
    player.sendChanges();
    player.sendPacket(SystemMsg.THE_TRANSACTION_IS_COMPLETE);
  }

  
  public static boolean CanSendItem(ItemInstance paramItemInstance) {
    if (!paramItemInstance.getTemplate().isTradeable())
    {
      return false;
    }
    if (paramItemInstance.isEquipped() || paramItemInstance.getTemplate().isQuest() || paramItemInstance.isAugmented())
    {
      return false;
    }
    return true;
  }
}
