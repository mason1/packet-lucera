package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.KeyPacket;
import l2.gameserver.network.l2.s2c.SendStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class ProtocolVersion
  extends L2GameClientPacket
{
  private static final Logger _log = LoggerFactory.getLogger(ProtocolVersion.class);

  
  private int XK;


  
  protected void readImpl() { this.XK = readD(); }



  
  protected void runImpl() {
    if (this.XK == -2) {
      
      ((GameClient)this._client).closeNow(false);
      return;
    } 
    if (this.XK == -3) {
      
      _log.info("Status request from IP : " + ((GameClient)getClient()).getIpAddr());
      ((GameClient)getClient()).close(new SendStatus());
      return;
    } 
    if (this.XK < Config.MIN_PROTOCOL_REVISION || this.XK > Config.MAX_PROTOCOL_REVISION) {
      
      _log.warn("Unknown protocol revision : " + this.XK + ", client : " + this._client);
      ((GameClient)getClient()).close(new KeyPacket(null));
      
      return;
    } 
    ((GameClient)getClient()).setRevision(this.XK);
    sendPacket(new KeyPacket(((GameClient)this._client).enableCrypt()));
  }
}
