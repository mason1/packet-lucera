package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.HennaUnequipList;





public class RequestHennaUnequipList
  extends L2GameClientPacket
{
  private int _symbolId;
  
  protected void readImpl() { this._symbolId = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    HennaUnequipList hennaUnequipList = new HennaUnequipList(player);
    player.sendPacket(hennaUnequipList);
  }
}
