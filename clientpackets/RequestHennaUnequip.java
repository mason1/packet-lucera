package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.templates.Henna;





public class RequestHennaUnequip
  extends L2GameClientPacket
{
  private int _symbolId;
  
  protected void readImpl() { this._symbolId = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    for (byte b = 1; b <= 3; b++) {
      
      Henna henna = player.getHenna(b);
      if (henna != null)
      {
        
        if (henna.getSymbolId() == this._symbolId) {
          
          long l = henna.getPrice() / 5L;
          if (player.getAdena() < l) {
            
            player.sendPacket(SystemMsg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
            
            break;
          } 
          player.reduceAdena(l);
          
          player.removeHenna(b);
          
          player.sendPacket(SystemMsg.THE_SYMBOL_HAS_BEEN_DELETED);
          break;
        } 
      }
    } 
  }
}
