package l2.gameserver.network.l2.c2s;

import l2.gameserver.data.xml.holder.RecipeHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.Recipe;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.RecipeItemMakeInfo;






public class RequestRecipeItemMakeInfo
  extends L2GameClientPacket
{
  private int _id;
  
  protected void readImpl() { this._id = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Recipe recipe = RecipeHolder.getInstance().getRecipeById(this._id);
    if (recipe == null) {
      
      player.sendActionFailed();
      
      return;
    } 
    sendPacket(new RecipeItemMakeInfo(player, recipe, -1));
  }
}
