package l2.gameserver.network.l2.c2s;

import java.util.HashMap;
import java.util.StringTokenizer;
import l2.gameserver.Config;
import l2.gameserver.data.xml.holder.MultiSellHolder;
import l2.gameserver.handler.admincommands.AdminCommandHandler;
import l2.gameserver.handler.bbs.CommunityBoardManager;
import l2.gameserver.handler.bbs.ICommunityBoardHandler;
import l2.gameserver.handler.voicecommands.IVoicedCommandHandler;
import l2.gameserver.handler.voicecommands.VoicedCommandHandler;
import l2.gameserver.instancemanager.BypassManager;
import l2.gameserver.model.Effect;
import l2.gameserver.model.GameObject;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.model.entity.oly.CompetitionController;
import l2.gameserver.model.entity.oly.HeroController;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.model.instances.TrainerInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExEnchantSkill;
import l2.gameserver.network.l2.s2c.ExEnchantSkillInfo;
import l2.gameserver.network.l2.s2c.ExEnchantSkillList;
import l2.gameserver.network.l2.s2c.NpcHtmlMessage;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.scripts.Scripts;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




public class RequestBypassToServer
  extends L2GameClientPacket
{
  private static final Logger _log = LoggerFactory.getLogger(RequestBypassToServer.class);
  private BypassManager.DecodedBypass Yf = null;

  
  private String _bypass;

  
  protected void readImpl() { this._bypass = readS(); }



  
  protected void runImpl() {
    GameClient gameClient = (GameClient)getClient();









    
    Player player = gameClient.getActiveChar();
    if (player == null) {
      return;
    }
    try {
      if (this._bypass == null || this._bypass.isEmpty() || (this.Yf = gameClient.decodeBypass(this._bypass)) == null) {
        return;
      }

      
      NpcInstance npcInstance = player.getLastNpc();
      GameObject gameObject = player.getTarget();
      if (npcInstance == null && gameObject != null && gameObject.isNpc())
      {
        npcInstance = (NpcInstance)gameObject;
      }
      
      if (this.Yf.bypass.startsWith("admin_")) {
        
        AdminCommandHandler.getInstance().useAdminCommandHandler(player, this.Yf.bypass);
      }
      else if (this.Yf.bypass.equals("come_here") && player.isGM()) {
        
        a((GameClient)getClient());
      }
      else if (this.Yf.bypass.startsWith("player_help ")) {
        
        o(player, this.Yf.bypass.substring(12));
      }
      else if (this.Yf.bypass.startsWith("scripts_")) {
        
        String str = this.Yf.bypass.substring(8).trim();
        String[] arrayOfString1 = str.split("\\s+");
        String[] arrayOfString2 = str.substring(arrayOfString1[0].length()).trim().split("\\s+");
        String[] arrayOfString3 = arrayOfString1[0].split(":");
        if (arrayOfString3.length != 2) {
          
          _log.warn("Bad Script bypass!");
          
          return;
        } 
        HashMap hashMap = null;
        if (npcInstance != null) {
          
          hashMap = new HashMap(1);
          hashMap.put("npc", npcInstance.getRef());
        } 
        
        if (arrayOfString1.length == 1)
        {
          Scripts.getInstance().callScripts(player, arrayOfString3[0], arrayOfString3[1], hashMap);
        }
        else
        {
          Scripts.getInstance().callScripts(player, arrayOfString3[0], arrayOfString3[1], new Object[] { arrayOfString2 }, hashMap);
        }
      
      } else if (this.Yf.bypass.startsWith("user_")) {
        
        String str1 = this.Yf.bypass.substring(5).trim();
        String str2 = str1.split("\\s+")[0];
        String str3 = str1.substring(str2.length()).trim();
        IVoicedCommandHandler iVoicedCommandHandler = VoicedCommandHandler.getInstance().getVoicedCommandHandler(str2);
        
        if (iVoicedCommandHandler != null)
        {
          iVoicedCommandHandler.useVoicedCommand(str2, player, str3);
        }
        else
        {
          _log.warn("Unknown voiced command '" + str2 + "'");
        }
      
      } else if (this.Yf.bypass.startsWith("npc_")) {
        String str;
        int i = this.Yf.bypass.indexOf('_', 5);
        
        if (i > 0) {
          
          str = this.Yf.bypass.substring(4, i);
        }
        else {
          
          str = this.Yf.bypass.substring(4);
        } 
        GameObject gameObject1 = player.getVisibleObject(Integer.parseInt(str));
        if (gameObject1 != null && gameObject1.isNpc() && i > 0 && gameObject1.isInActingRange(player))
        {
          player.setLastNpc((NpcInstance)gameObject1);
          ((NpcInstance)gameObject1).onBypassFeedback(player, this.Yf.bypass.substring(i + 1));
        }
      
      } else if (this.Yf.bypass.startsWith("_olympiad") && this.Yf.bypass.contains("?command=move_op_field&field=")) {
        
        if (!Config.OLY_SPECTATION_ALLOWED) {
          return;
        }
        
        int i = -1;
        
        try {
          i = Integer.parseInt(this.Yf.bypass.substring(38));
          CompetitionController.getInstance().watchCompetition(player, i);
        
        }
        catch (Exception exception) {
          
          _log.warn("OlyObserver", exception);
          exception.printStackTrace();
        }
      
      } else if (this.Yf.bypass.startsWith("_diary")) {
        
        String str = this.Yf.bypass.substring(this.Yf.bypass.indexOf("?") + 1);
        StringTokenizer stringTokenizer = new StringTokenizer(str, "&");
        int i = Integer.parseInt(stringTokenizer.nextToken().split("=")[1]);
        int j = Integer.parseInt(stringTokenizer.nextToken().split("=")[1]);
        HeroController.getInstance().showHeroDiary(player, i, j);
      }
      else if (this.Yf.bypass.startsWith("_match")) {
        
        String str = this.Yf.bypass.substring(this.Yf.bypass.indexOf("?") + 1);
        StringTokenizer stringTokenizer = new StringTokenizer(str, "&");
        int i = Integer.parseInt(stringTokenizer.nextToken().split("=")[1]);
        int j = Integer.parseInt(stringTokenizer.nextToken().split("=")[1]);
        
        HeroController.getInstance().showHistory(player, i, j);
      }
      else if (this.Yf.bypass.startsWith("manor_menu_select")) {
        
        GameObject gameObject1 = player.getTarget();
        if (gameObject1 != null && gameObject1.isNpc())
        {
          ((NpcInstance)gameObject1).onBypassFeedback(player, this.Yf.bypass);
        }
      }
      else if (this.Yf.bypass.startsWith(ExEnchantSkillList.EX_ENCHANT_SKILLLIST_BYPASS)) {
        
        if (npcInstance != null && npcInstance instanceof TrainerInstance && NpcInstance.canBypassCheck(player, npcInstance)) {
          
          TrainerInstance trainerInstance = (TrainerInstance)npcInstance;
          String str = this.Yf.bypass.substring(ExEnchantSkillList.EX_ENCHANT_SKILLLIST_BYPASS.length()).trim();
          if (StringUtils.isNumeric(str)) {
            player.sendPacket(ExEnchantSkillList.packetFor(player, trainerInstance, Integer.parseInt(str)));
          }
        } 
      } else if (this.Yf.bypass.startsWith(ExEnchantSkillInfo.EX_ENCHANT_SKILLINFO_BYPASS)) {
        
        if (npcInstance != null && npcInstance instanceof TrainerInstance && NpcInstance.canBypassCheck(player, npcInstance))
        {
          TrainerInstance trainerInstance = (TrainerInstance)npcInstance;
          String str = this.Yf.bypass.substring(ExEnchantSkillInfo.EX_ENCHANT_SKILLINFO_BYPASS.length()).trim();
          player.sendPacket(ExEnchantSkillInfo.packetFor(player, trainerInstance, StringUtils.split(str, ' ')));
        }
      
      } else if (this.Yf.bypass.startsWith(ExEnchantSkill.EX_ENCHANT_SKILL_BYPASS)) {
        
        if (npcInstance != null && npcInstance instanceof TrainerInstance && NpcInstance.canBypassCheck(player, npcInstance))
        {
          TrainerInstance trainerInstance = (TrainerInstance)npcInstance;
          String str = this.Yf.bypass.substring(ExEnchantSkill.EX_ENCHANT_SKILL_BYPASS.length()).trim();
          player.sendPacket(ExEnchantSkill.packetFor(player, trainerInstance, StringUtils.split(str, ' ')));
        }
      
      } else if (Config.CUSTOM_DISPELL_EFFECTS && this.Yf.bypass.startsWith("_dispel")) {

        
        int i = this.Yf.bypass.indexOf(':');
        if (i < 0)
          return; 
        String str1 = this.Yf.bypass.substring(i + 1);
        int j = str1.indexOf(',');
        if (j <= 0)
          return; 
        String str2 = str1.substring(0, j).trim();
        String str3 = str1.substring(j + 1).trim();
        if (!StringUtils.isNumeric(str2) || !StringUtils.isNumeric(str3))
          return; 
        c(player, Integer.parseInt(str2), Integer.parseInt(str3));
      }
      else if (this.Yf.bypass.startsWith("multisell ")) {
        
        MultiSellHolder.getInstance().SeparateAndSend(Integer.parseInt(this.Yf.bypass.substring(10)), player, 0.0D);
      }
      else if (this.Yf.bypass.startsWith("Quest ")) {
        
        String str = this.Yf.bypass.substring(6).trim();
        int i = str.indexOf(' ');
        if (i < 0)
        {
          player.processQuestEvent(str, "", npcInstance);
        }
        else
        {
          player.processQuestEvent(str.substring(0, i), str.substring(i).trim(), npcInstance);
        }
      
      } else if (this.Yf.bbs) {
        
        if (!Config.COMMUNITYBOARD_ENABLED) {
          
          player.sendPacket(new SystemMessage(938));
        }
        else {
          
          ICommunityBoardHandler iCommunityBoardHandler = CommunityBoardManager.getInstance().getCommunityHandler(this.Yf.bypass, player);
          if (iCommunityBoardHandler != null) {
            iCommunityBoardHandler.onBypassCommand(player, this.Yf.bypass);
          }
        } 
      } 
    } catch (Exception exception) {

      
      String str = "Bad RequestBypassToServer: " + this.Yf.bypass;
      GameObject gameObject = (player != null) ? player.getTarget() : null;
      if (gameObject != null && gameObject.isNpc())
      {
        str = str + " via NPC #" + ((NpcInstance)gameObject).getNpcId();
      }
      _log.error(str, exception);
    } 
  }

  
  private static void a(GameClient paramGameClient) {
    GameObject gameObject = paramGameClient.getActiveChar().getTarget();
    if (gameObject != null && gameObject.isNpc()) {
      
      NpcInstance npcInstance = (NpcInstance)gameObject;
      Player player = paramGameClient.getActiveChar();
      npcInstance.setTarget(player);
      npcInstance.moveToLocation(player.getLoc(), 0, true);
    } 
  }

  
  private static void o(Player paramPlayer, String paramString) {
    NpcHtmlMessage npcHtmlMessage = new NpcHtmlMessage(5);
    npcHtmlMessage.setFile(paramString);
    paramPlayer.sendPacket(npcHtmlMessage);
  }

  
  private static void c(Player paramPlayer, int paramInt1, int paramInt2) {
    boolean bool = false;
    if (paramPlayer.isOlyParticipant() && !Config.CUSTOM_DISPELL_EFFECTS_ON_OLYMPIAD) {
      return;
    }
    for (Effect effect : paramPlayer.getEffectList().getAllEffects()) {
      if (effect.getDisplayId() == paramInt1 && effect.getDisplayLevel() == paramInt2) {
        
        if (!effect.isOffensive() && 
          !effect.getSkill().isMusic() && effect
          .getSkill().isSelfDispellable() && effect
          .getSkill().getSkillType() != Skill.SkillType.TRANSFORMATION) {
          
          effect.exit();
          bool = true; continue;
        } 
        return;
      } 
    } 
    if (bool)
      paramPlayer.sendPacket((new SystemMessage(92)).addSkillName(paramInt1, paramInt2)); 
  }
}
