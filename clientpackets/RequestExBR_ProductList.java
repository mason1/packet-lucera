package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExBR_ProductList;



public class RequestExBR_ProductList
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    
    if (player == null) {
      return;
    }
    player.sendPacket(new ExBR_ProductList());
  }
}
