package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ManagePledgePower;


public class RequestPledgePower
  extends L2GameClientPacket
{
  private int VM;
  private int fc;
  private int VN;
  
  protected void readImpl() {
    this.VM = readD();
    this.fc = readD();
    if (this.fc == 2) {
      this.VN = readD();
    }
  }

  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    if (this.fc == 2) {
      
      if (this.VM < 1 || this.VM > 9)
        return; 
      if (player.getClan() != null && (player.getClanPrivileges() & 0x10) == 16)
      {
        if (this.VM == 9)
          this.VN = (this.VN & 0x8) + (this.VN & 0x400) + (this.VN & 0x8000) + (this.VN & 0x800) + (this.VN & 0x40000); 
        player.getClan().setRankPrivs(this.VM, this.VN);
        player.getClan().updatePrivsForRank(this.VM);
      }
    
    } else if (player.getClan() != null) {
      player.sendPacket(new ManagePledgePower(player, this.fc, this.VM));
    } else {
      player.sendActionFailed();
    } 
  }
}
