package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.ItemInfoCache;
import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.network.l2.s2c.ActionFail;
import l2.gameserver.network.l2.s2c.ExRpItemLink;



public class RequestExRqItemLink
  extends L2GameClientPacket
{
  private int Bq;
  
  protected void readImpl() { this.Bq = readD(); }



  
  protected void runImpl() {
    ItemInfo itemInfo;
    if ((itemInfo = ItemInfoCache.getInstance().get(this.Bq)) == null) {
      sendPacket(ActionFail.STATIC);
    } else {
      sendPacket(new ExRpItemLink(itemInfo));
    } 
  }
}
