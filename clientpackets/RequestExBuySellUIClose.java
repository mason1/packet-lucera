package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;





public class RequestExBuySellUIClose
  extends L2GameClientPacket
{
  protected void runImpl() {}
  
  protected void readImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    player.setBuyListId(0);
    player.sendItemList(true);
  }
}
