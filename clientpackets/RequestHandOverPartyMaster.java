package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.Party;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;


public class RequestHandOverPartyMaster
  extends L2GameClientPacket
{
  private String _name;
  
  protected void readImpl() { this._name = readS(16); }



  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    Party party = player1.getParty();
    
    if (party == null || !player1.getParty().isLeader(player1)) {
      
      player1.sendActionFailed();
      
      return;
    } 
    Player player2 = party.getPlayerByName(this._name);
    
    if (player2 == player1) {
      
      player1.sendPacket(Msg.YOU_CANNOT_TRANSFER_RIGHTS_TO_YOURSELF);
      
      return;
    } 
    if (player2 == null) {
      
      player1.sendPacket(Msg.YOU_CAN_TRANSFER_RIGHTS_ONLY_TO_ANOTHER_PARTY_MEMBER);
      
      return;
    } 
    player1.getParty().changePartyLeader(player2);
  }
}
