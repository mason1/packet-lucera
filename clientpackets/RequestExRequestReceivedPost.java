package l2.gameserver.network.l2.c2s;

import l2.commons.dao.JdbcEntityState;
import l2.gameserver.dao.MailDAO;
import l2.gameserver.model.Player;
import l2.gameserver.model.mail.Mail;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExChangePostState;
import l2.gameserver.network.l2.s2c.ExReplyReceivedPost;
import l2.gameserver.network.l2.s2c.ExShowReceivedPostList;









public class RequestExRequestReceivedPost
  extends L2GameClientPacket
{
  private int YI;
  
  protected void readImpl() { this.YI = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Mail mail = MailDAO.getInstance().getReceivedMailByMailId(player.getObjectId(), this.YI);
    if (mail != null) {
      
      if (mail.isUnread()) {
        
        mail.setUnread(false);
        mail.setJdbcState(JdbcEntityState.UPDATED);
        mail.update();
        player.sendPacket(new ExChangePostState(true, 1, new Mail[] { mail }));
      } 
      
      player.sendPacket(new ExReplyReceivedPost(mail));
      
      return;
    } 
    player.sendPacket(new ExShowReceivedPostList(player));
  }
}
