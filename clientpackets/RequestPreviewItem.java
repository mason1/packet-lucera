package l2.gameserver.network.l2.c2s;

import java.util.HashMap;
import l2.commons.threading.RunnableImpl;
import l2.gameserver.Config;
import l2.gameserver.ThreadPoolManager;
import l2.gameserver.cache.Msg;
import l2.gameserver.data.xml.holder.BuyListHolder;
import l2.gameserver.data.xml.holder.ItemHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.model.items.Inventory;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ShopPreviewInfo;
import l2.gameserver.network.l2.s2c.ShopPreviewList;
import l2.gameserver.templates.item.ItemTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;







public class RequestPreviewItem
  extends L2GameClientPacket
{
  private static final Logger _log = LoggerFactory.getLogger(RequestPreviewItem.class);
  
  private int ZD;
  
  private int vU;
  
  private int _count;
  
  private int[] Yd;
  
  protected void readImpl() {
    this.ZD = readD();
    this.vU = readD();
    this._count = readD();
    if (this._count * 4 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      return;
    } 
    this.Yd = new int[this._count];
    for (byte b = 0; b < this._count; b++) {
      this.Yd[b] = readD();
    }
  }

  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0) {
      return;
    }
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && player.getKarma() > 0 && !player.isGM()) {
      
      player.sendActionFailed();
      
      return;
    } 
    NpcInstance npcInstance = player.getLastNpc();
    boolean bool1 = (npcInstance != null && npcInstance.isMerchantNpc()) ? 1 : 0;
    if (!player.isGM() && (npcInstance == null || !bool1 || !npcInstance.isInActingRange(player))) {
      
      player.sendActionFailed();
      
      return;
    } 
    BuyListHolder.NpcTradeList npcTradeList = BuyListHolder.getInstance().getBuyList(this.vU);
    if (npcTradeList == null) {

      
      player.sendActionFailed();
      
      return;
    } 
    boolean bool2 = false;
    long l = 0L;
    
    HashMap hashMap = new HashMap();
    
    try {
      for (byte b = 0; b < this._count; b++) {
        
        int i = this.Yd[b];
        if (npcTradeList.getItemByItemId(i) == null) {
          
          player.sendActionFailed();
          
          return;
        } 
        ItemTemplate itemTemplate = ItemHolder.getInstance().getTemplate(i);
        if (itemTemplate != null)
        {
          
          if (itemTemplate.isEquipable()) {

            
            int j = Inventory.getPaperdollIndex(itemTemplate.getBodyPart());
            if (j >= 0) {


              
              if (hashMap.containsKey(Integer.valueOf(j))) {
                
                player.sendPacket(Msg.THOSE_ITEMS_MAY_NOT_BE_TRIED_ON_SIMULTANEOUSLY);
                
                return;
              } 
              hashMap.put(Integer.valueOf(j), Integer.valueOf(i));
              
              l += ShopPreviewList.getWearPrice(itemTemplate);
            } 
          }  } 
      }  if (!player.reduceAdena(l)) {
        
        player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
        
        return;
      } 
    } catch (ArithmeticException arithmeticException) {

      
      sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);
      
      return;
    } 
    if (!hashMap.isEmpty()) {
      
      player.sendPacket(new ShopPreviewInfo(hashMap));
      
      ThreadPoolManager.getInstance().schedule(new RemoveWearItemsTask(player), (Config.WEAR_DELAY * 1000));
    } 
  }

  
  private static class RemoveWearItemsTask
    extends RunnableImpl
  {
    private Player zm;
    
    public RemoveWearItemsTask(Player param1Player) { this.zm = param1Player; }


    
    public void runImpl() {
      this.zm.sendPacket(Msg.TRYING_ON_MODE_HAS_ENDED);
      this.zm.sendUserInfo(true);
    }
  }
}
