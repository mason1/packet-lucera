package l2.gameserver.network.l2.c2s;

import l2.commons.math.SafeMath;
import l2.commons.util.Rnd;
import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.data.xml.holder.ItemHolder;
import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.model.GameObject;
import l2.gameserver.model.Manor;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.model.instances.ManorManagerInstance;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.network.l2.s2c.SystemMessage2;
import l2.gameserver.templates.item.ItemTemplate;
import l2.gameserver.templates.manor.CropProcure;
import org.apache.commons.lang3.ArrayUtils;












public class RequestProcureCropList
  extends L2GameClientPacket
{
  private int _count;
  private int[] Yd;
  private int[] ZI;
  private int[] ZJ;
  private long[] Ye;
  
  protected void readImpl() {
    this._count = readD();
    if (this._count * 16 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      return;
    } 
    this.Yd = new int[this._count];
    this.ZI = new int[this._count];
    this.ZJ = new int[this._count];
    this.Ye = new long[this._count];
    for (byte b = 0; b < this._count; b++) {
      
      this.Yd[b] = readD();
      this.ZI[b] = readD();
      this.ZJ[b] = readD();
      this.Ye[b] = readD();
      if (this.ZI[b] < 1 || this.ZJ[b] < 1 || this.Ye[b] < 1L || ArrayUtils.indexOf(this.Yd, this.Yd[b]) < b) {
        
        this._count = 0;
        return;
      } 
    } 
  }


  
  protected void runImpl() {
    player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0) {
      return;
    }
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && player.getKarma() > 0 && !player.isGM()) {
      
      player.sendActionFailed();
      
      return;
    } 
    GameObject gameObject = player.getTarget();
    
    ManorManagerInstance manorManagerInstance = (gameObject != null && gameObject instanceof ManorManagerInstance) ? (ManorManagerInstance)gameObject : null;
    if (!player.isGM() && (manorManagerInstance == null || !manorManagerInstance.isInActingRange(player))) {
      
      player.sendActionFailed();
      
      return;
    } 
    boolean bool = (manorManagerInstance == null) ? 0 : manorManagerInstance.getCastle().getId();
    
    long l1 = 0L;
    byte b = 0;
    long l2 = 0L;

    
    try {
      for (byte b1 = 0; b1 < this._count; b1++) {
        
        int i = this.Yd[b1];
        int j = this.ZI[b1];
        int k = this.ZJ[b1];
        long l3 = this.Ye[b1];
        
        ItemInstance itemInstance = player.getInventory().getItemByObjectId(i);
        if (itemInstance == null || itemInstance.getCount() < l3 || itemInstance.getItemId() != j) {
          return;
        }
        Castle castle = (Castle)ResidenceHolder.getInstance().getResidence(Castle.class, k);
        if (castle == null) {
          return;
        }
        CropProcure cropProcure = castle.getCrop(j, 0);
        if (cropProcure == null || cropProcure.getId() == 0 || cropProcure.getPrice() == 0L) {
          return;
        }
        if (l3 > cropProcure.getAmount()) {
          return;
        }
        long l4 = SafeMath.mulAndCheck(l3, cropProcure.getPrice());
        long l5 = 0L;
        if (bool && k != bool) {
          l5 = l4 * 5L / 100L;
        }
        l1 = SafeMath.addAndCheck(l1, l5);
        
        int m = Manor.getInstance().getRewardItem(j, cropProcure.getReward());
        
        ItemTemplate itemTemplate = ItemHolder.getInstance().getTemplate(m);
        if (itemTemplate == null) {
          return;
        }
        l2 = SafeMath.addAndCheck(l2, SafeMath.mulAndCheck(l3, itemTemplate.getWeight()));
        if (!itemTemplate.isStackable() || player.getInventory().getItemByItemId(j) == null) {
          b++;
        }
      } 
    } catch (ArithmeticException arithmeticException) {

      
      sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);
      
      return;
    } 
    player.getInventory().writeLock();

    
    try { if (!player.getInventory().validateWeight(l2)) {
        
        sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
        
        return;
      } 
      if (!player.getInventory().validateCapacity(b)) {
        
        sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
        
        return;
      } 
      if (player.getInventory().getAdena() < l1) {
        
        player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
        
        return;
      } 
      
      for (byte b1 = 0; b1 < this._count; b1++) {
        
        int i = this.Yd[b1];
        int j = this.ZI[b1];
        int k = this.ZJ[b1];
        long l = this.Ye[b1];

        
        ItemInstance itemInstance = player.getInventory().getItemByObjectId(i);
        if (itemInstance != null && itemInstance.getCount() >= l && itemInstance.getItemId() == j) {

          
          Castle castle = (Castle)ResidenceHolder.getInstance().getResidence(Castle.class, k);
          if (castle != null) {

            
            CropProcure cropProcure = castle.getCrop(j, 0);
            if (cropProcure != null && cropProcure.getId() != 0 && cropProcure.getPrice() != 0L)
            {
              
              if (l <= cropProcure.getAmount()) {

                
                int m = Manor.getInstance().getRewardItem(j, cropProcure.getReward());
                long l3 = l * cropProcure.getPrice();
                long l4 = ItemHolder.getInstance().getTemplate(m).getReferencePrice();
                
                if (l4 != 0L)
                
                { 
                  double d = l3 / l4;
                  long l5 = (long)d + ((Rnd.nextDouble() <= d % 1.0D) ? true : false);
                  
                  if (l5 < 1L)
                  
                  { SystemMessage systemMessage = new SystemMessage(1491);
                    systemMessage.addItemName(j);
                    systemMessage.addNumber(l);
                    player.sendPacket(systemMessage); }
                  
                  else
                  
                  { long l6 = 0L;
                    if (bool && k != bool) {
                      l6 = l3 * 5L / 100L;
                    }
                    if (player.getInventory().destroyItemByObjectId(i, l))
                    {
                      
                      if (!player.reduceAdena(l6, false))
                      
                      { SystemMessage systemMessage = new SystemMessage(1491);
                        systemMessage.addItemName(j);
                        systemMessage.addNumber(l);
                        player.sendPacket(new IStaticPacket[] { systemMessage, Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA }); }
                      
                      else
                      
                      { cropProcure.setAmount(cropProcure.getAmount() - l);
                        castle.updateCrop(cropProcure.getId(), cropProcure.getAmount(), 0);
                        castle.addToTreasuryNoTax(l6, false, false);
                        
                        if (player.getInventory().addItem(m, l5) != null)
                        
                        { 
                          player.sendPacket(new IStaticPacket[] { (new SystemMessage(1490)).addItemName(j).addNumber(l), SystemMessage2.removeItems(j, l), SystemMessage2.obtainItems(m, l5, 0) });
                          if (l6 > 0L)
                            player.sendPacket((new SystemMessage(1607)).addNumber(l6));  }  }  }  }  } 
              }  } 
          } 
        } 
      }  }
    finally { player.getInventory().writeUnlock(); }

    
    player.sendChanges();
  }
}
