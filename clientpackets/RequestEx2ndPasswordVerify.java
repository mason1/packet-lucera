package l2.gameserver.network.l2.c2s;

import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.SecondPasswordAuth;
import l2.gameserver.network.l2.s2c.Ex2ndPasswordCheck;
import l2.gameserver.network.l2.s2c.Ex2ndPasswordVerify;







public class RequestEx2ndPasswordVerify
  extends L2GameClientPacket
{
  private String YC;
  
  protected void readImpl() { this.YC = readS(8); }



  
  protected void runImpl() {
    GameClient gameClient = (GameClient)getClient();
    SecondPasswordAuth secondPasswordAuth = gameClient.getSecondPasswordAuth();
    if (secondPasswordAuth == null) {
      
      gameClient.sendPacket(new Ex2ndPasswordVerify(Ex2ndPasswordVerify.Ex2ndPasswordVerifyResult.ERROR));
      return;
    } 
    if (!secondPasswordAuth.isSecondPasswordSet()) {
      
      gameClient.sendPacket(new Ex2ndPasswordCheck(Ex2ndPasswordCheck.Ex2ndPasswordCheckResult.CREATE));
      return;
    } 
    if (secondPasswordAuth.isValidSecondPassword(this.YC)) {
      
      gameClient.sendPacket(new Ex2ndPasswordVerify(Ex2ndPasswordVerify.Ex2ndPasswordVerifyResult.SUCCESS));
      gameClient.setSecondPasswordAuthed(true);

    
    }
    else if (secondPasswordAuth.isBlocked()) {
      gameClient.sendPacket(new Ex2ndPasswordVerify(Ex2ndPasswordVerify.Ex2ndPasswordVerifyResult.BLOCK_HOMEPAGE));
    } else {
      gameClient.sendPacket(new Ex2ndPasswordVerify(Ex2ndPasswordVerify.Ex2ndPasswordVerifyResult.FAILED, secondPasswordAuth.getTrysCount()));
    } 
  }
}
