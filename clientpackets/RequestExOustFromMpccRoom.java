package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.GameObjectsStorage;
import l2.gameserver.model.Player;
import l2.gameserver.model.matching.MatchingRoom;
import l2.gameserver.network.l2.GameClient;





public class RequestExOustFromMpccRoom
  extends L2GameClientPacket
{
  private int Bq;
  
  protected void readImpl() { this.Bq = readD(); }



  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    MatchingRoom matchingRoom = player1.getMatchingRoom();
    if (matchingRoom == null || matchingRoom.getType() != MatchingRoom.CC_MATCHING) {
      return;
    }
    if (matchingRoom.getLeader() != player1) {
      return;
    }
    Player player2 = GameObjectsStorage.getPlayer(this.Bq);
    if (player2 == null) {
      return;
    }
    if (player2 == matchingRoom.getLeader()) {
      return;
    }
    matchingRoom.removeMember(player2, true);
  }
}
