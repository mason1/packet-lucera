package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.CommandChannel;
import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.network.l2.s2c.SystemMessage2;





public class RequestExMPCCAcceptJoin
  extends L2GameClientPacket
{
  private int pI;
  private int YQ;
  
  protected void readImpl() {
    this.pI = this._buf.hasRemaining() ? readD() : 0;
    this.YQ = this._buf.hasRemaining() ? readD() : 0;
  }


  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    request = player1.getRequest();
    if (request == null || !request.isTypeOf(Request.L2RequestType.CHANNEL)) {
      return;
    }
    if (!request.isInProgress()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isOutOfControl()) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    Player player2 = request.getRequestor();
    if (player2 == null) {
      
      request.cancel();
      player1.sendPacket(Msg.THAT_PLAYER_IS_NOT_ONLINE);
      player1.sendActionFailed();
      
      return;
    } 
    if (player2.getRequest() != request) {
      
      request.cancel();
      player1.sendActionFailed();
      
      return;
    } 
    if (this.pI == 0) {
      
      request.cancel();
      player2.sendPacket((new SystemMessage(1680)).addString(player1.getName()));
      
      return;
    } 
    if (!player2.isInParty() || !player1.isInParty() || player1.getParty().isInCommandChannel()) {
      
      request.cancel();
      player2.sendPacket(Msg.NO_USER_HAS_BEEN_INVITED_TO_THE_COMMAND_CHANNEL);
      
      return;
    } 
    if (player1.isTeleporting()) {
      
      request.cancel();
      player1.sendPacket(Msg.YOU_CANNOT_JOIN_A_COMMAND_CHANNEL_WHILE_TELEPORTING);
      player2.sendPacket(Msg.NO_USER_HAS_BEEN_INVITED_TO_THE_COMMAND_CHANNEL);
      
      return;
    } 
    if (player2.getParty().isInCommandChannel()) {
      
      CommandChannel commandChannel = player2.getParty().getCommandChannel();
      if (commandChannel.getParties().size() >= Config.COMMAND_CHANNEL_PARY_COUNT_LIMIT) {
        
        player2.sendPacket(SystemMsg.THE_COMMAND_CHANNEL_IS_FULL);
        request.cancel();
        
        return;
      } 
    } 
    
    if (player2.getParty().isInCommandChannel()) {
      
      try
      {
        player2.getParty().getCommandChannel().addParty(player1.getParty());
      }
      finally
      {
        request.done();
      }
    
    } else if (CommandChannel.checkAuthority(player2)) {

      
      boolean bool = (player2.getSkillLevel(Integer.valueOf(391)) > 0) ? 1 : 0;
      boolean bool1 = false;
      if (!bool)
      {
        bool1 = (player2.getInventory().getCountOf(8871) > 1L);
      }
      
      if (!bool && !bool1) {
        
        player2.sendPacket(Msg.YOU_DO_NOT_HAVE_AUTHORITY_TO_USE_THE_COMMAND_CHANNEL);

        
        return;
      } 
      
      try {
        if (!bool)
        {
          if (bool1 = player2.getInventory().destroyItemByItemId(8871, 1L)) {
            player2.sendPacket(SystemMessage2.removeItems(8871, 1L));
          }
        }
        if (!bool && !bool1) {
          return;
        }

        
        CommandChannel commandChannel = new CommandChannel(player2);
        player2.sendPacket(Msg.THE_COMMAND_CHANNEL_HAS_BEEN_FORMED);
        commandChannel.addParty(player1.getParty());
      }
      finally {
        
        request.done();
      } 
    } 
  }
}
