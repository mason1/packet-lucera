package l2.gameserver.network.l2.c2s;

import l2.commons.lang.ArrayUtils;
import l2.gameserver.Config;
import l2.gameserver.data.xml.holder.SkillAcquireHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.model.SkillLearn;
import l2.gameserver.model.base.AcquireType;
import l2.gameserver.model.base.ClassId;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.network.l2.s2c.SkillList;
import l2.gameserver.network.l2.s2c.SystemMessage2;
import l2.gameserver.tables.SkillTable;


public class RequestAquireSkill
  extends L2GameClientPacket
{
  private AcquireType XO;
  private int _id;
  private int _level;
  
  protected void readImpl() {
    this._id = readD();
    this._level = readD();
    this.XO = (AcquireType)ArrayUtils.valid(AcquireType.VALUES, readD());
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || player.getTransformation() != 0 || player.isCursedWeaponEquipped() || this.XO == null) {
      return;
    }
    NpcInstance npcInstance = player.getLastNpc();
    if (npcInstance == null || !npcInstance.isInActingRange(player)) {
      return;
    }
    Skill skill = SkillTable.getInstance().getInfo(this._id, this._level);
    if (skill == null) {
      return;
    }
    int i = player.getVarInt("AcquireSkillClassId", player.getClassId().getId());
    ClassId classId = (i >= 0 && i < ClassId.VALUES.length) ? ClassId.VALUES[i] : null;
    
    if (!SkillAcquireHolder.getInstance().isSkillPossible(player, classId, skill, this.XO)) {
      return;
    }
    SkillLearn skillLearn = SkillAcquireHolder.getInstance().getSkillLearn(player, classId, this._id, this._level, this.XO);
    
    if (skillLearn == null) {
      return;
    }
    if (!a(player, skillLearn)) {
      
      player.sendPacket(SystemMsg.YOU_DO_NOT_HAVE_THE_NECESSARY_MATERIALS_OR_PREREQUISITES_TO_LEARN_THIS_SKILL);
      
      return;
    } 
    switch (this.XO) {
      
      case NORMAL:
        a(player, skillLearn, skill);
        if (npcInstance != null)
          npcInstance.showSkillList(player, classId); 
        break;
      case FISHING:
        b(player, skillLearn, skill);
        if (npcInstance != null)
          NpcInstance.showFishingSkillList(player); 
        break;
      case CLAN:
        a(player, skillLearn, npcInstance, skill);
        break;
    } 
  }




  
  private static void a(Player paramPlayer, SkillLearn paramSkillLearn, Skill paramSkill) {
    int i = paramPlayer.getSkillLevel(Integer.valueOf(paramSkillLearn.getId()), 0);
    if (i != paramSkillLearn.getLevel() - 1) {
      return;
    }
    c(paramPlayer, paramSkillLearn, paramSkill);
  }

  
  private static void b(Player paramPlayer, SkillLearn paramSkillLearn, Skill paramSkill) {
    int i = paramPlayer.getSkillLevel(Integer.valueOf(paramSkillLearn.getId()), 0);
    if (i != paramSkillLearn.getLevel() - 1) {
      return;
    }
    d(paramPlayer, paramSkillLearn, paramSkill);
  }

  
  private static void c(Player paramPlayer, SkillLearn paramSkillLearn, Skill paramSkill) {
    if (paramPlayer.getSp() < paramSkillLearn.getCost()) {
      
      paramPlayer.sendPacket(SystemMsg.YOU_DO_NOT_HAVE_ENOUGH_SP_TO_LEARN_THIS_SKILL);
      
      return;
    } 
    if (!Config.ALT_DISABLE_SPELLBOOKS && paramSkillLearn.getItemId() > 0)
    {
      if (!paramPlayer.consumeItem(paramSkillLearn.getItemId(), paramSkillLearn.getItemCount())) {
        
        paramPlayer.sendPacket(SystemMsg.YOU_DO_NOT_HAVE_THE_NECESSARY_MATERIALS_OR_PREREQUISITES_TO_LEARN_THIS_SKILL);
        
        return;
      } 
    }
    paramPlayer.sendPacket((new SystemMessage2(SystemMsg.YOU_HAVE_EARNED_S1_SKILL)).addSkillName(paramSkill.getId(), paramSkill.getLevel()));
    
    paramPlayer.setSp(paramPlayer.getSp() - paramSkillLearn.getCost());
    paramPlayer.addSkill(paramSkill, true);
    paramPlayer.sendUserInfo();
    paramPlayer.updateStats();
    
    paramPlayer.sendPacket(new SkillList(paramPlayer));
    
    RequestExEnchantSkill.updateSkillShortcuts(paramPlayer, paramSkill.getId(), paramSkill.getLevel());
  }

  
  private static void d(Player paramPlayer, SkillLearn paramSkillLearn, Skill paramSkill) {
    if (paramPlayer.getSp() < paramSkillLearn.getCost()) {
      
      paramPlayer.sendPacket(SystemMsg.YOU_DO_NOT_HAVE_ENOUGH_SP_TO_LEARN_THIS_SKILL);
      
      return;
    } 
    if (paramSkillLearn.getItemId() > 0)
    {
      if (!paramPlayer.consumeItem(paramSkillLearn.getItemId(), paramSkillLearn.getItemCount())) {
        
        paramPlayer.sendPacket(SystemMsg.YOU_DO_NOT_HAVE_THE_NECESSARY_MATERIALS_OR_PREREQUISITES_TO_LEARN_THIS_SKILL);
        
        return;
      } 
    }
    paramPlayer.sendPacket((new SystemMessage2(SystemMsg.YOU_HAVE_EARNED_S1_SKILL)).addSkillName(paramSkill.getId(), paramSkill.getLevel()));
    
    paramPlayer.setSp(paramPlayer.getSp() - paramSkillLearn.getCost());
    paramPlayer.addSkill(paramSkill, true);
    paramPlayer.sendUserInfo();
    paramPlayer.updateStats();
    
    paramPlayer.sendPacket(new SkillList(paramPlayer));
    
    RequestExEnchantSkill.updateSkillShortcuts(paramPlayer, paramSkill.getId(), paramSkill.getLevel());
  }

  
  private static void a(Player paramPlayer, SkillLearn paramSkillLearn, NpcInstance paramNpcInstance, Skill paramSkill) {
    if (!(paramNpcInstance instanceof l2.gameserver.model.instances.VillageMasterInstance)) {
      return;
    }
    if (!paramPlayer.isClanLeader()) {
      
      paramPlayer.sendPacket(SystemMsg.ONLY_THE_CLAN_LEADER_IS_ENABLED);
      
      return;
    } 
    Clan clan = paramPlayer.getClan();
    int i = clan.getSkillLevel(paramSkillLearn.getId(), 0);
    if (i != paramSkillLearn.getLevel() - 1)
      return; 
    if (clan.getReputationScore() < paramSkillLearn.getCost()) {
      
      paramPlayer.sendPacket(SystemMsg.THE_CLAN_REPUTATION_SCORE_IS_TOO_LOW);
      
      return;
    } 
    if (paramSkillLearn.getItemId() != 0 && !paramPlayer.consumeItem(paramSkillLearn.getItemId(), paramSkillLearn.getItemCount())) {
      
      paramPlayer.sendPacket(SystemMsg.YOU_DO_NOT_HAVE_THE_NECESSARY_MATERIALS_OR_PREREQUISITES_TO_LEARN_THIS_SKILL);
      
      return;
    } 
    clan.incReputation(-paramSkillLearn.getCost(), false, "AquireSkill: " + paramSkillLearn.getId() + ", lvl " + paramSkillLearn.getLevel());
    clan.addSkill(paramSkill, true);
    clan.broadcastToOnlineMembers(new L2GameServerPacket[] { (new SystemMessage2(SystemMsg.THE_CLAN_SKILL_S1_HAS_BEEN_ADDED)).addSkillName(paramSkill) });
    
    NpcInstance.showClanSkillList(paramPlayer);
  }

  
  private static boolean a(Player paramPlayer, SkillLearn paramSkillLearn) {
    if (Config.ALT_DISABLE_SPELLBOOKS) {
      return true;
    }
    if (paramSkillLearn.getItemId() == 0) {
      return true;
    }
    
    if (paramSkillLearn.isClicked()) {
      return false;
    }
    return (paramPlayer.getInventory().getCountOf(paramSkillLearn.getItemId()) >= paramSkillLearn.getItemCount());
  }
}
