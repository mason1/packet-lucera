package l2.gameserver.network.l2.c2s;

import java.util.Map;
import l2.commons.util.Rnd;
import l2.gameserver.cache.Msg;
import l2.gameserver.data.xml.holder.EnchantSkillHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.model.actor.instances.player.ShortCut;
import l2.gameserver.model.base.Experience;
import l2.gameserver.model.instances.TrainerInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExEnchantSkillList;
import l2.gameserver.network.l2.s2c.ShortCutRegister;
import l2.gameserver.network.l2.s2c.SkillList;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.scripts.Functions;
import l2.gameserver.skills.TimeStamp;
import l2.gameserver.tables.SkillTable;
import l2.gameserver.templates.SkillEnchant;
import l2.gameserver.utils.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;







public class RequestExEnchantSkill
  extends L2GameClientPacket
{
  private static final Logger LOG = LoggerFactory.getLogger(RequestExEnchantSkill.class);
  
  private int _skillId;
  
  private int YK;
  
  protected void readImpl() {
    this._skillId = readD();
    this.YK = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }

    
    if (player.getClassId().getLevel() < 4 || player.getLevel() < 76) {
      
      player.sendPacket(new SystemMessage(1438));
      
      return;
    } 
    Skill skill1 = player.getKnownSkill(this._skillId);
    if (skill1 == null) {
      
      player.sendPacket(new SystemMessage(1438));
      return;
    } 
    int i = skill1.getLevel();
    int j = skill1.getBaseLevel();
    Map map = EnchantSkillHolder.getInstance().getLevelsOf(this._skillId);
    
    if (map == null || map.isEmpty()) {
      
      player.sendPacket(new SystemMessage(1438));
      
      return;
    } 
    SkillEnchant skillEnchant1 = (SkillEnchant)map.get(Integer.valueOf(i));
    SkillEnchant skillEnchant2 = (SkillEnchant)map.get(Integer.valueOf(this.YK));
    
    if (skillEnchant2 == null) {
      
      player.sendPacket(new SystemMessage(1438));
      
      return;
    } 
    if (skillEnchant1 != null) {
      
      if (skillEnchant1.getRouteId() != skillEnchant2.getRouteId() || skillEnchant2.getEnchantLevel() != skillEnchant1.getEnchantLevel() + 1) {
        
        player.sendPacket(new SystemMessage(1438));
        
        return;
      } 
    } else if (skillEnchant2.getEnchantLevel() != 1 || i != j) {
      
      player.sendPacket(new SystemMessage(1438));
      LOG.warn("Player \"" + player.toString() + "\" trying to use enchant  exploit" + skill1.toString() + " to " + this.YK + "(enchant level " + skillEnchant2.getEnchantLevel() + ")");
      
      return;
    } 
    int[] arrayOfInt = skillEnchant2.getChances();
    int k = Experience.LEVEL.length - arrayOfInt.length - 1;
    
    if (player.getLevel() < k) {
      
      sendPacket((new SystemMessage(607))
          .addNumber(k));
      
      return;
    } 
    if (player.getSp() < skillEnchant2.getSp()) {
      
      sendPacket(new SystemMessage(1443));
      
      return;
    } 
    if (player.getExp() < skillEnchant2.getExp()) {
      
      sendPacket(new SystemMessage(1444));
      
      return;
    } 
    if (skillEnchant2.getItemId() > 0 && skillEnchant2.getItemCount() > 0L)
    {
      if (Functions.removeItem(player, skillEnchant2.getItemId(), skillEnchant2.getItemCount()) < skillEnchant2.getItemCount()) {
        
        sendPacket(Msg.ITEMS_REQUIRED_FOR_SKILL_ENCHANT_ARE_INSUFFICIENT);
        
        return;
      } 
    }
    int m = Math.max(0, Math.min(player.getLevel() - k, arrayOfInt.length - 1));
    int n = arrayOfInt[m];
    
    player.addExpAndSp(-1L * skillEnchant2.getExp(), (-1 * skillEnchant2.getSp()));
    player.sendPacket((new SystemMessage(538)).addNumber(skillEnchant2.getSp()));
    player.sendPacket((new SystemMessage(539)).addNumber(skillEnchant2.getExp()));
    
    TimeStamp timeStamp = player.getSkillReuse(skill1);
    
    Skill skill2 = null;
    
    if (Rnd.chance(n)) {
      
      skill2 = SkillTable.getInstance().getInfo(skillEnchant2.getSkillId(), skillEnchant2.getSkillLevel());
      player.sendPacket((new SystemMessage(1440)).addSkillName(this._skillId, this.YK));
      Log.add(player.getName() + "|Successfully enchanted|" + this._skillId + "|to+" + this.YK + "|" + n, "enchant_skills");
    }
    else {
      
      skill2 = SkillTable.getInstance().getInfo(skill1.getId(), skill1.getBaseLevel());
      player.sendPacket((new SystemMessage(1441)).addSkillName(this._skillId, this.YK));
      Log.add(player.getName() + "|Failed to enchant|" + this._skillId + "|to+" + this.YK + "|" + n, "enchant_skills");
    } 
    if (timeStamp != null && timeStamp.hasNotPassed())
    {
      player.disableSkill(skill2, timeStamp.getReuseCurrent());
    }
    player.addSkill(skill2, true);
    player.sendPacket(new SkillList(player));
    updateSkillShortcuts(player, this._skillId, this.YK);
    player.sendPacket(ExEnchantSkillList.packetFor(player, (TrainerInstance)player.getLastNpc()));
  }

  
  protected static void updateSkillShortcuts(Player paramPlayer, int paramInt1, int paramInt2) {
    for (ShortCut shortCut : paramPlayer.getAllShortCuts()) {
      
      if (shortCut.getId() == paramInt1 && shortCut.getType() == 2) {
        
        ShortCut shortCut1 = new ShortCut(shortCut.getSlot(), shortCut.getPage(), shortCut.getType(), shortCut.getId(), paramInt2, 1);
        paramPlayer.sendPacket(new ShortCutRegister(paramPlayer, shortCut1));
        paramPlayer.registerShortCut(shortCut1);
      } 
    } 
  }
}
