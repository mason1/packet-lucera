package l2.gameserver.network.l2.c2s;

import l2.gameserver.data.xml.holder.RecipeHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.Recipe;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.RecipeBookItemList;


public class RequestRecipeItemDelete
  extends L2GameClientPacket
{
  private int UE;
  
  protected void readImpl() { this.UE = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.getPrivateStoreType() == 5) {
      
      player.sendActionFailed();
      
      return;
    } 
    Recipe recipe = RecipeHolder.getInstance().getRecipeById(this.UE);
    if (recipe == null) {
      
      player.sendActionFailed();
      
      return;
    } 
    player.unregisterRecipe(this.UE);
    player.sendPacket(new RecipeBookItemList(player, (recipe.getType() == Recipe.ERecipeType.ERT_DWARF)));
  }
}
