package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.matching.MatchingRoom;
import l2.gameserver.network.l2.GameClient;








public class RequestExDismissMpccRoom
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    MatchingRoom matchingRoom = player.getMatchingRoom();
    if (matchingRoom == null || matchingRoom.getType() != MatchingRoom.CC_MATCHING) {
      return;
    }
    if (matchingRoom.getLeader() != player) {
      return;
    }
    matchingRoom.disband();
  }
}
