package l2.gameserver.network.l2.c2s;

import java.util.ArrayList;
import l2.gameserver.instancemanager.CursedWeaponsManager;
import l2.gameserver.model.CursedWeapon;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExCursedWeaponLocation;
import l2.gameserver.utils.Location;






public class RequestCursedWeaponLocation
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    ArrayList arrayList = new ArrayList();
    for (CursedWeapon cursedWeapon : CursedWeaponsManager.getInstance().getCursedWeapons()) {
      
      Location location = cursedWeapon.getWorldPosition();
      if (location != null) {
        arrayList.add(new ExCursedWeaponLocation.CursedWeaponInfo(location, cursedWeapon.getItemId(), cursedWeapon.isActivated() ? 1 : 0));
      }
    } 
    player.sendPacket(new ExCursedWeaponLocation(arrayList));
  }
}
