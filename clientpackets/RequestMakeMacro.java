package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.actor.instances.player.Macro;
import l2.gameserver.network.l2.GameClient;























public class RequestMakeMacro
  extends L2GameClientPacket
{
  private Macro Zo;
  
  protected void readImpl() {
    int i = readD();
    String str1 = readS(32);
    String str2 = readS(64);
    String str3 = readS(4);
    int j = readD();
    int k = readC();
    if (k > 12)
      k = 12; 
    Macro.L2MacroCmd[] arrayOfL2MacroCmd = new Macro.L2MacroCmd[k];
    for (byte b = 0; b < k; b++) {
      
      int m = readC();
      int n = readC();
      int i1 = readD();
      int i2 = readC();
      String str = readS().replace(";", "").replace(",", "");
      arrayOfL2MacroCmd[b] = new Macro.L2MacroCmd(m, n, i1, i2, str);
    } 
    this.Zo = new Macro(i, j, str1, str2, str3, arrayOfL2MacroCmd);
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.getMacroses().getAllMacroses().length > 48) {
      
      player.sendPacket(Msg.YOU_MAY_CREATE_UP_TO_48_MACROS);
      
      return;
    } 
    if (this.Zo.name.length() == 0) {
      
      player.sendPacket(Msg.ENTER_THE_NAME_OF_THE_MACRO);
      
      return;
    } 
    if (this.Zo.descr.length() > 32) {
      
      player.sendPacket(Msg.MACRO_DESCRIPTIONS_MAY_CONTAIN_UP_TO_32_CHARACTERS);
      
      return;
    } 
    player.registerMacro(this.Zo);
  }
}
