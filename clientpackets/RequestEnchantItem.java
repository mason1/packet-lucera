package l2.gameserver.network.l2.c2s;

import l2.commons.util.Rnd;
import l2.gameserver.Config;
import l2.gameserver.data.xml.holder.EnchantItemHolder;
import l2.gameserver.data.xml.holder.OneDayRewardHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.PcInventory;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.EnchantResult;
import l2.gameserver.network.l2.s2c.InventoryUpdate;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.network.l2.s2c.MagicSkillUse;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.templates.item.support.EnchantScroll;
import l2.gameserver.templates.item.support.EnchantScrollOnFailAction;
import l2.gameserver.utils.ItemFunctions;
import l2.gameserver.utils.Log;






public class RequestEnchantItem
  extends L2GameClientPacket
{
  private int Bq;
  
  protected void readImpl() { this.Bq = readD(); }



  
  protected void runImpl() {
    GameClient gameClient = (GameClient)getClient();
    if (gameClient == null) {
      return;
    }
    
    player = gameClient.getActiveChar();
    if (player == null) {
      return;
    }

    
    if (player.isActionsDisabled()) {
      
      player.setEnchantScroll(null);
      player.sendActionFailed();
      
      return;
    } 
    long l = System.currentTimeMillis();
    if (l - gameClient.getLastIncomePacketTimeStamp(RequestEnchantItem.class) < Config.ENCHANT_PACKET_DELAY) {
      
      player.setEnchantScroll(null);
      player.sendActionFailed();
      
      return;
    } 
    gameClient.setLastIncomePacketTimeStamp(RequestEnchantItem.class, l);
    
    if (player.isInTrade()) {
      
      player.setEnchantScroll(null);
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.setEnchantScroll(null);
      player.sendPacket(EnchantResult.CANCEL);
      player.sendPacket(SystemMsg.YOU_CANNOT_ENCHANT_WHILE_OPERATING_A_PRIVATE_STORE_OR_PRIVATE_WORKSHOP);
      player.sendActionFailed();
      
      return;
    } 
    pcInventory = player.getInventory();
    pcInventory.writeLock();
    
    try {
      ItemInstance itemInstance1 = pcInventory.getItemByObjectId(this.Bq);
      ItemInstance itemInstance2 = player.getEnchantScroll();
      
      if (itemInstance1 == null || itemInstance2 == null) {
        
        player.sendActionFailed();
        
        return;
      } 
      EnchantScroll enchantScroll = EnchantItemHolder.getInstance().getEnchantScroll(itemInstance2.getItemId());
      
      if (enchantScroll == null) {
        
        player.sendActionFailed();
        
        return;
      } 
      if (!itemInstance1.canBeEnchanted(false)) {
        
        player.sendPacket(EnchantResult.CANCEL);
        player.sendPacket(SystemMsg.INAPPROPRIATE_ENCHANT_CONDITIONS);
        player.sendActionFailed();
        
        return;
      } 
      if (!enchantScroll.isUsableWith(itemInstance1, null)) {
        
        player.sendPacket(EnchantResult.CANCEL);
        player.sendPacket(SystemMsg.DOES_NOT_FIT_STRENGTHENING_CONDITIONS_OF_THE_SCROLL);
        player.sendActionFailed();
        
        return;
      } 
      double d1 = 1.0D + enchantScroll.getChanceMod();
      int i = itemInstance1.getEnchantLevel() + enchantScroll.getIncrement();
      d1 *= player.getEnchantBonusMul();
      
      if (!pcInventory.destroyItem(itemInstance2, 1L)) {
        
        player.sendPacket(EnchantResult.CANCEL);
        player.sendPacket(SystemMsg.INAPPROPRIATE_ENCHANT_CONDITIONS);
        player.sendActionFailed();
        
        return;
      } 
      double d2 = ItemFunctions.getEnchantChance(itemInstance1);
      if (enchantScroll.isInfallible() || Rnd.chance(d2 * d1)) {
        
        b(player, itemInstance1, i);
      }
      else {
        
        switch (enchantScroll.getOnFailAction()) {
          
          case CRYSTALIZE:
            a(player, itemInstance1);
            break;
          case RESET:
            c(player, itemInstance1, enchantScroll.getFailResultLevel());
            break;
          case NONE:
            b(player, itemInstance1);
            break;
        } 

      
      } 
    } finally {
      pcInventory.writeUnlock();

      
      player.updateStats();
    } 
  }

  
  private void b(Player paramPlayer, ItemInstance paramItemInstance, int paramInt) {
    pcInventory = paramPlayer.getInventory();
    if (paramInt >= 65535) {
      
      Log.LogItem(paramPlayer, Log.ItemLog.EnchantFail, paramItemInstance);
      
      paramPlayer.sendPacket(new IStaticPacket[] { EnchantResult.CANCEL, SystemMsg.DOES_NOT_FIT_STRENGTHENING_CONDITIONS_OF_THE_SCROLL });
      paramPlayer.sendActionFailed();
      
      return;
    } 
    bool = paramItemInstance.isEquipped();
    i = paramItemInstance.getBodyPart();
    
    if (bool) {
      
      paramItemInstance.setEquipped(false);
      pcInventory.getListeners().onUnequip(i, paramItemInstance);
    } 
    
    try {
      paramItemInstance.setEnchantLevel(paramInt);
      Log.LogItem(paramPlayer, Log.ItemLog.EnchantSuccess, paramItemInstance);
    }
    finally {
      
      if (bool) {
        
        pcInventory.getListeners().onEquip(i, paramItemInstance);
        paramItemInstance.setEquipped(true);
      } 
      
      paramItemInstance.save();
    } 
    
    paramPlayer.sendPacket((new InventoryUpdate()).addModifiedItem(paramItemInstance));
    paramPlayer.sendPacket(new EnchantResult(paramItemInstance.getEnchantLevel()));
    
    if (Config.SHOW_ENCHANT_EFFECT_RESULT)
    {
      c(paramPlayer, paramItemInstance);
    }
    OneDayRewardHolder.getInstance().fireRequirements(paramPlayer, null, l2.gameserver.model.entity.oneDayReward.requirement.EnchantItemRequirement.class);
  }
  
  private void a(Player paramPlayer, ItemInstance paramItemInstance) {
    PcInventory pcInventory = paramPlayer.getInventory();
    boolean bool = paramItemInstance.isEquipped();
    int i = paramItemInstance.getItemId();
    int j = paramItemInstance.getEnchantLevel();
    int k = (paramItemInstance.getCrystalType()).cry;
    int m = paramItemInstance.getTemplate().getCrystalCount();
    
    if (bool) {
      
      paramPlayer.sendDisarmMessage(paramItemInstance);
      pcInventory.unEquipItem(paramItemInstance);
    } 
    
    Log.LogItem(paramPlayer, Log.ItemLog.EnchantFail, paramItemInstance);
    
    if (!pcInventory.destroyItem(paramItemInstance, 1L)) {
      
      paramPlayer.sendActionFailed();
      
      return;
    } 
    if (k > 0 && m > 0) {
      
      int n = (int)(m * 0.87D);
      if (j > 3)
      {
        n = (int)(n + m * 0.25D * (j - 3));
      }
      if (n < 1)
      {
        n = 1;
      }
      
      paramPlayer.sendPacket(new IStaticPacket[] { new EnchantResult(1, k, n), (new SystemMessage(65))
            
            .addNumber(j)
            .addItemName(i) });
      
      ItemFunctions.addItem(paramPlayer, k, n, true);
    }
    else {
      
      paramPlayer.sendPacket(new IStaticPacket[] { EnchantResult.FAILED_NO_CRYSTALS, (new SystemMessage(64))
            
            .addItemName(paramItemInstance.getItemId()) });
    } 
  }
  
  private void c(Player paramPlayer, ItemInstance paramItemInstance, int paramInt) {
    pcInventory = paramPlayer.getInventory();
    bool = paramItemInstance.isEquipped();
    i = paramItemInstance.getBodyPart();
    int j = Math.min(paramItemInstance.getEnchantLevel(), paramInt);
    
    if (bool) {
      
      paramItemInstance.setEquipped(false);
      pcInventory.getListeners().onUnequip(i, paramItemInstance);
    } 
    
    try {
      paramItemInstance.setEnchantLevel(j);
      Log.LogItem(paramPlayer, Log.ItemLog.EnchantFail, paramItemInstance);
    }
    finally {
      
      if (bool) {
        
        pcInventory.getListeners().onEquip(i, paramItemInstance);
        paramItemInstance.setEquipped(true);
      } 
      
      paramItemInstance.save();
    } 
    
    paramPlayer.sendPacket(new IStaticPacket[] { (new InventoryUpdate()).addModifiedItem(paramItemInstance), EnchantResult.BLESSED_FAILED, SystemMsg.THE_BLESSED_ENCHANT_FAILED });
  }



  
  private void b(Player paramPlayer, ItemInstance paramItemInstance) {
    Log.LogItem(paramPlayer, Log.ItemLog.EnchantFail, paramItemInstance);
    paramPlayer.sendPacket(new IStaticPacket[] { EnchantResult.ANCIENT_FAILED, SystemMsg.INAPPROPRIATE_ENCHANT_CONDITIONS });
  }

  
  private static final void c(Player paramPlayer, ItemInstance paramItemInstance) {
    if (paramItemInstance.getTemplate().getType2() == 0) {
      
      if (Config.SHOW_ENCHANT_EFFECT_RESULT_EVERY_NEXT_SUCCESS ? (paramItemInstance.getEnchantLevel() == Config.WEAPON_FIRST_ENCHANT_EFFECT_LEVEL || paramItemInstance.getEnchantLevel() >= Config.WEAPON_SECOND_ENCHANT_EFFECT_LEVEL) : (paramItemInstance.getEnchantLevel() == Config.WEAPON_FIRST_ENCHANT_EFFECT_LEVEL || paramItemInstance.getEnchantLevel() == Config.WEAPON_SECOND_ENCHANT_EFFECT_LEVEL))
      {
        paramPlayer.broadcastPacket(new L2GameServerPacket[] { new MagicSkillUse(paramPlayer, paramPlayer, 2025, 1, 500, 1500L) });
        paramPlayer.broadCastCustomMessage("_C1_HAS_SUCCESSFULLY_ENCHANTED_A_S2_S3", paramPlayer, new Object[] { paramPlayer, paramItemInstance, Integer.valueOf(paramItemInstance.getEnchantLevel()) });
      }
    
    } else if (Config.SHOW_ENCHANT_EFFECT_RESULT_EVERY_NEXT_SUCCESS ? (paramItemInstance.getEnchantLevel() >= Config.ARMOR_ENCHANT_EFFECT_LEVEL) : (paramItemInstance.getEnchantLevel() == Config.ARMOR_ENCHANT_EFFECT_LEVEL)) {
      
      paramPlayer.broadcastPacket(new L2GameServerPacket[] { new MagicSkillUse(paramPlayer, paramPlayer, 2025, 1, 500, 1500L) });
      paramPlayer.broadCastCustomMessage("_C1_HAS_SUCCESSFULLY_ENCHANTED_A_S2_S3", paramPlayer, new Object[] { paramPlayer, paramItemInstance, Integer.valueOf(paramItemInstance.getEnchantLevel()) });
    } 
  }
}
