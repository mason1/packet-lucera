package l2.gameserver.network.l2.c2s;

import java.util.List;
import l2.commons.util.Rnd;
import l2.gameserver.cache.Msg;
import l2.gameserver.data.xml.holder.RecipeHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.Recipe;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.ManufactureItem;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.s2c.RecipeShopItemInfo;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.network.l2.s2c.SystemMessage2;
import l2.gameserver.templates.item.ItemTemplate;
import l2.gameserver.utils.ItemFunctions;
import l2.gameserver.utils.TradeHelper;
import org.apache.commons.lang3.tuple.Pair;




public class RequestRecipeShopMakeDo
  extends L2GameClientPacket
{
  private int _manufacturerId;
  private int UE;
  private long _price;
  
  protected void readImpl() {
    this._manufacturerId = readD();
    this.UE = readD();
    this._price = readD();
  }


  
  protected void runImpl() {
    player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    if (player1.isActionsDisabled()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isInStoreMode()) {
      
      player1.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player1.isInTrade()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isFishing()) {
      
      player1.sendPacket(Msg.YOU_CANNOT_DO_ANYTHING_ELSE_WHILE_FISHING);
      
      return;
    } 
    if (!(player1.getPlayerAccess()).UseTrade) {
      
      player1.sendPacket(Msg.THIS_ACCOUNT_CANOT_USE_PRIVATE_STORES);
      
      return;
    } 
    Player player2 = (Player)player1.getVisibleObject(this._manufacturerId);
    if (player2 == null || player2.getPrivateStoreType() != 5 || !player2.isInActingRange(player1)) {
      
      player1.sendActionFailed();
      
      return;
    } 
    Recipe recipe = null;
    for (ManufactureItem manufactureItem : player2.getCreateList()) {
      
      if (manufactureItem.getRecipeId() == this.UE)
      {
        if (this._price == manufactureItem.getCost()) {
          
          recipe = RecipeHolder.getInstance().getRecipeById(this.UE);
          
          break;
        } 
      }
    } 
    if (recipe == null) {
      
      player1.sendActionFailed();
      
      return;
    } 
    byte b = 0;
    
    if (recipe.getProducts().isEmpty() || recipe.getMaterials().isEmpty()) {
      
      player2.sendMessage((new CustomMessage("l2p.gameserver.RecipeController.NoRecipe", player2, new Object[0])).addItemName(recipe.getItem()));
      player1.sendMessage((new CustomMessage("l2p.gameserver.RecipeController.NoRecipe", player2, new Object[0])).addItemName(recipe.getItem()));
      
      return;
    } 
    if (!player2.findRecipe(this.UE)) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player2.getCurrentMp() < recipe.getMpConsume()) {
      
      player2.sendPacket(Msg.NOT_ENOUGH_MP);
      player1.sendPacket(new IStaticPacket[] { Msg.NOT_ENOUGH_MP, new RecipeShopItemInfo(player1, player2, this.UE, this._price, b) });
      
      return;
    } 
    List list1 = recipe.getMaterials();
    List list2 = recipe.getProducts();
    
    player1.getInventory().writeLock();
    
    try {
      if (player1.getAdena() < this._price) {
        
        player1.sendPacket(new IStaticPacket[] { Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA, new RecipeShopItemInfo(player1, player2, this.UE, this._price, b) });
        
        return;
      } 
      for (Pair pair : list1) {
        
        ItemTemplate itemTemplate = (ItemTemplate)pair.getKey();
        long l = ((Long)pair.getValue()).longValue();
        if (l <= 0L) {
          continue;
        }
        
        ItemInstance itemInstance = player1.getInventory().getItemByItemId(itemTemplate.getItemId());
        if (itemInstance == null || itemInstance.getCount() < l) {
          
          player1.sendPacket(new IStaticPacket[] { Msg.NOT_ENOUGH_MATERIALS, new RecipeShopItemInfo(player1, player2, this.UE, this._price, b) });
          
          return;
        } 
      } 
      int i = 0;
      long l1 = 0L;
      for (Pair pair : list2) {
        
        i = (int)(i + ((ItemTemplate)pair.getKey()).getWeight() * ((Long)pair.getValue()).longValue());
        l1 += (((ItemTemplate)pair.getKey()).isStackable() ? 1L : ((Long)pair.getValue()).longValue());
      } 
      
      if (!player1.getInventory().validateWeight(i) || !player1.getInventory().validateCapacity(l1)) {
        
        player1.sendPacket(new IStaticPacket[] { Msg.THE_WEIGHT_AND_VOLUME_LIMIT_OF_INVENTORY_MUST_NOT_BE_EXCEEDED, new RecipeShopItemInfo(player1, player2, this.UE, this._price, b) });
        
        return;
      } 
      
      if (!player1.reduceAdena(this._price, false)) {
        
        player1.sendPacket(new IStaticPacket[] { Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA, new RecipeShopItemInfo(player1, player2, this.UE, this._price, b) });
        
        return;
      } 
      for (Pair pair : list1) {
        
        ItemTemplate itemTemplate = (ItemTemplate)pair.getKey();
        long l = ((Long)pair.getValue()).longValue();
        if (l <= 0L) {
          continue;
        }
        
        player1.getInventory().destroyItemByItemId(itemTemplate.getItemId(), l);
        
        player1.sendPacket(SystemMessage2.removeItems(itemTemplate.getItemId(), l));
      } 
      
      long l2 = TradeHelper.getTax(player2, this._price);
      if (l2 > 0L) {
        
        this._price -= l2;
        player2.sendMessage((new CustomMessage("trade.HavePaidTax", player2, new Object[0])).addNumber(l2));
      } 
      
      player2.addAdena(this._price);
    }
    finally {
      
      player1.getInventory().writeUnlock();
    } 
    
    for (Pair pair : list2)
    {
      player2.sendMessage((new CustomMessage("l2p.gameserver.RecipeController.GotOrder", player2, new Object[0])).addItemName((ItemTemplate)pair.getKey()));
    }
    
    player2.reduceCurrentMp(recipe.getMpConsume(), null);
    player2.sendStatusUpdate(false, false, new int[] { 11 });
    
    if (Rnd.chance(recipe.getSuccessRate())) {
      
      for (Pair pair : list2) {
        
        int i = ((ItemTemplate)pair.getKey()).getItemId();
        long l = ((Long)pair.getValue()).longValue();
        ItemFunctions.addItem(player1, i, l, true);
      } 
      b = 1;
    } 

    
    if (b == 0) {
      
      for (Pair pair : list2)
      {
        int i = ((ItemTemplate)pair.getKey()).getItemId();
        SystemMessage systemMessage = new SystemMessage(1150);
        systemMessage.addString(player2.getName());
        systemMessage.addItemName(i);
        systemMessage.addNumber(this._price);
        player1.sendPacket(systemMessage);
        
        systemMessage = new SystemMessage(1149);
        systemMessage.addString(player1.getName());
        systemMessage.addItemName(i);
        systemMessage.addNumber(this._price);
        player2.sendPacket(systemMessage);
      }
    
    } else {
      
      for (Pair pair : list2) {
        
        int i = ((ItemTemplate)pair.getKey()).getItemId();
        long l = ((Long)pair.getValue()).longValue();
        if (l > 1L) {
          
          SystemMessage systemMessage1 = new SystemMessage(1148);
          systemMessage1.addString(player2.getName());
          systemMessage1.addItemName(i);
          systemMessage1.addNumber(l);
          systemMessage1.addNumber(this._price);
          player1.sendPacket(systemMessage1);
          
          systemMessage1 = new SystemMessage(1152);
          systemMessage1.addString(player1.getName());
          systemMessage1.addItemName(i);
          systemMessage1.addNumber(l);
          systemMessage1.addNumber(this._price);
          player2.sendPacket(systemMessage1);
          
          continue;
        } 
        SystemMessage systemMessage = new SystemMessage(1146);
        systemMessage.addString(player2.getName());
        systemMessage.addItemName(i);
        systemMessage.addNumber(this._price);
        player1.sendPacket(systemMessage);
        
        systemMessage = new SystemMessage(1151);
        systemMessage.addString(player1.getName());
        systemMessage.addItemName(i);
        systemMessage.addNumber(this._price);
        player2.sendPacket(systemMessage);
      } 
    } 

    
    player1.sendChanges();
    player1.sendPacket(new RecipeShopItemInfo(player1, player2, this.UE, this._price, b));
  }
}
