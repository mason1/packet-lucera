package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.RecipeShopSellList;





public class RequestRecipeShopSellList
  extends L2GameClientPacket
{
  int _manufacturerId;
  
  protected void readImpl() { this._manufacturerId = readD(); }



  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    if (player1.isActionsDisabled()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    Player player2 = (Player)player1.getVisibleObject(this._manufacturerId);
    if (player2 == null || player2.getPrivateStoreType() != 5 || !player2.isInActingRange(player1)) {
      
      player1.sendActionFailed();
      
      return;
    } 
    player1.sendPacket(new RecipeShopSellList(player1, player2));
  }
}
