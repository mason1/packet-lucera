package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.PledgeReceiveWarList;


public class RequestPledgeWarList
  extends L2GameClientPacket
{
  private int _type;
  private int _page;
  
  protected void readImpl() {
    this._page = readD();
    this._type = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Clan clan = player.getClan();
    if (clan != null)
      player.sendPacket(new PledgeReceiveWarList(clan, this._type, this._page)); 
  }
}
