package l2.gameserver.network.l2.c2s;

import java.util.ArrayList;
import java.util.List;
import l2.commons.math.SafeMath;
import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.data.xml.holder.ItemHolder;
import l2.gameserver.data.xml.holder.MultiSellHolder;
import l2.gameserver.instancemanager.ReflectionManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.base.MultiSellEntry;
import l2.gameserver.model.base.MultiSellIngredient;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.model.items.ItemAttributes;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.PcInventory;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.network.l2.s2c.SystemMessage2;
import l2.gameserver.templates.item.ItemTemplate;
import l2.gameserver.utils.ItemFunctions;
import l2.gameserver.utils.Log;


public class RequestMultiSellChoose
  extends L2GameClientPacket
{
  private int vU;
  private int LE;
  private long Xf;
  
  private class ItemData
  {
    private final int _id;
    private final long Be;
    private final ItemInstance Ds;
    
    public ItemData(int param1Int, long param1Long, ItemInstance param1ItemInstance) {
      this._id = param1Int;
      this.Be = param1Long;
      this.Ds = param1ItemInstance;
    }


    
    public int getId() { return this._id; }



    
    public long getCount() { return this.Be; }



    
    public ItemInstance getItem() { return this.Ds; }



    
    public boolean equals(Object param1Object) {
      if (!(param1Object instanceof ItemData)) {
        return false;
      }
      ItemData itemData = (ItemData)param1Object;
      
      return (this._id == itemData._id && this.Be == itemData.Be && this.Ds == itemData.Ds);
    }
  }


  
  protected void readImpl() {
    this.vU = readD();
    this.LE = readD();
    this.Xf = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this.Xf < 1L) {
      return;
    }
    MultiSellHolder.MultiSellListContainer multiSellListContainer = player.getMultisell();
    if (multiSellListContainer == null) {
      
      player.sendActionFailed();
      player.setMultisell(null);
      
      return;
    } 
    
    if (multiSellListContainer.getListId() != this.vU) {
      
      Log.add("Player " + player.getName() + " trying to change multisell list id, ban this player!", "illegal-actions");
      player.sendActionFailed();
      player.setMultisell(null);
      
      return;
    } 
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player.isInTrade()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
      
      return;
    } 
    if (!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && player.getKarma() > 0 && !player.isGM()) {
      
      player.sendActionFailed();
      
      return;
    } 
    NpcInstance npcInstance = player.getLastNpc();
    if (multiSellListContainer.getListId() >= 0 && !player.isGM() && !NpcInstance.canBypassCheck(player, npcInstance)) {
      
      player.setMultisell(null);
      
      return;
    } 
    MultiSellEntry multiSellEntry = null;
    for (MultiSellEntry multiSellEntry1 : multiSellListContainer.getEntries()) {
      if (multiSellEntry1.getEntryId() == this.LE) {
        
        multiSellEntry = multiSellEntry1;
        break;
      } 
    } 
    if (multiSellEntry == null) {
      return;
    }
    boolean bool1 = multiSellListContainer.isKeepEnchant();
    boolean bool2 = multiSellListContainer.isNoTax();
    ArrayList arrayList = new ArrayList();
    
    pcInventory = player.getInventory();
    
    long l = 0L;

    
    Castle castle = (npcInstance != null) ? npcInstance.getCastle(player) : null;
    
    pcInventory.writeLock();
    
    try {
      long l1 = SafeMath.mulAndCheck(multiSellEntry.getTax(), this.Xf);
      
      long l2 = 0L;
      long l3 = 0L;
      for (MultiSellIngredient multiSellIngredient : multiSellEntry.getProduction()) {
        
        if (multiSellIngredient.getItemId() <= 0)
          continue; 
        ItemTemplate itemTemplate = ItemHolder.getInstance().getTemplate(multiSellIngredient.getItemId());
        
        l3 = SafeMath.addAndCheck(l3, SafeMath.mulAndCheck(SafeMath.mulAndCheck(multiSellIngredient.getItemCount(), this.Xf), itemTemplate.getWeight()));
        if (itemTemplate.isStackable()) {
          
          if (pcInventory.getItemByItemId(multiSellIngredient.getItemId()) == null)
            l2++; 
          continue;
        } 
        l2 = SafeMath.addAndCheck(l2, SafeMath.mulAndCheck(multiSellIngredient.getItemCount(), this.Xf));
      } 
      
      if (!pcInventory.validateWeight(l3)) {
        
        player.sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
        player.sendActionFailed();
        
        return;
      } 
      if (!pcInventory.validateCapacity(l2)) {
        
        player.sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
        player.sendActionFailed();
        
        return;
      } 
      if (multiSellEntry.getIngredients().size() == 0) {
        
        player.sendActionFailed();
        player.setMultisell(null);
        
        return;
      } 
      
      for (MultiSellIngredient multiSellIngredient : multiSellEntry.getIngredients()) {
        
        int m = multiSellIngredient.getItemId();
        long l4 = multiSellIngredient.getItemCount();
        int n = multiSellIngredient.getItemEnchant();
        long l5 = !multiSellIngredient.getMantainIngredient() ? SafeMath.mulAndCheck(l4, this.Xf) : l4;
        
        if (m == -200) {
          
          if (player.getClan() == null) {
            
            player.sendPacket(Msg.YOU_ARE_NOT_A_CLAN_MEMBER);
            
            return;
          } 
          if (player.getClan().getReputationScore() < l5) {
            
            player.sendPacket(Msg.THE_CLAN_REPUTATION_SCORE_IS_TOO_LOW);
            
            return;
          } 
          if (player.getClan().getLeaderId() != player.getObjectId()) {
            
            player.sendPacket((new SystemMessage(9)).addString(player.getName()));
            return;
          } 
          if (!multiSellIngredient.getMantainIngredient()) {
            arrayList.add(new ItemData(m, l5, null));
          }
        } else if (m == -100) {
          
          if (player.getPcBangPoints() < l5) {
            
            player.sendPacket(Msg.YOU_ARE_SHORT_OF_ACCUMULATED_POINTS);
            return;
          } 
          if (!multiSellIngredient.getMantainIngredient()) {
            arrayList.add(new ItemData(m, l5, null));
          }
        } else {
          
          ItemTemplate itemTemplate = ItemHolder.getInstance().getTemplate(m);
          
          if (!itemTemplate.isStackable()) {
            
            for (byte b = 0; b < l4 * this.Xf; b++) {
              
              List list = pcInventory.getItemsByItemId(m);
              
              ItemInstance itemInstance = null;
              if (bool1) {
                
                for (ItemInstance itemInstance1 : list) {
                  
                  ItemData itemData = new ItemData(itemInstance1.getItemId(), itemInstance1.getCount(), itemInstance1);
                  if ((itemInstance1.getEnchantLevel() == n || !itemInstance1.getTemplate().isEquipment()) && 
                    !arrayList.contains(itemData) && itemInstance1.canBeExchanged(player)) {
                    
                    itemInstance = itemInstance1;


                    
                    break;
                  } 
                } 
              } else {
                for (ItemInstance itemInstance1 : list) {
                  if (!arrayList.contains(new ItemData(itemInstance1.getItemId(), itemInstance1.getCount(), itemInstance1)) && (itemInstance == null || itemInstance1
                    .getEnchantLevel() < itemInstance.getEnchantLevel()) && 
                    !itemInstance1.isShadowItem() && !itemInstance1.isTemporalItem() && (
                    !itemInstance1.isAugmented() || Config.ALT_ALLOW_TRADE_AUGMENTED) && 
                    ItemFunctions.checkIfCanDiscard(player, itemInstance1)) {
                    
                    itemInstance = itemInstance1;
                    if (itemInstance.getEnchantLevel() == 0) {
                      break;
                    }
                  } 
                } 
              } 
              if (itemInstance == null || (this.Xf > 1L && itemInstance.isAugmented())) {
                
                player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_REQUIRED_ITEMS);
                
                return;
              } 
              if (!multiSellIngredient.getMantainIngredient()) {
                arrayList.add(new ItemData(itemInstance.getItemId(), 1L, itemInstance));
              }
            } 
          } else {
            
            if (m == 57)
              l = SafeMath.addAndCheck(l, SafeMath.mulAndCheck(l4, this.Xf)); 
            ItemInstance itemInstance = pcInventory.getItemByItemId(m);
            
            if (itemInstance == null || itemInstance.getCount() < l5) {
              
              player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_REQUIRED_ITEMS);
              
              return;
            } 
            if (!multiSellIngredient.getMantainIngredient()) {
              arrayList.add(new ItemData(itemInstance.getItemId(), l5, itemInstance));
            }
          } 
        } 
        if (player.getAdena() < l) {
          
          player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
          
          return;
        } 
      } 
      int i = 0;
      ItemAttributes itemAttributes = null;
      int j = 0;
      int k = 0;
      for (ItemData itemData : arrayList) {
        
        long l4 = itemData.getCount();
        if (l4 > 0L) {
          if (itemData.getId() == -200) {
            
            player.getClan().incReputation((int)-l4, false, "MultiSell");
            player.sendPacket((new SystemMessage(1787)).addNumber(l4)); continue;
          } 
          if (itemData.getId() == -100) {
            player.reducePcBangPoints((int)l4);
            continue;
          } 
          ItemInstance itemInstance = itemData.getItem();
          if (pcInventory.destroyItem(itemData.getItem(), l4)) {
            
            if (bool1 && itemData.getItem().canBeEnchanted(true)) {
              
              i = itemData.getItem().getEnchantLevel();
              itemAttributes = itemData.getItem().getAttributes();
              j = itemData.getItem().getVariationStat1();
              k = itemData.getItem().getVariationStat2();
            } 
            
            player.sendPacket(SystemMessage2.removeItems(itemData.getId(), l4));
            Log.LogItem(player, Log.ItemLog.MultiSellIngredient, itemInstance, l4, 0L, this.vU);
            
            continue;
          } 
          
          return;
        } 
      } 
      
      if (l1 > 0L && !bool2 && 
        castle != null) {
        
        player.sendMessage((new CustomMessage("trade.HavePaidTax", player, new Object[0])).addNumber(l1));
        if (npcInstance != null && npcInstance.getReflection() == ReflectionManager.DEFAULT) {
          castle.addToTreasury(l1, true, false);
        }
      } 
      for (MultiSellIngredient multiSellIngredient : multiSellEntry.getProduction()) {
        if (multiSellIngredient.getItemId() <= 0) {
          
          if (multiSellIngredient.getItemId() == -200) {
            
            player.getClan().incReputation((int)(multiSellIngredient.getItemCount() * this.Xf), false, "MultiSell");
            player.sendPacket((new SystemMessage(1781)).addNumber(multiSellIngredient.getItemCount() * this.Xf)); continue;
          } 
          if (multiSellIngredient.getItemId() == -100)
            player.addPcBangPoints((int)(multiSellIngredient.getItemCount() * this.Xf), false);  continue;
        } 
        if (ItemHolder.getInstance().getTemplate(multiSellIngredient.getItemId()).isStackable()) {
          
          long l4 = SafeMath.mulAndLimit(multiSellIngredient.getItemCount(), this.Xf);
          pcInventory.addItem(multiSellIngredient.getItemId(), l4);
          player.sendPacket(SystemMessage2.obtainItems(multiSellIngredient.getItemId(), l4, 0));
          Log.LogItem(player, Log.ItemLog.MultiSellProduct, multiSellIngredient.getItemId(), l4, 0L, this.vU);
          
          continue;
        } 
        for (byte b = 0; b < this.Xf; b++) {
          
          for (byte b1 = 0; b1 < multiSellIngredient.getItemCount(); b1++)
          {
            ItemInstance itemInstance = ItemFunctions.createItem(multiSellIngredient.getItemId());
            
            if (bool1) {
              
              if (itemInstance.canBeEnchanted(true)) {
                
                itemInstance.setEnchantLevel(i);
                if (itemAttributes != null)
                  itemInstance.setAttributes(itemAttributes.clone()); 
                if (j != 0 || k != 0)
                {
                  itemInstance.setVariationStat1(j);
                  itemInstance.setVariationStat2(k);
                }
              
              } 
            } else {
              
              itemInstance.setEnchantLevel(multiSellIngredient.getItemEnchant());
              itemInstance.setAttributes(multiSellIngredient.getItemAttributes().clone());
            } 

            
            pcInventory.addItem(itemInstance);
            player.sendPacket(SystemMessage2.obtainItems(itemInstance));
            Log.LogItem(player, Log.ItemLog.MultiSellProduct, itemInstance, itemInstance.getCount(), 0L, this.vU);
          }
        
        } 
      } 
    } catch (ArithmeticException arithmeticException) {

      
      sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);

      
      return;
    } finally {
      pcInventory.writeUnlock();
    } 
    
    player.sendChanges();
    
    if (!multiSellListContainer.isShowAll())
      MultiSellHolder.getInstance().SeparateAndSend(multiSellListContainer, player, (castle == null) ? 0.0D : castle.getTaxRate()); 
  }
}
