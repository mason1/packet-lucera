package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;





public class RequestShortCutDel
  extends L2GameClientPacket
{
  private int _slot;
  private int _page;
  
  protected void readImpl() {
    int i = readD();
    this._slot = i % 12;
    this._page = i / 12;
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    player.deleteShortCut(this._slot, this._page);
  }
}
