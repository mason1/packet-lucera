package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.instancemanager.games.FishingChampionShipManager;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;







public class RequestExFishRanking
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    if (Config.ALT_FISH_CHAMPIONSHIP_ENABLED)
      FishingChampionShipManager.getInstance().showMidResult(((GameClient)getClient()).getActiveChar()); 
  }
}
