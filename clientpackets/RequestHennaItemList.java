package l2.gameserver.network.lineage2.clientpackets;

import l2.gameserver.model.Player;
import l2.gameserver.network.lineage2.serverpackets.HennaEquipList;

public class RequestHennaItemList extends L2GameClientPacket {
    @Override
    protected void readImpl() {
    }

    @Override
    protected void runImpl() {
        final Player player = getClient().getActiveChar();
        if (player == null) {
            return;
        }
        player.sendPacket(new HennaEquipList(player));
    }
}
