package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.matching.MatchingRoom;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.components.SystemMsg;



public class RequestExManageMpccRoom
  extends L2GameClientPacket
{
  private int _id;
  private int YR;
  private int p;
  private int q;
  private String Vl;
  
  protected void readImpl() {
    this._id = readD();
    this.YR = readD();
    this.p = readD();
    this.q = readD();
    readD();
    this.Vl = readS();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    MatchingRoom matchingRoom = player.getMatchingRoom();
    if (matchingRoom == null || matchingRoom.getId() != this._id || matchingRoom.getType() != MatchingRoom.CC_MATCHING) {
      return;
    }
    if (matchingRoom.getLeader() != player) {
      return;
    }
    matchingRoom.setTopic(this.Vl);
    matchingRoom.setMaxMemberSize(this.YR);
    matchingRoom.setMinLevel(this.p);
    matchingRoom.setMaxLevel(this.q);
    matchingRoom.broadCast(new IStaticPacket[] { matchingRoom.infoRoomPacket() });
    
    player.sendPacket(SystemMsg.THE_COMMAND_CHANNEL_MATCHING_ROOM_INFORMATION_WAS_EDITED);
  }
}
