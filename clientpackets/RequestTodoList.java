package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExOneDayReceiveRewardList;






public class RequestTodoList
  extends L2GameClientPacket
{
  private int aad;
  
  protected void readImpl() { this.aad = readC(); }




  
  protected void runImpl() {
    if (!Config.EX_ONE_DAY_REWARD) {
      return;
    }
    
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }



    
    switch (this.aad) {
      
      case 9:
        player.sendPacket(new ExOneDayReceiveRewardList(player));
        break;
    } 
  }
}
