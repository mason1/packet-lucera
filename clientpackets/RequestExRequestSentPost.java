package l2.gameserver.network.l2.c2s;

import l2.gameserver.dao.MailDAO;
import l2.gameserver.model.Player;
import l2.gameserver.model.mail.Mail;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExReplySentPost;
import l2.gameserver.network.l2.s2c.ExShowSentPostList;










public class RequestExRequestSentPost
  extends L2GameClientPacket
{
  private int YI;
  
  protected void readImpl() { this.YI = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Mail mail = MailDAO.getInstance().getSentMailByMailId(player.getObjectId(), this.YI);
    if (mail != null) {
      
      player.sendPacket(new ExReplySentPost(mail));
      
      return;
    } 
    player.sendPacket(new ExShowSentPostList(player));
  }
}
