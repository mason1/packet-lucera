package l2.gameserver.network.l2.c2s;

import java.util.List;
import l2.commons.math.SafeMath;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.TradeItem;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.s2c.SendTradeDone;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.network.l2.s2c.TradePressOtherOk;
import l2.gameserver.utils.Log;







public class TradeDone
  extends L2GameClientPacket
{
  private int pI;
  
  protected void readImpl() { this.pI = readD(); }



  
  protected void runImpl() {
    player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    request = player1.getRequest();
    if (request == null || !request.isTypeOf(Request.L2RequestType.TRADE)) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (!request.isInProgress()) {
      
      request.cancel();
      player1.sendPacket(SendTradeDone.FAIL);
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isOutOfControl()) {
      
      request.cancel();
      player1.sendPacket(SendTradeDone.FAIL);
      player1.sendActionFailed();
      
      return;
    } 
    player2 = request.getOtherPlayer(player1);
    if (player2 == null) {
      
      request.cancel();
      player1.sendPacket(SendTradeDone.FAIL);
      player1.sendPacket(Msg.THAT_PLAYER_IS_NOT_ONLINE);
      player1.sendActionFailed();
      
      return;
    } 
    if (player2.getRequest() != request) {
      
      request.cancel();
      player1.sendPacket(SendTradeDone.FAIL);
      player1.sendActionFailed();
      
      return;
    } 
    if (this.pI == 0) {
      
      request.cancel();
      player1.sendPacket(SendTradeDone.FAIL);
      player2.sendPacket(new IStaticPacket[] { SendTradeDone.FAIL, (new SystemMessage(124)).addString(player1.getName()) });
      
      return;
    } 
    if (!player2.isInActingRange(player1)) {
      
      player1.sendPacket(Msg.YOUR_TARGET_IS_OUT_OF_RANGE);

      
      return;
    } 
    
    request.confirm(player1);
    player2.sendPacket(new IStaticPacket[] { (new SystemMessage(121)).addString(player1.getName()), TradePressOtherOk.STATIC });
    
    if (!request.isConfirmed(player2)) {
      
      player1.sendActionFailed();
      
      return;
    } 
    List list1 = player1.getTradeList();
    List list2 = player2.getTradeList();
    byte b = 0;
    long l = 0L;
    bool = false;
    
    player1.getInventory().writeLock();
    player2.getInventory().writeLock();
    
    try {
      b = 0;
      l = 0L;
      
      for (TradeItem tradeItem : list1) {
        
        ItemInstance itemInstance = player1.getInventory().getItemByObjectId(tradeItem.getObjectId());
        if (itemInstance == null || itemInstance.getCount() < tradeItem.getCount() || !itemInstance.canBeTraded(player1)) {
          return;
        }
        l = SafeMath.addAndCheck(l, SafeMath.mulAndCheck(tradeItem.getCount(), tradeItem.getItem().getWeight()));
        if (!tradeItem.getItem().isStackable() || player2.getInventory().getItemByItemId(tradeItem.getItemId()) == null) {
          b++;
        }
      } 
      if (!player2.getInventory().validateWeight(l)) {
        
        player2.sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
        
        return;
      } 
      if (!player2.getInventory().validateCapacity(b)) {
        
        player2.sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
        
        return;
      } 
      b = 0;
      l = 0L;
      
      for (TradeItem tradeItem : list2) {
        
        ItemInstance itemInstance = player2.getInventory().getItemByObjectId(tradeItem.getObjectId());
        if (itemInstance == null || itemInstance.getCount() < tradeItem.getCount() || !itemInstance.canBeTraded(player2)) {
          return;
        }
        l = SafeMath.addAndCheck(l, SafeMath.mulAndCheck(tradeItem.getCount(), tradeItem.getItem().getWeight()));
        if (!tradeItem.getItem().isStackable() || player1.getInventory().getItemByItemId(tradeItem.getItemId()) == null) {
          b++;
        }
      } 
      if (!player1.getInventory().validateWeight(l)) {
        
        player1.sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
        
        return;
      } 
      if (!player1.getInventory().validateCapacity(b)) {
        
        player1.sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
        
        return;
      } 
      for (TradeItem tradeItem : list1) {
        
        ItemInstance itemInstance = player1.getInventory().removeItemByObjectId(tradeItem.getObjectId(), tradeItem.getCount());
        Log.LogItem(player1, Log.ItemLog.TradeSell, itemInstance);
        Log.LogItem(player2, Log.ItemLog.TradeBuy, itemInstance);
        player2.getInventory().addItem(itemInstance);
      } 
      
      for (TradeItem tradeItem : list2) {
        
        ItemInstance itemInstance = player2.getInventory().removeItemByObjectId(tradeItem.getObjectId(), tradeItem.getCount());
        Log.LogItem(player2, Log.ItemLog.TradeSell, itemInstance);
        Log.LogItem(player1, Log.ItemLog.TradeBuy, itemInstance);
        player1.getInventory().addItem(itemInstance);
      } 
      
      player1.sendPacket(Msg.YOUR_TRADE_IS_SUCCESSFUL);
      player2.sendPacket(Msg.YOUR_TRADE_IS_SUCCESSFUL);
      
      bool = true;
    }
    finally {
      
      player2.getInventory().writeUnlock();
      player1.getInventory().writeUnlock();
      
      request.done();
      
      player1.sendPacket(bool ? SendTradeDone.SUCCESS : SendTradeDone.FAIL);
      player2.sendPacket(bool ? SendTradeDone.SUCCESS : SendTradeDone.FAIL);
    } 
  }
}
