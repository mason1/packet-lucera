package l2.gameserver.network.l2.c2s;

import java.util.List;
import l2.commons.math.SafeMath;
import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.data.xml.holder.OptionDataHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.Request;
import l2.gameserver.model.Skill;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.TradeItem;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.SendTradeDone;
import l2.gameserver.network.l2.s2c.TradeOtherAdd;
import l2.gameserver.network.l2.s2c.TradeOwnAdd;
import l2.gameserver.network.l2.s2c.TradeUpdate;
import l2.gameserver.templates.OptionDataTemplate;



public class AddTradeItem
  extends L2GameClientPacket
{
  private int Xe;
  private int Bq;
  private long Xf;
  
  protected void readImpl() {
    this.Xe = readD();
    this.Bq = readD();
    this.Xf = readD();
  }


  
  protected void runImpl() {
    Player player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null || this.Xf < 1L) {
      return;
    }
    Request request = player1.getRequest();
    if (request == null || !request.isTypeOf(Request.L2RequestType.TRADE)) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (!request.isInProgress()) {
      
      request.cancel();
      player1.sendPacket(SendTradeDone.FAIL);
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isOutOfControl()) {
      
      request.cancel();
      player1.sendPacket(SendTradeDone.FAIL);
      player1.sendActionFailed();
      
      return;
    } 
    Player player2 = request.getOtherPlayer(player1);
    if (player2 == null) {
      
      request.cancel();
      player1.sendPacket(SendTradeDone.FAIL);
      player1.sendPacket(Msg.THAT_PLAYER_IS_NOT_ONLINE);
      player1.sendActionFailed();
      
      return;
    } 
    if (player2.getRequest() != request) {
      
      request.cancel();
      player1.sendPacket(SendTradeDone.FAIL);
      player1.sendActionFailed();
      
      return;
    } 
    if (request.isConfirmed(player1) || request.isConfirmed(player2)) {
      
      player1.sendPacket(SystemMsg.YOU_MAY_NO_LONGER_ADJUST_ITEMS_IN_THE_TRADE_BECAUSE_THE_TRADE_HAS_BEEN_CONFIRMED);
      player1.sendActionFailed();
      
      return;
    } 
    ItemInstance itemInstance = player1.getInventory().getItemByObjectId(this.Bq);
    if (itemInstance == null || !itemInstance.canBeTraded(player1)) {
      
      player1.sendPacket(SystemMsg.THIS_ITEM_CANNOT_BE_TRADED_OR_SOLD);
      
      return;
    } 
    
    long l1 = Math.min(this.Xf, itemInstance.getCount());
    long l2 = l1;
    
    List list = player1.getTradeList();
    TradeItem tradeItem = null;


    
    try {
      for (TradeItem tradeItem1 : player1.getTradeList()) {
        if (tradeItem1.getObjectId() == this.Bq) {
          
          long l = tradeItem1.getCount();
          l1 = SafeMath.addAndCheck(l1, l);
          l1 = Math.min(l1, itemInstance.getCount());
          tradeItem1.setCount(l1);
          l2 = Math.max(0L, tradeItem1.getCount() - l);
          tradeItem = tradeItem1;
          break;
        } 
      } 
    } catch (ArithmeticException arithmeticException) {
      
      player1.sendPacket(SystemMsg.INCORRECT_ITEM_COUNT);
      
      return;
    } 
    if (tradeItem == null) {

      
      tradeItem = new TradeItem(itemInstance);
      tradeItem.setCount(l1);
      list.add(tradeItem);
    } 
    
    if (Config.ALT_ALLOW_TRADE_AUGMENTED && itemInstance.isAugmented()) {
      
      Skill skill = null;
      if (itemInstance.getVariationStat1() > 0 || itemInstance.getVariationStat2() > 0) {
        
        OptionDataTemplate optionDataTemplate1 = OptionDataHolder.getInstance().getTemplate(itemInstance.getVariationStat1());
        OptionDataTemplate optionDataTemplate2 = OptionDataHolder.getInstance().getTemplate(itemInstance.getVariationStat2());
        if (optionDataTemplate2 != null && !optionDataTemplate2.getSkills().isEmpty())
        {
          skill = (Skill)optionDataTemplate2.getSkills().get(0);
        }
        if (optionDataTemplate1 != null && !optionDataTemplate1.getSkills().isEmpty())
        {
          skill = (Skill)optionDataTemplate1.getSkills().get(0);
        }
      } 
      if (skill != null) {
        
        if (skill.isActive())
        {
          player2.sendMessage(new CustomMessage("trade.AugmentItemActive", player1, new Object[] { player1, itemInstance, skill, Integer.valueOf(skill.getLevel()) }));
        }
        else if (!skill.getTriggerList().isEmpty())
        {
          player2.sendMessage(new CustomMessage("trade.AugmentItemChance", player1, new Object[] { player1, itemInstance, skill, Integer.valueOf(skill.getLevel()) }));
        }
        else if (skill.isPassive())
        {
          player2.sendMessage(new CustomMessage("trade.AugmentItemPassive", player1, new Object[] { player1, itemInstance, skill, Integer.valueOf(skill.getLevel()) }));
        }
        else
        {
          player2.sendMessage(new CustomMessage("trade.AugmentItem", player1, new Object[] { player1, itemInstance, skill, Integer.valueOf(skill.getLevel()) }));
        }
      
      } else {
        
        player2.sendMessage(new CustomMessage("trade.AugmentItemWithoutSkill", player1, new Object[] { player1, itemInstance }));
      } 
    } 
    player1.sendPacket(new IStaticPacket[] { new TradeOwnAdd(true, tradeItem, tradeItem.getCount()), new TradeOwnAdd(false, tradeItem, tradeItem.getCount()) });
    player1.sendPacket(new IStaticPacket[] { new TradeUpdate(true, tradeItem, itemInstance.getCount() - tradeItem.getCount()), new TradeUpdate(false, tradeItem, itemInstance.getCount() - tradeItem.getCount()) });
    player2.sendPacket(new IStaticPacket[] { new TradeOtherAdd(true, tradeItem, tradeItem.getCount()), new TradeOtherAdd(false, tradeItem, tradeItem.getCount()) });
  }
}
