package l2.gameserver.network.l2.c2s;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import l2.commons.math.SafeMath;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.TradeItem;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.ExPrivateStoreSellingResult;
import l2.gameserver.utils.Log;
import l2.gameserver.utils.TradeHelper;
import org.apache.commons.lang3.ArrayUtils;




public class RequestPrivateStoreBuy
  extends L2GameClientPacket
{
  private int ZE;
  private int _count;
  private int[] Yd;
  private long[] Ye;
  private long[] ZF;
  
  protected void readImpl() {
    this.ZE = readD();
    this._count = readD();
    if (this._count * 20 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      
      return;
    } 
    this.Yd = new int[this._count];
    this.Ye = new long[this._count];
    this.ZF = new long[this._count];
    
    for (byte b = 0; b < this._count; b++) {
      
      this.Yd[b] = readD();
      this.Ye[b] = readQ();
      this.ZF[b] = readQ();
      
      if (this.Ye[b] < 1L || this.ZF[b] < 1L || ArrayUtils.indexOf(this.Yd, this.Yd[b]) < b) {
        
        this._count = 0;
        break;
      } 
    } 
  }


  
  protected void runImpl() {
    player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null || this._count == 0) {
      return;
    }
    if (player1.isActionsDisabled()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isInStoreMode()) {
      
      player1.sendPacket(SystemMsg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
      
      return;
    } 
    if (player1.isInTrade()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isFishing()) {
      
      player1.sendPacket(SystemMsg.YOU_CANNOT_DO_THAT_WHILE_FISHING_);
      
      return;
    } 
    player2 = (Player)player1.getVisibleObject(this.ZE);
    if (player2 == null || (player2.getPrivateStoreType() != 1 && player2.getPrivateStoreType() != 8) || !player2.isInRangeZ(player1, 200L)) {
      
      player1.sendPacket(Msg.THE_ATTEMPT_TO_TRADE_HAS_FAILED);
      player1.sendActionFailed();
      
      return;
    } 
    list = player2.getSellList();
    if (list.isEmpty()) {
      
      player1.sendPacket(Msg.THE_ATTEMPT_TO_TRADE_HAS_FAILED);
      player1.sendActionFailed();
      
      return;
    } 
    arrayList = new ArrayList();
    
    l1 = 0L;
    b = 0;
    l2 = 0L;
    
    player1.getInventory().writeLock();
    player2.getInventory().writeLock();
    
    try {
      byte b1 = 0; label172: while (true) { if (b1 < this._count)
        
        { int i = this.Yd[b1];
          long l3 = this.Ye[b1];
          long l4 = this.ZF[b1];
          
          TradeItem tradeItem = null;
          
          Iterator iterator = list.iterator(); for (;; b1++) { if (iterator.hasNext()) { TradeItem tradeItem1 = (TradeItem)iterator.next();
              if (tradeItem1.getObjectId() == i && 
                tradeItem1.getOwnersPrice() == l4)
              
              { if (l3 > tradeItem1.getCount()) {
                  break label172;
                }
                ItemInstance itemInstance = player2.getInventory().getItemByObjectId(i);
                if (itemInstance == null || itemInstance.getCount() < l3 || !itemInstance.canBeTraded(player2)) {
                  break;
                }
                l1 = SafeMath.addAndCheck(l1, SafeMath.mulAndCheck(l3, l4));
                l2 = SafeMath.addAndCheck(l2, SafeMath.mulAndCheck(l3, itemInstance.getTemplate().getWeight()));
                if (!itemInstance.isStackable() || player1.getInventory().getItemByItemId(itemInstance.getItemId()) == null) {
                  b++;
                }
                tradeItem = new TradeItem();
                tradeItem.setObjectId(i);
                tradeItem.setItemId(itemInstance.getItemId());
                tradeItem.setCount(l3);
                tradeItem.setOwnersPrice(l4);
                
                arrayList.add(tradeItem); } else { continue; }  } else { break; }  }
           }
        else { break; }
         b1++; }
    
    } catch (ArithmeticException arithmeticException) {

      
      arrayList.clear();
      player1.sendPacket(SystemMsg.INCORRECT_ITEM_COUNT);



      
      return;
    } finally {
      try {
        if (arrayList.size() != this._count || (player2.getPrivateStoreType() == 8 && arrayList.size() != list.size())) {
          
          player1.sendPacket(Msg.THE_ATTEMPT_TO_TRADE_HAS_FAILED);
          player1.sendActionFailed();
          
          return;
        } 
        if (!player1.getInventory().validateWeight(l2)) {
          
          player1.sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
          player1.sendActionFailed();
          
          return;
        } 
        if (!player1.getInventory().validateCapacity(b)) {
          
          player1.sendPacket(SystemMsg.YOUR_INVENTORY_IS_FULL);
          player1.sendActionFailed();
          
          return;
        } 
        if (!player1.reduceAdena(l1)) {
          
          player1.sendPacket(SystemMsg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
          player1.sendActionFailed();
          
          return;
        } 
        
        for (TradeItem tradeItem : arrayList) {
          
          ItemInstance itemInstance = player2.getInventory().removeItemByObjectId(tradeItem.getObjectId(), tradeItem.getCount());
          for (TradeItem tradeItem1 : list) {
            if (tradeItem1.getObjectId() == tradeItem.getObjectId()) {
              
              tradeItem1.setCount(tradeItem1.getCount() - tradeItem.getCount());
              if (tradeItem1.getCount() < 1L)
                list.remove(tradeItem1);  break;
            } 
          } 
          Log.LogItem(player2, Log.ItemLog.PrivateStoreSell, itemInstance, itemInstance.getCount(), tradeItem.getOwnersPrice());
          Log.LogItem(player1, Log.ItemLog.PrivateStoreBuy, itemInstance, itemInstance.getCount(), tradeItem.getOwnersPrice());
          player2.sendPacket(new ExPrivateStoreSellingResult(tradeItem.getObjectId(), tradeItem.getCount(), player1.getName()));
          player1.getInventory().addItem(itemInstance);
          TradeHelper.purchaseItem(player1, player2, tradeItem);
        } 
        
        long l = TradeHelper.getTax(player2, l1);
        if (l > 0L) {
          
          l1 -= l;
          player2.sendMessage((new CustomMessage("trade.HavePaidTax", player2, new Object[0])).addNumber(l));
        } 
        
        player2.addAdena(l1);
        player2.saveTradeList();
      }
      finally {
        
        player2.getInventory().writeUnlock();
        player1.getInventory().writeUnlock();
      } 
    } 
    
    if (list.isEmpty()) {
      TradeHelper.cancelStore(player2);
    }
    player2.sendChanges();
    player1.sendChanges();
    
    player1.sendActionFailed();
  }
}
