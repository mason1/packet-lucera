package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.tables.ClanTable;



public class RequestDismissAlly
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Clan clan = player.getClan();
    if (clan == null) {
      
      player.sendActionFailed();
      
      return;
    } 
    Alliance alliance = clan.getAlliance();
    if (alliance == null) {
      
      player.sendPacket(Msg.YOU_ARE_NOT_CURRENTLY_ALLIED_WITH_ANY_CLANS);
      
      return;
    } 
    if (!player.isAllyLeader()) {
      
      player.sendPacket(Msg.FEATURE_AVAILABLE_TO_ALLIANCE_LEADERS_ONLY);
      
      return;
    } 
    if (alliance.getMembersCount() > 1) {
      
      player.sendPacket(Msg.YOU_HAVE_FAILED_TO_DISSOLVE_THE_ALLIANCE);
      
      return;
    } 
    ClanTable.getInstance().dissolveAlly(player);
  }
}
