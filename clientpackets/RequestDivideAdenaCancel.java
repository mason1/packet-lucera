package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.ExDivideAdenaCancel;



public class RequestDivideAdenaCancel
  extends L2GameClientPacket
{
  private int Yz;
  
  protected void readImpl() { this.Yz = readC(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }

    
    if (this.Yz == 0) {
      
      player.sendPacket(SystemMsg.ADENA_DISTRIBUTION_HAS_BEEN_CANCELLED);
      player.sendPacket(ExDivideAdenaCancel.STATIC);
    } 
  }
}
