package l2.gameserver.network.l2.c2s;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import l2.commons.math.SafeMath;
import l2.gameserver.cache.Msg;
import l2.gameserver.data.xml.holder.ItemHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.TradeItem;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.network.l2.s2c.PrivateStoreManageListBuy;
import l2.gameserver.network.l2.s2c.PrivateStoreMsgBuy;
import l2.gameserver.templates.item.ItemTemplate;
import l2.gameserver.utils.TradeHelper;

public class SetPrivateStoreBuyList extends L2GameClientPacket {
  private List<SetPrivateStoreBuyListEntry> aao = Collections.emptyList();
  private int _count;
  
  private static class SetPrivateStoreBuyListEntry {
    public static final int IN_PACKET_SIZE = 54;
    private final int itemId;
    private final int enchant;
    private final int damage;
    private final long count;
    private final long price;
    
    private SetPrivateStoreBuyListEntry(int param1Int1, int param1Int2, int param1Int3, long param1Long1, long param1Long2) {
      this.itemId = param1Int1;
      this.enchant = param1Int2;
      this.damage = param1Int3;
      this.count = param1Long1;
      this.price = param1Long2;
    }


    
    public int getItemId() { return this.itemId; }



    
    public int getEnchant() { return this.enchant; }



    
    public int getDamage() { return this.damage; }



    
    public long getCount() { return this.count; }



    
    public long getPrice() { return this.price; }
  }



  
  protected void readImpl() {
    this._count = readD();
    if (this._count < 1 || this._buf.remaining() < this._count * 54 || this._count > 1024) {
      
      this._count = 0;
      
      return;
    } 
    ArrayList arrayList = new ArrayList(this._count);
    for (byte b = 0; b < this._count; b++) {
      
      int i = getByteBuffer().position();
      int j = readD();
      int k = readH();
      int m = readH();
      long l1 = readQ();
      long l2 = readQ();
      getByteBuffer().position(i + 54);
      
      if (l1 < 1L || l2 < 1L) {
        
        arrayList.clear();
        return;
      } 
      arrayList.add(new SetPrivateStoreBuyListEntry(j, k, m, l1, l2, null));
    } 



    
    this.aao = arrayList;
  }


  
  protected void runImpl() {
    player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0 || this._count != this.aao.size()) {
      return;
    }
    if (!TradeHelper.checksIfCanOpenStore(player, 3)) {
      
      player.sendActionFailed();
      
      return;
    } 
    player.getInventory().readLock();
    CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
    
    long l = 0L;
    int i = 0;

    
    try {
      for (SetPrivateStoreBuyListEntry setPrivateStoreBuyListEntry : this.aao)
      {
        if (setPrivateStoreBuyListEntry.getItemId() == 57)
          continue; 
        ItemTemplate itemTemplate = ItemHolder.getInstance().getTemplate(setPrivateStoreBuyListEntry.getItemId());
        if (itemTemplate == null) {
          continue;
        }
        l = SafeMath.addAndCheck(l, SafeMath.mulAndCheck(setPrivateStoreBuyListEntry.getCount(), setPrivateStoreBuyListEntry.getPrice()));
        i = SafeMath.addAndCheck(i, SafeMath.mulAndCheck((int)setPrivateStoreBuyListEntry.getCount(), itemTemplate.getWeight()));
        
        TradeItem tradeItem = new TradeItem();
        tradeItem.setItemId(setPrivateStoreBuyListEntry.getItemId());
        tradeItem.setEnchantLevel(setPrivateStoreBuyListEntry.getEnchant());
        tradeItem.setCount(setPrivateStoreBuyListEntry.getCount());
        tradeItem.setOwnersPrice(setPrivateStoreBuyListEntry.getPrice());
        tradeItem.setReferencePrice(itemTemplate.getReferencePrice());
        
        if (tradeItem.getStorePrice() > setPrivateStoreBuyListEntry.getPrice()) {
          
          player.sendMessage((new CustomMessage("l2p.gameserver.clientpackets.SetPrivateStoreBuyList.TooLowPrice", player, new Object[0])).addItemName(itemTemplate).addNumber((itemTemplate.getReferencePrice() / 2)));
          
          continue;
        } 
        copyOnWriteArrayList.add(tradeItem);
      }
    
    } catch (ArithmeticException arithmeticException) {
      
      copyOnWriteArrayList.clear();
      sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);

      
      return;
    } finally {
      player.getInventory().readUnlock();
    } 
    
    if (!player.getInventory().validateWeight(i)) {
      
      sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
      
      return;
    } 
    if (!player.getInventory().validateCapacity(copyOnWriteArrayList.size())) {
      
      sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
      
      return;
    } 
    if (copyOnWriteArrayList.size() > player.getTradeLimit() || l > 2147483647L || copyOnWriteArrayList.size() != this._count) {
      
      player.sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);
      player.sendPacket(new IStaticPacket[] { new PrivateStoreManageListBuy(true, player), new PrivateStoreManageListBuy(false, player) });
      
      return;
    } 
    if (l > player.getAdena()) {
      
      player.sendPacket(Msg.THE_PURCHASE_PRICE_IS_HIGHER_THAN_THE_AMOUNT_OF_MONEY_THAT_YOU_HAVE_AND_SO_YOU_CANNOT_OPEN_A_PERSONAL_STORE);
      player.sendPacket(new IStaticPacket[] { new PrivateStoreManageListBuy(true, player), new PrivateStoreManageListBuy(false, player) });
      
      return;
    } 
    if (!copyOnWriteArrayList.isEmpty()) {
      
      player.getInventory().writeLock();
      
      try {
        player.setBuyList(copyOnWriteArrayList);
        player.setPrivateStoreType(3);
        player.saveTradeList();
        player.broadcastPacket(new L2GameServerPacket[] { new PrivateStoreMsgBuy(player) });
      }
      finally {
        
        player.getInventory().writeUnlock();
      }
    
    } else {
      
      player.setBuyList(Collections.emptyList());
      player.setPrivateStoreType(0);
    } 
    
    player.sendActionFailed();
  }
}
