package l2.gameserver.network.l2.c2s;

import l2.gameserver.dao.CharacterPostFriendDAO;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.SystemMessage2;
import org.apache.commons.lang3.StringUtils;
import org.napile.primitive.maps.IntObjectMap;






public class RequestExDeletePostFriendForPostBox
  extends L2GameClientPacket
{
  private String _name;
  
  protected void readImpl() { this._name = readS(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (StringUtils.isEmpty(this._name)) {
      return;
    }
    int i = 0;
    IntObjectMap intObjectMap = player.getPostFriends();
    for (IntObjectMap.Entry entry : intObjectMap.entrySet()) {
      
      if (((String)entry.getValue()).equalsIgnoreCase(this._name)) {
        i = entry.getKey();
      }
    } 
    if (i == 0) {
      
      player.sendPacket(SystemMsg.THE_NAME_IS_NOT_CURRENTLY_REGISTERED);
      
      return;
    } 
    player.getPostFriends().remove(i);
    
    CharacterPostFriendDAO.getInstance().delete(player, i);
    player.sendPacket((new SystemMessage2(SystemMsg.S1_WAS_SUCCESSFULLY_DELETED_FROM_YOUR_CONTACT_LIST)).addString(this._name));
  }
}
