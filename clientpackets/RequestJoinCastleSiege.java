package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.dao.SiegeClanDAO;
import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.events.impl.CastleSiegeEvent;
import l2.gameserver.model.entity.events.impl.ClanHallSiegeEvent;
import l2.gameserver.model.entity.events.objects.SiegeClanObject;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.model.entity.residence.ClanHall;
import l2.gameserver.model.entity.residence.Residence;
import l2.gameserver.model.entity.residence.ResidenceType;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.model.pledge.Privilege;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.CastleSiegeAttackerList;
import l2.gameserver.network.l2.s2c.CastleSiegeDefenderList;





public class RequestJoinCastleSiege
  extends L2GameClientPacket
{
  private int _id;
  private boolean Zj;
  private boolean Zk;
  
  protected void readImpl() {
    this._id = readD();
    this.Zj = (readD() == 1);
    this.Zk = (readD() == 1);
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (!player.hasPrivilege(Privilege.CS_FS_SIEGE_WAR)) {
      
      player.sendPacket(SystemMsg.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT);
      
      return;
    } 
    Residence residence = ResidenceHolder.getInstance().getResidence(this._id);
    
    if (residence.getType() == ResidenceType.Castle) {
      a(player, (Castle)residence, this.Zj, this.Zk);
    } else if (residence.getType() == ResidenceType.ClanHall && this.Zj) {
      a(player, (ClanHall)residence, this.Zk);
    } 
  }
  
  private static void a(Player paramPlayer, Castle paramCastle, boolean paramBoolean1, boolean paramBoolean2) {
    CastleSiegeEvent castleSiegeEvent = (CastleSiegeEvent)paramCastle.getSiegeEvent();
    
    Clan clan = paramPlayer.getClan();
    
    if (paramPlayer.getClan().isPlacedForDisband()) {
      
      paramPlayer.sendPacket(SystemMsg.DISPERSION_HAS_ALREADY_BEEN_REQUESTED);
      
      return;
    } 
    SiegeClanObject siegeClanObject = null;
    if (paramBoolean1) {
      siegeClanObject = castleSiegeEvent.getSiegeClan("attackers", clan);
    } else {
      
      siegeClanObject = castleSiegeEvent.getSiegeClan("defenders", clan);
      if (siegeClanObject == null) {
        siegeClanObject = castleSiegeEvent.getSiegeClan("defenders_waiting", clan);
      }
    } 
    if (paramBoolean2) {
      
      Residence residence = null;
      for (Residence residence1 : ResidenceHolder.getInstance().getResidenceList(Castle.class)) {
        
        SiegeClanObject siegeClanObject1 = residence1.getSiegeEvent().getSiegeClan("attackers", clan);
        
        if (siegeClanObject1 == null) {
          siegeClanObject1 = residence1.getSiegeEvent().getSiegeClan("defenders", clan);
        }
        if (siegeClanObject1 == null) {
          siegeClanObject1 = residence1.getSiegeEvent().getSiegeClan("defenders_waiting", clan);
        }
        if (siegeClanObject1 != null) {
          residence = residence1;
        }
      } 
      if (paramBoolean1)
      {
        if (paramCastle.getOwnerId() == clan.getClanId()) {
          
          paramPlayer.sendPacket(SystemMsg.CASTLE_OWNING_CLANS_ARE_AUTOMATICALLY_REGISTERED_ON_THE_DEFENDING_SIDE);
          
          return;
        } 
        Alliance alliance = clan.getAlliance();
        if (alliance != null)
        {
          for (Clan clan1 : alliance.getMembers()) {
            
            if (clan1.getCastle() == paramCastle.getId()) {
              
              paramPlayer.sendPacket(SystemMsg.YOU_CANNOT_REGISTER_AS_AN_ATTACKER_BECAUSE_YOU_ARE_IN_AN_ALLIANCE_WITH_THE_CASTLE_OWNING_CLAN);
              return;
            } 
          } 
        }
        if (clan.getCastle() > 0) {
          
          paramPlayer.sendPacket(SystemMsg.A_CLAN_THAT_OWNS_A_CASTLE_CANNOT_PARTICIPATE_IN_ANOTHER_SIEGE);
          
          return;
        } 
        if (siegeClanObject != null) {
          
          paramPlayer.sendPacket(SystemMsg.YOU_ARE_ALREADY_REGISTERED_TO_THE_ATTACKER_SIDE_AND_MUST_CANCEL_YOUR_REGISTRATION_BEFORE_SUBMITTING_YOUR_REQUEST);
          
          return;
        } 
        if (clan.getLevel() < Config.MIN_CLAN_LEVEL_FOR_SIEGE_REGISTRATION) {
          
          paramPlayer.sendPacket(SystemMsg.ONLY_CLANS_OF_LEVEL_5_OR_HIGHER_MAY_REGISTER_FOR_A_CASTLE_SIEGE);
          
          return;
        } 
        if (residence != null) {
          
          paramPlayer.sendPacket(SystemMsg.YOU_HAVE_ALREADY_REQUESTED_A_CASTLE_SIEGE);
          
          return;
        } 
        if (castleSiegeEvent.isRegistrationOver()) {
          
          paramPlayer.sendPacket(SystemMsg.YOU_ARE_TOO_LATE_THE_REGISTRATION_PERIOD_IS_OVER);
          
          return;
        } 
        if (paramCastle.getSiegeDate().getTimeInMillis() == 0L) {
          
          paramPlayer.sendPacket(SystemMsg.THIS_IS_NOT_THE_TIME_FOR_SIEGE_REGISTRATION_AND_SO_REGISTRATION_AND_CANCELLATION_CANNOT_BE_DONE);
          
          return;
        } 
        int i = castleSiegeEvent.getObjects("attackers").size();
        if (i >= 20) {
          
          paramPlayer.sendPacket(SystemMsg.NO_MORE_REGISTRATIONS_MAY_BE_ACCEPTED_FOR_THE_ATTACKER_SIDE);
          
          return;
        } 
        siegeClanObject = new SiegeClanObject("attackers", clan, 0L);
        castleSiegeEvent.addObject("attackers", siegeClanObject);
        
        SiegeClanDAO.getInstance().insert(paramCastle, siegeClanObject);
        
        paramPlayer.sendPacket(new CastleSiegeAttackerList(paramCastle));
      }
      else
      {
        if (paramCastle.getOwnerId() == 0) {
          return;
        }
        if (paramCastle.getOwnerId() == clan.getClanId()) {
          
          paramPlayer.sendPacket(SystemMsg.CASTLE_OWNING_CLANS_ARE_AUTOMATICALLY_REGISTERED_ON_THE_DEFENDING_SIDE);
          
          return;
        } 
        if (clan.getCastle() > 0) {
          
          paramPlayer.sendPacket(SystemMsg.A_CLAN_THAT_OWNS_A_CASTLE_CANNOT_PARTICIPATE_IN_ANOTHER_SIEGE);
          
          return;
        } 
        if (siegeClanObject != null) {
          
          paramPlayer.sendPacket(SystemMsg.YOU_HAVE_ALREADY_REGISTERED_TO_THE_DEFENDER_SIDE_AND_MUST_CANCEL_YOUR_REGISTRATION_BEFORE_SUBMITTING_YOUR_REQUEST);
          
          return;
        } 
        if (clan.getLevel() < Config.MIN_CLAN_LEVEL_FOR_SIEGE_REGISTRATION) {
          
          paramPlayer.sendPacket(SystemMsg.ONLY_CLANS_OF_LEVEL_5_OR_HIGHER_MAY_REGISTER_FOR_A_CASTLE_SIEGE);
          
          return;
        } 
        if (residence != null) {
          
          paramPlayer.sendPacket(SystemMsg.YOU_HAVE_ALREADY_REQUESTED_A_CASTLE_SIEGE);
          
          return;
        } 
        if (paramCastle.getSiegeDate().getTimeInMillis() == 0L) {
          
          paramPlayer.sendPacket(SystemMsg.THIS_IS_NOT_THE_TIME_FOR_SIEGE_REGISTRATION_AND_SO_REGISTRATION_AND_CANCELLATION_CANNOT_BE_DONE);
          
          return;
        } 
        if (castleSiegeEvent.isRegistrationOver()) {
          
          paramPlayer.sendPacket(SystemMsg.YOU_ARE_TOO_LATE_THE_REGISTRATION_PERIOD_IS_OVER);
          
          return;
        } 
        siegeClanObject = new SiegeClanObject("defenders_waiting", clan, 0L);
        castleSiegeEvent.addObject("defenders_waiting", siegeClanObject);
        
        SiegeClanDAO.getInstance().insert(paramCastle, siegeClanObject);
        
        paramPlayer.sendPacket(new CastleSiegeDefenderList(paramCastle));
      }
    
    } else {
      
      if (siegeClanObject == null) {
        siegeClanObject = castleSiegeEvent.getSiegeClan("defenders_refused", clan);
      }
      if (siegeClanObject == null) {
        
        paramPlayer.sendPacket(SystemMsg.YOU_ARE_NOT_YET_REGISTERED_FOR_THE_CASTLE_SIEGE);
        
        return;
      } 
      if (castleSiegeEvent.isRegistrationOver()) {
        
        paramPlayer.sendPacket(SystemMsg.YOU_ARE_TOO_LATE_THE_REGISTRATION_PERIOD_IS_OVER);
        
        return;
      } 
      castleSiegeEvent.removeObject(siegeClanObject.getType(), siegeClanObject);
      
      SiegeClanDAO.getInstance().delete(paramCastle, siegeClanObject);
      if (siegeClanObject.getType() == "attackers") {
        paramPlayer.sendPacket(new CastleSiegeAttackerList(paramCastle));
      } else {
        paramPlayer.sendPacket(new CastleSiegeDefenderList(paramCastle));
      } 
    } 
  }
  
  private static void a(Player paramPlayer, ClanHall paramClanHall, boolean paramBoolean) {
    ClanHallSiegeEvent clanHallSiegeEvent = (ClanHallSiegeEvent)paramClanHall.getSiegeEvent();
    
    Clan clan = paramPlayer.getClan();
    
    SiegeClanObject siegeClanObject = clanHallSiegeEvent.getSiegeClan("attackers", clan);
    
    if (paramBoolean) {
      
      if (clan.getHasHideout() > 0) {
        
        paramPlayer.sendPacket(SystemMsg.A_CLAN_THAT_OWNS_A_CLAN_HALL_MAY_NOT_PARTICIPATE_IN_A_CLAN_HALL_SIEGE);
        
        return;
      } 
      if (siegeClanObject != null) {
        
        paramPlayer.sendPacket(SystemMsg.YOU_ARE_ALREADY_REGISTERED_TO_THE_ATTACKER_SIDE_AND_MUST_CANCEL_YOUR_REGISTRATION_BEFORE_SUBMITTING_YOUR_REQUEST);
        
        return;
      } 
      if (clan.getLevel() < 4) {
        
        paramPlayer.sendPacket(SystemMsg.ONLY_CLANS_WHO_ARE_LEVEL_4_OR_ABOVE_CAN_REGISTER_FOR_BATTLE_AT_DEVASTATED_CASTLE_AND_FORTRESS_OF_THE_DEAD);
        
        return;
      } 
      if (clanHallSiegeEvent.isRegistrationOver()) {
        
        paramPlayer.sendPacket(SystemMsg.YOU_ARE_TOO_LATE_THE_REGISTRATION_PERIOD_IS_OVER);
        
        return;
      } 
      int i = clanHallSiegeEvent.getObjects("attackers").size();
      if (i >= 20) {
        
        paramPlayer.sendPacket(SystemMsg.NO_MORE_REGISTRATIONS_MAY_BE_ACCEPTED_FOR_THE_ATTACKER_SIDE);
        
        return;
      } 
      siegeClanObject = new SiegeClanObject("attackers", clan, 0L);
      clanHallSiegeEvent.addObject("attackers", siegeClanObject);
      
      SiegeClanDAO.getInstance().insert(paramClanHall, siegeClanObject);
    }
    else {
      
      if (siegeClanObject == null) {
        
        paramPlayer.sendPacket(SystemMsg.YOU_ARE_NOT_YET_REGISTERED_FOR_THE_CASTLE_SIEGE);
        
        return;
      } 
      if (clanHallSiegeEvent.isRegistrationOver()) {
        
        paramPlayer.sendPacket(SystemMsg.YOU_ARE_TOO_LATE_THE_REGISTRATION_PERIOD_IS_OVER);
        
        return;
      } 
      clanHallSiegeEvent.removeObject(siegeClanObject.getType(), siegeClanObject);
      
      SiegeClanDAO.getInstance().delete(paramClanHall, siegeClanObject);
    } 
    
    paramPlayer.sendPacket(new CastleSiegeAttackerList(paramClanHall));
  }
}
