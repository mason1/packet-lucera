package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.model.Player;
import l2.gameserver.model.World;
import l2.gameserver.network.l2.GameClient;




public class RequestReload
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    GameClient gameClient = (GameClient)getClient();
    Player player = gameClient.getActiveChar();
    if (player == null)
      return; 
    long l = System.currentTimeMillis();
    if (l - gameClient.getLastIncomePacketTimeStamp(RequestReload.class) < Config.RELOAD_PACKET_DELAY) {
      
      player.sendActionFailed();
      return;
    } 
    gameClient.setLastIncomePacketTimeStamp(RequestReload.class, l);
    
    player.sendUserInfo(true);
    World.showObjectsToPlayer(player);
  }
}
