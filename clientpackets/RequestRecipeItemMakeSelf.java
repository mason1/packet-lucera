package l2.gameserver.network.l2.c2s;

import java.util.List;
import l2.commons.util.Rnd;
import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.data.xml.holder.RecipeHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.Recipe;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.s2c.ActionFail;
import l2.gameserver.network.l2.s2c.RecipeItemMakeInfo;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.network.l2.s2c.SystemMessage2;
import l2.gameserver.templates.item.EtcItemTemplate;
import l2.gameserver.templates.item.ItemTemplate;
import l2.gameserver.utils.ItemFunctions;
import org.apache.commons.lang3.tuple.Pair;






public class RequestRecipeItemMakeSelf
  extends L2GameClientPacket
{
  private int UE;
  
  protected void readImpl() { this.UE = readD(); }



  
  protected void runImpl() {
    player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isInStoreMode()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isProcessingRequest()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.isFishing()) {
      
      player.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
      
      return;
    } 
    if (player.isInDuel()) {
      
      player.sendActionFailed();
      
      return;
    } 
    Recipe recipe = RecipeHolder.getInstance().getRecipeById(this.UE);
    
    if (recipe == null || recipe.getMaterials().isEmpty() || recipe.getProducts().isEmpty()) {
      
      player.sendPacket(Msg.THE_RECIPE_IS_INCORRECT);
      
      return;
    } 
    if (player.getCurrentMp() < recipe.getMpConsume()) {
      
      player.sendPacket(new IStaticPacket[] { Msg.NOT_ENOUGH_MP, new RecipeItemMakeInfo(player, recipe, 0) });
      
      return;
    } 
    if (!player.findRecipe(this.UE)) {
      
      player.sendPacket(new IStaticPacket[] { Msg.PLEASE_REGISTER_A_RECIPE, ActionFail.STATIC });
      
      return;
    } 
    boolean bool = false;
    List list1 = recipe.getMaterials();
    List list2 = recipe.getProducts();
    
    player.getInventory().writeLock();
    
    try {
      for (Pair pair : list1) {
        
        ItemTemplate itemTemplate = (ItemTemplate)pair.getKey();
        long l1 = ((Long)pair.getValue()).longValue();
        if (l1 <= 0L) {
          continue;
        }

        
        if (Config.ALT_GAME_UNREGISTER_RECIPE && itemTemplate.getItemType() == EtcItemTemplate.EtcItemType.RECIPE) {
          
          Recipe recipe1 = RecipeHolder.getInstance().getRecipeByItem(itemTemplate);
          if (player.hasRecipe(recipe1)) {
            continue;
          }
          
          player.sendPacket(new IStaticPacket[] { Msg.NOT_ENOUGH_MATERIALS, new RecipeItemMakeInfo(player, recipe, 0) });
          
          return;
        } 
        ItemInstance itemInstance = player.getInventory().getItemByItemId(itemTemplate.getItemId());
        if (itemInstance == null || itemInstance.getCount() < l1) {
          
          player.sendPacket(new IStaticPacket[] { Msg.NOT_ENOUGH_MATERIALS, new RecipeItemMakeInfo(player, recipe, 0) });
          
          return;
        } 
      } 
      int i = 0;
      long l = 0L;
      for (Pair pair : list2) {
        
        i = (int)(i + ((ItemTemplate)pair.getKey()).getWeight() * ((Long)pair.getValue()).longValue());
        l += (((ItemTemplate)pair.getKey()).isStackable() ? 1L : ((Long)pair.getValue()).longValue());
      } 
      
      if (!player.getInventory().validateWeight(i) || !player.getInventory().validateCapacity(l)) {
        
        player.sendPacket(new IStaticPacket[] { Msg.WEIGHT_AND_VOLUME_LIMIT_HAS_BEEN_EXCEEDED_THAT_SKILL_IS_CURRENTLY_UNAVAILABLE, new RecipeItemMakeInfo(player, recipe, 0) });
        
        return;
      } 
      for (Pair pair : list1)
      {
        ItemTemplate itemTemplate = (ItemTemplate)pair.getKey();
        long l1 = ((Long)pair.getValue()).longValue();
        if (l1 <= 0L) {
          continue;
        }

        
        if (Config.ALT_GAME_UNREGISTER_RECIPE && itemTemplate.getItemType() == EtcItemTemplate.EtcItemType.RECIPE) {
          
          player.unregisterRecipe(RecipeHolder.getInstance().getRecipeByItem(itemTemplate).getId());
          
          continue;
        } 
        if (!player.getInventory().destroyItemByItemId(itemTemplate.getItemId(), l1)) {
          continue;
        }
        
        player.sendPacket(SystemMessage2.removeItems(itemTemplate.getItemId(), l1));
      }
    
    }
    finally {
      
      player.getInventory().writeUnlock();
    } 
    
    player.resetWaitSitTime();
    player.reduceCurrentMp(recipe.getMpConsume(), null);
    
    if (Rnd.chance(recipe.getSuccessRate())) {
      
      for (Pair pair : list2) {
        
        int i = ((ItemTemplate)pair.getKey()).getItemId();
        long l = ((Long)pair.getValue()).longValue();
        ItemFunctions.addItem(player, i, l, true);
      } 
      bool = true;
    } 
    
    if (!bool)
    {
      for (Pair pair : list2)
      {
        player.sendPacket((new SystemMessage(960)).addItemName(((ItemTemplate)pair.getKey()).getItemId()));
      }
    }
    player.sendPacket(new RecipeItemMakeInfo(player, recipe, bool ? 1 : 0));
  }
}
