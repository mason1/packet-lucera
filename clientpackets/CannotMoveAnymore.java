package l2.gameserver.network.l2.c2s;

import l2.gameserver.ai.CtrlEvent;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.StopMove;
import l2.gameserver.utils.Location;

public class CannotMoveAnymore extends L2GameClientPacket {
  private Location _loc = new Location();
















  
  protected void readImpl() {
    this._loc.x = readD();
    this._loc.y = readD();
    this._loc.z = readD();
    this._loc.h = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.isOlyObserver()) {
      
      player.sendPacket(new StopMove(player.getObjectId(), this._loc));
      
      return;
    } 
    if (!player.isOutOfControl()) {
      
      player.getAI().notifyEvent(CtrlEvent.EVT_ARRIVED_BLOCKED, this._loc, null);
      player.stopMove();
    } 
  }
}
