package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Party;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExMPCCShowPartyMemberInfo;


public class RequestExMPCCShowPartyMembersInfo
  extends L2GameClientPacket
{
  private int Bq;
  
  protected void readImpl() { this.Bq = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    
    if (player == null || !player.isInParty() || !player.getParty().isInCommandChannel()) {
      return;
    }
    for (Party party : player.getParty().getCommandChannel().getParties()) {
      
      Player player1 = party.getPartyLeader();
      if (player1 != null && player1.getObjectId() == this.Bq) {
        
        player.sendPacket(new ExMPCCShowPartyMemberInfo(party));
        break;
      } 
    } 
  }
}
