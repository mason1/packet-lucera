package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Party;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;


public class RequestPartyLootModification
  extends L2GameClientPacket
{
  private byte Zt;
  
  protected void readImpl() { this.Zt = (byte)readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (this.Zt < 0 || this.Zt > 4) {
      return;
    }
    Party party = player.getParty();
    if (party == null || this.Zt == party.getLootDistribution() || party.getPartyLeader() != player) {
      return;
    }
    party.requestLootChange(this.Zt);
  }
}
