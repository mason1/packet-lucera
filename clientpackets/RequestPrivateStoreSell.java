package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.utils.TradeHelper;





@Deprecated
public class RequestPrivateStoreSell
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (player.isActionsDisabled()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.getSittingTask()) {
      
      player.sendActionFailed();
      return;
    } 
    if (player.isInStoreMode()) {
      player.setPrivateStoreType(0);
    } else if (!TradeHelper.checksIfCanOpenStore(player, 1)) {
      
      player.sendActionFailed();
      return;
    } 
  }
}
