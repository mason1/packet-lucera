package l2.gameserver.network.l2.c2s;

import l2.gameserver.handler.usercommands.IUserCommandHandler;
import l2.gameserver.handler.usercommands.UserCommandHandler;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;







public class BypassUserCmd
  extends L2GameClientPacket
{
  private int Xt;
  
  protected void readImpl() { this.Xt = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    IUserCommandHandler iUserCommandHandler = UserCommandHandler.getInstance().getUserCommandHandler(this.Xt);
    
    if (iUserCommandHandler == null) {
      player.sendMessage((new CustomMessage("common.S1NotImplemented", player, new Object[0])).addString(String.valueOf(this.Xt)));
    } else {
      iUserCommandHandler.useUserCommand(this.Xt, player);
    } 
  }
}
