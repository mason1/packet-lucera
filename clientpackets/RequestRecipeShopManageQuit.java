package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;



public class RequestRecipeShopManageQuit
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    player.setPrivateStoreType(0);
  }
}
