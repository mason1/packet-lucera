package l2.gameserver.network.l2.c2s;

import java.util.concurrent.CopyOnWriteArrayList;
import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ManufactureItem;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.L2GameServerPacket;
import l2.gameserver.network.l2.s2c.RecipeShopMsg;
import l2.gameserver.utils.TradeHelper;


public class RequestRecipeShopListSet
  extends L2GameClientPacket
{
  private int[] ZM;
  private long[] ZN;
  private int _count;
  
  protected void readImpl() {
    this._count = readD();
    if (this._count * 12 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      return;
    } 
    this.ZM = new int[this._count];
    this.ZN = new long[this._count];
    for (byte b = 0; b < this._count; b++) {
      
      this.ZM[b] = readD();
      this.ZN[b] = readQ();
      if (this.ZN[b] < 0L) {
        
        this._count = 0;
        return;
      } 
    } 
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0) {
      return;
    }
    if (!TradeHelper.checksIfCanOpenStore(player, 5)) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (this._count > Config.MAX_PVTCRAFT_SLOTS) {
      
      sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);
      
      return;
    } 
    CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
    for (byte b = 0; b < this._count; b++) {
      
      int i = this.ZM[b];
      long l = this.ZN[b];
      if (player.findRecipe(i)) {

        
        ManufactureItem manufactureItem = new ManufactureItem(i, l);
        copyOnWriteArrayList.add(manufactureItem);
      } 
    } 
    if (!copyOnWriteArrayList.isEmpty()) {
      
      player.setCreateList(copyOnWriteArrayList);
      player.saveTradeList();
      player.setPrivateStoreType(5);
      player.broadcastPacket(new L2GameServerPacket[] { new RecipeShopMsg(player) });
    } 
    
    player.sendActionFailed();
  }
}
