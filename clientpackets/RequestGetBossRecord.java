package l2.gameserver.network.l2.c2s;

import java.util.ArrayList;
import java.util.Map;
import l2.gameserver.instancemanager.RaidBossSpawnManager;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExGetBossRecord;









public class RequestGetBossRecord
  extends L2GameClientPacket
{
  private int Zh;
  
  protected void readImpl() { this.Zh = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    int i = 0;
    int j = 0;
    
    if (player == null) {
      return;
    }
    ArrayList arrayList = new ArrayList();
    Map map = RaidBossSpawnManager.getInstance().getPointsForOwnerId(player.getObjectId());
    if (map != null && !map.isEmpty()) {
      for (Map.Entry entry : map.entrySet()) {
        switch (((Integer)entry.getKey()).intValue()) {
          
          case -1:
            j = ((Integer)entry.getValue()).intValue();
            continue;
          case 0:
            i = ((Integer)entry.getValue()).intValue();
            continue;
        } 
        arrayList.add(new ExGetBossRecord.BossRecordInfo(((Integer)entry.getKey()).intValue(), ((Integer)entry.getValue()).intValue(), 0));
      } 
    }
    player.sendPacket(new ExGetBossRecord(j, i, arrayList));
  }
}
