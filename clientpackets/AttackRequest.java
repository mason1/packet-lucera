package l2.gameserver.network.l2.c2s;

import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.GameObject;
import l2.gameserver.model.Playable;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import l2.gameserver.network.l2.s2c.ActionFail;


public class AttackRequest
  extends L2GameClientPacket
{
  private int Bq;
  private int Xh;
  private int Xi;
  private int Xj;
  private int Xk;
  
  protected void readImpl() {
    this.Bq = readD();
    this.Xh = readD();
    this.Xi = readD();
    this.Xj = readD();
    this.Xk = readC();
  }


  
  protected void runImpl() {
    GameClient gameClient = (GameClient)getClient();
    Player player = gameClient.getActiveChar();
    if (player == null) {
      return;
    }
    player.setActive();
    
    if (player.isOutOfControl()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (!(player.getPlayerAccess()).CanAttack) {
      
      player.sendActionFailed();
      
      return;
    } 
    GameObject gameObject = player.getVisibleObject(this.Bq);
    if (gameObject == null) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (player.getAggressionTarget() != null && player.getAggressionTarget() != gameObject && !player.getAggressionTarget().isDead()) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (gameObject.isPlayer() && (player.isInBoat() || gameObject.isInBoat())) {
      
      player.sendPacket(new IStaticPacket[] { Msg.THIS_IS_NOT_ALLOWED_WHILE_USING_A_FERRY, ActionFail.STATIC });
      
      return;
    } 
    if (gameObject.isPlayable()) {
      
      if (player.isInZonePeace()) {
        
        player.sendPacket(new IStaticPacket[] { Msg.YOU_MAY_NOT_ATTACK_IN_A_PEACEFUL_ZONE, ActionFail.STATIC });
        return;
      } 
      if (((Playable)gameObject).isInZonePeace()) {
        
        player.sendPacket(new IStaticPacket[] { Msg.YOU_MAY_NOT_ATTACK_THIS_TARGET_IN_A_PEACEFUL_ZONE, ActionFail.STATIC });
        
        return;
      } 
    } 
    long l = System.currentTimeMillis();
    if (l - gameClient.getLastIncomePacketTimeStamp(AttackRequest.class) < Config.ATTACK_PACKET_DELAY) {
      
      player.sendActionFailed();
      return;
    } 
    gameClient.setLastIncomePacketTimeStamp(AttackRequest.class, l);

    
    if (player.getTarget() != gameObject) {
      
      gameObject.onAction(player, (this.Xk == 1));
      
      return;
    } 
    if (gameObject.getObjectId() != player.getObjectId() && !player.isInStoreMode() && !player.isProcessingRequest())
      gameObject.onForcedAttack(player, (this.Xk == 1)); 
  }
}
