package l2.gameserver.network.l2.c2s;

import java.util.ArrayList;
import java.util.HashMap;
import l2.gameserver.Config;
import l2.gameserver.cache.Msg;
import l2.gameserver.dao.CharacterDAO;
import l2.gameserver.database.mysql;
import l2.gameserver.model.GameObjectsStorage;
import l2.gameserver.model.Player;
import l2.gameserver.model.World;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.mail.Mail;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.CustomMessage;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.network.l2.s2c.ExNoticePostArrived;
import l2.gameserver.network.l2.s2c.ExReplyWritePost;
import l2.gameserver.network.l2.s2c.SystemMessage;
import l2.gameserver.scripts.Functions;
import l2.gameserver.utils.Log;
import l2.gameserver.utils.Util;
import org.apache.commons.lang3.ArrayUtils;














public class RequestExSendPost
  extends L2GameClientPacket
{
  private int YY;
  private String YZ;
  private String Vl;
  private String Za;
  private int _count;
  private int[] Yd;
  private long[] Ye;
  private long _price;
  
  protected void readImpl() {
    this.YZ = readS(35);
    this.YY = readD();
    this.Vl = readS(127);
    this.Za = readS(32767);
    
    this._count = readD();
    if (this._count * 12 + 4 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      
      return;
    } 
    this.Yd = new int[this._count];
    this.Ye = new long[this._count];
    
    for (byte b = 0; b < this._count; b++) {
      
      this.Yd[b] = readD();
      this.Ye[b] = readQ();
      if (this.Ye[b] < 1L || ArrayUtils.indexOf(this.Yd, this.Yd[b]) < b) {
        
        this._count = 0;
        
        return;
      } 
    } 
    this._price = readQ();
    
    if (this._price < 0L) {
      
      this._count = 0;
      this._price = 0L;
    } 
  }

  
  protected void runImpl() {
    int i;
    player1 = ((GameClient)getClient()).getActiveChar();
    if (player1 == null) {
      return;
    }
    if (player1.isActionsDisabled()) {
      
      player1.sendActionFailed();
      
      return;
    } 
    
    if (player1.isGM() && this.YZ.equalsIgnoreCase("ONLINE_ALL")) {
      
      HashMap hashMap = new HashMap();
      if (this.Yd != null && this.Yd.length > 0) {
        for (byte b = 0; b < this.Yd.length; b++) {
          
          ItemInstance itemInstance = player1.getInventory().getItemByObjectId(this.Yd[b]);
          hashMap.put(Integer.valueOf(itemInstance.getItemId()), Long.valueOf(this.Ye[b]));
        } 
      }
      for (Player player : GameObjectsStorage.getAllPlayersForIterate()) {
        if (player != null && player.isOnline())
          Functions.sendSystemMail(player, this.Vl, this.Za, hashMap); 
      } 
      player1.sendPacket(ExReplyWritePost.STATIC_TRUE);
      player1.sendPacket(Msg.MAIL_SUCCESSFULLY_SENT);
      
      return;
    } 
    if (!Config.ALLOW_MAIL) {
      
      player1.sendMessage(new CustomMessage("mail.Disabled", player1, new Object[0]));
      player1.sendActionFailed();
      
      return;
    } 
    if (player1.isInStoreMode()) {
      
      player1.sendPacket(Msg.YOU_CANNOT_FORWARD_BECAUSE_THE_PRIVATE_SHOP_OR_WORKSHOP_IS_IN_PROGRESS);
      
      return;
    } 
    if (player1.isInTrade()) {
      
      player1.sendPacket(Msg.YOU_CANNOT_FORWARD_DURING_AN_EXCHANGE);
      
      return;
    } 
    if (player1.getEnchantScroll() != null) {
      
      player1.sendPacket(Msg.YOU_CANNOT_FORWARD_DURING_AN_ITEM_ENHANCEMENT_OR_ATTRIBUTE_ENHANCEMENT);
      
      return;
    } 
    if (player1.getName().equalsIgnoreCase(this.YZ)) {
      
      player1.sendPacket(Msg.YOU_CANNOT_SEND_A_MAIL_TO_YOURSELF);
      
      return;
    } 
    if (this._count > 0 && !player1.isInPeaceZone()) {
      
      player1.sendPacket(Msg.YOU_CANNOT_FORWARD_IN_A_NON_PEACE_ZONE_LOCATION);
      
      return;
    } 
    if (player1.isFishing()) {
      
      player1.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
      
      return;
    } 
    if (this._price > 0L) {
      
      if (!(player1.getPlayerAccess()).UseTrade) {
        
        player1.sendPacket(Msg.THIS_ACCOUNT_CANOT_TRADE_ITEMS);
        player1.sendActionFailed();
        
        return;
      } 
      String str = player1.getVar("tradeBan");
      if (str != null && (str.equals("-1") || Long.parseLong(str) >= System.currentTimeMillis())) {
        
        if (str.equals("-1")) {
          player1.sendMessage(new CustomMessage("common.TradeBannedPermanently", player1, new Object[0]));
        } else {
          player1.sendMessage((new CustomMessage("common.TradeBanned", player1, new Object[0])).addString(Util.formatTime((int)(Long.parseLong(str) / 1000L - System.currentTimeMillis() / 1000L))));
        } 
        
        return;
      } 
    } 
    if (player1.isInBlockList(this.YZ)) {
      
      player1.sendPacket((new SystemMessage(2057)).addString(this.YZ));
      
      return;
    } 
    
    Player player2 = World.getPlayer(this.YZ);
    if (player2 != null) {
      
      i = player2.getObjectId();
      this.YZ = player2.getName();
      if (player2.isInBlockList(player1)) {
        
        player1.sendPacket((new SystemMessage(1228)).addString(this.YZ));

        
        return;
      } 
    } else {
      i = CharacterDAO.getInstance().getObjectIdByName(this.YZ);
      if (i > 0)
      {
        if (mysql.simple_get_int("target_Id", "character_blocklist", "obj_Id=" + i + " AND target_Id=" + player1.getObjectId()) > 0) {
          
          player1.sendPacket((new SystemMessage(1228)).addString(this.YZ));
          return;
        } 
      }
    } 
    if (i == 0) {
      
      player1.sendPacket(Msg.WHEN_THE_RECIPIENT_DOESN_T_EXIST_OR_THE_CHARACTER_HAS_BEEN_DELETED_SENDING_MAIL_IS_NOT_POSSIBLE);
      
      return;
    } 
    int j = ((this.YY == 1) ? '\f' : 'Ũ') * 'ฐ' + (int)(System.currentTimeMillis() / 1000L);
    
    if (this._count > 8) {
      
      player1.sendPacket(SystemMsg.INCORRECT_ITEM_COUNT);
      
      return;
    } 
    long l = (100 + this._count * 1000);
    
    ArrayList arrayList = new ArrayList();
    
    player1.getInventory().writeLock();
    
    try {
      if (player1.getAdena() < l) {
        
        player1.sendPacket(Msg.YOU_CANNOT_FORWARD_BECAUSE_YOU_DON_T_HAVE_ENOUGH_ADENA);
        
        return;
      } 
      
      if (this._count > 0) {
        for (byte b = 0; b < this._count; b++) {
          
          ItemInstance itemInstance = player1.getInventory().getItemByObjectId(this.Yd[b]);
          if (itemInstance == null || itemInstance.getCount() < this.Ye[b] || (itemInstance.getItemId() == 57 && itemInstance.getCount() < this.Ye[b] + l) || !itemInstance.canBeTraded(player1)) {
            
            player1.sendPacket(Msg.THE_ITEM_THAT_YOU_RE_TRYING_TO_SEND_CANNOT_BE_FORWARDED_BECAUSE_IT_ISN_T_PROPER);
            return;
          } 
        } 
      }
      if (!player1.reduceAdena(l, true)) {
        
        player1.sendPacket(Msg.YOU_CANNOT_FORWARD_BECAUSE_YOU_DON_T_HAVE_ENOUGH_ADENA);
        
        return;
      } 
      if (this._count > 0)
      {
        for (byte b = 0; b < this._count; b++)
        {
          ItemInstance itemInstance = player1.getInventory().removeItemByObjectId(this.Yd[b], this.Ye[b]);
          
          Log.LogItem(player1, Log.ItemLog.PostSend, itemInstance);
          
          itemInstance.setOwnerId(player1.getObjectId());
          itemInstance.setLocation(ItemInstance.ItemLocation.MAIL);
          itemInstance.save();
          
          arrayList.add(itemInstance);
        }
      
      }
    } finally {
      
      player1.getInventory().writeUnlock();
    } 
    
    Mail mail = new Mail();
    mail.setSenderId(player1.getObjectId());
    mail.setSenderName(player1.getName());
    mail.setReceiverId(i);
    mail.setReceiverName(this.YZ);
    mail.setTopic(this.Vl);
    mail.setBody(this.Za);
    mail.setPrice((this.YY > 0) ? this._price : 0L);
    mail.setUnread(true);
    mail.setType(Mail.SenderType.NORMAL);
    mail.setExpireTime(j);
    for (ItemInstance itemInstance : arrayList)
      mail.addAttachment(itemInstance); 
    mail.save();
    
    player1.sendPacket(ExReplyWritePost.STATIC_TRUE);
    player1.sendPacket(Msg.MAIL_SUCCESSFULLY_SENT);
    
    if (player2 != null) {
      
      player2.sendPacket(ExNoticePostArrived.STATIC_TRUE);
      player2.sendPacket(Msg.THE_MAIL_HAS_ARRIVED);
    } 
  }
}
