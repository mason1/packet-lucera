package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;





public class RequestDeleteMacro
  extends L2GameClientPacket
{
  private int _id;
  
  protected void readImpl() { this._id = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    player.deleteMacro(this._id);
  }
}
