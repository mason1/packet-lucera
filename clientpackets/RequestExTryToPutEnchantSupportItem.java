package l2.gameserver.network.l2.c2s;

import l2.gameserver.data.xml.holder.EnchantItemHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.PcInventory;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.ExPutEnchantSupportItemResult;
import l2.gameserver.templates.item.support.EnchantCatalyzer;

public class RequestExTryToPutEnchantSupportItem
  extends L2GameClientPacket
{
  private int _itemId;
  private int Zd;
  
  protected void readImpl() {
    this.Zd = readD();
    this._itemId = readD();
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    PcInventory pcInventory = player.getInventory();
    ItemInstance itemInstance1 = pcInventory.getItemByObjectId(this._itemId);
    ItemInstance itemInstance2 = pcInventory.getItemByObjectId(this.Zd);
    
    if (itemInstance1 == null || itemInstance2 == null) {
      
      player.sendPacket(ExPutEnchantSupportItemResult.FAIL);
      
      return;
    } 
    EnchantCatalyzer enchantCatalyzer = EnchantItemHolder.getInstance().getEnchantCatalyzer(itemInstance2.getItemId());
    if (enchantCatalyzer == null || !enchantCatalyzer.isUsableWith(itemInstance1)) {
      
      player.sendPacket(ExPutEnchantSupportItemResult.FAIL);
      return;
    } 
    player.sendPacket(ExPutEnchantSupportItemResult.SUCCESS);
  }
}
