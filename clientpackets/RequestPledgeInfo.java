package l2.gameserver.network.l2.c2s;

import l2.gameserver.model.Player;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.PledgeInfo;
import l2.gameserver.tables.ClanTable;


public class RequestPledgeInfo
  extends L2GameClientPacket
{
  private int Bs;
  
  protected void readImpl() { this.Bs = readD(); }



  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null)
      return; 
    if (this.Bs < 10000000) {
      
      player.sendActionFailed();
      return;
    } 
    Clan clan = ClanTable.getInstance().getClan(this.Bs);
    if (clan == null) {


      
      player.sendActionFailed();
      
      return;
    } 
    player.sendPacket(new PledgeInfo(clan));
  }
}
