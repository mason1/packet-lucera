package l2.gameserver.network.l2.c2s;

import l2.gameserver.cache.CrestCache;
import l2.gameserver.model.Player;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.network.l2.GameClient;

public class RequestSetAllyCrest
  extends L2GameClientPacket
{
  private int ZU;
  private byte[] _data;
  
  protected void readImpl() {
    this.ZU = readD();
    if (this.ZU == 192 && this.ZU == this._buf.remaining()) {
      
      this._data = new byte[this.ZU];
      readB(this._data);
    } 
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    Alliance alliance = player.getAlliance();
    if (alliance != null && player.isAllyLeader()) {
      
      int i = 0;
      
      if (this._data != null && CrestCache.isValidCrestData(this._data)) {
        i = CrestCache.getInstance().saveAllyCrest(alliance.getAllyId(), this._data);
      } else if (alliance.hasAllyCrest()) {
        CrestCache.getInstance().removeAllyCrest(alliance.getAllyId());
      } 
      alliance.setAllyCrestId(i);
      alliance.broadcastAllyStatus();
    } 
  }
}
