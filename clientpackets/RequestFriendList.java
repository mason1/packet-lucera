package l2.gameserver.network.l2.c2s;

import java.util.Map;
import l2.gameserver.cache.Msg;
import l2.gameserver.model.Player;
import l2.gameserver.model.World;
import l2.gameserver.model.actor.instances.player.Friend;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.s2c.SystemMessage;




public class RequestFriendList
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    player.sendPacket(Msg._FRIENDS_LIST_);
    Map map = player.getFriendList().getList();
    for (Map.Entry entry : map.entrySet()) {
      
      Player player1 = World.getPlayer(((Integer)entry.getKey()).intValue());
      if (player1 != null) {
        player.sendPacket((new SystemMessage(488)).addName(player1)); continue;
      } 
      player.sendPacket((new SystemMessage(489)).addString(((Friend)entry.getValue()).getName()));
    } 
    player.sendPacket(Msg.__EQUALS__);
  }
}
