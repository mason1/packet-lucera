package l2.gameserver.network.l2.c2s;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.instancemanager.CastleManorManager;
import l2.gameserver.model.Manor;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.templates.manor.CropProcure;













public class RequestSetCrop
  extends L2GameClientPacket
{
  private int _count;
  private int EJ;
  private RequestSetCropEntry[] ZV = RequestSetCropEntry.EMPTY_ARRAY;
  
  private static class RequestSetCropEntry {
    public static final RequestSetCropEntry[] EMPTY_ARRAY = new RequestSetCropEntry[0];
    
    private final int id;
    private final long ZW;
    private final long price;
    private final int type;
    
    private RequestSetCropEntry(int param1Int1, long param1Long1, long param1Long2, int param1Int2) {
      this.id = param1Int1;
      this.ZW = param1Long1;
      this.price = param1Long2;
      this.type = param1Int2;
    }


    
    public int getId() { return this.id; }



    
    public long getSales() { return this.ZW; }



    
    public long getPrice() { return this.price; }



    
    public int getType() { return this.type; }
  }



  
  protected void readImpl() {
    this.EJ = readD();
    this._count = readD();
    if (this._count * 21 > this._buf.remaining() || this._count > 32767 || this._count < 1) {
      
      this._count = 0;
      return;
    } 
    this.ZV = new RequestSetCropEntry[this._count];
    for (byte b = 0; b < this.ZV.length; b++) {
      
      int i = readD();
      long l1 = readQ();
      long l2 = readQ();
      int j = readC();
      if (i < 1 || l1 < 0L || l2 < 0L || j < 0 || j > 2) {
        
        this._count = 0;
        return;
      } 
      this.ZV[b] = new RequestSetCropEntry(i, l1, l2, j, null);
    } 
  }


  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || this._count == 0) {
      return;
    }
    if (player.getClan() == null) {
      
      player.sendActionFailed();
      
      return;
    } 
    Castle castle = (Castle)ResidenceHolder.getInstance().getResidence(Castle.class, this.EJ);
    if (castle == null || castle.getOwnerId() != player.getClanId() || (player
      .getClanPrivileges() & 0x10000) != 65536) {
      
      player.sendActionFailed();
      
      return;
    } 
    if (castle.isNextPeriodApproved()) {
      
      player.sendPacket(SystemMsg.A_MANOR_CANNOT_BE_SET_UP_BETWEEN_430_AM_AND_8_PM);
      player.sendActionFailed();
      
      return;
    } 
    ArrayList arrayList = new ArrayList(this._count);
    List list = Manor.getInstance().getCropsForCastle(this.EJ);
    for (byte b = 0; b < this._count; b++) {
      
      RequestSetCropEntry requestSetCropEntry = this.ZV[b];
      
      if (requestSetCropEntry != null && requestSetCropEntry.getId() > 0)
      {
        for (Manor.SeedData seedData : list) {
          if (seedData.getCrop() == requestSetCropEntry.getId()) {
            
            if (requestSetCropEntry.getSales() > seedData.getCropLimit()) {
              break;
            }
            long l = Manor.getInstance().getCropBasicPrice(requestSetCropEntry.getId());
            if (requestSetCropEntry.getPrice() != 0L && (requestSetCropEntry.getPrice() < l * 60L / 100L || requestSetCropEntry.getPrice() > l * 10L)) {
              break;
            }
            CropProcure cropProcure = CastleManorManager.getInstance().getNewCropProcure(requestSetCropEntry.getId(), requestSetCropEntry.getSales(), requestSetCropEntry.getType(), requestSetCropEntry.getPrice(), requestSetCropEntry.getSales());
            arrayList.add(cropProcure);
            break;
          } 
        } 
      }
    } 
    castle.setCropProcure(arrayList, 1);
    castle.saveCropData(1);
  }
}
