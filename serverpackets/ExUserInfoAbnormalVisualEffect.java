package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.skills.AbnormalEffect;





public class ExUserInfoAbnormalVisualEffect
  extends L2GameServerPacket
{
  private final int objectId;
  private final int ahl;
  private final AbnormalEffect[] abnormalEffects;
  
  public ExUserInfoAbnormalVisualEffect(Player paramPlayer) {
    this.objectId = paramPlayer.getObjectId();
    this.ahl = paramPlayer.getTransformation();
    this.abnormalEffects = paramPlayer.getAbnormalEffects();
  }


  
  protected void writeImpl() {
    writeEx(344);
    writeD(this.objectId);
    writeD(this.ahl);
    writeD(this.abnormalEffects.length);
    for (AbnormalEffect abnormalEffect : this.abnormalEffects)
    {
      writeH(abnormalEffect.getClientId());
    }
  }
}
