package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.items.ItemInfo;





public class ExRpItemLink
  extends AbstractItemListPacket
{
  private ItemInfo acG;
  
  public ExRpItemLink(ItemInfo paramItemInfo) { this.acG = paramItemInfo; }



  
  protected final void writeImpl() {
    writeEx(109);
    writeItemInfo(this.acG);
  }
}
