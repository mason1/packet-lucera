package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;

public class L2Friend
  extends L2GameServerPacket {
  private boolean aiL;
  private boolean pa;
  private String _name;
  private int aiM;
  
  public L2Friend(Player paramPlayer, boolean paramBoolean) {
    this.aiL = paramBoolean;
    this._name = paramPlayer.getName();
    this.aiM = paramPlayer.getObjectId();
    this.pa = true;
  }

  
  public L2Friend(String paramString, boolean paramBoolean1, boolean paramBoolean2, int paramInt) {
    this._name = paramString;
    this.aiL = paramBoolean1;
    this.aiM = paramInt;
    this.pa = paramBoolean2;
  }


  
  protected final void writeImpl() {
    writeC(118);
    writeD(this.aiL ? 1 : 3);
    writeD(0);
    writeS(this._name);
    writeD(this.pa ? 1 : 0);
    writeD(this.aiM);
  }
}
