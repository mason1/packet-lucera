package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Summon;
import l2.gameserver.utils.Location;

public class PetStatusUpdate extends L2GameServerPacket {
  private int type;
  private int acr;
  private int level;
  private int ajx;
  private int ajw;
  private int maxHp;
  private int curHp;
  
  public PetStatusUpdate(Summon paramSummon) {
    this.type = paramSummon.getSummonType();
    this.acr = paramSummon.getObjectId();
    this._loc = paramSummon.getLoc();
    this.title = paramSummon.getTitle();
    this.curHp = (int)paramSummon.getCurrentHp();
    this.maxHp = paramSummon.getMaxHp();
    this.curMp = (int)paramSummon.getCurrentMp();
    this.maxMp = paramSummon.getMaxMp();
    this.ajw = paramSummon.getCurrentFed();
    this.ajx = paramSummon.getMaxFed();
    this.level = paramSummon.getLevel();
    this.ajI = paramSummon.getExp();
    this.ajJ = paramSummon.getExpForThisLevel();
    this.ajK = paramSummon.getExpForNextLevel();
  }
  
  private int maxMp;
  
  protected final void writeImpl() {
    writeC(182);
    writeD(this.type);
    writeD(this.acr);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
    writeS(this.title);
    writeD(this.ajw);
    writeD(this.ajx);
    writeD(this.curHp);
    writeD(this.maxHp);
    writeD(this.curMp);
    writeD(this.maxMp);
    writeD(this.level);
    writeQ(this.ajI);
    writeQ(this.ajJ);
    writeQ(this.ajK);
  }
  
  private int curMp;
  private long ajI;
  private long ajJ;
  private long ajK;
  private Location _loc;
  private String title;
}
