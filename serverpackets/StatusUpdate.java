package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Creature;
import l2.gameserver.model.GameObject;





























public class StatusUpdate
  extends L2GameServerPacket
{
  public static final int CUR_HP = 9;
  public static final int MAX_HP = 10;
  public static final int CUR_MP = 11;
  public static final int MAX_MP = 12;
  public static final int CUR_LOAD = 14;
  public static final int MAX_LOAD = 15;
  public static final int PVP_FLAG = 26;
  public static final int KARMA = 27;
  public static final int CUR_CP = 33;
  public static final int MAX_CP = 34;
  private final int Bq;
  private final List<Attribute> alO;
  private boolean alP;
  private int alQ;
  
  class Attribute
  {
    public final int id;
    public final int value;
    
    Attribute(int param1Int1, int param1Int2) {
      this.id = param1Int1;
      this.value = param1Int2;
    }
  }
  
  public StatusUpdate(Creature paramCreature) {
    this.alO = new ArrayList();
    this.Bq = paramCreature.getObjectId();
  }
  
  public StatusUpdate(int paramInt) {
    this.alO = new ArrayList();
    this.Bq = paramInt;
  }

  
  public StatusUpdate addAttribute(int paramInt1, int paramInt2) {
    this.alO.add(new Attribute(paramInt1, paramInt2));
    switch (paramInt1) {
      
      case 9:
      case 11:
      case 33:
        this.alP = true;
        break;
    } 
    return this;
  }

  
  public StatusUpdate setAttackerObjectId(int paramInt) {
    this.alQ = paramInt;
    return this;
  }

  
  public StatusUpdate setAttackerObject(GameObject paramGameObject) {
    this.alQ = paramGameObject.getObjectId();
    return this;
  }


  
  protected final void writeImpl() {
    writeC(24);
    writeD(this.Bq);
    writeD(this.alP ? this.alQ : 0);
    writeC(this.alP ? 1 : 0);
    writeC(this.alO.size());
    for (Attribute attribute : this.alO) {
      
      writeC(attribute.id);
      writeD(attribute.value);
    } 
  }


  
  public boolean hasAttributes() { return !this.alO.isEmpty(); }
}
