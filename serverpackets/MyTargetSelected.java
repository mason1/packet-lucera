package l2.gameserver.network.l2.s2c;






















public class MyTargetSelected
  extends L2GameServerPacket
{
  private int Bq;
  private int agp;
  
  public MyTargetSelected(int paramInt1, int paramInt2) {
    this.Bq = paramInt1;
    this.agp = paramInt2;
  }


  
  protected final void writeImpl() {
    writeC(185);
    writeD(1);
    writeD(this.Bq);
    writeH(this.agp);
    writeD(0);
  }
}
