package l2.gameserver.network.l2.s2c;

import java.util.Collection;
import java.util.List;
import l2.gameserver.model.Player;
import l2.gameserver.model.Recipe;
import l2.gameserver.model.items.ManufactureItem;


public class RecipeShopManageList
  extends L2GameServerPacket
{
  private List<ManufactureItem> akI;
  private Collection<Recipe> akJ;
  private int akK;
  private long adc;
  private boolean akL;
  
  public RecipeShopManageList(Player paramPlayer, boolean paramBoolean) {
    this.akK = paramPlayer.getObjectId();
    this.adc = paramPlayer.getAdena();
    this.akL = paramBoolean;
    if (this.akL) {
      this.akJ = paramPlayer.getDwarvenRecipeBook();
    } else {
      this.akJ = paramPlayer.getCommonRecipeBook();
    }  this.akI = paramPlayer.getCreateList();
    for (ManufactureItem manufactureItem : this.akI) {
      
      if (!paramPlayer.findRecipe(manufactureItem.getRecipeId())) {
        this.akI.remove(manufactureItem);
      }
    } 
  }

  
  protected final void writeImpl() {
    writeC(222);
    writeD(this.akK);
    writeD((int)Math.min(this.adc, 2147483647L));
    writeD(this.akL ? 0 : 1);
    writeD(this.akJ.size());
    byte b = 1;
    for (Recipe recipe : this.akJ) {
      
      writeD(recipe.getId());
      writeD(b++);
    } 
    writeD(this.akI.size());
    for (ManufactureItem manufactureItem : this.akI) {
      
      writeD(manufactureItem.getRecipeId());
      writeD(0);
      writeQ(manufactureItem.getCost());
    } 
  }
}
