package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.commons.lang.ArrayUtils;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.Warehouse;






public class PackageSendableList
  extends AbstractItemListPacket
{
  private int aae;
  private long abb;
  private List<ItemInfo> aji;
  
  public PackageSendableList(int paramInt, Player paramPlayer) {
    this.abb = paramPlayer.getAdena();
    this.aae = paramInt;
    
    ItemInstance[] arrayOfItemInstance = paramPlayer.getInventory().getItems();
    ArrayUtils.eqSort(arrayOfItemInstance, Warehouse.ItemClassComparator.getInstance());
    this.aji = new ArrayList(arrayOfItemInstance.length);
    for (ItemInstance itemInstance : arrayOfItemInstance) {
      if (itemInstance.getTemplate().isFreightable()) {
        this.aji.add(new ItemInfo(itemInstance));
      }
    } 
  }
  
  protected final void writeImpl() {
    writeC(210);
    writeD(this.aae);
    writeQ(this.abb);
    writeD(this.aji.size());
    for (ItemInfo itemInfo : this.aji) {
      
      writeItemInfo(itemInfo);
      writeD(itemInfo.getObjectId());
    } 
  }
}
