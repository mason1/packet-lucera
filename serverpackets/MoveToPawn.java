package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;
import l2.gameserver.model.GameObject;

public class MoveToPawn extends L2GameServerPacket {
  private int abo;
  private int _targetId;
  private int ajb;
  private int _x;
  
  public MoveToPawn(Creature paramCreature, GameObject paramGameObject, int paramInt) {
    this.abo = paramCreature.getObjectId();
    this._targetId = paramGameObject.getObjectId();
    this.ajb = paramInt;
    this._x = paramCreature.getX();
    this._y = paramCreature.getY();
    this._z = paramCreature.getZ();
    this.aaW = paramGameObject.getX();
    this.aaX = paramGameObject.getY();
    this.aaY = paramGameObject.getZ();
  }
  
  private int _y;
  
  protected final void writeImpl() {
    writeC(114);
    
    writeD(this.abo);
    writeD(this._targetId);
    writeD(this.ajb);
    
    writeD(this._x);
    writeD(this._y);
    writeD(this._z);
    
    writeD(this.aaW);
    writeD(this.aaX);
    writeD(this.aaY);
  }
  
  private int _z;
  private int aaW;
  private int aaX;
  private int aaY;
}
