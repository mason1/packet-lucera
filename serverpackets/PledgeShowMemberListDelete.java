package l2.gameserver.network.l2.s2c;


public class PledgeShowMemberListDelete
  extends L2GameServerPacket
{
  private String ako;
  
  public PledgeShowMemberListDelete(String paramString) { this.ako = paramString; }



  
  protected final void writeImpl() {
    writeC(93);
    writeS(this.ako);
  }
}
