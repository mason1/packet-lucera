package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.mail.Mail;









public class ExReplySentPost
  extends AbstractItemListPacket
{
  private final Mail UW;
  
  public ExReplySentPost(Mail paramMail) { this.UW = paramMail; }




  
  protected void writeImpl() {
    writeEx(174);
    
    writeD(this.UW.getType().ordinal());
    writeD(this.UW.getMessageId());
    writeD(this.UW.isPayOnDelivery() ? 1 : 0);
    
    writeS(this.UW.getReceiverName());
    writeS(this.UW.getTopic());
    writeS(this.UW.getBody());
    
    writeD(this.UW.getAttachments().size());
    for (ItemInstance itemInstance : this.UW.getAttachments()) {
      
      writeItemInfo(new ItemInfo(itemInstance));
      writeD(itemInstance.getObjectId());
    } 
    
    writeQ(this.UW.getPrice());
    writeD(0);
    writeD(0);
  }
}
