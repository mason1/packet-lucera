package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import l2.gameserver.model.Player;
import l2.gameserver.model.SubClass;

public class ExSubjobInfo
  extends L2GameServerPacket
{
  private final int ahf;
  private final int ahg;
  private final int raceId;
  private List<SubInfo> ahh;
  
  private static class SubInfo
  {
    int index;
    int classId;
    int level;
    int type;
    
    private SubInfo() {}
  }
  
  public ExSubjobInfo(Player paramPlayer) {
    this.ahh = new ArrayList(4);


    
    this.ahf = paramPlayer.getClassId().getId();
    this.raceId = paramPlayer.getRace().ordinal();
    
    SubClass subClass = paramPlayer.getActiveClass();
    this.ahg = subClass.isBase() ? 0 : 0;
    
    Map map = paramPlayer.getSubClasses();
    boolean bool = false;
    for (SubClass subClass1 : map.values()) {
      
      SubInfo subInfo = new SubInfo(null);
      subInfo.index = bool;
      subInfo.classId = subClass1.getClassId();
      subInfo.level = subClass1.getLevel();
      subInfo.type = subClass1.isBase() ? 0 : 0;
      
      this.ahh.add(subInfo);
    } 
  }


  
  protected void writeImpl() {
    writeEx(234);
    
    writeC(this.ahg);
    writeD(this.ahf);
    writeD(this.raceId);
    writeD(this.ahh.size());
    for (SubInfo subInfo : this.ahh) {
      
      writeD(subInfo.index);
      writeD(subInfo.classId);
      writeD(subInfo.level);
      writeC(subInfo.type);
    } 
  }
}
