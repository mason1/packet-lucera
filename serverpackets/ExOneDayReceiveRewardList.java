package l2.gameserver.network.l2.s2c;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import l2.gameserver.data.xml.holder.OneDayRewardHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.oneDayReward.OneDayReward;
import l2.gameserver.model.entity.oneDayReward.OneDayRewardRequirement;
import l2.gameserver.model.entity.oneDayReward.OneDayRewardStatus;
import l2.gameserver.taskmanager.actionrunner.tasks.OneDayRewardMonthlyResetTask;
import l2.gameserver.taskmanager.actionrunner.tasks.OneDayRewardWeeklyResetTask;
import l2.gameserver.taskmanager.tasks.OneDayRewardDailyResetTask;

public class ExOneDayReceiveRewardList extends L2GameServerPacket {
  private final int afa;
  private final DayOfWeek afb;
  private final List<RewardInfo> afc;
  private int afd;
  private int afe;
  private int aff;
  
  public ExOneDayReceiveRewardList(Player paramPlayer) {
    this.afc = new ArrayList();




    
    this.afa = paramPlayer.getActiveClassId();
    this.afb = LocalDate.now().getDayOfWeek();
    
    this.afd = (int)(OneDayRewardDailyResetTask.getInstance().getLeftTime() / 1000L);
    this.afe = (int)(OneDayRewardWeeklyResetTask.getInstance().getLeftTime() / 1000L);
    this.aff = (int)(OneDayRewardMonthlyResetTask.getInstance().getLeftTime() / 1000L);
    
    Collection collection = OneDayRewardHolder.getInstance().getOneDayRewards(paramPlayer);
    
    for (OneDayReward oneDayReward : collection) {





      
      byte b = 2;
      OneDayRewardStatus oneDayRewardStatus = paramPlayer.getOneDayRewardStore().getStatus(oneDayReward);
      
      OneDayRewardRequirement oneDayRewardRequirement = oneDayReward.getRequirement();
      if (oneDayRewardStatus.isReceived()) {
        
        b = 3;
      }
      else if (oneDayRewardRequirement != null && oneDayRewardRequirement.isDone(oneDayRewardStatus)) {
        
        b = 1;
      } 
      
      RewardInfo rewardInfo = new RewardInfo(oneDayReward.getId(), b, (oneDayRewardRequirement != null), (oneDayRewardRequirement != null) ? oneDayRewardRequirement.getRequiredProgress() : 0, oneDayRewardStatus.getCurrentProgress(), null);
      this.afc.add(rewardInfo);
    } 
  }


  
  protected void writeImpl() {
    writeEx(424);
    writeD(this.afd);
    writeD(this.afe);
    writeD(this.aff);
    
    writeC(0);
    
    writeD(this.afa);
    writeD(this.afb.ordinal());
    writeD(this.afc.size());
    for (RewardInfo rewardInfo : this.afc) {
      
      writeH(rewardInfo.id);
      writeC(rewardInfo.status);
      writeC(rewardInfo.hasProgress);
      writeD(rewardInfo.currentProgress);
      writeD(rewardInfo.requiredProgress);
    } 
  }

  
  private static class RewardInfo
  {
    int id;
    int status;
    boolean hasProgress;
    int requiredProgress;
    int currentProgress;
    
    private RewardInfo(int param1Int1, int param1Int2, boolean param1Boolean, int param1Int3, int param1Int4) {
      this.id = param1Int1;
      this.status = param1Int2;
      this.hasProgress = param1Boolean;
      this.requiredProgress = param1Int3;
      this.currentProgress = param1Int4;
    }
  }
}
