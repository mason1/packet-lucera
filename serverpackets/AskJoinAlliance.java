package l2.gameserver.network.l2.s2c;












public class AskJoinAlliance
  extends L2GameServerPacket
{
  private String aaT;
  private String aaU;
  private int aaV;
  
  public AskJoinAlliance(int paramInt, String paramString1, String paramString2) {
    this.aaT = paramString1;
    this.aaU = paramString2;
    this.aaV = paramInt;
  }


  
  protected final void writeImpl() {
    writeC(187);
    writeD(this.aaV);
    writeS(this.aaT);
    writeS("");
    writeS(this.aaU);
  }
}
