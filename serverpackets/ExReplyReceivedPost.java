package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.mail.Mail;











public class ExReplyReceivedPost
  extends AbstractItemListPacket
{
  private final Mail UW;
  
  public ExReplyReceivedPost(Mail paramMail) { this.UW = paramMail; }




  
  protected void writeImpl() {
    writeEx(172);
    
    writeD(this.UW.getType().ordinal());
    writeD(this.UW.getMessageId());
    writeD(this.UW.isPayOnDelivery() ? 1 : 0);
    writeD((this.UW.getType() == Mail.SenderType.NORMAL) ? 0 : 1);
    
    writeS(this.UW.getSenderName());
    writeS(this.UW.getTopic());
    writeS(this.UW.getBody());
    
    writeD(this.UW.getAttachments().size());
    for (ItemInstance itemInstance : this.UW.getAttachments()) {
      
      writeItemInfo(new ItemInfo(itemInstance));
      writeD(itemInstance.getObjectId());
    } 
    
    writeQ(this.UW.getPrice());
    writeD((this.UW.getAttachments().size() > 0) ? 1 : 0);
  }
}
