package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.instances.StaticObjectInstance;




public class ChairSit
  extends L2GameServerPacket
{
  private int Bq;
  private int abn;
  
  public ChairSit(Player paramPlayer, StaticObjectInstance paramStaticObjectInstance) {
    this.Bq = paramPlayer.getObjectId();
    this.abn = paramStaticObjectInstance.getUId();
  }


  
  protected final void writeImpl() {
    writeC(237);
    writeD(this.Bq);
    writeD(this.abn);
  }
}
