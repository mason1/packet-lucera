package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;
import l2.gameserver.utils.Location;




public class ValidateLocation
  extends L2GameServerPacket
{
  private int ahw;
  private Location _loc;
  
  public ValidateLocation(Creature paramCreature) {
    this.ahw = paramCreature.getObjectId();
    this._loc = paramCreature.getLoc();
  }


  
  protected final void writeImpl() {
    writeC(121);
    
    writeD(this.ahw);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
    writeD(this._loc.h);
  }
}
