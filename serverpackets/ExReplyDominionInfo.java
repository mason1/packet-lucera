package l2.gameserver.network.l2.s2c;

import java.util.Collections;
import java.util.List;

public class ExReplyDominionInfo
  extends L2GameServerPacket
{
  private List<TerritoryInfo> afW = Collections.emptyList();


  
  protected void writeImpl() {
    writeEx(146);
    writeD(this.afW.size());
    for (TerritoryInfo territoryInfo : this.afW) {
      
      writeD(territoryInfo.id);
      writeS(territoryInfo.terr);
      writeS(territoryInfo.clan);
      writeD(territoryInfo.flags.length);
      for (int i : territoryInfo.flags)
        writeD(i); 
      writeD(territoryInfo.startTime);
    } 
  }

  
  private class TerritoryInfo
  {
    public int id;
    public String terr;
    public String clan;
    public int[] flags;
    public int startTime;
    
    public TerritoryInfo(int param1Int1, String param1String1, String param1String2, int[] param1ArrayOfInt, int param1Int2) {
      this.id = param1Int1;
      this.terr = param1String1;
      this.clan = param1String2;
      this.flags = param1ArrayOfInt;
      this.startTime = param1Int2;
    }
  }
}
