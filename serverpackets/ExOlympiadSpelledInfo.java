package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Player;


public class ExOlympiadSpelledInfo
  extends L2GameServerPacket
{
  private int char_obj_id = 0;



  
  private List<AbnormalStatusUpdate.Effect> DF = new ArrayList();




  
  public void addEffect(int paramInt1, int paramInt2, int paramInt3) { this.DF.add(new AbnormalStatusUpdate.Effect(paramInt1, paramInt2, paramInt3, 0, 0)); }


  
  public void addSpellRecivedPlayer(Player paramPlayer) {
    if (paramPlayer != null) {
      this.char_obj_id = paramPlayer.getObjectId();
    }
  }

  
  protected final void writeImpl() {
    writeEx(124);
    
    writeD(this.char_obj_id);
    writeD(this.DF.size());
    for (AbnormalStatusUpdate.Effect effect : this.DF) {
      
      writeD(effect.skillId);
      writeH(effect.skillLevel);
      writeH(0);
      writeD(effect.clientAbnormalId);
      writeOptionalD(effect.duration);
    } 
  }
}
