package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.instances.PetInstance;
import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.ItemInstance;


public class PetItemList
  extends AbstractItemListPacket
{
  private ItemInstance[] ajN;
  
  public PetItemList(PetInstance paramPetInstance) { this.ajN = paramPetInstance.getInventory().getItems(); }



  
  protected final void writeImpl() {
    writeC(179);
    writeH(this.ajN.length);
    
    for (ItemInstance itemInstance : this.ajN)
      writeItemInfo(new ItemInfo(itemInstance)); 
  }
}
