package l2.gameserver.network.l2.s2c;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.templates.manor.CropProcure;







public class ExShowProcureCropDetail
  extends L2GameServerPacket
{
  private int _cropId;
  private Map<Integer, CropProcure> agL;
  
  public ExShowProcureCropDetail(int paramInt) {
    this._cropId = paramInt;
    this.agL = new TreeMap();
    
    List list = ResidenceHolder.getInstance().getResidenceList(Castle.class);
    for (Castle castle : list) {
      
      CropProcure cropProcure = castle.getCrop(this._cropId, 0);
      if (cropProcure != null && cropProcure.getAmount() > 0L) {
        this.agL.put(Integer.valueOf(castle.getId()), cropProcure);
      }
    } 
  }

  
  public void writeImpl() {
    writeEx(120);
    
    writeD(this._cropId);
    writeD(this.agL.size());
    
    for (Iterator iterator = this.agL.keySet().iterator(); iterator.hasNext(); ) { int i = ((Integer)iterator.next()).intValue();
      
      CropProcure cropProcure = (CropProcure)this.agL.get(Integer.valueOf(i));
      writeD(i);
      writeQ(cropProcure.getAmount());
      writeQ(cropProcure.getPrice());
      writeC(cropProcure.getReward()); }
  
  }
}
