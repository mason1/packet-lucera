package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.instances.DoorInstance;


@Deprecated
public class DoorStatusUpdate
  extends L2GameServerPacket
{
  private final int acu;
  private final boolean acv;
  private final int _dmg;
  private final boolean acw;
  private final int acx;
  private final int acy;
  private final int _maxHp;
  
  public DoorStatusUpdate(DoorInstance paramDoorInstance, Player paramPlayer) {
    this.acu = paramDoorInstance.getObjectId();
    this.acx = paramDoorInstance.getDoorId();
    this.acv = !paramDoorInstance.isOpen();
    this.acw = paramDoorInstance.isAutoAttackable(paramPlayer);
    this.acy = (int)paramDoorInstance.getCurrentHp();
    this._maxHp = paramDoorInstance.getMaxHp();
    this._dmg = paramDoorInstance.getDamage();
  }


  
  protected void writeImpl() {
    writeC(77);
    writeD(this.acu);
    writeD(this.acv);
    writeD(this._dmg);
    writeD(this.acw);
    writeD(this.acx);
    writeD(this._maxHp);
    writeD(this.acy);
  }
}
