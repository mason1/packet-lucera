package l2.gameserver.network.l2.s2c;

import l2.commons.lang.ArrayUtils;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.Warehouse;

public class GMViewWarehouseWithdrawList
  extends AbstractItemListPacket
{
  private final ItemInstance[] adl;
  private final String adj;
  private final long aiy;
  
  public GMViewWarehouseWithdrawList(Player paramPlayer) {
    this.adj = paramPlayer.getName();
    this.aiy = paramPlayer.getAdena();
    this.adl = paramPlayer.getWarehouse().getItems();
    ArrayUtils.eqSort(this.adl, Warehouse.ItemClassComparator.getInstance());
  }


  
  protected final void writeImpl() {
    writeC(155);
    writeS(this.adj);
    writeQ(this.aiy);
    writeH(this.adl.length);
    for (ItemInstance itemInstance : this.adl) {
      
      writeItemInfo(new ItemInfo(itemInstance));
      writeD(itemInstance.getObjectId());
    } 
  }
}
