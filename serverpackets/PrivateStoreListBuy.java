package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.TradeItem;







public class PrivateStoreListBuy
  extends AbstractItemListPacket
{
  private int ZG;
  private long abb;
  private List<TradeItem> Gl;
  
  public PrivateStoreListBuy(Player paramPlayer1, Player paramPlayer2) {
    this.abb = paramPlayer1.getAdena();
    this.ZG = paramPlayer2.getObjectId();
    this.Gl = new ArrayList();
    
    List list = paramPlayer2.getBuyList();
    ItemInstance[] arrayOfItemInstance = paramPlayer1.getInventory().getItems();
    
    for (TradeItem tradeItem1 : list) {
      
      TradeItem tradeItem2 = null;
      for (ItemInstance itemInstance : arrayOfItemInstance) {
        if (itemInstance.getItemId() == tradeItem1.getItemId() && itemInstance.canBeTraded(paramPlayer1)) {
          
          tradeItem2 = new TradeItem(itemInstance);
          this.Gl.add(tradeItem2);
          tradeItem2.setOwnersPrice(tradeItem1.getOwnersPrice());
          tradeItem2.setCount(tradeItem1.getCount());
          tradeItem2.setCurrentValue(Math.min(tradeItem1.getCount(), itemInstance.getCount()));
        } 
      }  if (tradeItem2 == null) {
        
        tradeItem2 = new TradeItem();
        tradeItem2.setItemId(tradeItem1.getItemId());
        tradeItem2.setOwnersPrice(tradeItem1.getOwnersPrice());
        tradeItem2.setCount(tradeItem1.getCount());
        tradeItem2.setCurrentValue(0L);
        this.Gl.add(tradeItem2);
      } 
    } 
  }


  
  protected final void writeImpl() {
    writeC(190);
    
    writeD(this.ZG);
    writeQ(this.abb);
    writeD(0);
    writeD(this.Gl.size());
    for (TradeItem tradeItem : this.Gl) {
      
      writeItemInfo(tradeItem, tradeItem.getCurrentValue());
      writeD(tradeItem.getObjectId());
      writeQ(tradeItem.getOwnersPrice());
      writeQ(tradeItem.getStorePrice());
      writeQ(tradeItem.getCount());
    } 
  }
}
