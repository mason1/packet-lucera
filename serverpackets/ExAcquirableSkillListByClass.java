package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.base.AcquireType;


public class ExAcquirableSkillListByClass
  extends L2GameServerPacket
{
  private AcquireType XO;
  private final List<Skill> _skills;
  
  class Skill
  {
    public int id;
    public int nextLevel;
    public int getLevel;
    public int cost;
    public int requirements;
    public int subUnit;
    
    Skill(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6) {
      this.id = param1Int1;
      this.nextLevel = param1Int2;
      this.getLevel = param1Int3;
      this.cost = param1Int4;
      this.requirements = param1Int5;
      this.subUnit = param1Int6;
    }
  }

  
  public ExAcquirableSkillListByClass(AcquireType paramAcquireType, int paramInt) {
    this._skills = new ArrayList(paramInt);
    this.XO = paramAcquireType;
  }


  
  public void addSkill(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) { this._skills.add(new Skill(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6)); }



  
  public void addSkill(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) { this._skills.add(new Skill(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, 0)); }



  
  protected final void writeImpl() {
    writeEx(250);
    writeH(this.XO.ordinal());
    writeH(this._skills.size());
    
    for (Skill skill : this._skills) {
      
      writeD(skill.id);
      writeH(skill.nextLevel);
      writeH(skill.nextLevel);
      writeC(skill.getLevel);
      writeQ(skill.cost);
      writeC(skill.requirements);
      if (this.XO == AcquireType.SUB_UNIT)
        writeH(skill.subUnit); 
    } 
  }
}
