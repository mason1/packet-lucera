package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.tables.SkillTreeTable;




public class SkillList
  extends L2GameServerPacket
{
  private final List<SkillListEntry> _skills = new ArrayList();
  private int alw = 0;

  
  public SkillList(Player paramPlayer) {
    for (Skill skill : paramPlayer.getAllSkills()) {

      
      int i = skill.getDisplayLevel();
      int j = i;
      int k = 0;
      if (i > 100) {
        
        j = skill.getBaseLevel();
        int m = i % 100;
        k = (1 + m / 40) * 1000 + m % 40;
      } 
      this._skills.add(new SkillListEntry(skill
            .getDisplayId(), j, k, -1, (


            
            !skill.isActive() && !skill.isToggle()), paramPlayer
            .isUnActiveSkill(skill.getId()), (paramPlayer
            .getTransformation() == 0 && SkillTreeTable.isEnchantable(skill) != 0)));
    } 
  }



  
  public void setLastLearnedSkillId(int paramInt) { this.alw = paramInt; }



  
  protected final void writeImpl() {
    writeC(95);
    writeD(this._skills.size());
    
    for (SkillListEntry skillListEntry : this._skills) {
      
      writeD(skillListEntry.passive ? 1 : 0);
      writeH(skillListEntry.level);
      writeH(skillListEntry.subLevel);
      writeD(skillListEntry.id);
      writeD(skillListEntry.reuseDelayGroup);
      writeC(skillListEntry.disabled ? 1 : 0);
      writeC(skillListEntry.enchanted ? 1 : 0);
    } 
    
    writeD(this.alw);
  }

  
  static class SkillListEntry
  {
    public int id;
    public int level;
    public int subLevel;
    public int reuseDelayGroup;
    public boolean passive;
    public boolean disabled;
    public boolean enchanted;
    
    SkillListEntry(int param1Int1, int param1Int2, int param1Int3, int param1Int4, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3) {
      this.id = param1Int1;
      this.level = param1Int2;
      this.subLevel = param1Int3;
      this.reuseDelayGroup = param1Int4;
      this.passive = param1Boolean1;
      this.disabled = param1Boolean2;
      this.enchanted = param1Boolean3;
    }
  }
}
