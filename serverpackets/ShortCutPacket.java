package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.actor.instances.player.ShortCut;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.skills.TimeStamp;
import l2.gameserver.tables.SkillTable;



public abstract class ShortCutPacket
  extends L2GameServerPacket
{
  public static ShortcutInfo convert(Player paramPlayer, ShortCut paramShortCut) {
    ItemInstance itemInstance;
    int i1, n, m, k, j;
    null = null;
    int i = paramShortCut.getSlot() + paramShortCut.getPage() * 12;
    switch (paramShortCut.getType()) {
      
      case 1:
        j = -1; k = 0; m = 0; n = 0; i1 = 0;
        itemInstance = paramPlayer.getInventory().getItemByObjectId(paramShortCut.getId());
        if (itemInstance != null) {
          
          n = itemInstance.getVariationStat1();
          i1 = itemInstance.getVariationStat2();
          j = itemInstance.getTemplate().getDisplayReuseGroup();
          if (itemInstance.getTemplate().getReuseDelay() > 0) {
            
            TimeStamp timeStamp = paramPlayer.getSharedGroupReuse(itemInstance.getTemplate().getReuseGroup());
            if (timeStamp != null) {
              
              k = (int)Math.round(timeStamp.getReuseCurrent() / 1000.0D);
              m = (int)Math.round(timeStamp.getReuseBasic() / 1000.0D);
            } 
          } 
        } 
        return new ItemShortcutInfo(paramShortCut.getType(), i, paramShortCut.getId(), j, k, m, n, i1, paramShortCut.getCharacterType());
      
      case 2:
        if (paramShortCut.getLevel() < 100) {
          SkillShortcutInfo skillShortcutInfo = new SkillShortcutInfo(paramShortCut.getType(), i, paramShortCut.getId(), paramShortCut.getLevel(), paramShortCut.getCharacterType(), -1);
        } else {
          
          int i2 = SkillTable.getInstance().getBaseLevel(paramShortCut.getId());
          int i3 = paramShortCut.getLevel() % 100;
          int i4 = (1 + i3 / 40) * 1000 + i3 % 40;
          null = new SkillShortcutInfo(paramShortCut.getType(), i, paramShortCut.getId(), i4 << 16 | i2, paramShortCut.getCharacterType(), -1);
        } 




        
        return null;
    } 
    return new ShortcutInfo(paramShortCut.getType(), i, paramShortCut.getId(), paramShortCut.getCharacterType());
  }
  
  protected static class ItemShortcutInfo extends ShortcutInfo {
    private int alh;
    private int ali;
    private int alj;
    private int alk;
    private int all;
    
    public ItemShortcutInfo(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, int param1Int7, int param1Int8, int param1Int9) {
      super(param1Int1, param1Int2, param1Int3, param1Int9);
      this.alh = param1Int4;
      this.ali = param1Int5;
      this.alj = param1Int6;
      this.alk = param1Int7;
      this.all = param1Int8;
    }


    
    protected void write0(ShortCutPacket param1ShortCutPacket) {
      param1ShortCutPacket.writeD(this._id);
      param1ShortCutPacket.writeD(this._characterType);
      param1ShortCutPacket.writeD(this.alh);
      param1ShortCutPacket.writeD(this.ali);
      param1ShortCutPacket.writeD(this.alj);
      param1ShortCutPacket.writeD(this.alk);
      param1ShortCutPacket.writeD(this.all);
      param1ShortCutPacket.writeD(0);
    }
  }
  
  protected static class SkillShortcutInfo
    extends ShortcutInfo
  {
    private final int _level;
    private final int alm;
    
    public SkillShortcutInfo(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6) {
      super(param1Int1, param1Int2, param1Int3, param1Int5);
      this._level = param1Int4;
      this.alm = param1Int6;
    }


    
    protected void write0(ShortCutPacket param1ShortCutPacket) {
      param1ShortCutPacket.writeD(this._id);
      param1ShortCutPacket.writeD(this._level);
      param1ShortCutPacket.writeD(this.alm);
      param1ShortCutPacket.writeC(0);
      param1ShortCutPacket.writeD(this._characterType);
    }
  }

  
  protected static class ShortcutInfo
  {
    protected final int _type;
    protected final int _page;
    protected final int _id;
    protected final int _characterType;
    
    public ShortcutInfo(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      this._type = param1Int1;
      this._page = param1Int2;
      this._id = param1Int3;
      this._characterType = param1Int4;
    }

    
    protected void write(ShortCutPacket param1ShortCutPacket) {
      param1ShortCutPacket.writeD(this._type);
      param1ShortCutPacket.writeD(this._page);
      write0(param1ShortCutPacket);
    }

    
    protected void write0(ShortCutPacket param1ShortCutPacket) {
      param1ShortCutPacket.writeD(this._id);
      param1ShortCutPacket.writeD(this._characterType);
    }
  }
}
