package l2.gameserver.network.l2.s2c;



public class AutoAttackStart
  extends L2GameServerPacket
{
  private int _targetId;
  
  public AutoAttackStart(int paramInt) { this._targetId = paramInt; }



  
  protected final void writeImpl() {
    writeC(37);
    writeD(this._targetId);
  }
}
