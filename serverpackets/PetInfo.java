package l2.gameserver.network.l2.s2c;public class PetInfo extends L2GameServerPacket { private int abq; private int abr; private int ajs; private int ajt; private int pvp_flag; private int karma;
  private int aju;
  private int _type;
  private int acr;
  private int afC;
  private int ajv;
  private int incombat;
  private int dead;
  private int _sp;
  private int level;
  private int ajw;
  private int ajx;
  private int curHp;
  private int maxHp;
  private int curMp;
  private int maxMp;
  private int aho;
  private int ahp;
  private int ajy;
  private int ajz;
  private int ajA;
  private int ajB;
  private int ajC;
  private int ajD;
  
  public PetInfo(Summon paramSummon) {
    this._type = paramSummon.getSummonType();
    this.acr = paramSummon.getObjectId();
    this.afC = (paramSummon.getTemplate()).npcId;
    this._loc = paramSummon.getLoc();
    this.ajs = paramSummon.getMAtkSpd();
    this.ajt = paramSummon.getPAtkSpd();
    this.abq = paramSummon.getRunSpeed();
    this.abr = paramSummon.getWalkSpeed();
    this.ajH = paramSummon.getColRadius();
    this.abD = paramSummon.getColHeight();
    this.ajv = paramSummon.isRunning() ? 1 : 0;
    this.incombat = paramSummon.isInCombat() ? 1 : 0;
    this.dead = paramSummon.isAlikeDead() ? 1 : 0;
    this._name = paramSummon.getName().equalsIgnoreCase((paramSummon.getTemplate()).name) ? "" : paramSummon.getName();
    this.title = paramSummon.getTitle();
    this.pvp_flag = paramSummon.getPvpFlag();
    this.karma = paramSummon.getKarma();
    this.ajw = paramSummon.getCurrentFed();
    this.ajx = paramSummon.getMaxFed();
    this.curHp = (int)paramSummon.getCurrentHp();
    this.maxHp = paramSummon.getMaxHp();
    this.curMp = (int)paramSummon.getCurrentMp();
    this.maxMp = paramSummon.getMaxMp();
    this._sp = paramSummon.getSp();
    this.level = paramSummon.getLevel();
    this.ajI = paramSummon.getExp();
    this.ajJ = paramSummon.getExpForThisLevel();
    this.ajK = paramSummon.getExpForNextLevel();
    this.aho = paramSummon.isPet() ? paramSummon.getInventory().getTotalWeight() : 0;
    this.ahp = paramSummon.getMaxLoad();
    this.ajy = paramSummon.getPAtk(null);
    this.ajz = paramSummon.getPDef(null);
    this.ajA = paramSummon.getMAtk(null, null);
    this.ajB = paramSummon.getMDef(null, null);
    this.ajC = paramSummon.getAccuracy();
    this.ajD = paramSummon.getEvasionRate(null);
    this.ajE = paramSummon.getCriticalHit(null, null);
    this.abnormalEffects = paramSummon.getAbnormalEffects();
    this.ajL = paramSummon.getMovementSpeedMultiplier();
    this.ajM = paramSummon.getAttackSpeedMultiplier();
    
    if (paramSummon.getPlayer().getTransformation() != 0) {
      this.aju = 0;
    } else {
      this.aju = PetDataTable.isMountable(this.afC) ? 1 : 0;
    }  this._team = paramSummon.getTeam();
    this.ajG = paramSummon.getSoulshotConsumeCount();
    this.ajF = paramSummon.getSpiritshotConsumeCount();
    this._showSpawnAnimation = paramSummon.getSpawnAnimation();
    this.type = paramSummon.getFormId();
    
    Player player = paramSummon.getPlayer();
    if (paramSummon.isAutoAttackable(player))
    {
      this.flags |= 0x1;
    }
    this.flags |= 0x2;
    if (paramSummon.isRunning())
    {
      this.flags |= 0x4;
    }
    if (paramSummon.isInCombat())
    {
      this.flags |= 0x8;
    }
    if (paramSummon.isAlikeDead())
    {
      this.flags |= 0x10;
    }
    if (PetDataTable.isMountable(this.afC))
    {
      this.flags |= 0x20; } 
  }
  private int ajE; private int ajF; private int ajG; private int type; private int _showSpawnAnimation;
  private Location _loc;
  
  public PetInfo update() {
    this._showSpawnAnimation = 1;
    return this;
  }
  private double ajH; private double abD; private long ajI; private long ajJ; private long ajK; private String _name; private String title; private TeamType _team; private AbnormalEffect[] abnormalEffects; private int flags; private double ajL;
  private double ajM;
  
  protected final void writeImpl() {
    writeC(178);
    writeC(this._type);
    writeD(this.acr);
    writeD(this.afC + 1000000);
    
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
    writeD(this._loc.h);
    
    writeD(this.ajs);
    writeD(this.ajt);
    
    writeH(this.abq);
    writeH(this.abr);
    writeH(this.abq);
    writeH(this.abr);
    writeH(this.abq);
    writeH(this.abr);
    writeH(this.abq);
    writeH(this.abr);
    
    writeF(this.ajL);
    writeF(this.ajM);
    writeF(this.ajH);
    writeF(this.abD);
    
    writeD(0);
    writeD(0);
    writeD(0);
    
    writeC(this._showSpawnAnimation);
    writeD(-1);
    writeS(this._name);
    writeD(-1);
    writeS(this.title);
    
    writeC(this.pvp_flag);
    writeD(this.karma);
    
    writeD(this.ajw);
    writeD(this.ajx);
    writeD(this.curHp);
    writeD(this.maxHp);
    writeD(this.curMp);
    writeD(this.maxMp);
    
    writeQ(this._sp);
    writeC(this.level);
    writeQ(this.ajI);
    writeQ(this.ajJ);
    writeQ(this.ajK);
    
    writeD(this.aho);
    writeD(this.ahp);
    
    writeD(this.ajy);
    writeD(this.ajz);
    writeD(this.ajC);
    writeD(this.ajD);
    writeD(this.ajE);
    
    writeD(this.ajA);
    writeD(this.ajB);
    writeD(0);
    writeD(0);
    writeD(0);
    
    writeD(this.abq);
    
    writeD(this.ajt);
    writeD(this.ajs);
    
    writeC(this.aju);
    writeC(this._team.ordinal());
    writeC(this.ajG);
    writeC(this.ajF);
    
    writeD(0);
    writeD(this.type);
    
    writeC(0);
    writeC(0);
    
    writeH(this.abnormalEffects.length);
    for (AbnormalEffect abnormalEffect : this.abnormalEffects)
    {
      writeH(abnormalEffect.getClientId());
    }
    
    writeC(this.flags);
  } }
