package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;
import l2.gameserver.utils.Location;




public class StopMove
  extends L2GameServerPacket
{
  private final int Bq;
  private final int _x;
  private final int _y;
  private final int _z;
  private final int alR;
  
  public StopMove(Creature paramCreature) {
    this.Bq = paramCreature.getObjectId();
    this._x = paramCreature.getX();
    this._y = paramCreature.getY();
    this._z = paramCreature.getZ();
    this.alR = paramCreature.getHeading();
  }

  
  public StopMove(int paramInt, Location paramLocation) {
    this.Bq = paramInt;
    this._x = paramLocation.x;
    this._y = paramLocation.y;
    this._z = paramLocation.z;
    this.alR = paramLocation.h;
  }


  
  protected final void writeImpl() {
    writeC(71);
    writeD(this.Bq);
    writeD(this._x);
    writeD(this._y);
    writeD(this._z);
    writeD(this.alR);
  }
}
