package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.pledge.RankPrivs;

public class ManagePledgePower extends L2GameServerPacket {
  private int fc;
  private int Bs;
  private int aiW;
  
  public ManagePledgePower(Player paramPlayer, int paramInt1, int paramInt2) {
    this.Bs = paramPlayer.getClanId();
    this.fc = paramInt1;
    RankPrivs rankPrivs = paramPlayer.getClan().getRankPrivs(paramInt2);
    this.aiW = (rankPrivs == null) ? 0 : rankPrivs.getPrivs();
    paramPlayer.sendPacket(new PledgeReceiveUpdatePower(this.aiW));
  }


  
  protected final void writeImpl() {
    writeC(42);
    writeD(this.Bs);
    writeD(this.fc);
    writeD(this.aiW);
  }
}
