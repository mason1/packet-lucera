package l2.gameserver.network.l2.s2c;

import l2.gameserver.Config;
import l2.gameserver.model.pledge.Clan;

public class PledgeInfo
  extends L2GameServerPacket {
  private int clan_id;
  private String clan_name;
  private String ally_name;
  
  public PledgeInfo(Clan paramClan) {
    this.clan_id = paramClan.getClanId();
    this.clan_name = paramClan.getName();
    this.ally_name = (paramClan.getAlliance() == null) ? "" : paramClan.getAlliance().getAllyName();
  }


  
  protected final void writeImpl() {
    writeC(137);
    writeD(Config.REQUEST_ID);
    writeD(this.clan_id);
    writeS(this.clan_name);
    writeS(this.ally_name);
  }
}
