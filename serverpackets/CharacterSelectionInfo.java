package l2.gameserver.network.l2.s2c;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import l2.commons.dbutils.DbUtils;
import l2.gameserver.Config;
import l2.gameserver.dao.CharacterDAO;
import l2.gameserver.database.DatabaseFactory;
import l2.gameserver.model.CharSelectInfoPackage;
import l2.gameserver.model.base.Experience;
import l2.gameserver.model.entity.oly.HeroController;
import l2.gameserver.model.items.Inventory;
import l2.gameserver.tables.CharTemplateTable;
import l2.gameserver.templates.PlayerTemplate;
import l2.gameserver.utils.AutoBan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class CharacterSelectionInfo
  extends L2GameServerPacket
{
  private static final Logger _log = LoggerFactory.getLogger(CharacterSelectionInfo.class);
  
  private String Xl;
  
  private int pp;
  
  private CharSelectInfoPackage[] acl;

  
  public CharacterSelectionInfo(String paramString, int paramInt) {
    this.pp = paramInt;
    this.Xl = paramString;
    this.acl = loadCharacterSelectInfo(paramString);
  }


  
  public CharSelectInfoPackage[] getCharInfo() { return this.acl; }



  
  protected final void writeImpl() {
    int i = (this.acl != null) ? this.acl.length : 0;
    
    writeC(9);
    
    byte b = 7;
    writeD(i);
    writeD(b);
    writeC((b == i) ? 1 : 0);
    
    writeC(1);
    writeD(2);
    writeC(0);
    writeC(0);
    
    long l = -1L;
    byte b1 = -1; byte b2;
    for (b2 = 0; b2 < i; b2++) {
      
      if (l < this.acl[b2].getLastAccess()) {
        
        l = this.acl[b2].getLastAccess();
        b1 = b2;
      } 
    } 
    
    for (b2 = 0; b2 < i; b2++) {
      
      CharSelectInfoPackage charSelectInfoPackage = this.acl[b2];
      
      writeS(charSelectInfoPackage.getName());
      writeD(charSelectInfoPackage.getCharId());
      writeS(this.Xl);
      writeD(this.pp);
      writeD(charSelectInfoPackage.getClanId());
      writeD(0);
      
      writeD(charSelectInfoPackage.getSex());
      writeD(charSelectInfoPackage.getRace());
      writeD(charSelectInfoPackage.getBaseClassId());
      
      writeD(1);
      
      writeD(charSelectInfoPackage.getX());
      writeD(charSelectInfoPackage.getY());
      writeD(charSelectInfoPackage.getZ());
      
      writeF(charSelectInfoPackage.getCurrentHp());
      writeF(charSelectInfoPackage.getCurrentMp());
      
      writeQ(charSelectInfoPackage.getSp());
      writeQ(charSelectInfoPackage.getExp());
      int j = charSelectInfoPackage.getLevel();
      writeF(Experience.getExpPercent(j, charSelectInfoPackage.getExp()));
      writeD(j);
      
      writeD(charSelectInfoPackage.getKarma());
      writeD(charSelectInfoPackage.getPk());
      writeD(charSelectInfoPackage.getPvP());
      
      writeD(0);
      writeD(0);
      writeD(0);
      writeD(0);
      writeD(0);
      writeD(0);
      writeD(0);
      
      writeD(0);
      writeD(0);
      
      for (int n : Inventory.PAPERDOLL_ORDER)
      {
        writeD(charSelectInfoPackage.getPaperdollItemId(n));
      }
      
      for (int n : Inventory.PAPERDOLL_VISUAL_ORDER)
      {
        writeD(charSelectInfoPackage.getPaperdollVisualItemId(n));
      }
      
      writeD(0);
      
      writeH(charSelectInfoPackage.getPaperdollEnchantEffect(6));
      writeH(charSelectInfoPackage.getPaperdollEnchantEffect(11));
      writeH(charSelectInfoPackage.getPaperdollEnchantEffect(1));
      writeH(charSelectInfoPackage.getPaperdollEnchantEffect(10));
      writeH(charSelectInfoPackage.getPaperdollEnchantEffect(12));
      
      writeD(charSelectInfoPackage.getHairStyle());
      writeD(charSelectInfoPackage.getHairColor());
      writeD(charSelectInfoPackage.getFace());
      
      writeF(charSelectInfoPackage.getMaxHp());
      writeF(charSelectInfoPackage.getMaxMp());
      
      writeD((charSelectInfoPackage.getAccessLevel() > -100) ? charSelectInfoPackage.getDeleteTimer() : -1);
      writeD(charSelectInfoPackage.getClassId());
      writeD((b2 == b1) ? 1 : 0);
      
      writeC(Math.min(charSelectInfoPackage.getPaperdollEnchantEffect(5), 127));
      int k = charSelectInfoPackage.getPaperdollAugmentationId(5);
      writeD(k & 0xFFFF);
      writeD(k >> 16);
      
      int m = charSelectInfoPackage.getPaperdollItemId(5);
      if (m == 8190) {
        
        writeD(301);
      }
      else if (m == 8689) {
        
        writeD(302);
      }
      else {
        
        writeD(0);
      } 
      
      writeD(0);
      writeD(0);
      writeD(0);
      writeD(0);
      writeF(0.0D);
      writeF(0.0D);
      
      writeD(charSelectInfoPackage.getVitalityPoints());
      writeD(0);
      writeD(0);
      writeD((charSelectInfoPackage.getAccessLevel() == -100) ? 0 : 1);
      writeC(0);
      writeC(charSelectInfoPackage.getCurrentHero());
      writeC(1);
    } 
  }


  
  public static CharSelectInfoPackage[] loadCharacterSelectInfo(String paramString) {
    ArrayList arrayList = new ArrayList();
    
    connection = null;
    preparedStatement = null;
    resultSet = null;
    
    try {
      connection = DatabaseFactory.getInstance().getConnection();
      preparedStatement = connection.prepareStatement("SELECT * FROM `characters` AS `c` LEFT JOIN `character_subclasses` AS `cs` ON (`c`.`obj_Id`=`cs`.`char_obj_id` AND `cs`.`active`=1) WHERE `account_name`=? LIMIT 7");
      preparedStatement.setString(1, paramString);
      resultSet = preparedStatement.executeQuery();
      while (resultSet.next()) {
        
        CharSelectInfoPackage charSelectInfoPackage = b(resultSet);
        if (charSelectInfoPackage != null) {
          arrayList.add(charSelectInfoPackage);
        }
      } 
    } catch (Exception exception) {
      
      _log.error("could not restore charinfo:", exception);
    }
    finally {
      
      DbUtils.closeQuietly(connection, preparedStatement, resultSet);
    } 
    
    return (CharSelectInfoPackage[])arrayList.toArray(new CharSelectInfoPackage[arrayList.size()]);
  }

  
  private static int V(int paramInt) {
    int i = 0;
    
    connection = null;
    preparedStatement = null;
    resultSet = null;
    
    try {
      connection = DatabaseFactory.getInstance().getConnection();
      preparedStatement = connection.prepareStatement("SELECT `class_id` FROM `character_subclasses` WHERE `char_obj_id`=? AND `isBase`=1");
      preparedStatement.setInt(1, paramInt);
      resultSet = preparedStatement.executeQuery();
      while (resultSet.next()) {
        i = resultSet.getInt("class_id");
      }
    } catch (Exception exception) {
      
      _log.error("could not restore base class id:", exception);
    }
    finally {
      
      DbUtils.closeQuietly(connection, preparedStatement, resultSet);
    } 
    
    return i;
  }

  
  private static CharSelectInfoPackage b(ResultSet paramResultSet) {
    CharSelectInfoPackage charSelectInfoPackage = null;
    
    try {
      int i = paramResultSet.getInt("obj_Id");
      int j = paramResultSet.getInt("class_id");
      int k = paramResultSet.getInt("base_class_id");
      boolean bool = (paramResultSet.getInt("sex") == 1);
      PlayerTemplate playerTemplate = CharTemplateTable.getInstance().getTemplate(k, bool);
      if (playerTemplate == null) {
        
        _log.error("restoreChar fail | templ == null | objectId: " + i + " | classid: " + k + " | female: " + bool);
        return null;
      } 
      String str = paramResultSet.getString("char_name");
      charSelectInfoPackage = new CharSelectInfoPackage(i, str);
      charSelectInfoPackage.setLevel(paramResultSet.getInt("level"));
      charSelectInfoPackage.setMaxHp(paramResultSet.getInt("maxHp"));
      charSelectInfoPackage.setCurrentHp(paramResultSet.getDouble("curHp"));
      charSelectInfoPackage.setMaxMp(paramResultSet.getInt("maxMp"));
      charSelectInfoPackage.setCurrentMp(paramResultSet.getDouble("curMp"));
      
      charSelectInfoPackage.setX(paramResultSet.getInt("x"));
      charSelectInfoPackage.setY(paramResultSet.getInt("y"));
      charSelectInfoPackage.setZ(paramResultSet.getInt("z"));
      charSelectInfoPackage.setPk(paramResultSet.getInt("pkkills"));
      charSelectInfoPackage.setPvP(paramResultSet.getInt("pvpkills"));
      
      charSelectInfoPackage.setFace(paramResultSet.getInt("face"));
      charSelectInfoPackage.setHairStyle(paramResultSet.getInt("hairstyle"));
      charSelectInfoPackage.setHairColor(paramResultSet.getInt("haircolor"));
      charSelectInfoPackage.setSex(bool ? 1 : 0);
      
      charSelectInfoPackage.setExp(paramResultSet.getLong("exp"));
      charSelectInfoPackage.setSp(paramResultSet.getInt("sp"));
      charSelectInfoPackage.setClanId(paramResultSet.getInt("clanid"));
      
      charSelectInfoPackage.setKarma(-paramResultSet.getInt("karma"));
      charSelectInfoPackage.setRace(playerTemplate.race.ordinal());
      charSelectInfoPackage.setClassId(j);
      charSelectInfoPackage.setBaseClassId(k);
      long l = paramResultSet.getLong("deletetime");
      int m = 0;
      if (Config.DELETE_DAYS > 0)
      {
        if (l > 0L) {
          
          l = (int)(System.currentTimeMillis() / 1000L - l);
          m = (int)(l / 3600L / 24L);
          if (m >= Config.DELETE_DAYS) {
            
            CharacterDAO.getInstance().deleteCharacterDataByObjId(i);
            return null;
          } 
          l = (Config.DELETE_DAYS * 3600 * 24) - l;
        }
        else {
          
          l = 0L;
        } 
      }
      charSelectInfoPackage.setDeleteTimer((int)l);
      charSelectInfoPackage.setLastAccess(paramResultSet.getLong("lastAccess") * 1000L);
      charSelectInfoPackage.setAccessLevel(paramResultSet.getInt("accesslevel"));
      int n = paramResultSet.getInt("vitality") + (int)((System.currentTimeMillis() - charSelectInfoPackage.getLastAccess()) / 15.0D);
      if (n > 20000) {
        n = 20000;
      } else if (n < 0) {
        n = 0;
      }  charSelectInfoPackage.setVitalityPoints(n);
      
      if (charSelectInfoPackage.getAccessLevel() < 0 && !AutoBan.isBanned(i)) {
        charSelectInfoPackage.setAccessLevel(0);
      }
      charSelectInfoPackage.setCurrentHero(HeroController.getInstance().isCurrentHero(i) ? 2 : 0);
    }
    catch (Exception exception) {
      
      _log.error("", exception);
    } 
    
    return charSelectInfoPackage;
  }
}
