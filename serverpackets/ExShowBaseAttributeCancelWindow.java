package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Player;
import l2.gameserver.model.base.Element;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.templates.item.ItemTemplate;




public class ExShowBaseAttributeCancelWindow
  extends L2GameServerPacket
{
  private final List<ItemInstance> _items = new ArrayList();

  
  public ExShowBaseAttributeCancelWindow(Player paramPlayer) {
    for (ItemInstance itemInstance : paramPlayer.getInventory().getItems()) {
      
      if (itemInstance.getAttributeElement() != Element.NONE && itemInstance.canBeEnchanted(true) && getAttributeRemovePrice(itemInstance) != 0L)
      {
        this._items.add(itemInstance);
      }
    } 
  }

  
  protected final void writeImpl() {
    writeEx(116);
    writeD(this._items.size());
    for (ItemInstance itemInstance : this._items) {
      
      writeD(itemInstance.getObjectId());
      writeQ(getAttributeRemovePrice(itemInstance));
    } 
  }

  
  public static long getAttributeRemovePrice(ItemInstance paramItemInstance) {
    switch (paramItemInstance.getCrystalType()) {
      
      case S:
        return (paramItemInstance.getTemplate().getType2() == 0) ? 50000L : 40000L;
    } 
    return 0L;
  }
}
