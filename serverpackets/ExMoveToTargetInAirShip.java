package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.entity.boat.Boat;
import l2.gameserver.utils.Location;

public class ExMoveToTargetInAirShip
  extends L2GameServerPacket
{
  private int char_id;
  private int aeS;
  
  public ExMoveToTargetInAirShip(Player paramPlayer, Boat paramBoat, int paramInt1, int paramInt2, Location paramLocation) {
    this.char_id = paramPlayer.getObjectId();
    this.aeS = paramBoat.getObjectId();
    this.aeT = paramInt1;
    this.aeU = paramInt2;
    this._loc = paramLocation;
  }
  private int aeT; private int aeU;
  private Location _loc;
  
  protected final void writeImpl() {
    writeEx(113);
    
    writeD(this.char_id);
    writeD(this.aeT);
    writeD(this.aeU);
    writeD(this._loc.y);
    writeD(this._loc.z);
    writeD(this._loc.h);
    writeD(this.aeS);
  }
}
