package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.entity.SevenSigns;

public class ShowMiniMap
  extends L2GameServerPacket {
  private int alu;
  private int Uj;
  
  public ShowMiniMap(Player paramPlayer, int paramInt) {
    this.alu = paramInt;
    this.Uj = SevenSigns.getInstance().getCurrentPeriod();
  }


  
  protected final void writeImpl() {
    writeC(163);
    writeD(this.alu);
    writeC(this.Uj);
  }
}
