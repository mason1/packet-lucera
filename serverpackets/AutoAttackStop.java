package l2.gameserver.network.l2.s2c;






public class AutoAttackStop
  extends L2GameServerPacket
{
  private int _targetId;
  
  public AutoAttackStop(int paramInt) { this._targetId = paramInt; }



  
  protected final void writeImpl() {
    writeC(38);
    writeD(this._targetId);
  }
}
