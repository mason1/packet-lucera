package l2.gameserver.network.l2.s2c;

public class Snoop
  extends L2GameServerPacket
{
  private int alx;
  private String _name;
  private int _type;
  private int aly;
  private String alz;
  private String[] _params;
  
  public Snoop(int paramInt1, String paramString1, int paramInt2, String paramString2, String paramString3, int paramInt3, String... paramVarArgs) {
    this.alx = paramInt1;
    this._name = paramString1;
    this._type = paramInt2;
    this.alz = paramString2;
    this.aly = paramInt3;
    this._params = paramVarArgs;
  }


  
  protected final void writeImpl() {
    writeC(219);
    
    writeD(this.alx);
    writeS(this._name);
    writeD(0);
    writeD(this._type);
    writeS(this.alz);
    writeD(this.aly);
    for (String str : this._params)
      writeS(str); 
  }
}
