package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.templates.Henna;

public class HennaInfo extends L2GameServerPacket {
  private final Henna[] aiB = new Henna[3];
  private final int ahG;
  private final int ahH;
  private final int ahI;
  private final int ahJ;
  private final int ahK;
  private final int ahL;
  private int _count = 0;
  public HennaInfo(Player paramPlayer) {
    for (byte b = 0; b < 3; b++) {
      Henna henna; if ((henna = paramPlayer.getHenna(b + true)) != null)
        this.aiB[this._count++] = new Henna(henna.getSymbolId(), henna.isForThisClass(paramPlayer)); 
    } 
    this.ahG = paramPlayer.getHennaStatSTR();
    this.ahH = paramPlayer.getHennaStatCON();
    this.ahI = paramPlayer.getHennaStatDEX();
    this.ahJ = paramPlayer.getHennaStatINT();
    this.ahK = paramPlayer.getHennaStatWIT();
    this.ahL = paramPlayer.getHennaStatMEN();
    
    this.aiC = (paramPlayer.getLevel() < 40) ? 2 : 3;
  }
  
  private int aiC;
  
  protected final void writeImpl() {
    writeC(229);
    writeH(this.ahJ);
    writeH(this.ahG);
    writeH(this.ahH);
    writeH(this.ahL);
    writeH(this.ahI);
    writeH(this.ahK);
    writeH(0);
    writeH(0);
    writeD(this.aiC);
    writeD(this._count);
    for (byte b = 0; b < this._count; b++) {
      
      writeD((this.aiB[b])._symbolId);
      writeD((this.aiB[b]).aiD ? (this.aiB[b])._symbolId : 0);
    } 
    writeD(0);
    writeD(0);
    writeD(0);
  }

  
  private static class Henna
  {
    private int _symbolId;
    private boolean aiD;
    
    public Henna(int param1Int, boolean param1Boolean) {
      this._symbolId = param1Int;
      this.aiD = param1Boolean;
    }
  }
}
