package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.entity.SevenSigns;




















public class SSQInfo
  extends L2GameServerPacket
{
  private int _state = 0;

  
  public SSQInfo() {
    int i = SevenSigns.getInstance().getCabalHighestScore();
    if (SevenSigns.getInstance().isSealValidationPeriod()) {
      if (i == 2) {
        this._state = 2;
      } else if (i == 1) {
        this._state = 1;
      } 
    }
  }
  
  public SSQInfo(int paramInt) { this._state = paramInt; }



  
  protected final void writeImpl() {
    writeC(115);
    switch (this._state) {
      
      case 1:
        writeH(257);
        return;
      case 2:
        writeH(258);
        return;
    } 
    writeH(256);
  }
}
