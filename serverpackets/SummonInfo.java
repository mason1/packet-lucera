package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;
import l2.gameserver.model.Summon;





public class SummonInfo
  extends AbstractNpcPacket
{
  public SummonInfo(Summon paramSummon, Creature paramCreature) {
    if (paramSummon.getPlayer() != null && paramSummon.getPlayer().isInvisible()) {
      return;
    }

    
    this._npcId = (paramSummon.getTemplate()).npcId;
    this._isAttackable = paramSummon.isAutoAttackable(paramCreature);
    this._rhand = 0;
    this._lhand = 0;
    this._enchantEffect = 0;
    this._showName = true;
    this._name = paramSummon.getName();
    this._title = paramSummon.getTitle();
    this._showSpawnAnimation = paramSummon.getSpawnAnimation();
    this._isPet = true;
    
    setValues(paramSummon, NpcInfoType.VALUES);
  }


  
  protected void writeImpl() {
    writeC(139);
    
    writeData();
  }
}
