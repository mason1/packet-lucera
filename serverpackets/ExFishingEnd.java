package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;






public class ExFishingEnd
  extends L2GameServerPacket
{
  private int Br;
  private boolean ael;
  
  public ExFishingEnd(Player paramPlayer, boolean paramBoolean) {
    this.Br = paramPlayer.getObjectId();
    this.ael = paramBoolean;
  }


  
  protected final void writeImpl() {
    writeEx(31);
    writeD(this.Br);
    writeC(this.ael ? 1 : 0);
  }
}
