package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.utils.Location;

public class StopMoveToLocationInVehicle extends L2GameServerPacket {
  private int YW;
  private int aeK;
  private int CG;
  private Location _loc;
  
  public StopMoveToLocationInVehicle(Player paramPlayer) {
    this.YW = paramPlayer.getBoat().getObjectId();
    this.aeK = paramPlayer.getObjectId();
    this._loc = paramPlayer.getInBoatPosition();
    this.CG = paramPlayer.getHeading();
  }


  
  protected final void writeImpl() {
    writeC(127);
    writeD(this.aeK);
    writeD(this.YW);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
    writeD(this.CG);
  }
}
