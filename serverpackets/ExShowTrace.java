package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.GameObject;
import l2.gameserver.utils.Location;

public class ExShowTrace
  extends L2GameServerPacket
{
  private final List<Trace> agY = new ArrayList();
  private int _time = Integer.MIN_VALUE;

  
  static final class Trace
  {
    public final int _x;
    public final int _y;
    public final int _z;
    
    public Trace(int param1Int1, int param1Int2, int param1Int3) {
      this._x = param1Int1;
      this._y = param1Int2;
      this._z = param1Int3;
    }
  }

  
  public void addTrace(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.agY.add(new Trace(paramInt1, paramInt2, paramInt3));
    this._time = Math.max(this._time, paramInt4);
  }


  
  public void addLine(Location paramLocation1, Location paramLocation2, int paramInt1, int paramInt2) { addLine(paramLocation1.x, paramLocation1.y, paramLocation1.z, paramLocation2.x, paramLocation2.y, paramLocation2.z, paramInt1, paramInt2); }


  
  public void addLine(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    int i = paramInt4 - paramInt1;
    int j = paramInt5 - paramInt2;
    int k = paramInt6 - paramInt3;
    double d1 = Math.sqrt((i * i + j * j));
    double d2 = Math.sqrt(d1 * d1 + (k * k));
    int m = (int)(d2 / paramInt7);
    
    addTrace(paramInt1, paramInt2, paramInt3, paramInt8);
    if (m > 1) {
      
      int n = i / m;
      int i1 = j / m;
      int i2 = k / m;
      
      for (int i3 = 1; i3 < m; i3++)
        addTrace(paramInt1 + n * i3, paramInt2 + i1 * i3, paramInt3 + i2 * i3, paramInt8); 
    } 
    addTrace(paramInt4, paramInt5, paramInt6, paramInt8);
  }


  
  public void addTrace(GameObject paramGameObject, int paramInt) { addTrace(paramGameObject.getX(), paramGameObject.getY(), paramGameObject.getZ(), paramInt); }


  
  public void addCircle(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    double d1 = Math.min(12.566370614359172D / Math.max(1, paramInt4), 6.283185307179586D); double d2;
    for (d2 = 0.0D; d2 < 6.283185307179586D; d2 += d1) {
      addTrace((int)Math.round(paramInt1 + Math.cos(d2) * paramInt4), (int)Math.round(paramInt2 + Math.sin(d2) * paramInt4), paramInt3, paramInt5);
    }
  }

  
  protected final void writeImpl() {
    writeEx(103);
    
    writeH(0);
    writeD(this._time);
    writeH(this.agY.size());
    for (Trace trace : this.agY) {
      
      writeD(trace._x);
      writeD(trace._y);
      writeD(trace._z);
    } 
  }
}
