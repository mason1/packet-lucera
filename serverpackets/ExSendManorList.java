package l2.gameserver.network.l2.s2c;

import java.util.List;
import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.model.entity.residence.Residence;



























public class ExSendManorList
  extends L2GameServerPacket
{
  protected void writeImpl() {
    writeEx(34);
    List list = ResidenceHolder.getInstance().getResidenceList(l2.gameserver.model.entity.residence.Castle.class);
    writeD(list.size());
    for (Residence residence : list) {
      
      writeD(residence.getId());
      writeS(residence.getName().toLowerCase());
    } 
  }
}
