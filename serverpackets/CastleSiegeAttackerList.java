package l2.gameserver.network.l2.s2c;

import java.util.Collections;
import java.util.List;
import l2.gameserver.model.entity.events.objects.SiegeClanObject;
import l2.gameserver.model.entity.residence.Residence;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.model.pledge.Clan;

























public class CastleSiegeAttackerList
  extends L2GameServerPacket
{
  private int _id;
  private int abf;
  private List<SiegeClanObject> abg;
  
  public CastleSiegeAttackerList(Residence paramResidence) {
    this.abg = Collections.emptyList();


    
    this._id = paramResidence.getId();
    this.abf = !paramResidence.getSiegeEvent().isRegistrationOver() ? 1 : 0;
    this.abg = paramResidence.getSiegeEvent().getObjects("attackers");
  }


  
  protected final void writeImpl() {
    writeC(202);
    
    writeD(this._id);
    
    writeD(0);
    writeD(this.abf);
    writeD(0);
    
    writeD(this.abg.size());
    writeD(this.abg.size());
    
    for (SiegeClanObject siegeClanObject : this.abg) {
      
      Clan clan = siegeClanObject.getClan();
      
      writeD(clan.getClanId());
      writeS(clan.getName());
      writeS(clan.getLeaderName());
      writeD(clan.getCrestId());
      writeD((int)(siegeClanObject.getDate() / 1000L));
      
      Alliance alliance = clan.getAlliance();
      writeD(clan.getAllyId());
      if (alliance != null) {
        
        writeS(alliance.getAllyName());
        writeS(alliance.getAllyLeaderName());
        writeD(alliance.getAllyCrestId());
        
        continue;
      } 
      writeS("");
      writeS("");
      writeD(0);
    } 
  }
}
