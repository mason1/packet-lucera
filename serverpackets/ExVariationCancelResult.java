package l2.gameserver.network.l2.s2c;

public class ExVariationCancelResult
  extends L2GameServerPacket {
  public static final ExVariationCancelResult FAIL_PACKET = new ExVariationCancelResult(0);
  
  private int ahq;
  
  private int _unk1;
  
  public ExVariationCancelResult(int paramInt) {
    this.ahq = 1;
    this._unk1 = paramInt;
  }


  
  protected void writeImpl() {
    writeEx(89);
    writeD(this._unk1);
    writeD(this.ahq);
  }
}
