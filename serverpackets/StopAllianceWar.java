package l2.gameserver.network.l2.s2c;

public class StopAllianceWar
  extends L2GameServerPacket
{
  private String akm;
  private String alH;
  
  public StopAllianceWar(String paramString1, String paramString2) {
    this.akm = paramString1;
    this.alH = paramString2;
  }


  
  protected final void writeImpl() {
    writeC(196);
    writeS(this.akm);
    writeS(this.alH);
  }
}
