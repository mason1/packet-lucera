package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;


public class ExPCCafePointInfo
  extends L2GameServerPacket
{
  private int afg;
  private int afh;
  private int afi;
  private int HG;
  private int afj;
  
  public ExPCCafePointInfo(Player paramPlayer, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.HG = paramPlayer.getPcBangPoints();
    this.afg = paramInt1;
    this.afh = paramInt2;
    this.afi = paramInt3;
    this.afj = paramInt4;
  }


  
  protected final void writeImpl() {
    writeEx(50);
    writeD(this.HG);
    writeD(this.afg);
    writeC(this.afh);
    writeD(this.afj);
    writeC(this.afi);
  }
}
