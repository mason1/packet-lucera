package l2.gameserver.network.l2.s2c;

import java.util.Collections;
import java.util.List;

public class ExShowFortressInfo
  extends L2GameServerPacket {
  private List<FortressInfo> agt = Collections.emptyList();


  
  protected final void writeImpl() {
    writeEx(21);
    writeD(this.agt.size());
    for (FortressInfo fortressInfo : this.agt) {
      
      writeD(fortressInfo._id);
      writeS(fortressInfo._owner);
      writeD(fortressInfo._status);
      writeD(fortressInfo._siege);
    } 
  }
  
  static class FortressInfo
  {
    public int _id;
    public int _siege;
    public String _owner;
    public boolean _status;
    
    public FortressInfo(String param1String, int param1Int1, boolean param1Boolean, int param1Int2) {
      this._owner = param1String;
      this._id = param1Int1;
      this._status = param1Boolean;
      this._siege = param1Int2;
    }
  }
}
