package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import org.apache.commons.lang3.StringUtils;





public class PrivateStoreMsgBuy
  extends L2GameServerPacket
{
  private int abx;
  private String _name;
  
  public PrivateStoreMsgBuy(Player paramPlayer) {
    this.abx = paramPlayer.getObjectId();
    this._name = StringUtils.defaultString(paramPlayer.getBuyStoreName());
  }


  
  protected final void writeImpl() {
    writeC(191);
    writeD(this.abx);
    writeS(this._name);
  }
}
