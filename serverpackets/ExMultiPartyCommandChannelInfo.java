package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.CommandChannel;
import l2.gameserver.model.Party;
import l2.gameserver.model.Player;


public class ExMultiPartyCommandChannelInfo
  extends L2GameServerPacket
{
  private String aeV;
  private int MemberCount;
  private List<ChannelPartyInfo> aeW;
  
  public ExMultiPartyCommandChannelInfo(CommandChannel paramCommandChannel) {
    this.aeV = paramCommandChannel.getChannelLeader().getName();
    this.MemberCount = paramCommandChannel.getMemberCount();
    
    this.aeW = new ArrayList();
    for (Party party : paramCommandChannel.getParties()) {
      
      Player player = party.getPartyLeader();
      if (player != null) {
        this.aeW.add(new ChannelPartyInfo(player.getName(), player.getObjectId(), party.getMemberCount()));
      }
    } 
  }

  
  protected void writeImpl() {
    writeEx(49);
    writeS(this.aeV);
    writeD(0);
    writeD(this.MemberCount);
    writeD(this.aeW.size());
    
    for (ChannelPartyInfo channelPartyInfo : this.aeW) {
      
      writeS(channelPartyInfo.Leader_name);
      writeD(channelPartyInfo.Leader_obj_id);
      writeD(channelPartyInfo.MemberCount);
    } 
  }
  
  static class ChannelPartyInfo
  {
    public String Leader_name;
    public int Leader_obj_id;
    public int MemberCount;
    
    public ChannelPartyInfo(String param1String, int param1Int1, int param1Int2) {
      this.Leader_name = param1String;
      this.Leader_obj_id = param1Int1;
      this.MemberCount = param1Int2;
    }
  }
}
