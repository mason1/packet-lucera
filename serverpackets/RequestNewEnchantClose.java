package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.c2s.L2GameClientPacket;





public class RequestNewEnchantClose
  extends L2GameClientPacket
{
  protected void readImpl() {}
  
  protected void runImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }

    
    player.sendPacket(EnchantResult.CANCEL);
  }
}
