package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.entity.boat.Boat;
import l2.gameserver.utils.Location;

public class GetOnVehicle
  extends L2GameServerPacket {
  private int aeK;
  private int YW;
  private Location _loc;
  
  public GetOnVehicle(Player paramPlayer, Boat paramBoat, Location paramLocation) {
    this._loc = paramLocation;
    this.aeK = paramPlayer.getObjectId();
    this.YW = paramBoat.getObjectId();
  }


  
  protected final void writeImpl() {
    writeC(110);
    writeD(this.aeK);
    writeD(this.YW);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
  }
}
