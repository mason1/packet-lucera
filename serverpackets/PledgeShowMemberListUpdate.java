package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.pledge.SubUnit;
import l2.gameserver.model.pledge.UnitMember;

public class PledgeShowMemberListUpdate
  extends L2GameServerPacket
{
  private String _name;
  private int ZY;
  private int Bu;
  private int BB;
  private int ahA;
  private int Bq;
  private int GA;
  private int akp;
  
  public PledgeShowMemberListUpdate(Player paramPlayer) {
    this._name = paramPlayer.getName();
    this.ZY = paramPlayer.getLevel();
    this.Bu = paramPlayer.getClassId().getId();
    this.BB = paramPlayer.getSex();
    this.Bq = paramPlayer.getObjectId();
    this.ahA = paramPlayer.isOnline() ? 1 : 0;
    this.GA = paramPlayer.getPledgeType();
    SubUnit subUnit = paramPlayer.getSubUnit();
    UnitMember unitMember = (subUnit == null) ? null : subUnit.getUnitMember(this.Bq);
    if (unitMember != null) {
      this.akp = unitMember.hasSponsor() ? 1 : 0;
    }
  }
  
  public PledgeShowMemberListUpdate(UnitMember paramUnitMember) {
    this._name = paramUnitMember.getName();
    this.ZY = paramUnitMember.getLevel();
    this.Bu = paramUnitMember.getClassId();
    this.BB = paramUnitMember.getSex();
    this.Bq = paramUnitMember.getObjectId();
    this.ahA = paramUnitMember.isOnline() ? 1 : 0;
    this.GA = paramUnitMember.getPledgeType();
    this.akp = paramUnitMember.hasSponsor() ? 1 : 0;
  }


  
  protected final void writeImpl() {
    writeC(91);
    writeS(this._name);
    writeD(this.ZY);
    writeD(this.Bu);
    writeD(this.BB);
    writeD(this.Bq);
    writeD(this.ahA);
    writeD(this.GA);
    writeD(this.akp);
  }
}
