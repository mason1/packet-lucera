package l2.gameserver.network.l2.s2c;

import l2.gameserver.Config;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.utils.Location;



public class DropItem
  extends L2GameServerPacket
{
  private Location _loc;
  private int acn;
  private int acz;
  private int acA;
  private int acB;
  private long Be;
  
  public DropItem(ItemInstance paramItemInstance, int paramInt) {
    this.acn = paramInt;
    this.acz = paramItemInstance.getObjectId();
    this.acA = paramItemInstance.getItemId();
    this._loc = paramItemInstance.getLoc();
    this.acB = paramItemInstance.isStackable() ? 1 : 0;
    this.Be = paramItemInstance.getCount();
  }

  
  public DropItem(int paramInt1, int paramInt2, int paramInt3, Location paramLocation, boolean paramBoolean, int paramInt4) {
    this.acn = paramInt1;
    this.acz = paramInt2;
    this.acA = paramInt3;
    this._loc = paramLocation.clone();
    this.acB = paramBoolean ? 1 : 0;
    this.Be = paramInt4;
  }


  
  protected final void writeImpl() {
    writeC(22);
    writeD(this.acn);
    writeD(this.acz);
    writeD(this.acA);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z + Config.CLIENT_Z_SHIFT);
    writeC(this.acB);
    writeQ(this.Be);
    writeC(0);
    writeC(0);
    writeC(0);
    writeC(0);
  }
}
