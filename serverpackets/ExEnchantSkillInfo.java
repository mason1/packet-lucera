package l2.gameserver.network.l2.s2c;

import java.util.Map;
import l2.gameserver.data.StringHolder;
import l2.gameserver.data.xml.holder.EnchantSkillHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.model.base.Experience;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.model.instances.TrainerInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.tables.SkillTable;
import l2.gameserver.templates.SkillEnchant;
import org.apache.commons.lang3.StringUtils;

public class ExEnchantSkillInfo
  extends NpcHtmlMessage
{
  public static String EX_ENCHANT_SKILLINFO_BYPASS = "ExEnchantSkillInfo";
  
  private SkillEnchant aef;
  private int chance = 0;
  private int aeg = 0;


  
  public ExEnchantSkillInfo(Player paramPlayer, NpcInstance paramNpcInstance) { super(paramPlayer, paramNpcInstance); }


  
  public static L2GameServerPacket packetFor(Player paramPlayer, TrainerInstance paramTrainerInstance, String... paramVarArgs) {
    ExEnchantSkillInfo exEnchantSkillInfo = new ExEnchantSkillInfo(paramPlayer, paramTrainerInstance);
    
    if (paramPlayer.getClassId().getLevel() < 4 || paramPlayer.getLevel() < 76 || paramVarArgs.length < 2)
    {
      return new SystemMessage(1438);
    }
    
    int i = 0;
    int j = 0;
    int k = 0;
    
    try {
      i = Integer.parseInt(paramVarArgs[0]);
      j = Integer.parseInt(paramVarArgs[1]);
      if (paramVarArgs.length > 2) {
        k = Integer.parseInt(paramVarArgs[2]);
      }
    } catch (Exception exception) {
      
      return new SystemMessage(1438);
    } 
    
    Skill skill = paramPlayer.getKnownSkill(i);
    if (skill == null)
    {
      return new SystemMessage(1438);
    }
    int m = skill.getLevel();
    Map map = EnchantSkillHolder.getInstance().getLevelsOf(i);
    
    if (map == null || map.isEmpty())
    {
      return new SystemMessage(1438);
    }
    
    SkillEnchant skillEnchant1 = (SkillEnchant)map.get(Integer.valueOf(m));
    SkillEnchant skillEnchant2 = (SkillEnchant)map.get(Integer.valueOf(j));
    
    if (skillEnchant2 == null)
    {
      return new SystemMessage(1438);
    }
    
    if (skillEnchant1 != null) {
      
      if (skillEnchant1.getRouteId() != skillEnchant2.getRouteId() || skillEnchant2.getEnchantLevel() != skillEnchant1.getEnchantLevel() + 1)
      {
        return new SystemMessage(1438);
      }
    }
    else if (skillEnchant2.getEnchantLevel() != 1) {
      
      return new SystemMessage(1438);
    } 
    
    int[] arrayOfInt = skillEnchant2.getChances();
    int n = Experience.LEVEL.length - arrayOfInt.length - 1;
    
    if (paramPlayer.getLevel() < n)
    {
      return (new SystemMessage(607)).addNumber(n);
    }
    
    int i1 = Math.max(0, Math.min(paramPlayer.getLevel() - n, arrayOfInt.length - 1));
    exEnchantSkillInfo.aef = skillEnchant2;
    exEnchantSkillInfo.chance = arrayOfInt[i1];
    exEnchantSkillInfo.aeg = k;
    return exEnchantSkillInfo;
  }



  
  public void processHtml(GameClient paramGameClient) {
    try {
      Player player = paramGameClient.getActiveChar();
      if (player == null)
        return; 
      setFile("trainer/ExEnchantSkillInfo.htm");
      if (this.aef == null) {
        return;
      }
      Skill skill = SkillTable.getInstance().getInfo(this.aef.getSkillId(), this.aef.getSkillLevel());
      
      replace("%skill_id%", String.valueOf(this.aef.getSkillId()));
      replace("%skill_level%", String.valueOf(this.aef.getSkillLevel()));
      
      replace("%skill_icon%", StringUtils.defaultString(skill.getIcon(), ""));
      replace("%skill_name%", StringUtils.defaultString(skill.getName()));
      replace("%skill_enchant_route_name%", StringUtils.defaultString(skill.getEnchantRouteName()));
      replace("%skill_enchant_level%", String.valueOf(this.aef.getEnchantLevel()));
      
      if (this.aef.getItemId() != 0 && this.aef.getItemCount() > 0L) {
        
        String str = StringHolder.getInstance().getNotNull(player, "l2.gameserver.ExEnchantSkillInfo.RequiredItem");
        str = StringUtils.replace(str, "%item_id%", String.valueOf(this.aef.getItemId()));
        str = StringUtils.replace(str, "%item_count%", String.valueOf(this.aef.getItemCount()));
        replace("%required_item%", str);
      }
      else {
        
        replace("%required_item%", "&nbsp;");
      } 
      replace("%backPageNum%", String.valueOf(this.aeg));
      replace("%required_exp%", String.valueOf(this.aef.getExp()));
      replace("%required_sp%", String.valueOf(this.aef.getSp()));
      
      replace("%current_exp%", String.valueOf(player.getExp()));
      replace("%current_sp%", String.valueOf(player.getSp()));
      
      replace("%chance%", String.valueOf(this.chance));
    }
    finally {
      
      super.processHtml(paramGameClient);
    } 
  }
}
