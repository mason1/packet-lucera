package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.Summon;





public class ExPetInfo
  extends AbstractNpcPacket
{
  public ExPetInfo(Summon paramSummon, Player paramPlayer) {
    this._npcId = (paramSummon.getTemplate()).npcId;
    this._isAttackable = paramSummon.isAutoAttackable(paramPlayer);
    this._rhand = 0;
    this._lhand = 0;
    this._enchantEffect = 0;
    this._showName = true;
    this._name = paramSummon.getName();
    this._title = paramSummon.getTitle();
    this._showSpawnAnimation = paramSummon.getSpawnAnimation();
    this._isPet = true;
    
    setValues(paramSummon, NpcInfoType.VALUES);
  }


  
  protected void writeImpl() {
    writeEx(350);
    writeData();
  }
}
