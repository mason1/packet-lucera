package l2.gameserver.network.l2.s2c;
public class ExOlympiadUserInfo extends L2GameServerPacket {
  private int Pz;
  private int class_id;
  private int curHp;
  private int maxHp;
  
  public ExOlympiadUserInfo(Player paramPlayer, int paramInt) {
    this.acr = 0;



    
    this.Pz = paramInt;
    this.acr = paramPlayer.getObjectId();
    this.class_id = paramPlayer.getClassId().getId();
    this._name = paramPlayer.getName();
    this.curHp = (int)paramPlayer.getCurrentHp();
    this.maxHp = paramPlayer.getMaxHp();
    this.curCp = (int)paramPlayer.getCurrentCp();
    this.maxCp = paramPlayer.getMaxCp();
  }
  private int curCp; private int maxCp; private int acr; private String _name;
  public ExOlympiadUserInfo(Player paramPlayer) {
    this.acr = 0;
    this.Pz = paramPlayer.getOlyParticipant().getSide();
    this.acr = paramPlayer.getObjectId();
    this.class_id = paramPlayer.getClassId().getId();
    this._name = paramPlayer.getName();
    this.curHp = (int)paramPlayer.getCurrentHp();
    this.maxHp = paramPlayer.getMaxHp();
    this.curCp = (int)paramPlayer.getCurrentCp();
    this.maxCp = paramPlayer.getMaxCp();
  }


  
  protected final void writeImpl() {
    writeEx(123);
    writeC(this.Pz);
    writeD(this.acr);
    writeS(this._name);
    writeD(this.class_id);
    writeD(this.curHp);
    writeD(this.maxHp);
    writeD(this.curCp);
    writeD(this.maxCp);
  }
}
