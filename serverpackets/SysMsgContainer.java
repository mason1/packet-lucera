package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Creature;
import l2.gameserver.model.GameObject;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.model.Summon;
import l2.gameserver.model.base.Element;
import l2.gameserver.model.entity.residence.Residence;
import l2.gameserver.model.instances.DoorInstance;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.model.instances.StaticObjectInstance;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.components.SystemMsg;
import l2.gameserver.tables.SkillTable;
import l2.gameserver.utils.Location;


public abstract class SysMsgContainer<T extends SysMsgContainer<T>>
  extends L2GameServerPacket
{
  protected SystemMsg _message;
  protected List<IArgument> _arguments;
  
  public enum Types
  {
    TEXT,
    NUMBER,
    NPC_NAME,
    ITEM_NAME,
    SKILL_NAME,
    RESIDENCE_NAME,
    LONG,
    ZONE_NAME,
    ITEM_NAME_WITH_AUGMENTATION,
    ELEMENT_NAME,
    INSTANCE_NAME,
    STATIC_OBJECT_NAME,
    PLAYER_NAME,
    SYSTEM_STRING;
  }






  
  @Deprecated
  protected SysMsgContainer(int paramInt) { this(SystemMsg.valueOf(paramInt)); }


  
  protected SysMsgContainer(SystemMsg paramSystemMsg) {
    if (paramSystemMsg == null) {
      throw new IllegalArgumentException("SystemMsg is null");
    }
    this._message = paramSystemMsg;
    this._arguments = new ArrayList(this._message.size());
  }


  
  public L2GameServerPacket packet(Player paramPlayer) {
    L2GameServerPacket l2GameServerPacket = super.packet(paramPlayer);
    if (this._message.size() != this._arguments.size())
      throw new IllegalArgumentException("Wrong count of arguments: " + this._message); 
    return l2GameServerPacket;
  }

  
  protected void writeElements() {
    writeH(this._message.getId());
    writeC(this._arguments.size());
    for (IArgument iArgument : this._arguments) {
      iArgument.write(this);
    }
  }

  
  public T addName(GameObject paramGameObject) {
    if (paramGameObject == null) {
      return (T)add(new StringArgument(null));
    }
    if (paramGameObject.isNpc())
      return (T)add(new NpcNameArgument(((NpcInstance)paramGameObject).getNpcId() + 1000000)); 
    if (paramGameObject instanceof Summon)
      return (T)add(new NpcNameArgument(((Summon)paramGameObject).getNpcId() + 1000000)); 
    if (paramGameObject.isItem())
      return (T)add(new ItemNameArgument(((ItemInstance)paramGameObject).getItemId())); 
    if (paramGameObject.isPlayer())
      return (T)add(new PlayerNameArgument((Player)paramGameObject)); 
    if (paramGameObject.isDoor())
      return (T)add(new StaticObjectNameArgument(((DoorInstance)paramGameObject).getDoorId())); 
    if (paramGameObject instanceof StaticObjectInstance) {
      return (T)add(new StaticObjectNameArgument(((StaticObjectInstance)paramGameObject).getUId()));
    }
    return (T)add(new StringArgument(paramGameObject.getName()));
  }


  
  public T addInstanceName(int paramInt) { return (T)add(new InstanceNameArgument(paramInt)); }



  
  public T addSysString(int paramInt) { return (T)add(new SysStringArgument(paramInt)); }



  
  public T addSkillName(Skill paramSkill) { return (T)addSkillName(paramSkill.getDisplayId(), paramSkill.getDisplayLevel()); }



  
  public T addSkillName(int paramInt1, int paramInt2) { return (T)add(new SkillArgument(paramInt1, paramInt2)); }



  
  public T addItemName(int paramInt) { return (T)add(new ItemNameArgument(paramInt)); }



  
  @Deprecated
  public T addItemNameWithAugmentation(ItemInstance paramItemInstance) { return (T)add(new ItemNameWithAugmentationArgument(paramItemInstance.getItemId(), paramItemInstance.getVariationStat1(), paramItemInstance.getVariationStat2())); }



  
  public T addZoneName(Creature paramCreature) { return (T)addZoneName(paramCreature.getX(), paramCreature.getY(), paramCreature.getZ()); }



  
  public T addZoneName(Location paramLocation) { return (T)add(new ZoneArgument(paramLocation.x, paramLocation.y, paramLocation.z)); }



  
  public T addZoneName(int paramInt1, int paramInt2, int paramInt3) { return (T)add(new ZoneArgument(paramInt1, paramInt2, paramInt3)); }



  
  public T addResidenceName(Residence paramResidence) { return (T)add(new ResidenceArgument(paramResidence.getId())); }



  
  public T addResidenceName(int paramInt) { return (T)add(new ResidenceArgument(paramInt)); }



  
  public T addElementName(int paramInt) { return (T)add(new ElementNameArgument(paramInt)); }



  
  public T addElementName(Element paramElement) { return (T)add(new ElementNameArgument(paramElement.getId())); }



  
  public T addInteger(double paramDouble) { return (T)add(new IntegerArgument((int)paramDouble)); }



  
  public T addLong(long paramLong) { return (T)add(new LongArgument(paramLong)); }



  
  public T addString(String paramString) { return (T)add(new StringArgument(paramString)); }



  
  protected T add(IArgument paramIArgument) {
    this._arguments.add(paramIArgument);
    
    return (T)this;
  }





  
  public static abstract class IArgument
  {
    void write(SysMsgContainer param1SysMsgContainer) {
      param1SysMsgContainer.writeC(getType().ordinal());
      
      writeData(param1SysMsgContainer);
    }

    
    abstract SysMsgContainer.Types getType();
    
    abstract void writeData(SysMsgContainer param1SysMsgContainer);
  }
  
  public static class IntegerArgument
    extends IArgument
  {
    private final int alS;
    
    public IntegerArgument(int param1Int) { this.alS = param1Int; }




    
    public void writeData(SysMsgContainer param1SysMsgContainer) { param1SysMsgContainer.writeD(this.alS); }




    
    SysMsgContainer.Types getType() { return SysMsgContainer.Types.NUMBER; }
  }


  
  public static class NpcNameArgument
    extends IntegerArgument
  {
    public NpcNameArgument(int param1Int) { super(param1Int); }




    
    SysMsgContainer.Types getType() { return SysMsgContainer.Types.NPC_NAME; }
  }


  
  public static class ItemNameArgument
    extends IntegerArgument
  {
    public ItemNameArgument(int param1Int) { super(param1Int); }




    
    SysMsgContainer.Types getType() { return SysMsgContainer.Types.ITEM_NAME; }
  }

  
  public static class ItemNameWithAugmentationArgument
    extends IArgument
  {
    private final int _itemId;
    private final int alT;
    private final int alU;
    
    public ItemNameWithAugmentationArgument(int param1Int1, int param1Int2, int param1Int3) {
      this._itemId = param1Int1;
      this.alT = param1Int2;
      this.alU = param1Int3;
    }



    
    SysMsgContainer.Types getType() { return SysMsgContainer.Types.ITEM_NAME_WITH_AUGMENTATION; }



    
    void writeData(SysMsgContainer param1SysMsgContainer) {
      param1SysMsgContainer.writeD(this._itemId);
      param1SysMsgContainer.writeH(this.alT);
      param1SysMsgContainer.writeH(this.alU);
    }
  }

  
  public static class InstanceNameArgument
    extends IntegerArgument
  {
    public InstanceNameArgument(int param1Int) { super(param1Int); }




    
    SysMsgContainer.Types getType() { return SysMsgContainer.Types.INSTANCE_NAME; }
  }


  
  public static class SysStringArgument
    extends IntegerArgument
  {
    public SysStringArgument(int param1Int) { super(param1Int); }




    
    SysMsgContainer.Types getType() { return SysMsgContainer.Types.SYSTEM_STRING; }
  }

  
  public static class ResidenceArgument
    extends IArgument
  {
    private final int alW;
    
    public ResidenceArgument(int param1Int) { this.alW = param1Int; }




    
    SysMsgContainer.Types getType() { return SysMsgContainer.Types.RESIDENCE_NAME; }




    
    void writeData(SysMsgContainer param1SysMsgContainer) { param1SysMsgContainer.writeH(this.alW); }
  }


  
  public static class StaticObjectNameArgument
    extends IntegerArgument
  {
    public StaticObjectNameArgument(int param1Int) { super(param1Int); }




    
    SysMsgContainer.Types getType() { return SysMsgContainer.Types.STATIC_OBJECT_NAME; }
  }

  
  public static class LongArgument
    extends IArgument
  {
    private final long alV;

    
    public LongArgument(long param1Long) { this.alV = param1Long; }




    
    void writeData(SysMsgContainer param1SysMsgContainer) { param1SysMsgContainer.writeQ(this.alV); }




    
    SysMsgContainer.Types getType() { return SysMsgContainer.Types.LONG; }
  }

  
  public static class StringArgument
    extends IArgument
  {
    private final String alX;

    
    public StringArgument(String param1String) { this.alX = (param1String == null) ? "null" : param1String; }




    
    void writeData(SysMsgContainer param1SysMsgContainer) { param1SysMsgContainer.writeS(this.alX); }




    
    SysMsgContainer.Types getType() { return SysMsgContainer.Types.TEXT; }
  }

  
  public static class SkillArgument
    extends IArgument
  {
    private final int _skillId;
    private final int _skillLevel;
    
    public SkillArgument(int param1Int1, int param1Int2) {
      this._skillId = param1Int1;
      if (param1Int2 < 100) {
        
        this._skillLevel = param1Int2;
      }
      else {
        
        int i = SkillTable.getInstance().getBaseLevel(param1Int1);
        int j = param1Int2 % 100;
        int k = (1 + j / 40) * 1000 + j % 40;
        this._skillLevel = i | k << 16;
      } 
    }


    
    void writeData(SysMsgContainer param1SysMsgContainer) {
      param1SysMsgContainer.writeD(this._skillId);
      param1SysMsgContainer.writeD(this._skillLevel);
    }



    
    SysMsgContainer.Types getType() { return SysMsgContainer.Types.SKILL_NAME; }
  }

  
  public static class ZoneArgument
    extends IArgument
  {
    private final int _x;
    private final int _y;
    private final int _z;
    
    public ZoneArgument(int param1Int1, int param1Int2, int param1Int3) {
      this._x = param1Int1;
      this._y = param1Int2;
      this._z = param1Int3;
    }


    
    void writeData(SysMsgContainer param1SysMsgContainer) {
      param1SysMsgContainer.writeD(this._x);
      param1SysMsgContainer.writeD(this._y);
      param1SysMsgContainer.writeD(this._z);
    }



    
    SysMsgContainer.Types getType() { return SysMsgContainer.Types.ZONE_NAME; }
  }


  
  public static class ElementNameArgument
    extends IntegerArgument
  {
    public ElementNameArgument(int param1Int) { super(param1Int); }




    
    SysMsgContainer.Types getType() { return SysMsgContainer.Types.ELEMENT_NAME; }
  }


  
  public static class PlayerNameArgument
    extends StringArgument
  {
    public PlayerNameArgument(Creature param1Creature) { super(param1Creature.getName()); }




    
    SysMsgContainer.Types getType() { return SysMsgContainer.Types.TEXT; }
  }
}
