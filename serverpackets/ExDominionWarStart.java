package l2.gameserver.network.l2.s2c;






public class ExDominionWarStart
  extends L2GameServerPacket
{
  private int Bq;
  private int aed;
  private boolean aee;
  
  protected void writeImpl() {
    writeEx(163);
    writeD(this.Bq);
    writeD(1);
    writeD(this.aed);
    writeD(this.aee ? 1 : 0);
    writeD(this.aee ? this.aed : 0);
  }
}
