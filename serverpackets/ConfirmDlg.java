package l2.gameserver.network.l2.s2c;

import l2.gameserver.network.l2.components.SystemMsg;





public class ConfirmDlg
  extends SysMsgContainer<ConfirmDlg>
{
  private int _time;
  private int Xw;
  
  public ConfirmDlg(SystemMsg paramSystemMsg, int paramInt) {
    super(paramSystemMsg);
    this._time = paramInt;
  }



  
  protected final void writeImpl() {
    writeC(243);
    writeD(this._message.getId());
    writeD(this._arguments.size());
    for (SysMsgContainer.IArgument iArgument : this._arguments) {
      
      writeD(iArgument.getType().ordinal());
      iArgument.writeData(this);
    } 
    writeD(this._time);
    writeD(this.Xw);
  }


  
  public void setRequestId(int paramInt) { this.Xw = paramInt; }
}
