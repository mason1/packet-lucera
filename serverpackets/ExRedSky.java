package l2.gameserver.network.l2.s2c;


public class ExRedSky
  extends L2GameServerPacket
{
  private int aaw;
  
  public ExRedSky(int paramInt) { this.aaw = paramInt; }



  
  protected final void writeImpl() {
    writeEx(66);
    writeD(this.aaw);
  }
}
