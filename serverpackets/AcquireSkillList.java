package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.base.AcquireType;





public class AcquireSkillList
  extends L2GameServerPacket
{
  private AcquireType XO;
  private final List<Skill> _skills;
  
  class Skill
  {
    public int id;
    public int nextLevel;
    public int maxLevel;
    public int cost;
    public int requirements;
    public int subUnit;
    
    Skill(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6) {
      this.id = param1Int1;
      this.nextLevel = param1Int2;
      this.maxLevel = param1Int3;
      this.cost = param1Int4;
      this.requirements = param1Int5;
      this.subUnit = param1Int6;
    }
  }

  
  public AcquireSkillList(AcquireType paramAcquireType, int paramInt) {
    this._skills = new ArrayList(paramInt);
    this.XO = paramAcquireType;
  }


  
  public void addSkill(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) { this._skills.add(new Skill(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6)); }



  
  public void addSkill(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) { this._skills.add(new Skill(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, 0)); }



  
  protected final void writeImpl() {
    writeC(144);
    writeD(this.XO.ordinal());
    writeD(this._skills.size());
    
    for (Skill skill : this._skills) {
      
      writeD(skill.id);
      writeD(skill.nextLevel);
      writeD(skill.maxLevel);
      writeD(skill.cost);
      writeD(skill.requirements);
    } 
  }
}
