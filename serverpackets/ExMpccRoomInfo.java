package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.matching.MatchingRoom;

public class ExMpccRoomInfo
  extends L2GameServerPacket
{
  private int _index;
  private int YR;
  private int p;
  private int q;
  private int Vk;
  private int YO;
  private String Vl;
  
  public ExMpccRoomInfo(MatchingRoom paramMatchingRoom) {
    this._index = paramMatchingRoom.getId();
    this.YO = paramMatchingRoom.getLocationId();
    this.Vl = paramMatchingRoom.getTopic();
    this.p = paramMatchingRoom.getMinLevel();
    this.q = paramMatchingRoom.getMaxLevel();
    this.YR = paramMatchingRoom.getMaxMembersSize();
    this.Vk = paramMatchingRoom.getLootType();
  }


  
  public void writeImpl() {
    writeEx(156);
    
    writeD(this._index);
    writeD(this.YR);
    writeD(this.p);
    writeD(this.q);
    writeD(this.Vk);
    writeD(this.YO);
    writeS(this.Vl);
  }
}
