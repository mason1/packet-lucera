package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import l2.gameserver.model.SkillLearn;
import l2.gameserver.model.base.AcquireType;






public class AcquireSkillInfo
  extends L2GameServerPacket
{
  private SkillLearn aaH;
  private AcquireType XO;
  private List<Require> aaI;
  
  public AcquireSkillInfo(AcquireType paramAcquireType, SkillLearn paramSkillLearn) { this(paramAcquireType, paramSkillLearn, paramSkillLearn.getItemId(), (int)paramSkillLearn.getItemCount()); }

  
  public AcquireSkillInfo(AcquireType paramAcquireType, SkillLearn paramSkillLearn, int paramInt1, int paramInt2) {
    this.aaI = Collections.emptyList();
    this.XO = paramAcquireType;
    this.aaH = paramSkillLearn;
    if (this.aaH.getItemId() != 0) {
      
      this.aaI = new ArrayList(1);
      this.aaI.add(new Require(99, this.aaH.getItemId(), this.aaH.getItemCount(), 50));
    } 
  }


  
  public void writeImpl() {
    writeC(145);
    writeD(this.aaH.getId());
    writeD(this.aaH.getLevel());
    writeQ(this.aaH.getCost());
    writeD(this.XO.ordinal());
    
    writeD(this.aaI.size());
    
    for (Require require : this.aaI) {
      
      writeD(require.type);
      writeD(require.itemId);
      writeQ(require.count);
      writeD(require.unk);
    } 
  }

  
  private static class Require
  {
    public int itemId;
    public long count;
    public int type;
    public int unk;
    
    public Require(int param1Int1, int param1Int2, long param1Long, int param1Int3) {
      this.itemId = param1Int2;
      this.type = param1Int1;
      this.count = param1Long;
      this.unk = param1Int3;
    }
  }
}
