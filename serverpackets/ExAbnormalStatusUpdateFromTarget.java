package l2.gameserver.network.l2.s2c;

import java.util.LinkedList;
import java.util.List;
import l2.gameserver.model.Effect;
import l2.gameserver.model.Player;












public class ExAbnormalStatusUpdateFromTarget
  extends L2GameServerPacket
{
  private final List<AbnormalStatusUpdate.Effect> adb;
  private final int objectId;
  
  public ExAbnormalStatusUpdateFromTarget(Player paramPlayer) { this(paramPlayer, false); }

  
  public ExAbnormalStatusUpdateFromTarget(Player paramPlayer, boolean paramBoolean) {
    this.adb = new LinkedList();
    this.objectId = paramPlayer.getObjectId();
    if (paramBoolean)
    {
      for (Effect effect : paramPlayer.getEffectList().getAllFirstEffects()) {
        
        if (effect != null && effect.isInUse()) {
          
          if (!effect.isActive() || effect.isHidden() || effect.getSkill().isToggle()) {
            continue;
          }
          
          addEffect(effect);
        } 
      } 
    }
  }

  
  public void addEffect(Effect paramEffect) {
    int i = paramEffect.getDisplayLevel();
    if (i < 100) {
      addEffect(paramEffect.getDisplayId(), i, paramEffect.getTimeLeft(), 0, (paramEffect.getEffector() != null) ? paramEffect.getEffector().getObjectId() : 0);
    } else {
      addEffect(paramEffect.getDisplayId(), (paramEffect.getSkill() != null) ? paramEffect.getSkill().getBaseLevel() : 1, paramEffect.getTimeLeft(), 0, (paramEffect.getEffector() != null) ? paramEffect.getEffector().getObjectId() : 0);
    } 
  }

  
  public void addEffect(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) { this.adb.add(new AbnormalStatusUpdate.Effect(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5)); }



  
  protected void writeImpl() {
    writeEx(230);
    writeD(this.objectId);
    writeH(this.adb.size());
    for (AbnormalStatusUpdate.Effect effect : this.adb) {
      
      writeD(effect.skillId);
      writeH(effect.skillLevel);
      writeH(effect.clientAbnormalId);
      writeOptionalD(effect.duration);
      writeD(effect.effectorObjectId);
    } 
  }
}
