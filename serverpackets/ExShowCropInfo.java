package l2.gameserver.network.l2.s2c;

import java.util.List;
import l2.gameserver.model.Manor;
import l2.gameserver.templates.manor.CropProcure;









public class ExShowCropInfo
  extends L2GameServerPacket
{
  private List<CropProcure> agu;
  private int EJ;
  
  public ExShowCropInfo(int paramInt, List<CropProcure> paramList) {
    this.EJ = paramInt;
    this.agu = paramList;
  }


  
  protected void writeImpl() {
    writeEx(36);
    writeC(0);
    writeD(this.EJ);
    writeD(0);
    writeD(this.agu.size());
    for (CropProcure cropProcure : this.agu) {
      
      writeD(cropProcure.getId());
      writeQ(cropProcure.getAmount());
      writeQ(cropProcure.getStartAmount());
      writeQ(cropProcure.getPrice());
      writeC(cropProcure.getReward());
      writeD(Manor.getInstance().getSeedLevelByCrop(cropProcure.getId()));
      
      writeC(1);
      writeD(Manor.getInstance().getRewardItem(cropProcure.getId(), 1));
      
      writeC(1);
      writeD(Manor.getInstance().getRewardItem(cropProcure.getId(), 2));
    } 
  }
}
