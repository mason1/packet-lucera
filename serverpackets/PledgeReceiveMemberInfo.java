package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.pledge.UnitMember;


public class PledgeReceiveMemberInfo
  extends L2GameServerPacket
{
  private UnitMember ajT;
  
  public PledgeReceiveMemberInfo(UnitMember paramUnitMember) { this.ajT = paramUnitMember; }



  
  protected final void writeImpl() {
    writeEx(63);
    
    writeD(this.ajT.getPledgeType());
    writeS(this.ajT.getName());
    writeS(this.ajT.getTitle());
    writeD(this.ajT.getPowerGrade());
    writeS(this.ajT.getSubUnit().getName());
    writeS(this.ajT.getRelatedName());
  }
}
