package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.entity.boat.Boat;

public class VehicleStart
  extends L2GameServerPacket {
  private int Bq;
  private int _state;
  
  public VehicleStart(Boat paramBoat) {
    this.Bq = paramBoat.getObjectId();
    this._state = paramBoat.getRunState();
  }


  
  protected void writeImpl() {
    writeC(192);
    writeD(this.Bq);
    writeD(this._state);
  }
}
