package l2.gameserver.network.l2.s2c;

import l2.gameserver.Config;
import l2.gameserver.model.GameObjectsStorage;
import l2.gameserver.model.Player;

public final class SendStatus
  extends L2GameServerPacket {
  private static final long akX = 30000L;
  private static int akY = 0;
  private static int akZ = 0;
  private static int ala = 0;
  private static long alb = 0L;

  
  public SendStatus() {
    byte b1 = 0;
    byte b2 = 0;
    for (Player player : GameObjectsStorage.getAllPlayersForIterate()) {
      if (player != null) {
        
        b1++;
        if (player.isInStoreMode() && (!Config.SENDSTATUS_TRADE_JUST_OFFLINE || player.isInOfflineMode()))
          b2++; 
      } 
    }  akY = (int)(b1 * Config.MUL_PLAYERS_ONLINE);
    ala = (int)Math.floor(b2 * Config.SENDSTATUS_TRADE_MOD);
    akZ = Math.max(akZ, akY);
  }


  
  protected final void writeImpl() {
    if (System.currentTimeMillis() - alb < 30000L)
      return; 
    alb = System.currentTimeMillis();
    writeC(46);
    writeD(1);
    writeD(akZ);
    writeD(akY);
    writeD(akY);
    writeD(ala);
    writeH(48);
    writeH(44);
    writeH(53);
    writeH(49);
    writeH(48);
    writeH(44);
    writeH(55);
    writeH(55);
    writeH(55);
    writeH(53);
    writeH(56);
    writeH(44);
    writeH(54);
    writeH(53);
    writeH(48);
    writeD(54);
    writeD(119);
    writeD(183);
    writeQ(159L);
    writeD(0);
    writeH(65);
    writeH(117);
    writeH(103);
    writeH(32);
    writeH(50);
    writeH(57);
    writeH(32);
    writeH(50);
    writeH(48);
    writeH(48);
    writeD(57);
    writeH(48);
    writeH(50);
    writeH(58);
    writeH(52);
    writeH(48);
    writeH(58);
    writeH(52);
    writeD(51);
    writeD(87);
    writeC(17);
    writeC(93);
    writeC(31);
    writeC(96);
  }
}
