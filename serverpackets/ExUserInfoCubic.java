package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.skills.effects.EffectCubic;





public class ExUserInfoCubic
  extends L2GameServerPacket
{
  private int objectId;
  private EffectCubic[] acc;
  private int ahm;
  
  public ExUserInfoCubic(Player paramPlayer) {
    this.objectId = paramPlayer.getObjectId();
    this.acc = (EffectCubic[])paramPlayer.getCubics().toArray(new EffectCubic[paramPlayer.getCubics().size()]);
    this.ahm = paramPlayer.getAgathionId();
  }


  
  protected void writeImpl() {
    writeEx(343);
    writeD(this.objectId);
    writeH(this.acc.length);
    for (EffectCubic effectCubic : this.acc)
    {
      writeH(effectCubic.getId());
    }
    writeD(this.ahm);
  }
}
