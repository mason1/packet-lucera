package l2.gameserver.network.l2.s2c;
public class Ex2ndPasswordAck extends L2GameServerPacket {
  private final Ex2ndPasswordAckResult acR;
  private final int acS;
  
  public enum Ex2ndPasswordAckResult {
    SUCCESS_CREATE(0, 0),
    SUCCESS_VERIFY(2, 0),
    FAIL_CREATE(0, 1),
    FAIL_VERIFY(2, 1),
    BLOCK_HOMEPAGE(0, 2),
    ERROR(3, 0);
    
    private int acT;
    private int acU;
    
    Ex2ndPasswordAckResult(int param1Int1, int param1Int2) {
      this.acT = param1Int1;
      this.acU = param1Int2;
    }


    
    public int getArg0() { return this.acT; }



    
    public int getArg1() { return this.acU; }
  }





  
  public Ex2ndPasswordAck(Ex2ndPasswordAckResult paramEx2ndPasswordAckResult) {
    this.acR = paramEx2ndPasswordAckResult;
    this.acS = 0;
  }

  
  public Ex2ndPasswordAck(Ex2ndPasswordAckResult paramEx2ndPasswordAckResult, int paramInt) {
    this.acR = paramEx2ndPasswordAckResult;
    this.acS = paramInt;
  }


  
  protected void writeImpl() {
    writeEx(263);
    writeC(this.acR.getArg0());
    writeD(this.acR.getArg1());
    writeD(this.acS);
  }
}
