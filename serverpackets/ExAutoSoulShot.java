package l2.gameserver.network.l2.s2c;

public class ExAutoSoulShot
  extends L2GameServerPacket
{
  private final int _itemId;
  private final boolean enabled;
  private final int type;
  
  public ExAutoSoulShot(int paramInt1, boolean paramBoolean, int paramInt2) {
    this._itemId = paramInt1;
    this.enabled = paramBoolean;
    this.type = paramInt2;
  }


  
  protected final void writeImpl() {
    writeEx(12);
    
    writeD(this._itemId);
    writeD(this.enabled);
    writeD(this.type);
  }
}
