package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.skills.TimeStamp;

public class SkillCoolTime
  extends L2GameServerPacket {
  private List<Skill> abd = Collections.emptyList();

  
  public SkillCoolTime(Player paramPlayer) {
    Collection collection = paramPlayer.getSkillReuses();
    this.abd = new ArrayList(collection.size());
    for (TimeStamp timeStamp : collection) {
      
      if (!timeStamp.hasNotPassed())
        continue; 
      Skill skill = paramPlayer.getKnownSkill(timeStamp.getId());
      if (skill == null)
        continue; 
      Skill skill1 = new Skill(null);
      skill1.skillId = skill.getId();
      skill1.level = skill.getLevel();
      skill1.reuseBase = (int)Math.floor(timeStamp.getReuseBasic() / 1000.0D);
      skill1.reuseCurrent = (int)Math.floor(timeStamp.getReuseCurrent() / 1000.0D);
      this.abd.add(skill1);
    } 
  }


  
  protected final void writeImpl() {
    writeC(199);
    writeD(this.abd.size());
    for (byte b = 0; b < this.abd.size(); b++) {
      
      Skill skill = (Skill)this.abd.get(b);
      writeD(skill.skillId);
      writeD(skill.level);
      writeD(skill.reuseBase);
      writeD(skill.reuseCurrent);
    } 
  }
  
  private static class Skill {
    public int skillId;
    public int level;
    public int reuseBase;
    public int reuseCurrent;
    
    private Skill() {}
  }
}
