package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.base.Element;
import l2.gameserver.model.base.Experience;
import l2.gameserver.model.items.Inventory;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.utils.Location;


public class GMViewCharacterInfo
  extends L2GameServerPacket
{
  private Location _loc;
  private int[][] abp;
  private int acr;
  private int Bt;
  private int BB;
  private int class_id;
  private int pvp_flag;
  private int karma;
  private int level;
  private int abS;
  private int ahG;
  private int ahH;
  private int ahI;
  private int ahJ;
  private int ahK;
  private int ahL;
  private int _sp;
  private int curHp;
  private int maxHp;
  
  public GMViewCharacterInfo(Player paramPlayer) {
    this._loc = paramPlayer.getLoc();
    this.acr = paramPlayer.getObjectId();
    this._name = paramPlayer.getName();
    this.Bt = paramPlayer.getRace().ordinal();
    this.BB = paramPlayer.getSex();
    this.class_id = paramPlayer.getClassId().getId();
    this.level = paramPlayer.getLevel();
    this._exp = paramPlayer.getExp();
    this.ahG = paramPlayer.getSTR();
    this.ahI = paramPlayer.getDEX();
    this.ahH = paramPlayer.getCON();
    this.ahJ = paramPlayer.getINT();
    this.ahK = paramPlayer.getWIT();
    this.ahL = paramPlayer.getMEN();
    this.curHp = (int)paramPlayer.getCurrentHp();
    this.maxHp = paramPlayer.getMaxHp();
    this.curMp = (int)paramPlayer.getCurrentMp();
    this.maxMp = paramPlayer.getMaxMp();
    this._sp = paramPlayer.getIntSp();
    this.aho = paramPlayer.getCurrentLoad();
    this.ahp = paramPlayer.getMaxLoad();
    this.ahO = paramPlayer.getPAtk(null);
    this.ahP = paramPlayer.getPAtkSpd();
    this.ahQ = paramPlayer.getPDef(null);
    this.ahR = paramPlayer.getEvasionRate(null);
    this.ahS = paramPlayer.getAccuracy();
    this.ahT = paramPlayer.getCriticalHit(null, null);
    this.ahU = paramPlayer.getMAtk(null, null);
    this.ahV = paramPlayer.getMAtkSpd();
    this.ahW = paramPlayer.getMDef(null, null);
    this.pvp_flag = paramPlayer.getPvpFlag();
    this.karma = paramPlayer.getKarma();
    this.abq = paramPlayer.getRunSpeed();
    this.abr = paramPlayer.getWalkSpeed();
    this.abs = paramPlayer.getSwimSpeed();
    this.aig = paramPlayer.getMovementSpeedMultiplier();
    this.aih = paramPlayer.getAttackSpeedMultiplier();
    this.abS = paramPlayer.getMountType();
    this.abC = paramPlayer.getColRadius();
    this.abD = paramPlayer.getColHeight();
    this.abE = paramPlayer.getHairStyle();
    this.abF = paramPlayer.getHairColor();
    this.abG = paramPlayer.getFace();
    this.ahX = paramPlayer.isGM() ? 1 : 0;
    this.title = paramPlayer.getTitle();
    this.ain = Experience.getExpPercent(paramPlayer.getLevel(), paramPlayer.getExp());
    
    Clan clan = paramPlayer.getClan();
    Alliance alliance = (clan == null) ? null : clan.getAlliance();
    
    this.clan_id = (clan == null) ? 0 : clan.getClanId();
    this.clan_crest_id = (clan == null) ? 0 : clan.getCrestId();
    
    this.ally_id = (alliance == null) ? 0 : alliance.getAllyId();

    
    this.abM = paramPlayer.isInObserverMode() ? 7 : paramPlayer.getPrivateStoreType();
    this.aie = Math.max(paramPlayer.getSkillLevel(Integer.valueOf(1320)), 0);
    this.aic = paramPlayer.getPkKills();
    this.aid = paramPlayer.getPvpKills();
    this.ahN = paramPlayer.getGivableRec();
    this.abz = paramPlayer.getReceivedRec();
    this.curCp = (int)paramPlayer.getCurrentCp();
    this.maxCp = paramPlayer.getMaxCp();
    this.running = paramPlayer.isRunning() ? 1 : 0;
    this.aif = paramPlayer.getPledgeClass();
    this.ahZ = paramPlayer.isNoble() ? 1 : 0;
    this.aia = paramPlayer.isHero() ? 1 : 0;
    this.aib = paramPlayer.getNameColor();
    this.ahY = paramPlayer.getTitleColor();
    this.aii = paramPlayer.getAttackElement();
    this.TQ = paramPlayer.getAttack(this.aii);
    this.TR = paramPlayer.getDefence(Element.FIRE);
    this.TS = paramPlayer.getDefence(Element.WATER);
    this.TT = paramPlayer.getDefence(Element.WIND);
    this.TU = paramPlayer.getDefence(Element.EARTH);
    this.TV = paramPlayer.getDefence(Element.HOLY);
    this.TW = paramPlayer.getDefence(Element.UNHOLY);

    
    this.ail = paramPlayer.getTalismanCount();
    
    this.abp = new int[59][3];
    for (int i : Inventory.PAPERDOLL_ORDER) {
      
      this.abp[i][0] = paramPlayer.getInventory().getPaperdollObjectId(i);
      this.abp[i][1] = paramPlayer.getInventory().getPaperdollItemId(i);
      this.abp[i][2] = paramPlayer.getInventory().getPaperdollAugmentationId(i);
    } 
  }
  private int curMp; private int maxMp; private int curCp; private int maxCp; private int aho; private int ahp; private int ahN; private int abz; private int ahO; private int ahP; private int ahQ; private int ahR; private int ahS; private int ahT; private int ahU; private int ahV; private int ahW; private int abE; private int abF; private int abG; private int ahX; private int clan_id; private int clan_crest_id; private int ally_id; private int ahY; private int ahZ; private int aia;
  private int abM;
  
  protected final void writeImpl() {
    writeC(149);
    
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
    writeD(this._loc.h);
    writeD(this.acr);
    writeS(this._name);
    writeD(this.Bt);
    writeD(this.BB);
    writeD(this.class_id);
    writeD(this.level);
    writeQ(this._exp);
    writeF(this.ain);
    
    writeD(this.ahG);
    writeD(this.ahI);
    writeD(this.ahH);
    writeD(this.ahJ);
    
    writeD(this.ahK);
    writeD(this.ahL);
    writeD(0);
    writeD(0);
    
    writeD(this.maxHp);
    writeD(this.curHp);
    writeD(this.maxMp);
    writeD(this.curMp);
    
    writeQ(this._sp);
    writeD(this.aho);
    writeD(this.ahp);
    writeD(this.aic);
    
    for (int i : Inventory.PAPERDOLL_ORDER) {
      writeD(this.abp[i][0]);
    }
    for (int i : Inventory.PAPERDOLL_ORDER) {
      writeD(this.abp[i][1]);
    }
    for (byte b = 0; b < 11; b++) {
      
      writeD(0);
      writeD(0);
    } 
    
    writeC(this.ail);
    writeC(this.aim ? 1 : 0);
    
    writeD(0);
    writeH(0);
    
    writeD(this.ahO);
    writeD(this.ahP);
    writeD(this.ahQ);
    writeD(this.ahR);
    
    writeD(this.ahS);
    writeD(this.ahT);
    writeD(this.ahU);
    writeD(this.ahV);
    
    writeD(this.ahP);
    writeD(this.ahW);
    writeD(0);
    writeD(0);
    
    writeD(0);
    writeD(this.pvp_flag);
    writeD(this.karma);
    writeD(this.abq);
    
    writeD(this.abr);
    writeD(this.abs);
    writeD(this.abs);
    writeD(this.abq);
    
    writeD(this.abr);
    writeD(this.abq);
    writeD(this.abr);
    
    writeF(this.aig);
    writeF(this.aih);
    writeF(this.abC);
    writeF(this.abD);
    
    writeD(this.abE);
    writeD(this.abF);
    writeD(this.abG);
    writeD(this.ahX);
    
    writeS(this.title);
    
    writeD(this.clan_id);
    writeD(this.clan_crest_id);
    writeD(this.ally_id);
    
    writeC(this.abS);
    writeC(this.abM);
    writeC(this.aie);
    
    writeD(this.aic);
    writeD(this.aid);
    
    writeH(this.ahN);
    writeH(this.abz);
    
    writeD(this.class_id);
    writeD(0);
    writeD(this.maxCp);
    writeD(this.curCp);
    writeC(this.running);
    writeC(321);
    writeD(this.aif);
    writeC(this.ahZ);
    writeC(this.aia);
    writeD(this.aib);
    writeD(this.ahY);
    
    writeH(this.aii.getId());
    writeH(this.TQ);
    writeH(this.TR);
    writeH(this.TS);
    writeH(this.TT);
    writeH(this.TU);
    writeH(this.TV);
    writeH(this.TW);
    
    writeD(0);
    writeD(0);
  }
  
  private int aib;
  private int aic;
  private int aid;
  private int abq;
  private int abr;
  private int abs;
  private int aie;
  private int running;
  private int aif;
  private String _name;
  private String title;
  private long _exp;
  private double aig;
  private double aih;
  private double abC;
  private double abD;
  private Element aii;
  private int TQ;
  private int TR;
  private int TS;
  private int TT;
  private int TU;
  private int TV;
  private int TW;
  private int aij;
  private int aik;
  private int ail;
  private boolean aim;
  private double ain;
}
