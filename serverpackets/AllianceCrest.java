package l2.gameserver.network.l2.s2c;

import l2.gameserver.Config;

public class AllianceCrest
  extends L2GameServerPacket
{
  private int Vv;
  private byte[] _data;
  
  public AllianceCrest(int paramInt, byte[] paramArrayOfByte) {
    this.Vv = paramInt;
    this._data = paramArrayOfByte;
  }


  
  protected final void writeImpl() {
    writeC(175);
    writeD(Config.REQUEST_ID);
    writeD(this.Vv);
    writeD(this._data.length);
    writeB(this._data);
  }
}
