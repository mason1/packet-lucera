package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;

public class ExVoteSystemInfo
  extends L2GameServerPacket {
  public static final ExVoteSystemInfo STATIC_EMPTY = new ExVoteSystemInfo();
  private int kv;
  private int ahu;
  
  public ExVoteSystemInfo(Player paramPlayer) {
    this.kv = paramPlayer.getGivableRec();
    this.ahu = paramPlayer.getReceivedRec();
  }
  private int _time; private int ahv; private int _mode;
  
  public ExVoteSystemInfo() {
    this.kv = 0;
    this.ahu = 0;
    this._time = 0;
    this.ahv = 0;
    this._mode = 11;
  }


  
  protected void writeImpl() {
    writeEx(202);
    writeD(this.kv);
    writeD(this.ahu);
    writeD(this._time);
    writeD(this.ahv);
    writeD(this._mode);
  }
}
