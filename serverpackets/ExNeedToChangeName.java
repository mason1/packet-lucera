package l2.gameserver.network.l2.s2c;

public class ExNeedToChangeName
  extends L2GameServerPacket {
  private int _type;
  private int pO;
  private String aeZ;
  
  public ExNeedToChangeName(int paramInt1, int paramInt2, String paramString) {
    this._type = paramInt1;
    this.pO = paramInt2;
    this.aeZ = paramString;
  }


  
  protected final void writeImpl() {
    writeEx(105);
    writeD(this._type);
    writeD(this.pO);
    writeS(this.aeZ);
  }
}
