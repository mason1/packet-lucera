package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.matching.MatchingRoom;

public class PartyRoomInfo
  extends L2GameServerPacket
{
  private int _id;
  private int p;
  private int q;
  private int Zx;
  private int Zy;
  private int ajl;
  private String _title;
  
  public PartyRoomInfo(MatchingRoom paramMatchingRoom) {
    this._id = paramMatchingRoom.getId();
    this.p = paramMatchingRoom.getMinLevel();
    this.q = paramMatchingRoom.getMaxLevel();
    this.Zx = paramMatchingRoom.getLootType();
    this.Zy = paramMatchingRoom.getMaxMembersSize();
    this.ajl = paramMatchingRoom.getLocationId();
    this._title = paramMatchingRoom.getTopic();
  }


  
  protected final void writeImpl() {
    writeC(157);
    writeD(this._id);
    writeD(this.Zy);
    writeD(this.p);
    writeD(this.q);
    writeD(this.Zx);
    writeD(this.ajl);
    writeS(this._title);
  }
}
