package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.commons.lang.ArrayUtils;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.Warehouse;



public class WareHouseDepositList
  extends AbstractItemListPacket
{
  private final boolean aks;
  private int amG;
  private long abb;
  private List<ItemInfo> aji;
  private int amH;
  
  public WareHouseDepositList(boolean paramBoolean, Player paramPlayer, Warehouse.WarehouseType paramWarehouseType) {
    this.aks = paramBoolean;
    this.amG = paramWarehouseType.ordinal();
    this.abb = paramPlayer.getAdena();
    switch (paramWarehouseType) {
      
      case PRIVATE:
        this.amH = paramPlayer.getWarehouse().getSize();
        break;
      case CLAN:
        this.amH = paramPlayer.getClan().getWarehouse().getSize();
        break;
    } 
    boolean bool = (this.amG == 1);
    ItemInstance[] arrayOfItemInstance = paramPlayer.getInventory().getItems();
    ArrayUtils.eqSort(arrayOfItemInstance, Warehouse.ItemClassComparator.getInstance());
    this.aji = new ArrayList(arrayOfItemInstance.length);
    for (ItemInstance itemInstance : arrayOfItemInstance) {
      if (itemInstance.canBeStored(paramPlayer, bool)) {
        this.aji.add(new ItemInfo(itemInstance));
      }
    } 
  }
  
  protected final void writeImpl() {
    writeC(65);
    writeC(this.aks ? 1 : 2);
    if (this.aks) {
      
      writeH(this.amG);
      writeQ(this.abb);
      writeD(this.amH);
      writeH(0);
      
      writeD(this.aji.size());
    }
    else {
      
      writeD(this.aji.size());
      writeD(this.aji.size());
      for (ItemInfo itemInfo : this.aji) {
        
        writeItemInfo(itemInfo);
        writeD(itemInfo.getObjectId());
      } 
    } 
  }
}
