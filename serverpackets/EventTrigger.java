package l2.gameserver.network.l2.s2c;





public class EventTrigger
  extends L2GameServerPacket
{
  private int acQ;
  private boolean _active;
  
  public EventTrigger(int paramInt, boolean paramBoolean) {
    this.acQ = paramInt;
    this._active = paramBoolean;
  }


  
  protected final void writeImpl() {
    writeC(207);
    writeD(this.acQ);
    writeC(this._active ? 1 : 0);
  }
}
