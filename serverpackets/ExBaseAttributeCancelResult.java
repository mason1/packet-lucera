package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.base.Element;
import l2.gameserver.model.items.ItemInstance;




public class ExBaseAttributeCancelResult
  extends L2GameServerPacket
{
  private boolean adA;
  private int Bq;
  private Element _element;
  
  public ExBaseAttributeCancelResult(boolean paramBoolean, ItemInstance paramItemInstance, Element paramElement) {
    this.adA = paramBoolean;
    this.Bq = paramItemInstance.getObjectId();
    this._element = paramElement;
  }


  
  protected void writeImpl() {
    writeEx(117);
    writeD(this.adA);
    writeD(this.Bq);
    writeD(this._element.getId());
  }
}
