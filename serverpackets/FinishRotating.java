package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;

public class FinishRotating extends L2GameServerPacket {
  private int Br;
  private int Xz;
  private int Fm;
  
  public FinishRotating(Creature paramCreature, int paramInt1, int paramInt2) {
    this.Br = paramCreature.getObjectId();
    this.Xz = paramInt1;
    this.Fm = paramInt2;
  }


  
  protected final void writeImpl() {
    writeC(97);
    writeD(this.Br);
    writeD(this.Xz);
    writeD(this.Fm);
    writeD(0);
  }
}
