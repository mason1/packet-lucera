package l2.gameserver.network.l2.s2c;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import l2.gameserver.Config;
import l2.gameserver.network.authcomm.ServerType;

public class KeyPacket
  extends L2GameServerPacket
{
  private Long aiK = null;


  
  public KeyPacket(Object paramObject) {
    if (paramObject != null) {
      
      ByteBuffer byteBuffer = ByteBuffer.wrap((byte[])paramObject).order(ByteOrder.LITTLE_ENDIAN);
      this.aiK = Long.valueOf(byteBuffer.getLong());
    } 
  }




  
  public void writeImpl() {
    writeC(46);
    if (this.aiK == null) {
      
      writeC(0);
      return;
    } 
    writeC(1);
    writeQ(this.aiK.longValue());
    writeD(1);
    writeD(0);
    writeC(0);
    writeD(0);
    writeC(((Config.AUTH_SERVER_SERVER_TYPE & ServerType.CLASSIC.getMask()) != 0));


















    
    writeC(0);
    writeC(0);
  }
}
