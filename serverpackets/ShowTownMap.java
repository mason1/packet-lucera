package l2.gameserver.network.l2.s2c;





public class ShowTownMap
  extends L2GameServerPacket
{
  String _texture;
  int _x;
  int _y;
  
  public ShowTownMap(String paramString, int paramInt1, int paramInt2) {
    this._texture = paramString;
    this._x = paramInt1;
    this._y = paramInt2;
  }


  
  protected final void writeImpl() {
    writeC(234);
    writeS(this._texture);
    writeD(this._x);
    writeD(this._y);
  }
}
