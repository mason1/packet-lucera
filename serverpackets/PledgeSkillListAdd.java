package l2.gameserver.network.l2.s2c;

public class PledgeSkillListAdd
  extends L2GameServerPacket
{
  private int _skillId;
  private int _skillLevel;
  
  public PledgeSkillListAdd(int paramInt1, int paramInt2) {
    this._skillId = paramInt1;
    this._skillLevel = paramInt2;
  }


  
  protected final void writeImpl() {
    writeEx(59);
    writeD(this._skillId);
    writeD(this._skillLevel);
  }
}
