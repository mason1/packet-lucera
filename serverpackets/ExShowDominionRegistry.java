package l2.gameserver.network.l2.s2c;

import java.util.Collections;
import java.util.List;

public class ExShowDominionRegistry
  extends L2GameServerPacket {
  private int YL;
  private String agw;
  private String agx;
  private String agy;
  private int agz;
  private int agA;
  private int agB;
  private int ms;
  private boolean agC;
  private boolean agD;
  private List<TerritoryFlagsInfo> agE = Collections.emptyList();


  
  protected void writeImpl() {
    writeEx(144);
    
    writeD(this.YL);
    writeS(this.agw);
    writeS(this.agx);
    writeS(this.agy);
    writeD(this.agz);
    writeD(this.agA);
    writeD(this.agB);
    writeD(this.ms);
    writeD(this.agD);
    writeD(this.agC);
    writeD(1);
    writeD(this.agE.size());
    for (TerritoryFlagsInfo territoryFlagsInfo : this.agE) {
      
      writeD(territoryFlagsInfo.id);
      writeD(territoryFlagsInfo.flags.length);
      for (int i : territoryFlagsInfo.flags) {
        writeD(i);
      }
    } 
  }
  
  private class TerritoryFlagsInfo
  {
    public int id;
    public int[] flags;
    
    public TerritoryFlagsInfo(int param1Int, int[] param1ArrayOfInt) {
      this.id = param1Int;
      this.flags = param1ArrayOfInt;
    }
  }
}
