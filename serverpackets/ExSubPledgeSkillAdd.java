package l2.gameserver.network.l2.s2c;


public class ExSubPledgeSkillAdd
  extends L2GameServerPacket
{
  private int _type;
  private int _id;
  private int _level;
  
  public ExSubPledgeSkillAdd(int paramInt1, int paramInt2, int paramInt3) {
    this._type = paramInt1;
    this._id = paramInt2;
    this._level = paramInt3;
  }


  
  protected void writeImpl() {
    writeEx(118);
    writeD(this._type);
    writeD(this._id);
    writeD(this._level);
  }
}
