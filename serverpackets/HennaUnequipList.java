package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Player;
import l2.gameserver.templates.Henna;

public class HennaUnequipList
  extends L2GameServerPacket {
  private int aiz;
  
  public HennaUnequipList(Player paramPlayer) {
    this.aiF = new ArrayList(3);


    
    this.abb = paramPlayer.getAdena();
    this.aiz = paramPlayer.getHennaEmptySlots();
    for (byte b = 1; b <= 3; b++) {
      if (paramPlayer.getHenna(b) != null)
        this.aiF.add(paramPlayer.getHenna(b)); 
    } 
  }
  private long abb; private List<Henna> aiF;
  
  protected final void writeImpl() {
    writeC(230);
    
    writeQ(this.abb);
    writeD(this.aiz);
    writeD(this.aiF.size());
    for (Henna henna : this.aiF) {
      
      writeD(henna.getSymbolId());
      writeD(henna.getDyeId());
      writeQ(henna.getDrawCount());
      writeQ(henna.getPrice());
      writeD(1);
    } 
  }
}
