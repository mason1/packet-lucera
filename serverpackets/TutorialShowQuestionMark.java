package l2.gameserver.network.l2.s2c;





public class TutorialShowQuestionMark
  extends L2GameServerPacket
{
  private int _number;
  
  public TutorialShowQuestionMark(int paramInt) { this._number = paramInt; }



  
  protected final void writeImpl() {
    writeC(167);
    writeD(this._number);
  }
}
