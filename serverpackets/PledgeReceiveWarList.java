package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import l2.gameserver.model.pledge.Clan;

public class PledgeReceiveWarList extends L2GameServerPacket {
  private List<WarInfo> ahh;
  
  public PledgeReceiveWarList(Clan paramClan, int paramInt1, int paramInt2) {
    this.ahh = new ArrayList();



    
    this._type = paramInt1;
    
    List list1 = paramClan.getAttackerClans();
    List list2 = paramClan.getEnemyClans();
    HashSet hashSet = new HashSet();
    hashSet.addAll(list1);
    hashSet.addAll(list2);
    
    for (Clan clan : hashSet) {


      
      byte b = 0;
      
      if (list1.contains(clan) && list2.contains(clan))
      {
        b = 2;
      }
      
      this.ahh.add(new WarInfo(clan.getName(), b));
    } 
  }
  
  private int _type;
  
  protected final void writeImpl() {
    writeEx(64);
    writeD(this._type);
    writeD(this.ahh.size());
    for (WarInfo warInfo : this.ahh) {
      
      writeS(warInfo.clan_name);
      writeD(warInfo.state);
      writeD(0);
      writeD(0);
      writeD(0);
      writeD(0);
    } 
  }

  
  static class WarInfo
  {
    public String clan_name;
    public int state;
    
    public WarInfo(String param1String, int param1Int) {
      this.clan_name = param1String;
      this.state = param1Int;
    }
  }
}
