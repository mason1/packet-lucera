package l2.gameserver.network.l2.s2c;

import java.util.List;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.TradeItem;







public class PrivateStoreListSell
  extends AbstractItemListPacket
{
  private int ZE;
  private long abb;
  private final boolean aaq;
  private List<TradeItem> Gl;
  
  public PrivateStoreListSell(Player paramPlayer1, Player paramPlayer2) {
    this.ZE = paramPlayer2.getObjectId();
    this.abb = paramPlayer1.getAdena();
    this.aaq = (paramPlayer2.getPrivateStoreType() == 8);
    this.Gl = paramPlayer2.getSellList();
  }


  
  protected final void writeImpl() {
    writeC(161);
    writeD(this.ZE);
    writeD(this.aaq ? 1 : 0);
    writeQ(this.abb);
    writeD(0);
    writeD(this.Gl.size());
    for (TradeItem tradeItem : this.Gl) {
      
      writeItemInfo(tradeItem);
      writeQ(tradeItem.getOwnersPrice());
      writeQ(tradeItem.getStorePrice());
    } 
  }
}
