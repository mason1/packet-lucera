package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;

public class RecipeShopMsg
  extends L2GameServerPacket
{
  private int Bq;
  private String akM;
  
  public RecipeShopMsg(Player paramPlayer) {
    this.Bq = paramPlayer.getObjectId();
    this.akM = paramPlayer.getManufactureName();
  }


  
  protected final void writeImpl() {
    writeC(225);
    writeD(this.Bq);
    writeS(this.akM);
  }
}
