package l2.gameserver.network.l2.s2c;

public class ExPutIntensiveResultForVariationMake
  extends L2GameServerPacket {
  public static final ExPutIntensiveResultForVariationMake FAIL_PACKET = new ExPutIntensiveResultForVariationMake(0, 0, 0, 0L, false);
  
  private int Ys;
  private int afH;
  
  public ExPutIntensiveResultForVariationMake(int paramInt1, int paramInt2, int paramInt3, long paramLong, boolean paramBoolean) {
    this.Ys = paramInt1;
    this.afH = paramInt2;
    this.afI = paramInt3;
    this.Yu = paramLong;
    this.oH = paramBoolean ? 1 : 0;
  }
  private int afI; private int oH;
  private long Yu;
  
  protected void writeImpl() {
    writeEx(85);
    writeD(this.Ys);
    writeD(this.afH);
    writeD(this.afI);
    writeQ(this.Yu);
    writeD(this.oH);
  }
}
