package l2.gameserver.network.l2.s2c;





public class ExShowFortressMapInfo
  extends L2GameServerPacket
{
  private int Ze;
  private boolean agF;
  private boolean[] agG;
  
  protected final void writeImpl() {
    writeEx(125);
    
    writeD(this.Ze);
    writeD(this.agF);
    writeD(this.agG.length);
    for (boolean bool : this.agG)
      writeD(bool); 
  }
}
