package l2.gameserver.network.l2.s2c;
import l2.gameserver.model.Player;
import l2.gameserver.templates.Henna;

public class GMHennaInfo extends L2GameServerPacket {
  private int _count;
  private int ahG;
  
  public GMHennaInfo(Player paramPlayer) {
    this.ahM = new Henna[3];


    
    this.ahG = paramPlayer.getHennaStatSTR();
    this.ahH = paramPlayer.getHennaStatCON();
    this.ahI = paramPlayer.getHennaStatDEX();
    this.ahJ = paramPlayer.getHennaStatINT();
    this.ahK = paramPlayer.getHennaStatWIT();
    this.ahL = paramPlayer.getHennaStatMEN();
    
    byte b1 = 0;
    for (byte b2 = 0; b2 < 3; b2++) {
      
      Henna henna = paramPlayer.getHenna(b2 + true);
      if (henna != null)
        this.ahM[b1++] = henna; 
    } 
    this._count = b1;
  }
  private int ahH; private int ahI; private int ahJ; private int ahK; private int ahL;
  private final Henna[] ahM;
  
  protected final void writeImpl() {
    writeC(240);
    
    writeH(this.ahJ);
    writeH(this.ahG);
    writeH(this.ahH);
    writeH(this.ahL);
    writeH(this.ahI);
    writeH(this.ahK);
    writeH(0);
    writeH(0);
    writeD(3);
    writeD(this._count);
    for (byte b = 0; b < this._count; b++) {
      
      writeD(this.ahM[b].getSymbolId());
      writeD(this.ahM[b].getSymbolId());
    } 
    writeD(0);
    writeD(0);
    writeD(0);
  }
}
