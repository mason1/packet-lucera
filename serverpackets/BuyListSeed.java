package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.data.xml.holder.BuyListHolder;
import l2.gameserver.model.items.TradeItem;














public final class BuyListSeed
  extends AbstractItemListPacket
{
  private int EJ;
  private List<TradeItem> abd;
  private long abe;
  
  public BuyListSeed(BuyListHolder.NpcTradeList paramNpcTradeList, int paramInt, long paramLong) {
    this.abd = new ArrayList();



    
    this.abe = paramLong;
    this.EJ = paramInt;
    this.abd = paramNpcTradeList.getItems();
  }


  
  protected final void writeImpl() {
    writeC(233);
    
    writeQ(this.abe);
    writeD(this.EJ);
    
    writeH(this.abd.size());
    
    for (TradeItem tradeItem : this.abd) {
      
      writeItemInfo(tradeItem);
      writeQ(tradeItem.getOwnersPrice());
    } 
  }
}
