package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.utils.Language;







public class ExResponseShowStepOne
  extends L2GameServerPacket
{
  private Language age;
  
  public ExResponseShowStepOne(Player paramPlayer) { this.age = paramPlayer.getLanguage(); }



  
  protected void writeImpl() {
    writeEx(174);
    writeD(0);
  }
}
