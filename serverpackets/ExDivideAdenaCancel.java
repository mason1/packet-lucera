package l2.gameserver.network.l2.s2c;




public class ExDivideAdenaCancel
  extends L2GameServerPacket
{
  public static final L2GameServerPacket STATIC = new ExDivideAdenaCancel();



  
  protected final void writeImpl() {
    writeEx(348);
    writeC(0);
  }
}
