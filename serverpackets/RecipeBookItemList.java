package l2.gameserver.network.l2.s2c;

import java.util.Collection;
import l2.gameserver.model.Player;
import l2.gameserver.model.Recipe;


public class RecipeBookItemList
  extends L2GameServerPacket
{
  private Collection<Recipe> akz;
  private final boolean akA;
  private final int akB;
  
  public RecipeBookItemList(Player paramPlayer, boolean paramBoolean) {
    this.akA = paramBoolean;
    this.akB = (int)paramPlayer.getCurrentMp();
    if (paramBoolean) {
      this.akz = paramPlayer.getDwarvenRecipeBook();
    } else {
      this.akz = paramPlayer.getCommonRecipeBook();
    } 
  }

  
  protected final void writeImpl() {
    writeC(220);
    writeD(this.akA ? 0 : 1);
    writeD(this.akB);
    
    writeD(this.akz.size());
    
    for (Recipe recipe : this.akz) {
      
      writeD(recipe.getId());
      writeD(1);
    } 
  }
}
