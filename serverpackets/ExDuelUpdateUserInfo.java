package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;








public class ExDuelUpdateUserInfo
  extends L2GameServerPacket
{
  private String _name;
  private int acr;
  private int class_id;
  private int level;
  private int curHp;
  private int maxHp;
  private int curMp;
  private int maxMp;
  private int curCp;
  private int maxCp;
  
  public ExDuelUpdateUserInfo(Player paramPlayer) {
    this._name = paramPlayer.getName();
    this.acr = paramPlayer.getObjectId();
    this.class_id = paramPlayer.getClassId().getId();
    this.level = paramPlayer.getLevel();
    this.curHp = (int)paramPlayer.getCurrentHp();
    this.maxHp = paramPlayer.getMaxHp();
    this.curMp = (int)paramPlayer.getCurrentMp();
    this.maxMp = paramPlayer.getMaxMp();
    this.curCp = (int)paramPlayer.getCurrentCp();
    this.maxCp = paramPlayer.getMaxCp();
  }


  
  protected final void writeImpl() {
    writeEx(81);
    writeS(this._name);
    writeD(this.acr);
    writeD(this.class_id);
    writeD(this.level);
    writeD(this.curHp);
    writeD(this.maxHp);
    writeD(this.curMp);
    writeD(this.maxMp);
    writeD(this.curCp);
    writeD(this.maxCp);
  }
}
