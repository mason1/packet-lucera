package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import l2.gameserver.data.StringHolder;
import l2.gameserver.data.xml.holder.EnchantSkillHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.model.instances.TrainerInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.tables.SkillTable;
import l2.gameserver.templates.SkillEnchant;
import org.apache.commons.lang3.StringUtils;

public class ExEnchantSkillList
  extends NpcHtmlMessage
{
  public static String EX_ENCHANT_SKILLLIST_BYPASS = "ExEnchantSkillList";
  
  private static final int aS = 9;
  private final List<SkillEnchantEntry> _skills;
  private int aeh;
  
  public ExEnchantSkillList(Player paramPlayer, NpcInstance paramNpcInstance, int paramInt) {
    super(paramPlayer, paramNpcInstance);
    this._skills = new ArrayList();
    this.aeh = paramInt;
  }

  
  class SkillEnchantEntry
  {
    private final SkillEnchant aei;

    
    public SkillEnchantEntry(SkillEnchant param1SkillEnchant) { this.aei = param1SkillEnchant; }


    
    public String toHtml(Player param1Player) {
      Skill skill = SkillTable.getInstance().getInfo(this.aei.getSkillId(), this.aei.getSkillLevel());
      null = StringHolder.getInstance().getNotNull(param1Player, "l2.gameserver.ExEnchantSkillList.SkillEntry");
      
      null = StringUtils.replace(null, "%skill_icon%", StringUtils.defaultString(skill.getIcon(), ""));
      null = StringUtils.replace(null, "%skill_name%", StringUtils.defaultString(skill.getName()));
      null = StringUtils.replace(null, "%skill_enchant_route_name%", StringUtils.defaultString(skill.getEnchantRouteName()));
      null = StringUtils.replace(null, "%skill_enchant_level%", String.valueOf(this.aei.getEnchantLevel()));
      return StringUtils.replace(null, "%skill_enchant_bypass%", ExEnchantSkillInfo.EX_ENCHANT_SKILLINFO_BYPASS + " " + this.aei.getSkillId() + " " + this.aei.getSkillLevel() + " " + ExEnchantSkillList.this.aeh);
    }
  }




  
  public static ExEnchantSkillList packetFor(Player paramPlayer, TrainerInstance paramTrainerInstance) { return packetFor(paramPlayer, paramTrainerInstance, 0); }


  
  public static ExEnchantSkillList packetFor(Player paramPlayer, TrainerInstance paramTrainerInstance, int paramInt) {
    Collection collection = paramPlayer.getAllSkills();
    ExEnchantSkillList exEnchantSkillList = new ExEnchantSkillList(paramPlayer, paramTrainerInstance, paramInt);
    for (Skill skill : collection) {
      
      int i = skill.getId();
      int j = skill.getLevel();
      int k = skill.getBaseLevel();
      
      if (j < k) {
        continue;
      }

      
      Map map = EnchantSkillHolder.getInstance().getRoutesOf(i);
      if (map != null && !map.isEmpty()) {
        
        SkillEnchant skillEnchant = EnchantSkillHolder.getInstance().getSkillEnchant(i, j);
        if (j == k) {
          
          for (Map map1 : map.values()) {
            
            for (SkillEnchant skillEnchant1 : map1.values()) {
              
              if (skillEnchant1.getEnchantLevel() == 1)
              {
                exEnchantSkillList.addSkill(skillEnchant1); } 
            } 
          } 
          continue;
        } 
        if (skillEnchant != null) {
          
          Map map1 = (Map)map.get(Integer.valueOf(skillEnchant.getRouteId()));
          int m = j + 1;
          SkillEnchant skillEnchant1 = (SkillEnchant)map1.get(Integer.valueOf(m));
          if (skillEnchant1 != null)
          {
            exEnchantSkillList.addSkill(skillEnchant1);
          }
        } 
      } 
    } 
    
    return exEnchantSkillList;
  }




  
  public void processHtml(GameClient paramGameClient) {
    try {
      Player player = paramGameClient.getActiveChar();
      if (player == null)
        return; 
      setFile("trainer/ExEnchantSkillList.htm");
      
      StringBuilder stringBuilder = new StringBuilder();
      int i = this._skills.size() / 9;
      this.aeh = Math.min(this.aeh, i);
      int j = this.aeh * 9;
      int k = Math.max(0, Math.min((this.aeh + 1) * 9 - 1, this._skills.size() - 1));
      for (int m = j; m <= k; m++) {
        
        SkillEnchantEntry skillEnchantEntry = (SkillEnchantEntry)this._skills.get(m);
        stringBuilder.append(skillEnchantEntry.toHtml(player));
      } 
      replace("%skill_enchant_list%", stringBuilder.toString());

      
      if (i > 1) {
        
        stringBuilder.setLength(0);
        String str = StringHolder.getInstance().getNotNull(player, "l2.gameserver.ExEnchantSkillList.paging");
        for (byte b = 0; b <= i; b++) {
          
          stringBuilder.append("<td>");
          if (b == this.aeh) {
            
            stringBuilder.append("&nbsp;").append(b + true).append("&nbsp;");
          }
          else {
            
            stringBuilder.append(" <a action=\"bypass -h ").append(EX_ENCHANT_SKILLLIST_BYPASS).append(" ").append(b).append("\">&nbsp;").append(b + 1).append("&nbsp;</a>");
          } 
          stringBuilder.append("</td>");
        } 
        replace("%paging%", str.replace("%pages%", stringBuilder.toString()));
        stringBuilder.setLength(0);
      } else {
        
        replace("%paging%", "");
      } 
    } finally {
      
      super.processHtml(paramGameClient);
    } 
  }



  
  public void addSkill(SkillEnchant paramSkillEnchant) { this._skills.add(new SkillEnchantEntry(paramSkillEnchant)); }
}
