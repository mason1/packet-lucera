package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;





public class ChangeMoveType
  extends L2GameServerPacket
{
  public static int WALK = 0;
  public static int RUN = 1;
  
  private int abo;
  
  private boolean CA;
  
  public ChangeMoveType(Creature paramCreature) {
    this.abo = paramCreature.getObjectId();
    this.CA = paramCreature.isRunning();
  }


  
  protected final void writeImpl() {
    writeC(40);
    writeD(this.abo);
    writeD(this.CA ? 1 : 0);
    writeD(0);
  }
}
