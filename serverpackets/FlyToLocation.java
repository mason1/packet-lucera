package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;
import l2.gameserver.utils.Location;

public class FlyToLocation
  extends L2GameServerPacket
{
  private int ahw;
  private final FlyType ahx;
  private Location _loc;
  private Location ahy;
  
  public enum FlyType {
    THROW_UP,
    THROW_HORIZONTAL,
    DUMMY,
    CHARGE,
    NONE;
  }

  
  public FlyToLocation(Creature paramCreature, Location paramLocation, FlyType paramFlyType) {
    this.ahy = paramLocation;
    this.ahx = paramFlyType;
    this.ahw = paramCreature.getObjectId();
    this._loc = paramCreature.getLoc();
  }


  
  protected void writeImpl() {
    writeC(212);
    writeD(this.ahw);
    writeD(this.ahy.x);
    writeD(this.ahy.y);
    writeD(this.ahy.z);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
    writeD(this.ahx.ordinal());
  }
}
