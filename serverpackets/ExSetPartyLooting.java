package l2.gameserver.network.l2.s2c;

public class ExSetPartyLooting
  extends L2GameServerPacket
{
  private int oH;
  private int _mode;
  
  public ExSetPartyLooting(int paramInt1, int paramInt2) {
    this.oH = paramInt1;
    this._mode = paramInt2;
  }


  
  protected void writeImpl() {
    writeEx(193);
    writeD(this.oH);
    writeD(this._mode);
  }
}
