package l2.gameserver.network.l2.s2c;

import l2.gameserver.utils.Location;






public class RadarControl
  extends L2GameServerPacket
{
  private int _x;
  private int _y;
  private int _z;
  private int _type;
  private int aky;
  
  public RadarControl(int paramInt1, int paramInt2, Location paramLocation) { this(paramInt1, paramInt2, paramLocation.x, paramLocation.y, paramLocation.z); }


  
  public RadarControl(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.aky = paramInt1;
    this._type = paramInt2;
    this._x = paramInt3;
    this._y = paramInt4;
    this._z = paramInt5;
  }


  
  protected final void writeImpl() {
    writeC(241);
    writeD(this.aky);
    writeD(this._type);
    writeD(this._x);
    writeD(this._y);
    writeD(this._z);
  }
}
