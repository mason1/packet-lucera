package l2.gameserver.network.l2.s2c;

import l2.gameserver.Config;
import l2.gameserver.model.items.ItemInstance;











public class SpawnItem
  extends L2GameServerPacket
{
  private int Bq;
  private int _itemId;
  private int _x;
  private int _y;
  private int _z;
  private int acB;
  private long Be;
  
  public SpawnItem(ItemInstance paramItemInstance) {
    this.Bq = paramItemInstance.getObjectId();
    this._itemId = paramItemInstance.getItemId();
    this._x = paramItemInstance.getX();
    this._y = paramItemInstance.getY();
    this._z = paramItemInstance.getZ();
    this.acB = paramItemInstance.isStackable() ? 1 : 0;
    this.Be = paramItemInstance.getCount();
  }


  
  protected final void writeImpl() {
    writeC(5);
    writeD(this.Bq);
    writeD(this._itemId);
    
    writeD(this._x);
    writeD(this._y);
    writeD(this._z + Config.CLIENT_Z_SHIFT);
    writeD(this.acB);
    writeQ(this.Be);
    writeD(0);
  }
}
