package l2.gameserver.network.l2.s2c;


public class ExChangeClientEffectInfo
  extends L2GameServerPacket
{
  private int _state;
  
  public ExChangeClientEffectInfo(int paramInt) { this._state = paramInt; }



  
  protected void writeImpl() {
    writeEx(193);
    writeD(0);
    writeD(this._state);
  }
}
