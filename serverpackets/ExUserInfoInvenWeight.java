package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;





public class ExUserInfoInvenWeight
  extends L2GameServerPacket
{
  private final int objectId;
  private final int aho;
  private final int ahp;
  
  public ExUserInfoInvenWeight(Player paramPlayer) {
    this.objectId = paramPlayer.getObjectId();
    this.aho = paramPlayer.getCurrentLoad();
    this.ahp = paramPlayer.getMaxLoad();
  }


  
  protected void writeImpl() {
    writeEx(358);
    writeD(this.objectId);
    writeD(this.aho);
    writeD(this.ahp);
  }
}
