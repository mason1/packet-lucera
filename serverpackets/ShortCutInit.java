package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import l2.gameserver.model.Player;
import l2.gameserver.model.actor.instances.player.ShortCut;

public class ShortCutInit
  extends ShortCutPacket
{
  private List<ShortCutPacket.ShortcutInfo> alg = Collections.emptyList();

  
  public ShortCutInit(Player paramPlayer) {
    Collection collection = paramPlayer.getAllShortCuts();
    this.alg = new ArrayList(collection.size());
    for (ShortCut shortCut : collection) {
      this.alg.add(convert(paramPlayer, shortCut));
    }
  }

  
  protected final void writeImpl() {
    writeC(69);
    writeD(this.alg.size());
    
    for (ShortCutPacket.ShortcutInfo shortcutInfo : this.alg)
      shortcutInfo.write(this); 
  }
}
