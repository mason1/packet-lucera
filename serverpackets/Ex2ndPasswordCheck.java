package l2.gameserver.network.l2.s2c;
public class Ex2ndPasswordCheck extends L2GameServerPacket {
  private final int acT;
  private final int acU;
  
  public enum Ex2ndPasswordCheckResult {
    CREATE(0, 0),
    CHECK(1, 0),
    BLOCK_TIME(1),
    SUCCESS(2, 0),
    ERROR(3, 0);
    
    private final int acT;
    private final int acU;
    
    Ex2ndPasswordCheckResult(int param1Int1, int param1Int2) {
      this.acT = param1Int1;
      this.acU = param1Int2;
    }

    
    Ex2ndPasswordCheckResult(int param1Int1) {
      this.acT = param1Int1;
      this.acU = -1;
    }


    
    public int getArg0() { return this.acT; }



    
    public int getArg1() { return this.acU; }
  }





  
  public Ex2ndPasswordCheck(Ex2ndPasswordCheckResult paramEx2ndPasswordCheckResult) {
    this.acT = paramEx2ndPasswordCheckResult.getArg0();
    this.acU = paramEx2ndPasswordCheckResult.getArg1();
  }

  
  public Ex2ndPasswordCheck(Ex2ndPasswordCheckResult paramEx2ndPasswordCheckResult, int paramInt) {
    this.acT = paramEx2ndPasswordCheckResult.getArg0();
    this.acU = paramInt;
  }


  
  protected void writeImpl() {
    writeEx(261);
    writeD(this.acT);
    writeD(this.acU);
  }
}
