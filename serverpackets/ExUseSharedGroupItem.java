package l2.gameserver.network.l2.s2c;

import l2.gameserver.skills.TimeStamp;

public class ExUseSharedGroupItem
  extends L2GameServerPacket {
  private int _itemId;
  private int ahi;
  
  public ExUseSharedGroupItem(int paramInt, TimeStamp paramTimeStamp) {
    this.ahi = paramInt;
    this._itemId = paramTimeStamp.getId();
    this.ahj = (int)(paramTimeStamp.getReuseCurrent() / 1000L);
    this.ahk = (int)(paramTimeStamp.getReuseBasic() / 1000L);
  }
  private int ahj;
  private int ahk;
  
  protected final void writeImpl() {
    writeEx(74);
    
    writeD(this._itemId);
    writeD(this.ahi);
    writeD(this.ahj);
    writeD(this.ahk);
  }
}
