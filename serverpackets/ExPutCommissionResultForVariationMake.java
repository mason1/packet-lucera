package l2.gameserver.network.l2.s2c;

public class ExPutCommissionResultForVariationMake
  extends L2GameServerPacket {
  public static final ExPutCommissionResultForVariationMake FAIL_PACKET = new ExPutCommissionResultForVariationMake();
  
  private int afF;
  private int pF;
  
  private ExPutCommissionResultForVariationMake() {
    this.afF = 0;
    this.pF = 1;
    this.Yu = 0L;
    this.afG = 0L;
    this.oH = 0;
  }
  private int oH; private long Yu; private long afG;
  
  public ExPutCommissionResultForVariationMake(int paramInt, long paramLong) {
    this.afF = paramInt;
    this.pF = 1;
    this.Yu = paramLong;
    this.afG = paramLong;
    this.oH = 1;
  }


  
  protected void writeImpl() {
    writeEx(86);
    writeD(this.afF);
    writeD(this.pF);
    writeQ(this.Yu);
    writeQ(this.afG);
    writeD(this.oH);
  }
}
