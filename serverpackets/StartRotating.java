package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;

public class StartRotating
  extends L2GameServerPacket {
  private int Br;
  private int Xz;
  
  public StartRotating(Creature paramCreature, int paramInt1, int paramInt2, int paramInt3) {
    this.Br = paramCreature.getObjectId();
    this.Xz = paramInt1;
    this.Pz = paramInt2;
    this.Fm = paramInt3;
  }
  private int Pz;
  private int Fm;
  
  protected final void writeImpl() {
    writeC(122);
    writeD(this.Br);
    writeD(this.Xz);
    writeD(this.Pz);
    writeD(this.Fm);
  }
}
