package l2.gameserver.network.l2.s2c;



















public class AskJoinParty
  extends L2GameServerPacket
{
  private String aaT;
  private int ES;
  
  public AskJoinParty(String paramString, int paramInt) {
    this.aaT = paramString;
    this.ES = paramInt;
  }


  
  protected final void writeImpl() {
    writeC(57);
    writeS(this.aaT);
    writeD(this.ES);
  }
}
