package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import l2.gameserver.model.Skill;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.model.pledge.SubUnit;




public class PledgeSkillList
  extends L2GameServerPacket
{
  private List<SkillInfo> akq = Collections.emptyList();
  private List<UnitSkillInfo> akr = new ArrayList();

  
  public PledgeSkillList(Clan paramClan) {
    Collection collection = paramClan.getSkills();
    this.akq = new ArrayList(collection.size());
    
    for (Skill skill : collection) {
      this.akq.add(new SkillInfo(skill.getId(), skill.getLevel()));
    }
    for (SubUnit subUnit : paramClan.getAllSubUnits()) {
      
      for (Skill skill : subUnit.getSkills()) {
        this.akr.add(new UnitSkillInfo(subUnit.getType(), skill.getId(), skill.getLevel()));
      }
    } 
  }

  
  protected final void writeImpl() {
    writeEx(58);
    writeD(this.akq.size());
    writeD(this.akr.size());
    
    for (SkillInfo skillInfo : this.akq) {
      
      writeD(skillInfo._id);
      writeD(skillInfo._level);
    } 
    
    for (UnitSkillInfo unitSkillInfo : this.akr) {
      
      writeD(unitSkillInfo._type);
      writeD(unitSkillInfo._id);
      writeD(unitSkillInfo._level);
    } 
  }
  
  static class SkillInfo
  {
    public int _id;
    public int _level;
    
    public SkillInfo(int param1Int1, int param1Int2) {
      this._id = param1Int1;
      this._level = param1Int2;
    }
  }
  
  static class UnitSkillInfo
    extends SkillInfo
  {
    private int _type;
    
    public UnitSkillInfo(int param1Int1, int param1Int2, int param1Int3) {
      super(param1Int2, param1Int3);
      this._type = param1Int1;
    }
  }
}
