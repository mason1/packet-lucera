package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.instances.DoorInstance;
import l2.gameserver.model.instances.StaticObjectInstance;

public class StaticObject
  extends L2GameServerPacket
{
  private final int abn;
  private final int Bq;
  private final int _type;
  private final int alI;
  private final int SS;
  private final int alJ;
  private final int alK;
  private final int _maxHp;
  private final int alL;
  private final int alM;
  private final int alN;
  
  public StaticObject(StaticObjectInstance paramStaticObjectInstance) {
    this.abn = paramStaticObjectInstance.getUId();
    this.Bq = paramStaticObjectInstance.getObjectId();
    this._type = 0;
    this.alI = 1;
    this.SS = paramStaticObjectInstance.getMeshIndex();
    this.alJ = 0;
    this.alK = 0;
    this._maxHp = 0;
    this.alL = 0;
    this.alM = 0;
    this.alN = 0;
  }

  
  public StaticObject(DoorInstance paramDoorInstance, Player paramPlayer) {
    this.abn = paramDoorInstance.getDoorId();
    this.Bq = paramDoorInstance.getObjectId();
    this._type = 1;
    this.alI = paramDoorInstance.getTemplate().isTargetable() ? 1 : 0;
    this.SS = 1;
    this.alJ = paramDoorInstance.isOpen() ? 0 : 1;
    this.alK = paramDoorInstance.isAutoAttackable(paramPlayer) ? 1 : 0;
    this.alL = (int)paramDoorInstance.getCurrentHp();
    this._maxHp = paramDoorInstance.getMaxHp();
    this.alM = paramDoorInstance.isHPVisible() ? 1 : 0;
    this.alN = paramDoorInstance.getDamage();
  }


  
  protected final void writeImpl() {
    writeC(159);
    writeD(this.abn);
    writeD(this.Bq);
    writeD(this._type);
    writeD(this.alI);
    writeD(this.SS);
    writeD(this.alJ);
    writeD(this.alK);
    writeD(this.alL);
    writeD(this._maxHp);
    writeD(this.alM);
    writeD(this.alN);
  }
}
