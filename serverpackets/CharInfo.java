package l2.gameserver.network.l2.s2c;

import l2.gameserver.Config;
import l2.gameserver.instancemanager.CursedWeaponsManager;
import l2.gameserver.instancemanager.ReflectionManager;
import l2.gameserver.model.Creature;
import l2.gameserver.model.Player;
import l2.gameserver.model.base.TeamType;
import l2.gameserver.model.entity.events.GlobalEvent;
import l2.gameserver.model.matching.MatchingRoom;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.skills.AbnormalEffect;
import l2.gameserver.skills.effects.EffectCubic;
import l2.gameserver.utils.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CharInfo
  extends L2GameServerPacket {
  public static final int[] PAPERDOLL_ORDER = { 
      0, 1, 5, 7, 10, 6, 11, 12, 28, 5, 2, 3 };













  
  public static int[] PAPERDOLL_ORDER_AUGMENT = { 5, 7, 5 };




  
  public static final int[] PAPERDOLL_ORDER_VISUAL_ID = { 5, 7, 5, 10, 6, 11, 12, 2, 3 };










  
  public static final int[] PAPERDOLL_SET_ORDER = new int[0];


  
  private static final Logger _log = LoggerFactory.getLogger(CharInfo.class); private int[][] abp; private int _mAtkSpd; private int _pAtkSpd; private int abq; private int abr; private int abs; private int abt; private int abu; private int abv; private int abw; private Location _loc; private Location DQ; private String _name; private String _title; private int abx; private int Bt; private int BB; private int aby; private int pvp_flag;
  private int karma;
  private int abz;
  private int abA;
  private double moveAnimMod;
  private double abB;
  private double abC;
  private double abD;
  private int abE;
  private int abF;
  private int abG;
  private int clan_id;
  private int clan_crest_id;
  private int abH;
  private int ally_id;
  private int ally_crest_id;
  private int class_id;
  private int abI;
  private int abJ;
  private int abK;
  private int abL;
  private int abM;
  private int abN;
  
  public CharInfo(Player paramPlayer) { this(paramPlayer); }
  private int abO; private int abP; private int abQ; private int abR; private int abS; private int abT; private int abU;
  private int abV;
  
  public CharInfo(Creature paramCreature) {
    if (paramCreature == null) {
      
      System.out.println("CharInfo: cha is null!");
      Thread.dumpStack();
      
      return;
    } 
    if (paramCreature.isInvisible()) {
      return;
    }
    if (paramCreature.isDeleted()) {
      return;
    }
    Player player = paramCreature.getPlayer();
    if (player == null) {
      return;
    }
    if (player.isInBoat())
    {
      this._loc = player.getInBoatPosition();
    }
    
    if (this._loc == null) {
      this._loc = paramCreature.getLoc();
    }
    this.abx = paramCreature.getObjectId();

    
    if (player.getTransformationName() != null || (player.getReflection() == ReflectionManager.GIRAN_HARBOR && player.getPrivateStoreType() != 0)) {
      
      this._name = (player.getTransformationName() != null) ? player.getTransformationName() : player.getName();
      this._title = "";
      this.clan_id = 0;
      this.clan_crest_id = 0;
      this.ally_id = 0;
      this.ally_crest_id = 0;
      this.abH = 0;
      if (player.isCursedWeaponEquipped()) {
        this.abW = CursedWeaponsManager.getInstance().getLevel(player.getCursedWeaponEquippedId());
      }
    } else {
      
      this._name = player.getName();
      if (player.getPrivateStoreType() != 0) {
        this._title = "";
      } else if (!player.isConnected()) {
        
        this._title = player.getDisconnectedTitle();
        this.abY = player.getDisconnectedTitleColor();
      }
      else {
        
        this._title = player.getTitle();
        this.abY = player.getTitleColor();
      } 
      
      Clan clan = player.getClan();
      Alliance alliance = (clan == null) ? null : clan.getAlliance();
      
      this.clan_id = (clan == null) ? 0 : clan.getClanId();
      this.clan_crest_id = (clan == null) ? 0 : clan.getCrestId();
      this.abH = (clan == null) ? 0 : clan.getCrestLargeId();
      
      this.ally_id = (alliance == null) ? 0 : alliance.getAllyId();
      this.ally_crest_id = (alliance == null) ? 0 : alliance.getAllyCrestId();
      
      this.abW = 0;
    } 
    
    if (player.isMounted()) {
      
      this.abN = 0;
      this.abO = 0;
      this.abX = player.getMountNpcId() + 1000000;
      this.abS = player.getMountType();
    }
    else {
      
      this.abN = player.getWeaponEnchantEffect();
      this.abO = player.getArmorSetEnchantLevel();
      this.abX = 0;
      this.abS = 0;
    } 
    
    this.abp = new int[59][4];
    
    for (int i : PAPERDOLL_ORDER) {
      
      this.abp[i][0] = player.getInventory().getPaperdollItemId(i);
      int j = player.getInventory().getPaperdollAugmentationId(i);
      this.abp[i][1] = j & 0xFFFF;
      this.abp[i][2] = j >> 16;
    } 
    
    this.maxHp = player.getMaxHp();
    this.maxMp = player.getMaxMp();
    this.acf = (int)player.getCurrentCp();
    this.currentHp = (int)player.getCurrentHp();
    this.currentMp = (int)player.getCurrentMp();
    
    this.abnormalEffects = player.getAbnormalEffects();
    
    this._mAtkSpd = player.getMAtkSpd();
    this._pAtkSpd = player.getPAtkSpd();
    this.moveAnimMod = player.getMovementSpeedMultiplier();
    this.abq = (int)(player.getRunSpeed() / this.moveAnimMod);
    this.abr = (int)(player.getWalkSpeed() / this.moveAnimMod);
    
    this.abt = 0;
    this.abu = 0;
    
    if (player.isFlying()) {
      
      this.abv = this.abq;
      this.abw = this.abr;
    }
    else {
      
      this.abv = 0;
      this.abw = 0;
    } 
    
    this.abs = player.getSwimSpeed();
    this.Bt = (player.getBaseTemplate()).race.ordinal();
    this.BB = player.getSex();
    this.aby = player.getBaseClassId();
    this.pvp_flag = player.getPvpFlag();
    this.karma = -player.getKarma();
    
    this.abB = player.getAttackSpeedMultiplier();
    this.abC = player.getColRadius();
    this.abD = player.getColHeight();
    this.abE = player.getHairStyle();
    this.abF = player.getHairColor();
    this.abG = player.getFace();
    if (this.clan_id > 0 && player.getClan() != null) {
      this.abV = player.getClan().getReputationScore();
    } else {
      this.abV = 0;
    }  this.abI = player.isSitting() ? 0 : 1;
    this.abJ = player.isRunning() ? 1 : 0;
    this.abK = player.isInCombat() ? 1 : 0;
    this.abL = player.isAlikeDead() ? 1 : 0;
    this.abM = player.isInObserverMode() ? 7 : player.getPrivateStoreType();
    this.acc = (EffectCubic[])player.getCubics().toArray(new EffectCubic[player.getCubics().size()]);
    this.abz = player.isGM() ? 0 : player.getGivableRec();
    this.class_id = player.getClassId().getId();
    this._team = player.getTeam();
    
    this.abP = player.isNoble() ? 1 : 0;
    this.abQ = (player.isHero() || (player.isGM() && Config.GM_HERO_AURA)) ? 2 : 0;
    this.abR = player.isFishing() ? 1 : 0;
    this.DQ = player.getFishLoc();
    this.FL = player.getNameColor();
    this.abT = player.getPledgeClass();
    this.abU = player.getPledgeType();
    this.abZ = player.getTransformation();
    this.aca = player.getAgathionId();
    this.acd = (player.getMatchingRoom() != null && player.getMatchingRoom().getType() == MatchingRoom.PARTY_MATCHING && player.getMatchingRoom().getLeader() == player);
    this.ace = player.isInFlyingTransform();
    this.abA = player.isClanLeader() ? 64 : 0;
    for (GlobalEvent globalEvent : player.getEvents())
    {
      this.abA = globalEvent.getUserRelation(player, this.abA); } 
  }
  private int abW; private int abX; private int FL; private int abY; private int abZ; private int aca; private int acb; private EffectCubic[] acc; private boolean acd; private boolean ace; private int acf; private int maxHp; private int maxMp; private int currentHp; private int currentMp;
  private TeamType _team;
  private AbnormalEffect[] abnormalEffects;
  
  protected final void writeImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null) {
      return;
    }
    if (this.abx == 0)
      return; 
    if (player.getObjectId() == this.abx) {
      
      _log.error("You cant send CharInfo about his character to active user!!!");
      
      return;
    } 
    writeC(49);
    writeC(0);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z + Config.CLIENT_Z_SHIFT);
    writeD(this.acb);
    writeD(this.abx);
    writeS(this._name);
    writeH(this.Bt);
    writeC(this.BB);
    writeD(this.aby);
    
    for (int i : PAPERDOLL_ORDER)
    {
      writeD(this.abp[i][0]);
    }
    
    for (int i : PAPERDOLL_ORDER_AUGMENT) {
      
      writeD(this.abp[i][1]);
      writeD(this.abp[i][2]);
    } 
    
    writeC(this.abO);
    
    for (int i : PAPERDOLL_ORDER_VISUAL_ID)
    {
      writeD(this.abp[i][3]);
    }
    
    writeC(this.pvp_flag);
    writeD(this.karma);
    
    writeD(this._mAtkSpd);
    writeD(this._pAtkSpd);
    
    writeH(this.abq);
    writeH(this.abr);
    writeH(this.abs);
    writeH(this.abs);
    writeH(this.abt);
    writeH(this.abu);
    writeH(this.abv);
    writeH(this.abw);
    
    writeF(this.moveAnimMod);
    writeF(this.abB);
    writeF(this.abC);
    writeF(this.abD);
    writeD(this.abE);
    writeD(this.abF);
    writeD(this.abG);
    writeS(this._title);
    writeD(this.clan_id);
    writeD(this.clan_crest_id);
    writeD(this.ally_id);
    writeD(this.ally_crest_id);
    
    writeC(this.abI);
    writeC(this.abJ);
    writeC(this.abK);
    writeC(this.abL);
    writeC(0);
    writeC(this.abS);
    writeC(this.abM);
    writeH(this.acc.length);
    for (EffectCubic effectCubic : this.acc)
      writeH((effectCubic == null) ? 0 : effectCubic.getId()); 
    writeC(this.acd ? 1 : 0);
    writeC(this.ace ? 2 : 0);
    writeH(this.abz);
    writeD(this.abX);
    writeD(this.class_id);
    writeD(0);
    writeC(this.abN);
    
    writeC(this._team.ordinal());
    
    writeD(this.abH);
    writeC(this.abP);
    writeC(this.abQ);
    
    writeC(this.abR);
    writeD(this.DQ.x);
    writeD(this.DQ.y);
    writeD(this.DQ.z);
    
    writeD(this.FL);
    writeD(this._loc.h);
    writeC(this.abT);
    writeH(this.abU);
    writeD(this.abY);
    
    writeC(this.abW);
    writeD(this.abV);
    writeD(this.abZ);
    writeD(this.aca);
    writeC(this.abA);
    
    writeD(this.acf);
    writeD(this.maxHp);
    writeD(this.currentHp);
    writeD(this.maxMp);
    writeD(this.currentMp);
    
    writeC(0);
    
    writeD(this.abnormalEffects.length);
    for (AbnormalEffect abnormalEffect : this.abnormalEffects)
    {
      writeH(abnormalEffect.getClientId());
    }
    
    writeC(0);
    writeC(1);
    writeC(0);
  }
}
