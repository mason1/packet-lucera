package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;
import l2.gameserver.model.Player;


public class MagicSkillCanceled
  extends L2GameServerPacket
{
  private int aiR;
  private final int aiS;
  private final int aiT;
  
  public MagicSkillCanceled(Creature paramCreature) {
    this.aiR = paramCreature.getObjectId();
    this.aiS = paramCreature.getX();
    this.aiT = paramCreature.getY();
  }


  
  protected final void writeImpl() {
    writeC(73);
    writeD(this.aiR);
  }


  
  public L2GameServerPacket packet(Player paramPlayer) {
    if (paramPlayer != null && !paramPlayer.isInObserverMode()) {
      
      if (paramPlayer.buffAnimRange() < 0)
        return null; 
      if (paramPlayer.buffAnimRange() == 0) {
        return (this.aiR == paramPlayer.getObjectId()) ? super.packet(paramPlayer) : null;
      }
      return (paramPlayer.getDistance(this.aiS, this.aiT) < paramPlayer.buffAnimRange()) ? super.packet(paramPlayer) : null;
    } 
    
    return super.packet(paramPlayer);
  }
}
