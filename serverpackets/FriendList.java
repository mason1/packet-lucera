package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import l2.gameserver.model.Player;
import l2.gameserver.model.actor.instances.player.Friend;





public class FriendList
  extends L2GameServerPacket
{
  private List<FriendInfo> ahE = Collections.emptyList();

  
  public FriendList(Player paramPlayer) {
    Map map = paramPlayer.getFriendList().getList();
    this.ahE = new ArrayList(map.size());
    for (Map.Entry entry : map.entrySet()) {
      
      Friend friend = (Friend)entry.getValue();
      FriendInfo friendInfo = new FriendInfo(null);
      friendInfo.name = friend.getName();
      friendInfo.classId = friend.getClassId();
      friendInfo.objectId = ((Integer)entry.getKey()).intValue();
      friendInfo.level = friend.getLevel();
      friendInfo.aeu = friend.isOnline();
      this.ahE.add(friendInfo);
    } 
  }


  
  protected void writeImpl() {
    writeC(88);
    writeD(this.ahE.size());
    for (FriendInfo friendInfo : this.ahE) {
      
      writeD(friendInfo.objectId);
      writeS(friendInfo.name);
      writeD(friendInfo.aeu);
      writeD(friendInfo.aeu ? friendInfo.objectId : 0);
      writeD(friendInfo.classId);
      writeD(friendInfo.level);
    } 
  }
  
  private class FriendInfo {
    private String name;
    private int objectId;
    private boolean aeu;
    private int level;
    private int classId;
    
    private FriendInfo() {}
  }
}
