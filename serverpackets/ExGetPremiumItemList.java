package l2.gameserver.network.l2.s2c;

import java.util.Map;
import l2.gameserver.model.Player;
import l2.gameserver.model.PremiumItem;






public class ExGetPremiumItemList
  extends L2GameServerPacket
{
  private int Bq;
  private Map<Integer, PremiumItem> aeL;
  
  public ExGetPremiumItemList(Player paramPlayer) {
    this.Bq = paramPlayer.getObjectId();
    this.aeL = paramPlayer.getPremiumItemList();
  }


  
  protected void writeImpl() {
    writeEx(134);
    if (!this.aeL.isEmpty()) {
      
      writeD(this.aeL.size());
      for (Map.Entry entry : this.aeL.entrySet()) {
        
        writeD(((Integer)entry.getKey()).intValue());
        writeD(this.Bq);
        writeD(((PremiumItem)entry.getValue()).getItemId());
        writeQ(((PremiumItem)entry.getValue()).getCount());
        writeD(0);
        writeS(((PremiumItem)entry.getValue()).getSender());
      } 
    } 
  }
}
