package l2.gameserver.network.l2.s2c;

public class AskJoinPledge
  extends L2GameServerPacket
{
  private int aaV;
  private String aaa;
  
  public AskJoinPledge(int paramInt, String paramString) {
    this.aaV = paramInt;
    this.aaa = paramString;
  }


  
  protected final void writeImpl() {
    writeC(44);
    writeD(this.aaV);
    writeS(this.aaa);
  }
}
