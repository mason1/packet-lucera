package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;

public class ExFishingStartCombat
  extends L2GameServerPacket
{
  int _time;
  int _hp;
  int _lureType;
  int _deceptiveMode;
  int _mode;
  private int char_obj_id;
  
  public ExFishingStartCombat(Creature paramCreature, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.char_obj_id = paramCreature.getObjectId();
    this._time = paramInt1;
    this._hp = paramInt2;
    this._mode = paramInt3;
    this._lureType = paramInt4;
    this._deceptiveMode = paramInt5;
  }


  
  protected final void writeImpl() {
    writeEx(39);
    
    writeD(this.char_obj_id);
    writeD(this._time);
    writeD(this._hp);
    writeC(this._mode);
    writeC(this._lureType);
    writeC(this._deceptiveMode);
  }
}
