package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import l2.gameserver.data.htm.HtmCache;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.SevenSignsFestival.SevenSignsFestival;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.NpcString;
import l2.gameserver.scripts.Functions;
import l2.gameserver.scripts.Scripts;
import l2.gameserver.utils.HtmlUtils;
import l2.gameserver.utils.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;













































































































public class NpcHtmlMessage
  extends L2GameServerPacket
{
  protected static final Logger _log = LoggerFactory.getLogger(NpcHtmlMessage.class);
  protected static final Pattern objectId = Pattern.compile("%objectId%");
  protected static final Pattern playername = Pattern.compile("%playername%");
  
  protected int _npcObjId;
  protected String _html;
  protected String _file = null;
  protected List<String> _replaces = new ArrayList();
  
  protected boolean have_appends = false;
  
  public NpcHtmlMessage(Player paramPlayer, int paramInt1, String paramString, int paramInt2) {
    List list = (List)Scripts.dialogAppends.get(Integer.valueOf(paramInt1));
    if (list != null && list.size() > 0) {
      
      this.have_appends = true;
      if (paramString != null && paramString.equalsIgnoreCase("npcdefault.htm")) {
        setHtml("");
      } else {
        setFile(paramString);
      } 
      String str = "";

      
      Object[] arrayOfObject = { new Integer(paramInt2) };
      for (Scripts.ScriptClassAndMethod scriptClassAndMethod : list) {
        
        Object object = Scripts.getInstance().callScripts(paramPlayer, scriptClassAndMethod.className, scriptClassAndMethod.methodName, arrayOfObject);
        if (object != null) {
          str = str + object;
        }
      } 
      if (!str.equals("")) {
        replace("</body>", "\n" + Strings.bbParse(str) + "</body>");
      }
    } else {
      setFile(paramString);
    } 
  }
  
  public NpcHtmlMessage(Player paramPlayer, NpcInstance paramNpcInstance, String paramString, int paramInt) {
    this(paramPlayer, paramNpcInstance.getNpcId(), paramString, paramInt);
    
    this._npcObjId = paramNpcInstance.getObjectId();

    
    paramPlayer.setLastNpc(paramNpcInstance);
    
    replace("%npcId%", String.valueOf(paramNpcInstance.getNpcId()));
    replace("%npcname%", paramNpcInstance.getName());
    replace("%festivalMins%", SevenSignsFestival.getInstance().getTimeToNextFestivalStr());
  }

  
  public NpcHtmlMessage(Player paramPlayer, NpcInstance paramNpcInstance) {
    if (paramNpcInstance == null) {
      
      this._npcObjId = 5;
      paramPlayer.setLastNpc(null);
    }
    else {
      
      this._npcObjId = paramNpcInstance.getObjectId();
      paramPlayer.setLastNpc(paramNpcInstance);
    } 
  }


  
  public NpcHtmlMessage(int paramInt) { this._npcObjId = paramInt; }



  
  public final NpcHtmlMessage setHtml(String paramString) {
    if (!paramString.contains("<html>"))
      paramString = "<html><body>" + paramString + "</body></html>"; 
    this._html = paramString;
    return this;
  }

  
  public final NpcHtmlMessage setFile(String paramString) {
    this._file = paramString;
    if (this._file.startsWith("data/html/")) {
      
      _log.info("NpcHtmlMessage: need fix : " + paramString, new Exception());
      this._file = this._file.replace("data/html/", "");
    } 
    return this;
  }

  
  public NpcHtmlMessage replace(String paramString1, String paramString2) {
    if (paramString1 == null || paramString2 == null)
      return this; 
    this._replaces.add(paramString1);
    this._replaces.add(paramString2);
    return this;
  }


  
  public NpcHtmlMessage replaceNpcString(String paramString, NpcString paramNpcString, Object... paramVarArgs) {
    if (paramString == null)
      return this; 
    if (paramNpcString.getSize() != paramVarArgs.length) {
      throw new IllegalArgumentException("Not valid size of parameters: " + paramNpcString);
    }
    this._replaces.add(paramString);
    this._replaces.add(HtmlUtils.htmlNpcString(paramNpcString, paramVarArgs));
    return this;
  }

  
  public void processHtml(GameClient paramGameClient) {
    Player player = paramGameClient.getActiveChar();
    if (this._file != null) {
      
      if (player != null)
      {
        if (player.isGM())
        {
          Functions.sendDebugMessage(player, "HTML: " + this._file);
        }
      }
      String str1 = HtmCache.getInstance().getNotNull(this._file, player);
      String str2 = HtmCache.getInstance().getNullable(this._file, player);
      if (str2 == null) {
        setHtml((this.have_appends && this._file.endsWith(".htm")) ? "" : str1);
      } else {
        setHtml(str1);
      } 
    } 
    for (byte b = 0; b < this._replaces.size(); b += 2)
    {
      this._html = this._html.replace((CharSequence)this._replaces.get(b), (CharSequence)this._replaces.get(b + 1));
    }
    
    if (this._html == null) {
      return;
    }
    Matcher matcher = objectId.matcher(this._html);
    if (matcher != null)
    {
      this._html = matcher.replaceAll(String.valueOf(this._npcObjId));
    }
    
    if (player != null)
    {
      this._html = playername.matcher(this._html).replaceAll(player.getName());
    }
    
    paramGameClient.cleanBypasses(false);
    this._html = paramGameClient.encodeBypasses(this._html, false);
  }


  
  protected void writeImpl() {
    if (this._html != null) {
      
      writeC(25);
      writeD(this._npcObjId);
      writeS(this._html);
      writeD(0);
    } 
  }
}
