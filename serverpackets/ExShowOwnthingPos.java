package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;



public class ExShowOwnthingPos
  extends L2GameServerPacket
{
  private List<WardInfo> agJ = new ArrayList(9);



  
  protected void writeImpl() {
    writeEx(147);
    writeD(this.agJ.size());
    for (WardInfo wardInfo : this.agJ) {
      
      writeD(wardInfo.agK);
      writeD(wardInfo._x);
      writeD(wardInfo._y);
      writeD(wardInfo._z);
    } 
  }
  
  private static class WardInfo
  {
    private int agK;
    private int _x;
    
    public WardInfo(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      this.agK = param1Int1;
      this._x = param1Int2;
      this._y = param1Int3;
      this._z = param1Int4;
    }
    
    private int _y;
    private int _z;
  }
}
