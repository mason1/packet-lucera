package l2.gameserver.network.l2.s2c;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import l2.gameserver.model.Manor;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.templates.manor.CropProcure;

public class ExShowSellCropList
  extends L2GameServerPacket {
  private int EJ;
  private Map<Integer, ItemInstance> agX;
  private Map<Integer, CropProcure> agL;
  
  public ExShowSellCropList(Player paramPlayer, int paramInt, List<CropProcure> paramList) {
    this.EJ = 1;




    
    this.EJ = paramInt;
    this.agL = new TreeMap();
    this.agX = new TreeMap();
    
    List list = Manor.getInstance().getAllCrops();
    for (null = list.iterator(); null.hasNext(); ) { int i = ((Integer)null.next()).intValue();
      
      ItemInstance itemInstance = paramPlayer.getInventory().getItemByItemId(i);
      if (itemInstance != null) {
        this.agX.put(Integer.valueOf(i), itemInstance);
      } }
    
    for (CropProcure cropProcure : paramList) {
      if (this.agX.containsKey(Integer.valueOf(cropProcure.getId())) && cropProcure.getAmount() > 0L) {
        this.agL.put(Integer.valueOf(cropProcure.getId()), cropProcure);
      }
    } 
  }

  
  public void writeImpl() {
    writeEx(44);
    
    writeD(this.EJ);
    writeD(this.agX.size());
    
    for (ItemInstance itemInstance : this.agX.values()) {
      
      writeD(itemInstance.getObjectId());
      writeD(itemInstance.getItemId());
      writeD(Manor.getInstance().getSeedLevelByCrop(itemInstance.getItemId()));
      
      writeC(1);
      writeD(Manor.getInstance().getRewardItem(itemInstance.getItemId(), 1));
      
      writeC(1);
      writeD(Manor.getInstance().getRewardItem(itemInstance.getItemId(), 2));
      
      if (this.agL.containsKey(Integer.valueOf(itemInstance.getItemId()))) {
        
        CropProcure cropProcure = (CropProcure)this.agL.get(Integer.valueOf(itemInstance.getItemId()));
        writeD(this.EJ);
        writeQ(cropProcure.getAmount());
        writeQ(cropProcure.getPrice());
        writeC(cropProcure.getReward());
      }
      else {
        
        writeD(-1);
        writeQ(0L);
        writeQ(0L);
        writeC(0);
      } 
      writeQ(itemInstance.getCount());
    } 
  }
}
