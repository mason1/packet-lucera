package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;










public class MagicAndSkillList
  extends L2GameServerPacket
{
  private int abo;
  private int _unk1;
  private int aiQ;
  
  public MagicAndSkillList(Creature paramCreature, int paramInt1, int paramInt2) {
    this.abo = paramCreature.getObjectId();
    this._unk1 = paramInt1;
    this.aiQ = paramInt2;
  }


  
  protected final void writeImpl() {
    writeC(64);
    writeD(this.abo);
    writeD(this._unk1);
    writeD(this.aiQ);
  }
}
