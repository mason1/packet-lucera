package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;

public class RecipeShopItemInfo extends L2GameServerPacket {
  private int UE;
  private int akF;
  private int akG;
  
  public RecipeShopItemInfo(Player paramPlayer1, Player paramPlayer2, int paramInt1, long paramLong, int paramInt2) {
    this.akH = -1;



    
    this.UE = paramInt1;
    this.akF = paramPlayer2.getObjectId();
    this._price = paramLong;
    this.akH = paramInt2;
    this.akG = (int)paramPlayer2.getCurrentMp();
    this._maxMp = paramPlayer2.getMaxMp();
  }
  
  private int _maxMp;
  
  protected final void writeImpl() {
    writeC(224);
    writeD(this.akF);
    writeD(this.UE);
    writeD(this.akG);
    writeD(this._maxMp);
    writeD(this.akH);
    writeQ(this._price);
  }
  
  private int akH;
  private long _price;
}
