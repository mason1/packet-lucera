package l2.gameserver.network.l2.s2c;


public class ShowXMasSeal
  extends L2GameServerPacket
{
  private int alv;
  
  public ShowXMasSeal(int paramInt) { this.alv = paramInt; }



  
  protected void writeImpl() {
    writeC(248);
    writeD(this.alv);
  }
}
