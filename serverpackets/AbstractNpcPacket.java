package l2.gameserver.network.l2.s2c;
import l2.gameserver.model.Creature;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.network.l2.components.NpcString;
import l2.gameserver.network.l2.s2c.mask.IUpdateTypeComponent;
import l2.gameserver.skills.AbnormalEffect;

public abstract class AbstractNpcPacket extends AbstractMaskPacket<NpcInfoType> {
  protected boolean can_writeImpl = false;
  protected int _npcObjId;
  protected int _npcId;
  protected int running;
  protected int incombat;
  protected int dead;
  protected int _showSpawnAnimation;
  protected int _mAtkSpd;
  protected int _pAtkSpd;
  protected int _rhand;
  protected int _lhand;
  protected int _enchantEffect;
  protected String _name = ""; protected int karma; protected int pvp_flag; protected int clan_id; protected int clan_crest_id; protected int clan_large_crest_id; protected int ally_id; protected int ally_crest_id; protected int _formId; protected boolean _isAttackable; protected boolean _isNameAbove; protected boolean isFlying; protected Location _loc;
  protected String _title = ""; protected boolean _showName;
  protected boolean _targetable;
  protected int _state;
  protected NpcString _nameNpcString = NpcString.NONE;
  protected NpcString _titleNpcString = NpcString.NONE; protected TeamType _team; protected boolean _isPet;
  protected int currentHp;
  protected int currentMp;
  protected int maxHp;
  protected int maxMp;
  protected double moveAnimMod;
  protected double atkSpeed;
  protected AbnormalEffect[] abnormalEffects = AbnormalEffect.EMPTY_ARRAY;
  
  private final byte[] aaE = { 0, 12, 12, 0, 0 };






  
  private int aaF = 0;
  private int aaG = 0;

  
  protected void setValues(Creature paramCreature, NpcInfoType... paramVarArgs) {
    this.currentHp = (int)paramCreature.getCurrentHp();
    this.currentMp = (int)paramCreature.getCurrentMp();
    this.maxHp = paramCreature.getMaxHp();
    this.maxMp = paramCreature.getMaxMp();
    
    this._npcObjId = paramCreature.getObjectId();
    this._loc = paramCreature.getLoc();
    this._mAtkSpd = paramCreature.getMAtkSpd();
    
    Clan clan = paramCreature.getClan();
    Alliance alliance = (clan == null) ? null : clan.getAlliance();
    
    this.clan_id = (clan == null) ? 0 : clan.getClanId();
    this.clan_crest_id = (clan == null) ? 0 : clan.getCrestId();
    
    this.ally_id = (alliance == null) ? 0 : alliance.getAllyId();
    this.ally_crest_id = (alliance == null) ? 0 : alliance.getAllyCrestId();
    
    this.karma = paramCreature.getKarma();
    this.pvp_flag = paramCreature.getPvpFlag();
    this._pAtkSpd = paramCreature.getPAtkSpd();
    this.running = paramCreature.isRunning() ? 1 : 0;
    this.incombat = paramCreature.isInCombat() ? 1 : 0;
    this.dead = paramCreature.isAlikeDead() ? 1 : 0;
    this.isFlying = paramCreature.isFlying();
    this._team = paramCreature.getTeam();
    this._formId = paramCreature.getFormId();
    this._isNameAbove = paramCreature.isNameAbove();
    this._targetable = paramCreature.isTargetable();
    
    this.moveAnimMod = paramCreature.getRunSpeed() * 1.0D / (paramCreature.getTemplate()).baseRunSpd;
    this.atkSpeed = paramCreature.getAttackSpeedMultiplier();
    
    this.abnormalEffects = paramCreature.getAbnormalEffects();
    
    addComponentType(paramVarArgs);
    
    this.can_writeImpl = true;
  }

  
  public AbstractNpcPacket update(boolean paramBoolean) {
    this._showSpawnAnimation = paramBoolean ? 1 : 0;
    return this;
  }



  
  protected byte[] getMasks() { return this.aaE; }




  
  protected void onNewMaskAdded(NpcInfoType paramNpcInfoType) { a(paramNpcInfoType); }


  
  private void a(NpcInfoType paramNpcInfoType) {
    switch (paramNpcInfoType) {

      
      case ATTACKABLE:
      case UNKNOWN1:
        this.aaF += paramNpcInfoType.getBlockLength();
        return;

      
      case TITLE:
        this.aaF += paramNpcInfoType.getBlockLength() + this._title.length() * 2;
        return;

      
      case NAME:
        this.aaG += paramNpcInfoType.getBlockLength() + this._name.length() * 2;
        return;
    } 

    
    this.aaG += paramNpcInfoType.getBlockLength();
  }




  
  protected void writeData() {
    writeD(this._npcObjId);
    writeC(this._showSpawnAnimation);
    writeH(37);
    writeB(this.aaE);
    
    writeC(this.aaF);
    
    if (containsMask(NpcInfoType.ATTACKABLE))
    {
      writeC(this._isAttackable ? 1 : 0);
    }
    
    if (containsMask(NpcInfoType.UNKNOWN1))
    {
      writeD(0);
    }
    
    if (containsMask(NpcInfoType.TITLE))
    {
      writeS(this._title);
    }
    
    writeH(this.aaG);
    if (containsMask(NpcInfoType.ID))
    {
      writeD(this._npcId + 1000000);
    }
    
    if (containsMask(NpcInfoType.POSITION)) {
      
      writeD(this._loc.x);
      writeD(this._loc.y);
      writeD(this._loc.z + Config.CLIENT_Z_SHIFT);
    } 
    
    if (containsMask(NpcInfoType.HEADING))
    {
      writeD(this._loc.h);
    }
    
    if (containsMask(NpcInfoType.UNKNOWN2))
    {
      writeD(0);
    }
    
    if (containsMask(NpcInfoType.ATK_CAST_SPEED)) {
      
      writeD(this._mAtkSpd);
      writeD(this._pAtkSpd);
    } 
    
    if (containsMask(NpcInfoType.SPEED_MULTIPLIER)) {
      
      writeE(this.moveAnimMod);
      writeE(this.atkSpeed);
    } 
    
    if (containsMask(NpcInfoType.EQUIPPED)) {
      
      writeD(this._rhand);
      writeD(0);
      writeD(this._lhand);
    } 
    
    if (containsMask(NpcInfoType.ALIVE))
    {
      writeC((this.dead == 0));
    }
    
    if (containsMask(NpcInfoType.RUNNING))
    {
      writeC(this.running);
    }
    
    if (containsMask(NpcInfoType.SWIM_OR_FLY))
    {
      writeC(this.isFlying ? 2 : 0);
    }
    
    if (containsMask(NpcInfoType.TEAM))
    {
      writeC(this._team.ordinal());
    }
    
    if (containsMask(NpcInfoType.ENCHANT))
    {
      writeD(this._enchantEffect);
    }
    
    if (containsMask(NpcInfoType.FLYING))
    {
      writeD(0);
    }
    
    if (containsMask(NpcInfoType.CLONE))
    {
      writeD(0);
    }
    
    if (containsMask(NpcInfoType.COLOR_EFFECT))
    {
      writeD(this._isPet);
    }
    
    if (containsMask(NpcInfoType.DISPLAY_EFFECT))
    {
      writeD(this._state);
    }
    
    if (containsMask(NpcInfoType.TRANSFORMATION))
    {
      writeD(this._formId);
    }
    
    if (containsMask(NpcInfoType.CURRENT_HP))
    {
      writeD(this.currentHp);
    }
    
    if (containsMask(NpcInfoType.CURRENT_MP))
    {
      writeD(this.currentMp);
    }
    
    if (containsMask(NpcInfoType.MAX_HP))
    {
      writeD(this.maxHp);
    }
    
    if (containsMask(NpcInfoType.MAX_MP))
    {
      writeD(this.maxMp);
    }
    
    if (containsMask(NpcInfoType.SUMMONED))
    {
      writeC(0);
    }
    
    if (containsMask(NpcInfoType.UNKNOWN12)) {
      
      writeD(0);
      writeD(0);
    } 
    
    if (containsMask(NpcInfoType.NAME))
    {
      writeS(this._name);
    }
    
    if (containsMask(NpcInfoType.NAME_NPCSTRINGID))
    {
      writeD(this._nameNpcString.getId());
    }
    
    if (containsMask(NpcInfoType.TITLE_NPCSTRINGID))
    {
      writeD(this._titleNpcString.getId());
    }
    
    if (containsMask(NpcInfoType.PVP_FLAG))
    {
      writeC(this.pvp_flag);
    }
    
    if (containsMask(NpcInfoType.REPUTATION))
    {
      writeD(this.karma);
    }
    
    if (containsMask(NpcInfoType.CLAN)) {
      
      writeD(this.clan_id);
      writeD(this.clan_crest_id);
      writeD(this.clan_large_crest_id);
      writeD(this.ally_id);
      writeD(this.ally_crest_id);
    } 
    
    if (containsMask(NpcInfoType.VISUAL_STATE)) {
      
      byte b = 0;
      if (this.incombat == 1)
      {
        b |= true;
      }
      if (this.dead == 1)
      {
        b |= 0x2;
      }
      if (this._targetable)
      {
        b |= 0x4;
      }
      if (this._showName)
      {
        b |= 0x8;
      }
      
      writeC(b);
    } 
    
    if (containsMask(NpcInfoType.ABNORMALS)) {
      
      writeH(this.abnormalEffects.length);
      for (AbnormalEffect abnormalEffect : this.abnormalEffects)
      {
        writeH(abnormalEffect.getClientId());
      }
    } 
  }
}
