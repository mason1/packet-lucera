package l2.gameserver.network.l2.s2c;

import l2.gameserver.data.xml.holder.OneDayRewardHolder;
import l2.gameserver.model.Player;






public class ExConnectedTimeAndGettableReward
  extends L2GameServerPacket
{
  private final int adV;
  
  public ExConnectedTimeAndGettableReward(Player paramPlayer) { this.adV = OneDayRewardHolder.getInstance().getAvaliableOneDayReward(paramPlayer); }



  
  protected void writeImpl() {
    writeEx(425);
    writeD(0);
    writeD(this.adV);
    writeD(0);
    writeD(0);
    writeD(0);
    writeD(0);
    writeD(0);
    writeD(0);
    writeD(0);
    writeD(0);
    writeD(0);
    writeD(0);
  }
}
