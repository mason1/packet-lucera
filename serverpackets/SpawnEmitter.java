package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.instances.NpcInstance;





public class SpawnEmitter
  extends L2GameServerPacket
{
  private int alA;
  private int alB;
  
  public SpawnEmitter(NpcInstance paramNpcInstance, Player paramPlayer) {
    this.alB = paramPlayer.getObjectId();
    this.alA = paramNpcInstance.getObjectId();
  }



  
  protected final void writeImpl() {
    writeEx(93);
    
    writeD(this.alA);
    writeD(this.alB);
    writeD(0);
  }
}
