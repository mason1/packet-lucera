package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.items.ItemInfo;

public class TradeOtherAdd
  extends AbstractItemListPacket
{
  private final boolean aks;
  private ItemInfo acG;
  private long Xf;
  
  public TradeOtherAdd(boolean paramBoolean, ItemInfo paramItemInfo, long paramLong) {
    this.aks = paramBoolean;
    this.acG = paramItemInfo;
    this.Xf = paramLong;
  }


  
  protected final void writeImpl() {
    writeC(27);
    writeC(this.aks ? 1 : 2);
    if (this.aks) {
      
      writeD(1);
    }
    else {
      
      writeD(1);
      writeD(1);
      writeItemInfo(this.acG, this.Xf);
    } 
  }
}
