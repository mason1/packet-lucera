package l2.gameserver.network.l2.s2c;

public class ExChangeNpcState
  extends L2GameServerPacket
{
  private int abx;
  private int _state;
  
  public ExChangeNpcState(int paramInt1, int paramInt2) {
    this.abx = paramInt1;
    this._state = paramInt2;
  }


  
  protected void writeImpl() {
    writeEx(190);
    writeD(this.abx);
    writeD(this._state);
  }
}
