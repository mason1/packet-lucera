package l2.gameserver.network.l2.s2c;

import l2.commons.net.nio.impl.SendablePacket;
import l2.gameserver.Config;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.network.l2.components.IStaticPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;






public abstract class L2GameServerPacket
  extends SendablePacket<GameClient>
  implements IStaticPacket
{
  private static final Logger _log = LoggerFactory.getLogger(L2GameServerPacket.class);


  
  public final boolean write() {
    if (Config.DEBUG)
    {
      System.out.println("SEND: " + getClass().getSimpleName());
    }
    
    try {
      writeImpl();
      return true;
    }
    catch (Exception exception) {
      
      _log.error("Client: " + getClient() + " - Failed writing: " + getType(), exception);
      
      return false;
    } 
  }
  
  protected abstract void writeImpl();
  
  protected void writeEx(int paramInt) {
    writeC(254);
    writeH(paramInt);
  }


  
  protected void writeD(boolean paramBoolean) { writeD(paramBoolean ? 1 : 0); }


  
  protected void writeOptionalD(int paramInt) {
    if (paramInt >= 32767) {
      
      writeH(32767);
      writeD(paramInt);
    }
    else {
      
      writeH(paramInt);
    } 
  }




  
  protected void writeDD(int[] paramArrayOfInt, boolean paramBoolean) {
    if (paramBoolean)
      getByteBuffer().putInt(paramArrayOfInt.length); 
    for (int i : paramArrayOfInt) {
      getByteBuffer().putInt(i);
    }
  }

  
  protected void writeDD(int[] paramArrayOfInt) { writeDD(paramArrayOfInt, false); }



  
  public String getType() { return "[S] " + getClass().getSimpleName(); }




  
  public L2GameServerPacket packet(Player paramPlayer) { return this; }
}
