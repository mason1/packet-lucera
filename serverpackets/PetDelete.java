package l2.gameserver.network.l2.s2c;

public class PetDelete
  extends L2GameServerPacket
{
  private int ajq;
  private int ajr;
  
  public PetDelete(int paramInt1, int paramInt2) {
    this.ajq = paramInt1;
    this.ajr = paramInt2;
  }


  
  protected final void writeImpl() {
    writeC(183);
    writeD(this.ajq);
    writeD(this.ajr);
  }
}
