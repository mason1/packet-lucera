package l2.gameserver.network.l2.s2c;
public class Ex2ndPasswordVerify extends L2GameServerPacket {
  private final Ex2ndPasswordVerifyResult acX;
  private final int acY;
  
  public enum Ex2ndPasswordVerifyResult {
    SUCCESS(0),
    FAILED(1),
    BLOCK_HOMEPAGE(2),
    ERROR(3);
    
    private int acZ;

    
    Ex2ndPasswordVerifyResult(int param1Int1) { this.acZ = param1Int1; }



    
    public int getVal() { return this.acZ; }
  }





  
  public Ex2ndPasswordVerify(Ex2ndPasswordVerifyResult paramEx2ndPasswordVerifyResult) {
    this.acX = paramEx2ndPasswordVerifyResult;
    this.acY = 0;
  }

  
  public Ex2ndPasswordVerify(Ex2ndPasswordVerifyResult paramEx2ndPasswordVerifyResult, int paramInt) {
    this.acX = paramEx2ndPasswordVerifyResult;
    this.acY = paramInt;
  }


  
  protected void writeImpl() {
    writeEx(262);
    writeD(this.acX.getVal());
    writeD(this.acY);
  }
}
