package l2.gameserver.network.l2.s2c;

import java.util.List;
import l2.gameserver.model.Manor;
import l2.gameserver.templates.manor.SeedProduction;







public class ExShowSeedInfo
  extends L2GameServerPacket
{
  private List<SeedProduction> agU;
  private int EJ;
  
  public ExShowSeedInfo(int paramInt, List<SeedProduction> paramList) {
    this.EJ = paramInt;
    this.agU = paramList;
  }


  
  protected void writeImpl() {
    writeEx(35);
    writeC(0);
    writeD(this.EJ);
    writeD(0);
    writeD(this.agU.size());
    for (SeedProduction seedProduction : this.agU) {
      
      writeD(seedProduction.getId());
      
      writeQ(seedProduction.getCanProduce());
      writeQ(seedProduction.getStartProduce());
      writeQ(seedProduction.getPrice());
      writeD(Manor.getInstance().getSeedLevel(seedProduction.getId()));
      
      writeC(1);
      writeD(Manor.getInstance().getRewardItemBySeed(seedProduction.getId(), 1));
      
      writeC(1);
      writeD(Manor.getInstance().getRewardItemBySeed(seedProduction.getId(), 2));
    } 
  }
}
