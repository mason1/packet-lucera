package l2.gameserver.network.l2.s2c;


public class GameGuardQuery
  extends L2GameServerPacket
{
  protected final void writeImpl() {
    writeC(116);
    writeD(0);
    writeD(0);
    writeD(0);
    writeD(0);
  }
}
