package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.s2c.mask.AbstractMaskPacket;



















public class ExUserInfoEquipSlot
  extends AbstractMaskPacket<InventorySlot>
{
  private final byte[] aaE;
  private int objectId;
  private List<int[]> ahn;
  
  public ExUserInfoEquipSlot(Player paramPlayer) { this(paramPlayer, InventorySlot.VALUES); }
  
  public ExUserInfoEquipSlot(Player paramPlayer, InventorySlot... paramVarArgs) {
    this.aaE = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 };
    this.ahn = new ArrayList();
    this.objectId = paramPlayer.getObjectId();
    
    for (InventorySlot inventorySlot : paramVarArgs) {
      
      addComponentType(new InventorySlot[] { inventorySlot });
      
      int i = paramPlayer.getInventory().getPaperdollAugmentationId(inventorySlot.getSlot());
      
      this.ahn.add(new int[] { paramPlayer
            .getInventory().getPaperdollObjectId(inventorySlot.getSlot()), paramPlayer
            .getInventory().getPaperdollItemId(inventorySlot.getSlot()), i & 0xFFFF, i >> 16, 0 });
    } 
  }








  
  protected byte[] getMasks() { return this.aaE; }



  
  protected void writeImpl() {
    writeEx(342);
    writeD(this.objectId);
    writeH(InventorySlot.VALUES.length);
    writeB(this.aaE);
    
    for (int[] arrayOfInt : this.ahn) {
      
      writeH(22);
      writeD(arrayOfInt[0]);
      writeD(arrayOfInt[1]);
      writeD(arrayOfInt[2]);
      writeD(arrayOfInt[3]);
      writeD(arrayOfInt[4]);
    } 
  }
}
