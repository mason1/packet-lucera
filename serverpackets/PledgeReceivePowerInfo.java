package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.pledge.RankPrivs;
import l2.gameserver.model.pledge.UnitMember;

public class PledgeReceivePowerInfo
  extends L2GameServerPacket
{
  private int ajU;
  private int aiW;
  private String ajV;
  
  public PledgeReceivePowerInfo(UnitMember paramUnitMember) {
    this.ajU = paramUnitMember.getPowerGrade();
    this.ajV = paramUnitMember.getName();
    if (paramUnitMember.isClanLeader()) {
      this.aiW = 8388606;
    } else {
      
      RankPrivs rankPrivs = paramUnitMember.getClan().getRankPrivs(paramUnitMember.getPowerGrade());
      if (rankPrivs != null) {
        this.aiW = rankPrivs.getPrivs();
      } else {
        this.aiW = 0;
      } 
    } 
  }

  
  protected final void writeImpl() {
    writeEx(62);
    writeD(this.ajU);
    writeS(this.ajV);
    writeD(this.aiW);
  }
}
