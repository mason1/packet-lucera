package l2.gameserver.network.l2.s2c;

import java.util.HashMap;
import java.util.Map;
import l2.gameserver.model.Player;
import l2.gameserver.utils.Location;

public class PartyMemberPosition
  extends L2GameServerPacket
{
  private final Map<Integer, Location> ajk = new HashMap();

  
  public PartyMemberPosition add(Player paramPlayer) {
    this.ajk.put(Integer.valueOf(paramPlayer.getObjectId()), paramPlayer.getLoc());
    return this;
  }


  
  public int size() { return this.ajk.size(); }



  
  protected final void writeImpl() {
    writeC(186);
    writeD(this.ajk.size());
    for (Map.Entry entry : this.ajk.entrySet()) {
      
      writeD(((Integer)entry.getKey()).intValue());
      writeD(((Location)entry.getValue()).x);
      writeD(((Location)entry.getValue()).y);
      writeD(((Location)entry.getValue()).z);
    } 
  }
}
