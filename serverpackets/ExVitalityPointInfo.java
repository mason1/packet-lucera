package l2.gameserver.network.l2.s2c;


public class ExVitalityPointInfo
  extends L2GameServerPacket
{
  private final int aht;
  
  public ExVitalityPointInfo(int paramInt) { this.aht = paramInt; }



  
  protected void writeImpl() {
    writeEx(160);
    writeD(this.aht);
  }
}
