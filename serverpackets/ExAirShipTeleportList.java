package l2.gameserver.network.l2.s2c;

import java.util.Collections;
import java.util.List;
import l2.gameserver.model.entity.events.objects.BoatPoint;

public class ExAirShipTeleportList
  extends L2GameServerPacket
{
  private int NX;
  private List<BoatPoint> adh = Collections.emptyList();


  
  protected void writeImpl() {
    writeEx(154);
    writeD(this.NX);
    writeD(this.adh.size());
    
    for (byte b = 0; b < this.adh.size(); b++) {
      
      BoatPoint boatPoint = (BoatPoint)this.adh.get(b);
      writeD(b - 1);
      writeD(boatPoint.getFuel());
      writeD(boatPoint.x);
      writeD(boatPoint.y);
      writeD(boatPoint.z);
    } 
  }
}
