package l2.gameserver.network.l2.s2c;

public class FriendRemove
  extends L2GameServerPacket
{
  private final int ahF;
  private final String adj;
  
  public FriendRemove(String paramString, int paramInt) {
    this.ahF = paramInt;
    this.adj = paramString;
  }



  
  protected void writeImpl() {
    writeC(87);
    
    writeD(this.ahF);
    writeS(this.adj);
  }
}
