package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.items.ItemInfo;

public class TradeUpdate
  extends AbstractItemListPacket
{
  private final boolean aeE;
  private ItemInfo acG;
  private long Xf;
  
  public TradeUpdate(boolean paramBoolean, ItemInfo paramItemInfo, long paramLong) {
    this.aeE = paramBoolean;
    this.acG = paramItemInfo;
    this.Xf = paramLong;
  }


  
  protected final void writeImpl() {
    writeC(129);
    writeC(this.aeE ? 1 : 2);
    if (this.aeE) {
      
      writeD(1);
    }
    else {
      
      writeD(1);
      writeD(1);
      
      writeH((this.Xf > 0L && this.acG.getItem().isStackable()) ? 3 : 2);
      writeItemInfo(this.acG, this.Xf);
    } 
  }
}
