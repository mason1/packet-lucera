package l2.gameserver.network.l2.s2c;

import java.util.List;




public class ExGetBossRecord
  extends L2GameServerPacket
{
  private List<BossRecordInfo> aeH;
  private int aeI;
  private int aeJ;
  
  public ExGetBossRecord(int paramInt1, int paramInt2, List<BossRecordInfo> paramList) {
    this.aeI = paramInt1;
    this.aeJ = paramInt2;
    this.aeH = paramList;
  }


  
  protected final void writeImpl() {
    writeEx(52);
    
    writeD(this.aeI);
    writeD(this.aeJ);
    
    writeD(this.aeH.size());
    for (BossRecordInfo bossRecordInfo : this.aeH) {
      
      writeD(bossRecordInfo._bossId);
      writeD(bossRecordInfo._points);
      writeD(bossRecordInfo._unk1);
    } 
  }

  
  public static class BossRecordInfo
  {
    public int _bossId;
    public int _points;
    public int _unk1;
    
    public BossRecordInfo(int param1Int1, int param1Int2, int param1Int3) {
      this._bossId = param1Int1;
      this._points = param1Int2;
      this._unk1 = param1Int3;
    }
  }
}
