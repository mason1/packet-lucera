package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;





public class ExAdenaInvenCount
  extends L2GameServerPacket
{
  private long adc;
  private int add;
  
  public ExAdenaInvenCount(Player paramPlayer) {
    this.adc = paramPlayer.getInventory().getAdena();
    this.add = paramPlayer.getInventory().getSize();
  }


  
  protected void writeImpl() {
    writeEx(318);
    writeQ(this.adc);
    writeH(this.add);
  }
}
