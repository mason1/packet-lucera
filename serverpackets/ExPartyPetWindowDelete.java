package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Summon;

public class ExPartyPetWindowDelete
  extends L2GameServerPacket
{
  private int afz;
  private int abj;
  private int afA;
  
  public ExPartyPetWindowDelete(Summon paramSummon) {
    this.afz = paramSummon.getObjectId();
    this.afA = paramSummon.getSummonType();
    this.abj = paramSummon.getPlayer().getObjectId();
  }


  
  protected final void writeImpl() {
    writeEx(107);
    writeD(this.afz);
    writeC(this.afA);
    writeD(this.abj);
  }
}
