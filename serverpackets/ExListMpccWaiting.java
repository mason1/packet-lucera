package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.instancemanager.MatchingRoomManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.matching.MatchingRoom;







public class ExListMpccWaiting
  extends L2GameServerPacket
{
  private static final int aeN = 10;
  private int aeO;
  private List<MatchingRoom> abd;
  
  public ExListMpccWaiting(Player paramPlayer, int paramInt1, int paramInt2, boolean paramBoolean) {
    int i = (paramInt1 - 1) * 10;
    int j = paramInt1 * 10;
    byte b = 0;
    List list = MatchingRoomManager.getInstance().getMatchingRooms(MatchingRoom.CC_MATCHING, paramInt2, paramBoolean, paramPlayer);
    this.aeO = list.size();
    this.abd = new ArrayList(10);
    for (MatchingRoom matchingRoom : list) {
      
      if (b < i || b >= j) {
        continue;
      }
      this.abd.add(matchingRoom);
      b++;
    } 
  }


  
  public void writeImpl() {
    writeEx(157);
    writeD(this.aeO);
    writeD(this.abd.size());
    for (MatchingRoom matchingRoom : this.abd) {
      
      writeD(matchingRoom.getId());
      writeS(matchingRoom.getTopic());
      writeD(matchingRoom.getPlayers().size());
      writeD(matchingRoom.getMinLevel());
      writeD(matchingRoom.getMaxLevel());
      writeD(1);
      writeD(matchingRoom.getMaxMembersSize());
      Player player = matchingRoom.getLeader();
      writeS((player == null) ? "" : player.getName());
    } 
  }
}
