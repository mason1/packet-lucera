package l2.gameserver.network.l2.s2c;


public class ChooseInventoryItem
  extends L2GameServerPacket
{
  private int acm;
  
  public ChooseInventoryItem(int paramInt) { this.acm = paramInt; }



  
  protected final void writeImpl() {
    writeC(124);
    writeD(this.acm);
  }
}
