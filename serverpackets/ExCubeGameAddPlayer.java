package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;








public class ExCubeGameAddPlayer
  extends L2GameServerPacket
{
  private int Bq;
  private String _name;
  boolean _isRedTeam;
  
  public ExCubeGameAddPlayer(Player paramPlayer, boolean paramBoolean) {
    this.Bq = paramPlayer.getObjectId();
    this._name = paramPlayer.getName();
    this._isRedTeam = paramBoolean;
  }


  
  protected void writeImpl() {
    writeEx(151);
    writeD(1);
    
    writeD(-1);
    
    writeD(this._isRedTeam ? 1 : 0);
    writeD(this.Bq);
    writeS(this._name);
  }
}
