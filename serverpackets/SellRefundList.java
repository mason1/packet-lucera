package l2.gameserver.network.l2.s2c;

import java.util.LinkedList;
import java.util.List;
import l2.gameserver.Config;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.TradeItem;


public class SellRefundList
  extends L2GameServerPacket
{
  private final List<TradeItem> Gl;
  private final int akV;
  private final boolean Jz;
  
  public SellRefundList(Player paramPlayer, boolean paramBoolean) {
    this.akV = (int)paramPlayer.getAdena();
    this.Jz = paramBoolean;
    ItemInstance[] arrayOfItemInstance = paramPlayer.getInventory().getItems();
    this.Gl = new LinkedList();
    for (ItemInstance itemInstance : arrayOfItemInstance) {
      
      if (itemInstance.canBeSold(paramPlayer))
      {
        this.Gl.add(new TradeItem(itemInstance));
      }
    } 
  }


  
  protected void writeImpl() {
    writeC(16);
    writeD(this.akV);
    writeD(this.Jz);
    writeH(this.Gl.size());
    for (TradeItem tradeItem : this.Gl) {
      
      writeH(tradeItem.getItem().getType1());
      writeD(tradeItem.getObjectId());
      writeD(tradeItem.getItemId());
      writeD((int)tradeItem.getCount());
      writeH(tradeItem.getItem().getType2ForPackets());
      writeH(tradeItem.getCustomType1());
      writeQ(tradeItem.getItem().getBodyPart());
      writeH(tradeItem.getEnchantLevel());
      writeH(tradeItem.getCustomType2());
      writeH(0);
      writeD((int)Math.max(1L, tradeItem.getReferencePrice() / Config.ALT_SHOP_REFUND_SELL_DIVISOR));
    } 
  }
}
