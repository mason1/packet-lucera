package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;

public class ExBR_PremiumState
  extends L2GameServerPacket
{
  private int Bq;
  private int _state;
  
  public ExBR_PremiumState(Player paramPlayer, boolean paramBoolean) {
    this.Bq = paramPlayer.getObjectId();
    this._state = paramBoolean ? 1 : 0;
  }


  
  protected void writeImpl() {
    writeEx(218);
    writeD(this.Bq);
    writeC(this._state);
  }
}
