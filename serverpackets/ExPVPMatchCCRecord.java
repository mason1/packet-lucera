package l2.gameserver.network.l2.s2c;

import java.util.LinkedList;
import java.util.List;
import l2.gameserver.model.Player;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

public class ExPVPMatchCCRecord
  extends L2GameServerPacket
{
  private List<Pair<String, Integer>> afk;
  private int afl;
  private PVPMatchCCAction afm;
  
  public enum PVPMatchCCAction
  {
    INIT(0),
    UPDATE(1),
    DONE(2);
    
    private final int acZ;

    
    PVPMatchCCAction(int param1Int1) { this.acZ = param1Int1; }



    
    public int getVal() { return this.acZ; }
  }






  
  public ExPVPMatchCCRecord(PVPMatchCCAction paramPVPMatchCCAction) {
    this.afm = paramPVPMatchCCAction;
    this.afl = 0;
    this.afk = new LinkedList();
  }

  
  public void addPlayer(Player paramPlayer, int paramInt) {
    this.afl++;
    this.afk.add(new ImmutablePair(paramPlayer.getName(), Integer.valueOf(paramInt)));
  }


  
  public void writeImpl() {
    writeEx(137);
    writeD(this.afm.getVal());
    writeD(this.afl);
    for (Pair pair : this.afk) {
      
      writeS((CharSequence)pair.getLeft());
      writeD(((Integer)pair.getRight()).intValue());
    } 
  }
}
