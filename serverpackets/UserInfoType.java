package l2.gameserver.network.l2.s2c;

import l2.gameserver.network.l2.s2c.mask.IUpdateTypeComponent;



public static enum UserInfoType
  implements IUpdateTypeComponent
{
  RELATION(0, 4),
  BASIC_INFO(1, 16),
  BASE_STATS(2, 18),
  MAX_HPCPMP(3, 14),
  CURRENT_HPMPCP_EXP_SP(4, 38),
  ENCHANTLEVEL(5, 4),
  APPAREANCE(6, 15),
  STATUS(7, 6),
  
  STATS(8, 56),
  ELEMENTALS(9, 14),
  POSITION(10, 18),
  SPEED(11, 18),
  MULTIPLIER(12, 18),
  COL_RADIUS_HEIGHT(13, 18),
  ATK_ELEMENTAL(14, 5),
  CLAN(15, 32),
  
  SOCIAL(16, 22),
  VITA_FAME(17, 15),
  SLOTS(18, 9),
  MOVEMENTS(19, 4),
  COLOR(20, 10),
  INVENTORY_LIMIT(21, 9),
  TRUE_HERO(22, 9),
  
  ATT_SPIRITS(23, 26);
  static  {
    VALUES = values();
  }

  
  public static final UserInfoType[] VALUES;
  
  private final int VK;
  private final int aje;
  
  UserInfoType(int paramInt1, int paramInt2) {
    this.VK = paramInt1;
    this.aje = paramInt2;
  }








  
  public int getMask() { return this.VK; }



  
  public int getBlockLength() { return this.aje; }
}
