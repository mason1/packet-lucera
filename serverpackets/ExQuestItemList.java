package l2.gameserver.network.l2.s2c;

import java.util.List;
import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.LockType;








public class ExQuestItemList
  extends AbstractItemListPacket
{
  private final boolean afM;
  private List<ItemInfo> _items;
  private LockType UG;
  private int[] UH;
  
  public ExQuestItemList(boolean paramBoolean, List<ItemInfo> paramList, LockType paramLockType, int... paramVarArgs) {
    this.afM = paramBoolean;
    this._items = paramList;
    this.UG = paramLockType;
    this.UH = paramVarArgs;
  }


  
  protected void writeImpl() {
    writeEx(199);
    writeC(this.afM ? 1 : 2);
    if (this.afM) {
      
      writeH(this.UH.length);
      if (this.UH.length > 0) {
        
        writeC(this.UG.ordinal());
        for (int i : this.UH)
        {
          writeD(i);
        }
      } 
      writeD(this._items.size());
    }
    else {
      
      writeD(this._items.size());
      writeD(this._items.size());
      
      for (ItemInfo itemInfo : this._items)
      {
        writeItemInfo(itemInfo);
      }
    } 
  }
}
