package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.utils.Location;

public class Ride extends L2GameServerPacket {
  private int akQ;
  private int _id;
  private int akR;
  private Location _loc;
  
  public Ride(Player paramPlayer) {
    this._id = paramPlayer.getObjectId();
    this.akQ = paramPlayer.getMountType();
    this.akR = paramPlayer.getMountNpcId() + 1000000;
    this._loc = paramPlayer.getLoc();
  }


  
  protected final void writeImpl() {
    writeC(140);
    writeD(this._id);
    writeD((this.akQ == 0) ? 0 : 1);
    writeD(this.akQ);
    writeD(this.akR);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
  }
}
