package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.tables.ClanTable;

public class ExShowCastleInfo
  extends L2GameServerPacket
{
  private List<CastleInfo> agt = Collections.emptyList();




  
  public ExShowCastleInfo() {
    List list = ResidenceHolder.getInstance().getResidenceList(Castle.class);
    this.agt = new ArrayList(list.size());
    for (Castle castle : list) {
      
      String str = ClanTable.getInstance().getClanName(castle.getOwnerId());
      int i = castle.getId();
      int j = castle.getTaxPercent();
      int k = (int)(castle.getSiegeDate().getTimeInMillis() / 1000L);
      this.agt.add(new CastleInfo(str, i, j, k, castle.getSiegeEvent().isInProgress()));
    } 
  }


  
  protected final void writeImpl() {
    writeEx(20);
    writeD(this.agt.size());
    for (CastleInfo castleInfo : this.agt) {
      
      writeD(castleInfo._id);
      writeS(castleInfo._ownerName);
      writeD(castleInfo._tax);
      writeD(castleInfo._nextSiege);
      writeC(castleInfo.siegeInProgress ? 1 : 0);
      writeC(0);
    } 
    this.agt.clear();
  }

  
  private static class CastleInfo
  {
    public String _ownerName;
    public int _id;
    public int _tax;
    public int _nextSiege;
    public boolean siegeInProgress;
    
    public CastleInfo(String param1String, int param1Int1, int param1Int2, int param1Int3, boolean param1Boolean) {
      this._ownerName = param1String;
      this._id = param1Int1;
      this._tax = param1Int2;
      this._nextSiege = param1Int3;
      this.siegeInProgress = param1Boolean;
    }
  }
}
