package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Effect;
import l2.gameserver.model.Playable;



public class PartySpelled
  extends L2GameServerPacket
{
  private final int _type;
  private final int abx;
  private final List<AbnormalStatusUpdate.Effect> DF;
  
  public PartySpelled(Playable paramPlayable, boolean paramBoolean) {
    this.abx = paramPlayable.getObjectId();
    this._type = paramPlayable.isPet() ? 1 : (paramPlayable.isSummon() ? 2 : 0);
    
    this.DF = new ArrayList();
    if (paramBoolean)
    {
      for (Effect effect : paramPlayable.getEffectList().getAllFirstEffects()) {
        if (effect != null && effect.isInUse()) {
          effect.addPartySpelledIcon(this);
        }
      } 
    }
  }
  
  protected final void writeImpl() {
    writeC(244);
    writeD(this._type);
    writeD(this.abx);
    writeD(this.DF.size());
    for (AbnormalStatusUpdate.Effect effect : this.DF) {
      
      writeD(effect.skillId);
      writeH(effect.skillLevel);
      writeD(effect.clientAbnormalId);
      writeOptionalD(effect.duration);
    } 
  }



  
  public void addPartySpelledEffect(int paramInt1, int paramInt2, int paramInt3) { this.DF.add(new AbnormalStatusUpdate.Effect(paramInt1, paramInt2, paramInt3, 0, 0)); }
}
