package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.utils.Location;




public class GetItem
  extends L2GameServerPacket
{
  private int acn;
  private int _itemObjId;
  private Location _loc;
  
  public GetItem(ItemInstance paramItemInstance, int paramInt) {
    this._itemObjId = paramItemInstance.getObjectId();
    this._loc = paramItemInstance.getLoc();
    this.acn = paramInt;
  }


  
  protected final void writeImpl() {
    writeC(23);
    writeD(this.acn);
    writeD(this._itemObjId);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
  }
}
