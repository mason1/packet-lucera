package l2.gameserver.network.l2.s2c;





public class ExBlockRemoveResult
  extends L2GameServerPacket
{
  private boolean adA;
  private String _name;
  
  public ExBlockRemoveResult(boolean paramBoolean, String paramString) {
    this.adA = paramBoolean;
    this._name = paramString;
  }


  
  protected void writeImpl() {
    writeEx(238);
    writeD(this.adA);
    writeS(this._name);
  }
}
