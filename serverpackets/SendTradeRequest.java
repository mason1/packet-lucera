package l2.gameserver.network.l2.s2c;


public class SendTradeRequest
  extends L2GameServerPacket
{
  private int alc;
  
  public SendTradeRequest(int paramInt) { this.alc = paramInt; }



  
  protected final void writeImpl() {
    writeC(112);
    writeD(this.alc);
  }
}
