package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;





public class MagicSkillUse
  extends L2GameServerPacket
{
  private int _targetId;
  private int _skillId;
  private int _skillLevel;
  private int _hitTime;
  private int aiV;
  private int abo;
  private int _x;
  private int _y;
  private int _z;
  private int aaW;
  private int aaX;
  private int aaY;
  
  public MagicSkillUse(Creature paramCreature1, Creature paramCreature2, int paramInt1, int paramInt2, int paramInt3, long paramLong) {
    this.abo = paramCreature1.getObjectId();
    this._targetId = paramCreature2.getObjectId();
    this._skillId = paramInt1;
    this._skillLevel = paramInt2;
    this._hitTime = paramInt3;
    this.aiV = (int)paramLong;
    this._x = paramCreature1.getX();
    this._y = paramCreature1.getY();
    this._z = paramCreature1.getZ();
    this.aaW = paramCreature2.getX();
    this.aaX = paramCreature2.getY();
    this.aaY = paramCreature2.getZ();
  }

  
  public MagicSkillUse(Creature paramCreature1, Creature paramCreature2, Skill paramSkill, long paramLong) {
    this.abo = paramCreature1.getObjectId();
    this._targetId = paramCreature2.getObjectId();
    this._skillId = paramSkill.getDisplayId();
    this._skillLevel = (paramSkill.getDisplayLevel() >= 100) ? paramSkill.getBaseLevel() : paramSkill.getDisplayLevel();
    this._hitTime = paramSkill.getHitTime();
    this.aiV = (int)paramLong;
    this._x = paramCreature1.getX();
    this._y = paramCreature1.getY();
    this._z = paramCreature1.getZ();
    this.aaW = paramCreature2.getX();
    this.aaX = paramCreature2.getY();
    this.aaY = paramCreature2.getZ();
  }

  
  public MagicSkillUse(Creature paramCreature1, Creature paramCreature2, Skill paramSkill, int paramInt, long paramLong) {
    this.abo = paramCreature1.getObjectId();
    this._targetId = paramCreature2.getObjectId();
    this._skillId = paramSkill.getDisplayId();
    this._skillLevel = (paramSkill.getDisplayLevel() >= 100) ? paramSkill.getBaseLevel() : paramSkill.getDisplayLevel();
    this._hitTime = paramInt;
    this.aiV = (int)paramLong;
    this._x = paramCreature1.getX();
    this._y = paramCreature1.getY();
    this._z = paramCreature1.getZ();
    this.aaW = paramCreature2.getX();
    this.aaX = paramCreature2.getY();
    this.aaY = paramCreature2.getZ();
  }

  
  public MagicSkillUse(Creature paramCreature, int paramInt1, int paramInt2, int paramInt3, long paramLong) {
    this.abo = paramCreature.getObjectId();
    this._targetId = paramCreature.getTargetId();
    this._skillId = paramInt1;
    this._skillLevel = paramInt2;
    this._hitTime = paramInt3;
    this.aiV = (int)paramLong;
    this._x = paramCreature.getX();
    this._y = paramCreature.getY();
    this._z = paramCreature.getZ();
    this.aaW = paramCreature.getX();
    this.aaX = paramCreature.getY();
    this.aaY = paramCreature.getZ();
  }

  
  public MagicSkillUse(Creature paramCreature, Skill paramSkill, int paramInt, long paramLong) {
    this.abo = paramCreature.getObjectId();
    this._targetId = paramCreature.getTargetId();
    this._skillId = paramSkill.getDisplayId();
    this._skillLevel = paramSkill.getDisplayLevel();
    this._hitTime = paramInt;
    this.aiV = (int)paramLong;
    this._x = paramCreature.getX();
    this._y = paramCreature.getY();
    this._z = paramCreature.getZ();
    this.aaW = paramCreature.getX();
    this.aaX = paramCreature.getY();
    this.aaY = paramCreature.getZ();
  }


  
  protected final void writeImpl() {
    writeC(72);
    writeD(0);
    writeD(this.abo);
    writeD(this._targetId);
    writeD(this._skillId);
    writeD(this._skillLevel);
    writeD(this._hitTime);
    writeD(0);
    writeD(this.aiV);
    writeD(this._x);
    writeD(this._y);
    writeD(this._z);
    
    writeH(0);
    
    writeH(0);
    
    writeD(this.aaW);
    writeD(this.aaX);
    writeD(this.aaY);
    
    writeD(0);
    writeD(0);
  }


  
  public L2GameServerPacket packet(Player paramPlayer) {
    if (paramPlayer != null && !paramPlayer.isInObserverMode()) {
      
      if (paramPlayer.buffAnimRange() < 0)
        return null; 
      if (paramPlayer.buffAnimRange() == 0) {
        return (this.abo == paramPlayer.getObjectId()) ? super.packet(paramPlayer) : null;
      }
      return (paramPlayer.getDistance(this._x, this._y) < paramPlayer.buffAnimRange()) ? super.packet(paramPlayer) : null;
    } 
    
    return super.packet(paramPlayer);
  }
}
