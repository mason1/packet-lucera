package l2.gameserver.network.l2.s2c;

import java.util.Map;
import l2.commons.util.Rnd;
import l2.gameserver.cache.Msg;
import l2.gameserver.data.xml.holder.EnchantSkillHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.model.actor.instances.player.ShortCut;
import l2.gameserver.model.base.Experience;
import l2.gameserver.model.instances.TrainerInstance;
import l2.gameserver.scripts.Functions;
import l2.gameserver.skills.TimeStamp;
import l2.gameserver.tables.SkillTable;
import l2.gameserver.templates.SkillEnchant;
import l2.gameserver.utils.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExEnchantSkill
{
  private static final Logger LOG = LoggerFactory.getLogger(ExEnchantSkill.class);
  public static String EX_ENCHANT_SKILL_BYPASS = "ExEnchantSkill";

  
  public static L2GameServerPacket packetFor(Player paramPlayer, TrainerInstance paramTrainerInstance, String... paramVarArgs) {
    if (paramPlayer.getClassId().getLevel() < 4 || paramPlayer.getLevel() < 76 || paramVarArgs.length < 2)
    {
      return new SystemMessage(1438);
    }
    
    int i = 0;
    int j = 0;
    int k = 0;
    
    try {
      i = Integer.parseInt(paramVarArgs[0]);
      j = Integer.parseInt(paramVarArgs[1]);
      if (paramVarArgs.length > 2) {
        k = Integer.parseInt(paramVarArgs[2]);
      }
    } catch (Exception exception) {
      
      return new SystemMessage(1438);
    } 
    
    Skill skill1 = paramPlayer.getKnownSkill(i);
    if (skill1 == null)
    {
      return new SystemMessage(1438);
    }
    int m = skill1.getLevel();
    int n = skill1.getBaseLevel();
    Map map = EnchantSkillHolder.getInstance().getLevelsOf(i);
    
    if (map == null || map.isEmpty())
    {
      return new SystemMessage(1438);
    }
    
    SkillEnchant skillEnchant1 = (SkillEnchant)map.get(Integer.valueOf(m));
    SkillEnchant skillEnchant2 = (SkillEnchant)map.get(Integer.valueOf(j));
    
    if (skillEnchant2 == null)
    {
      return new SystemMessage(1438);
    }
    
    if (skillEnchant1 != null) {
      
      if (skillEnchant1.getRouteId() != skillEnchant2.getRouteId() || skillEnchant2.getEnchantLevel() != skillEnchant1.getEnchantLevel() + 1)
      {
        return new SystemMessage(1438);
      }
    }
    else if (skillEnchant2.getEnchantLevel() != 1 || m != n) {
      
      LOG.warn("Player \"" + paramPlayer.toString() + "\" trying to use enchant  exploit" + skill1.toString() + " to " + j + "(enchant level " + skillEnchant2.getEnchantLevel() + ")");
      return new SystemMessage(1438);
    } 
    
    int[] arrayOfInt = skillEnchant2.getChances();
    int i1 = Experience.LEVEL.length - arrayOfInt.length - 1;
    
    if (paramPlayer.getLevel() < i1) {
      
      paramPlayer.sendPacket((new SystemMessage(607)).addNumber(i1));
      return ExEnchantSkillList.packetFor(paramPlayer, (TrainerInstance)paramPlayer.getLastNpc(), k);
    } 
    
    if (paramPlayer.getSp() < skillEnchant2.getSp()) {
      
      paramPlayer.sendPacket(new SystemMessage(1443));
      return ExEnchantSkillList.packetFor(paramPlayer, (TrainerInstance)paramPlayer.getLastNpc(), k);
    } 
    
    if (paramPlayer.getExp() < skillEnchant2.getExp()) {
      
      paramPlayer.sendPacket(new SystemMessage(1444));
      return ExEnchantSkillList.packetFor(paramPlayer, (TrainerInstance)paramPlayer.getLastNpc(), k);
    } 
    
    if (skillEnchant2.getItemId() > 0 && skillEnchant2.getItemCount() > 0L)
    {
      if (Functions.removeItem(paramPlayer, skillEnchant2.getItemId(), skillEnchant2.getItemCount()) < skillEnchant2.getItemCount()) {
        
        paramPlayer.sendPacket(Msg.ITEMS_REQUIRED_FOR_SKILL_ENCHANT_ARE_INSUFFICIENT);
        return ExEnchantSkillList.packetFor(paramPlayer, (TrainerInstance)paramPlayer.getLastNpc(), k);
      } 
    }
    
    int i2 = Math.max(0, Math.min(paramPlayer.getLevel() - i1, arrayOfInt.length - 1));
    int i3 = arrayOfInt[i2];
    
    paramPlayer.addExpAndSp(-1L * skillEnchant2.getExp(), (-1 * skillEnchant2.getSp()));
    paramPlayer.sendPacket((new SystemMessage(538)).addNumber(skillEnchant2.getSp()));
    paramPlayer.sendPacket((new SystemMessage(539)).addNumber(skillEnchant2.getExp()));
    
    TimeStamp timeStamp = paramPlayer.getSkillReuse(skill1);
    
    Skill skill2 = null;
    
    if (Rnd.chance(i3)) {
      
      skill2 = SkillTable.getInstance().getInfo(skillEnchant2.getSkillId(), skillEnchant2.getSkillLevel());
      paramPlayer.sendPacket((new SystemMessage(1440)).addSkillName(i, j));
      Log.add(paramPlayer.getName() + "|Successfully enchanted|" + i + "|to+" + j + "|" + i3, "enchant_skills");
    }
    else {
      
      skill2 = SkillTable.getInstance().getInfo(skill1.getId(), skill1.getBaseLevel());
      paramPlayer.sendPacket((new SystemMessage(1441)).addSkillName(i, j));
      Log.add(paramPlayer.getName() + "|Failed to enchant|" + i + "|to+" + j + "|" + i3, "enchant_skills");
    } 
    if (timeStamp != null && timeStamp.hasNotPassed())
    {
      paramPlayer.disableSkill(skill2, timeStamp.getReuseCurrent());
    }
    paramPlayer.addSkill(skill2, true);
    paramPlayer.sendPacket(new SkillList(paramPlayer));
    updateSkillShortcuts(paramPlayer, i, j);
    return ExEnchantSkillList.packetFor(paramPlayer, (TrainerInstance)paramPlayer.getLastNpc(), k);
  }

  
  protected static void updateSkillShortcuts(Player paramPlayer, int paramInt1, int paramInt2) {
    for (ShortCut shortCut : paramPlayer.getAllShortCuts()) {
      
      if (shortCut.getId() == paramInt1 && shortCut.getType() == 2) {
        
        ShortCut shortCut1 = new ShortCut(shortCut.getSlot(), shortCut.getPage(), shortCut.getType(), shortCut.getId(), paramInt2, 1);
        paramPlayer.sendPacket(new ShortCutRegister(paramPlayer, shortCut1));
        paramPlayer.registerShortCut(shortCut1);
      } 
    } 
  }
}
