package l2.gameserver.network.l2.s2c;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import l2.gameserver.utils.Location;

public class ExServerPrimitive
  extends L2GameServerPacket {
  private final String _name;
  private final int _x;
  private final int _y;
  
  public ExServerPrimitive(String paramString, int paramInt1, int paramInt2, int paramInt3) {
    this.agk = new ArrayList();
    this.agl = new ArrayList();








    
    this._name = paramString;
    this._x = paramInt1;
    this._y = paramInt2;
    this._z = paramInt3;
  }

  
  private final int _z;
  
  private final List<Point> agk;
  private final List<Line> agl;
  
  public ExServerPrimitive(String paramString, Location paramLocation) { this(paramString, paramLocation.getX(), paramLocation.getY(), paramLocation.getZ()); }












  
  public void addPoint(String paramString, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3, int paramInt4) { this.agk.add(new Point(paramString, paramInt1, paramBoolean, paramInt2, paramInt3, paramInt4)); }



  
  public void addGeoPoint(String paramString, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3, int paramInt4) { addPoint(paramString, paramInt1, paramBoolean, (new Location(paramInt2, paramInt3, (short)((short)(paramInt4 & 0xFFF0) >> 1))).geo2world()); }










  
  public void addPoint(String paramString, int paramInt, boolean paramBoolean, Location paramLocation) { addPoint(paramString, paramInt, paramBoolean, paramLocation.getX(), paramLocation.getY(), paramLocation.getZ()); }










  
  public void addPoint(int paramInt1, int paramInt2, int paramInt3, int paramInt4) { addPoint("", paramInt1, false, paramInt2, paramInt3, paramInt4); }








  
  public void addPoint(int paramInt, Location paramLocation) { addPoint("", paramInt, false, paramLocation); }












  
  public void addPoint(String paramString, Color paramColor, boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3) { addPoint(paramString, paramColor.getRGB(), paramBoolean, paramInt1, paramInt2, paramInt3); }










  
  public void addPoint(String paramString, Color paramColor, boolean paramBoolean, Location paramLocation) { addPoint(paramString, paramColor.getRGB(), paramBoolean, paramLocation); }










  
  public void addPoint(Color paramColor, int paramInt1, int paramInt2, int paramInt3) { addPoint("", paramColor, false, paramInt1, paramInt2, paramInt3); }








  
  public void addPoint(Color paramColor, Location paramLocation) { addPoint("", paramColor, false, paramLocation); }















  
  public void addLine(String paramString, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7) { this.agl.add(new Line(paramString, paramInt1, paramBoolean, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7)); }













  
  public void addLine(String paramString, int paramInt1, boolean paramBoolean, Location paramLocation, int paramInt2, int paramInt3, int paramInt4) { addLine(paramString, paramInt1, paramBoolean, paramLocation.getX(), paramLocation.getY(), paramLocation.getZ(), paramInt2, paramInt3, paramInt4); }













  
  public void addLine(String paramString, int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3, int paramInt4, Location paramLocation) { addLine(paramString, paramInt1, paramBoolean, paramInt2, paramInt3, paramInt4, paramLocation.getX(), paramLocation.getY(), paramLocation.getZ()); }











  
  public void addLine(String paramString, int paramInt, boolean paramBoolean, Location paramLocation1, Location paramLocation2) { addLine(paramString, paramInt, paramBoolean, paramLocation1, paramLocation2.getX(), paramLocation2.getY(), paramLocation2.getZ()); }













  
  public void addLine(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7) { addLine("", paramInt1, false, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7); }











  
  public void addLine(int paramInt1, Location paramLocation, int paramInt2, int paramInt3, int paramInt4) { addLine("", paramInt1, false, paramLocation, paramInt2, paramInt3, paramInt4); }











  
  public void addLine(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Location paramLocation) { addLine("", paramInt1, false, paramInt2, paramInt3, paramInt4, paramLocation); }









  
  public void addLine(int paramInt, Location paramLocation1, Location paramLocation2) { addLine("", paramInt, false, paramLocation1, paramLocation2); }















  
  public void addLine(String paramString, Color paramColor, boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) { addLine(paramString, paramColor.getRGB(), paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6); }













  
  public void addLine(String paramString, Color paramColor, boolean paramBoolean, Location paramLocation, int paramInt1, int paramInt2, int paramInt3) { addLine(paramString, paramColor.getRGB(), paramBoolean, paramLocation, paramInt1, paramInt2, paramInt3); }













  
  public void addLine(String paramString, Color paramColor, boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, Location paramLocation) { addLine(paramString, paramColor.getRGB(), paramBoolean, paramInt1, paramInt2, paramInt3, paramLocation); }











  
  public void addLine(String paramString, Color paramColor, boolean paramBoolean, Location paramLocation1, Location paramLocation2) { addLine(paramString, paramColor.getRGB(), paramBoolean, paramLocation1, paramLocation2); }













  
  public void addLine(Color paramColor, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) { addLine("", paramColor, false, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6); }











  
  public void addLine(Color paramColor, Location paramLocation, int paramInt1, int paramInt2, int paramInt3) { addLine("", paramColor, false, paramLocation, paramInt1, paramInt2, paramInt3); }











  
  public void addLine(Color paramColor, int paramInt1, int paramInt2, int paramInt3, Location paramLocation) { addLine("", paramColor, false, paramInt1, paramInt2, paramInt3, paramLocation); }









  
  public void addLine(Color paramColor, Location paramLocation1, Location paramLocation2) { addLine("", paramColor, false, paramLocation1, paramLocation2); }



  
  protected void writeImpl() {
    writeEx(17);

    
    writeS(this._name);
    writeD(this._x);
    writeD(this._y);
    writeD(this._z);
    writeD(65535);
    writeD(65535);
    
    writeD(this.agk.size() + this.agl.size());
    
    for (Point point : this.agk) {
      
      writeC(1);
      writeS(point.getName());
      int i = point.getColor();
      writeD(i >> 16 & 0xFF);
      writeD(i >> 8 & 0xFF);
      writeD(i & 0xFF);
      writeD(point.isNameColored() ? 1 : 0);
      writeD(point.getX());
      writeD(point.getY());
      writeD(point.getZ());
    } 
    
    for (Line line : this.agl) {
      
      writeC(2);
      writeS(line.getName());
      int i = line.getColor();
      writeD(i >> 16 & 0xFF);
      writeD(i >> 8 & 0xFF);
      writeD(i & 0xFF);
      writeD(line.isNameColored() ? 1 : 0);
      writeD(line.getX());
      writeD(line.getY());
      writeD(line.getZ());
      writeD(line.getX2());
      writeD(line.getY2());
      writeD(line.getZ2());
    } 
  }

  
  private static class Point
  {
    private final String _name;
    private final int agp;
    private final boolean agq;
    private final int _x;
    private final int _y;
    private final int _z;
    
    public Point(String param1String, int param1Int1, boolean param1Boolean, int param1Int2, int param1Int3, int param1Int4) {
      this._name = param1String;
      this.agp = param1Int1;
      this.agq = param1Boolean;
      this._x = param1Int2;
      this._y = param1Int3;
      this._z = param1Int4;
    }





    
    public String getName() { return this._name; }






    
    public int getColor() { return this.agp; }






    
    public boolean isNameColored() { return this.agq; }






    
    public int getX() { return this._x; }






    
    public int getY() { return this._y; }






    
    public int getZ() { return this._z; }
  }

  
  private static class Line
    extends Point
  {
    private final int agm;
    private final int agn;
    private final int ago;
    
    public Line(String param1String, int param1Int1, boolean param1Boolean, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, int param1Int7) {
      super(param1String, param1Int1, param1Boolean, param1Int2, param1Int3, param1Int4);
      this.agm = param1Int5;
      this.agn = param1Int6;
      this.ago = param1Int7;
    }





    
    public int getX2() { return this.agm; }






    
    public int getY2() { return this.agn; }






    
    public int getZ2() { return this.ago; }
  }
}
