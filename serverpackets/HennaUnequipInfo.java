package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.templates.Henna;

public class HennaUnequipInfo
  extends L2GameServerPacket {
  private int ahG;
  private int ahH;
  private int ahI;
  private int ahJ;
  
  public HennaUnequipInfo(Henna paramHenna, Player paramPlayer) {
    this.aiE = paramHenna;
    this.abb = paramPlayer.getAdena();
    this.ahG = paramPlayer.getSTR();
    this.ahI = paramPlayer.getDEX();
    this.ahH = paramPlayer.getCON();
    this.ahJ = paramPlayer.getINT();
    this.ahK = paramPlayer.getWIT();
    this.ahL = paramPlayer.getMEN();
  }
  
  private int ahK;
  private int ahL;
  
  protected final void writeImpl() {
    writeC(231);
    writeD(this.aiE.getSymbolId());
    writeD(this.aiE.getDyeId());
    
    writeQ(this.aiE.getDrawCount());
    writeQ(this.aiE.getPrice());
    writeD(1);
    writeQ(this.abb);
    
    writeD(this.ahJ);
    writeC(this.ahJ + this.aiE.getStatINT());
    writeD(this.ahG);
    writeC(this.ahG + this.aiE.getStatSTR());
    writeD(this.ahH);
    writeC(this.ahH + this.aiE.getStatCON());
    writeD(this.ahL);
    writeC(this.ahL + this.aiE.getStatMEN());
    writeD(this.ahI);
    writeC(this.ahI + this.aiE.getStatDEX());
    writeD(this.ahK);
    writeC(this.ahK + this.aiE.getStatWIT());
  }
  
  private long abb;
  private Henna aiE;
}
