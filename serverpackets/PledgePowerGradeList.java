package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.pledge.RankPrivs;


public class PledgePowerGradeList
  extends L2GameServerPacket
{
  private RankPrivs[] ajS;
  
  public PledgePowerGradeList(RankPrivs[] paramArrayOfRankPrivs) { this.ajS = paramArrayOfRankPrivs; }



  
  protected final void writeImpl() {
    writeEx(61);
    writeD(this.ajS.length);
    for (RankPrivs rankPrivs : this.ajS) {
      
      writeD(rankPrivs.getRank());
      writeD(rankPrivs.getParty());
    } 
  }
}
