package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import l2.gameserver.model.entity.oly.CompetitionType;
import l2.gameserver.model.entity.oly.Stadium;





public class ExReceiveOlympiadGameList
  extends L2GameServerPacket
{
  private ArrayList<GameRec> afP = new ArrayList();



  
  public void add(Stadium paramStadium, CompetitionType paramCompetitionType, int paramInt, String paramString1, String paramString2) { this.afP.add(new GameRec(paramStadium, paramCompetitionType, paramInt, paramString1, paramString2)); }



  
  protected void writeImpl() {
    writeEx(212);
    writeD(0);
    
    writeD(this.afP.size());
    writeD(0);
    
    for (GameRec gameRec : this.afP) {
      
      writeD(gameRec.stadium_id);
      writeD(gameRec.type);
      writeD(gameRec.state);
      writeS(gameRec.player0name);
      writeS(gameRec.player1name);
    } 
  }

  
  private class GameRec
  {
    int stadium_id;
    int type;
    
    public GameRec(Stadium param1Stadium, CompetitionType param1CompetitionType, int param1Int, String param1String1, String param1String2) {
      this.stadium_id = param1Stadium.getStadiumId();
      this.type = param1CompetitionType.getTypeIdx();
      this.state = param1Int;
      this.player0name = param1String1;
      this.player1name = param1String2;
    }
    
    int state;
    String player0name;
    String player1name;
  }
}
