package l2.gameserver.network.l2.s2c;

import java.util.Calendar;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.events.impl.CastleSiegeEvent;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.model.entity.residence.ClanHall;
import l2.gameserver.model.entity.residence.Residence;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.model.pledge.Clan;
import org.apache.commons.lang3.ArrayUtils;






















public class CastleSiegeInfo
  extends L2GameServerPacket
{
  private long abi;
  private int _id;
  private int abj;
  private int Vo;
  private boolean abk;
  private String _ownerName;
  private String abl;
  private String Vn;
  private int[] abm;
  
  public CastleSiegeInfo(Castle paramCastle, Player paramPlayer) {
    this(paramCastle, paramPlayer);
    
    CastleSiegeEvent castleSiegeEvent = (CastleSiegeEvent)paramCastle.getSiegeEvent();
    long l = paramCastle.getSiegeDate().getTimeInMillis();
    if (l == 0L) {
      this.abm = castleSiegeEvent.getNextSiegeTimes();
    } else {
      this.abi = (int)(l / 1000L);
    } 
  }
  
  public CastleSiegeInfo(ClanHall paramClanHall, Player paramPlayer) {
    this(paramClanHall, paramPlayer);
    
    this.abi = (int)(paramClanHall.getSiegeDate().getTimeInMillis() / 1000L); } protected CastleSiegeInfo(Residence paramResidence, Player paramPlayer) {
    this._ownerName = "NPC";
    this.abl = "";
    this.Vn = "";
    this.abm = ArrayUtils.EMPTY_INT_ARRAY;
    this._id = paramResidence.getId();
    this.abj = paramResidence.getOwnerId();
    Clan clan = paramResidence.getOwner();
    if (clan != null) {
      
      this.abk = (paramPlayer.isGM() || clan.getLeaderId(0) == paramPlayer.getObjectId());
      this._ownerName = clan.getName();
      this.abl = clan.getLeaderName(0);
      Alliance alliance = clan.getAlliance();
      if (alliance != null) {
        
        this.Vo = alliance.getAllyId();
        this.Vn = alliance.getAllyName();
      } 
    } 
  }


  
  protected void writeImpl() {
    writeC(201);
    writeD(this._id);
    writeD(this.abk ? 1 : 0);
    writeD(this.abj);
    writeS(this._ownerName);
    writeS(this.abl);
    writeD(this.Vo);
    writeS(this.Vn);
    writeD((int)(Calendar.getInstance().getTimeInMillis() / 1000L));
    writeD((int)this.abi);
    if (this.abi == 0L)
      writeDD(this.abm, true); 
  }
}
