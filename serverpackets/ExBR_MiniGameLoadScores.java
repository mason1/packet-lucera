package l2.gameserver.network.l2.s2c;

import java.util.List;
import java.util.Map;
import org.napile.primitive.maps.IntObjectMap;
import org.napile.primitive.maps.impl.TreeIntObjectMap;






public class ExBR_MiniGameLoadScores
  extends L2GameServerPacket
{
  private int adw;
  private int YH;
  private int adx;
  private IntObjectMap<List<Map.Entry<String, Integer>>> ady = new TreeIntObjectMap();


  
  protected void writeImpl() {
    writeEx(221);
    writeD(this.adw);
    writeD(this.YH);
    writeD(0);
    writeD(this.adx);
    for (IntObjectMap.Entry entry : this.ady.entrySet()) {
      for (Map.Entry entry1 : (List)entry.getValue()) {
        
        writeD(entry.getKey());
        writeS((CharSequence)entry1.getKey());
        writeD(((Integer)entry1.getValue()).intValue());
      } 
    } 
  }
}
