package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.network.l2.components.SystemMsg;






public class SystemMessage2
  extends SysMsgContainer<SystemMessage2>
{
  public SystemMessage2(SystemMsg paramSystemMsg) { super(paramSystemMsg); }


  
  public static SystemMessage2 obtainItems(int paramInt1, long paramLong, int paramInt2) {
    if (paramInt1 == 57)
      return (SystemMessage2)(new SystemMessage2(SystemMsg.YOU_HAVE_EARNED_S1_ADENA)).addLong(paramLong); 
    if (paramLong > 1L)
      return (SystemMessage2)((SystemMessage2)(new SystemMessage2(SystemMsg.YOU_HAVE_EARNED_S2_S1S)).addItemName(paramInt1)).addLong(paramLong); 
    if (paramInt2 > 0)
      return (SystemMessage2)((SystemMessage2)(new SystemMessage2(SystemMsg.YOU_HAVE_OBTAINED_A_S1_S2)).addInteger(paramInt2)).addItemName(paramInt1); 
    return (SystemMessage2)(new SystemMessage2(SystemMsg.YOU_HAVE_EARNED_S1)).addItemName(paramInt1);
  }


  
  public static SystemMessage2 obtainItems(ItemInstance paramItemInstance) { return obtainItems(paramItemInstance.getItemId(), paramItemInstance.getCount(), paramItemInstance.isEquipable() ? paramItemInstance.getEnchantLevel() : 0); }


  
  public static SystemMessage2 obtainItemsBy(int paramInt1, long paramLong, int paramInt2, Creature paramCreature) {
    if (paramLong > 1L)
      return (SystemMessage2)((SystemMessage2)((SystemMessage2)(new SystemMessage2(SystemMsg.C1_HAS_OBTAINED_S3_S2)).addName(paramCreature)).addItemName(paramInt1)).addLong(paramLong); 
    if (paramInt2 > 0)
      return (SystemMessage2)((SystemMessage2)((SystemMessage2)(new SystemMessage2(SystemMsg.C1_HAS_OBTAINED_S2S3)).addName(paramCreature)).addInteger(paramInt2)).addItemName(paramInt1); 
    return (SystemMessage2)((SystemMessage2)(new SystemMessage2(SystemMsg.C1_HAS_OBTAINED_S2)).addName(paramCreature)).addItemName(paramInt1);
  }


  
  public static SystemMessage2 obtainItemsBy(ItemInstance paramItemInstance, Creature paramCreature) { return obtainItemsBy(paramItemInstance.getItemId(), paramItemInstance.getCount(), paramItemInstance.isEquipable() ? paramItemInstance.getEnchantLevel() : 0, paramCreature); }


  
  public static SystemMessage2 removeItems(int paramInt, long paramLong) {
    if (paramInt == 57)
      return (SystemMessage2)(new SystemMessage2(SystemMsg.S1_ADENA_DISAPPEARED)).addLong(paramLong); 
    if (paramLong > 1L)
      return (SystemMessage2)((SystemMessage2)(new SystemMessage2(SystemMsg.S2_S1_HAS_DISAPPEARED)).addItemName(paramInt)).addLong(paramLong); 
    return (SystemMessage2)(new SystemMessage2(SystemMsg.S1_HAS_DISAPPEARED)).addItemName(paramInt);
  }


  
  public static SystemMessage2 removeItems(ItemInstance paramItemInstance) { return removeItems(paramItemInstance.getItemId(), paramItemInstance.getCount()); }



  
  protected void writeImpl() {
    writeC(98);
    writeElements();
  }
}
