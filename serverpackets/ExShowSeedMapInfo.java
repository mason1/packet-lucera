package l2.gameserver.network.l2.s2c;

import l2.gameserver.utils.Location;



















public class ExShowSeedMapInfo
  extends L2GameServerPacket
{
  private static final Location[] agV = { new Location(-246857, 251960, 4331, 1), new Location(-213770, 210760, 4400, 2) };


  
  protected void writeImpl() {
    writeEx(161);
    writeD(agV.length);
    for (Location location : agV) {
      
      writeD(location.x);
      writeD(location.y);
      writeD(location.z);
      writeD(location.h);
    } 
  }
}
