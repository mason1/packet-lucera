package l2.gameserver.network.l2.s2c;

import l2.gameserver.GameTimeController;
import l2.gameserver.model.Player;
import l2.gameserver.utils.Location;

public class CharSelected
  extends L2GameServerPacket {
  private int pp;
  private int char_id;
  private int clan_id;
  private int sex;
  private int race;
  private int class_id;
  private String _name;
  private String _title;
  
  public CharSelected(Player paramPlayer, int paramInt) {
    this.pp = paramInt;
    
    this._name = paramPlayer.getName();
    this.char_id = paramPlayer.getObjectId();
    this._title = paramPlayer.getTitle();
    this.clan_id = paramPlayer.getClanId();
    this.sex = paramPlayer.getSex();
    this.race = paramPlayer.getRace().ordinal();
    this.class_id = paramPlayer.getClassId().getId();
    this._loc = paramPlayer.getLoc();
    this.aci = paramPlayer.getCurrentHp();
    this.acj = paramPlayer.getCurrentMp();
    this.ack = paramPlayer.getIntSp();
    this._exp = paramPlayer.getExp();
    this.level = paramPlayer.getLevel();
    this.karma = -paramPlayer.getKarma();
    this.BD = paramPlayer.getPkKills();
  }
  private Location _loc; private double aci; private double acj; private int level; private int karma; private int BD; private long _exp;
  private long ack;
  
  protected final void writeImpl() {
    writeC(11);
    
    writeS(this._name);
    writeD(this.char_id);
    writeS(this._title);
    writeD(this.pp);
    writeD(this.clan_id);
    writeD(0);
    writeD(this.sex);
    writeD(this.race);
    writeD(this.class_id);
    writeD(1);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
    
    writeF(this.aci);
    writeF(this.acj);
    writeQ(this.ack);
    writeQ(this._exp);
    writeD(this.level);
    writeD(this.karma);
    writeD(this.BD);
    writeD(GameTimeController.getInstance().getGameTime());
    writeD(0);
    writeD(this.class_id);
    
    writeB(new byte[16]);
    
    writeD(0);
    writeD(0);
    writeD(0);
    writeD(0);
    
    writeD(0);
    
    writeD(0);
    writeD(0);
    writeD(0);
    writeD(0);
    
    writeB(new byte[28]);
    writeD(0);
  }
}
