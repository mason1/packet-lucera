package l2.gameserver.network.l2.s2c;

public class ExPledgeCrestLarge
  extends L2GameServerPacket
{
  private int Vv;
  private byte[] _data;
  
  public ExPledgeCrestLarge(int paramInt, byte[] paramArrayOfByte) {
    this.Vv = paramInt;
    this._data = paramArrayOfByte;
  }


  
  protected final void writeImpl() {
    writeEx(27);
    
    writeD(0);
    writeD(this.Vv);
    writeD(this._data.length);
    writeB(this._data);
  }
}
