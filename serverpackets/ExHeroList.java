package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collection;
import l2.gameserver.Config;
import l2.gameserver.model.entity.oly.HeroController;



















public class ExHeroList
  extends L2GameServerPacket
{
  private Collection<HeroController.HeroRecord> aeM = new ArrayList(); public ExHeroList() {
    for (HeroController.HeroRecord heroRecord : HeroController.getInstance().getCurrentHeroes()) {
      
      if (heroRecord != null && heroRecord.active && heroRecord.played) {
        this.aeM.add(heroRecord);
      }
    } 
  }

  
  protected final void writeImpl() {
    writeEx(122);
    
    writeD(this.aeM.size());
    for (HeroController.HeroRecord heroRecord : this.aeM) {
      
      writeS(heroRecord.name);
      writeD(heroRecord.class_id);
      writeS(heroRecord.clan_name);
      writeD(heroRecord.clan_crest);
      writeS(heroRecord.ally_name);
      writeD(heroRecord.ally_crest);
      writeD(heroRecord.count);
      writeD(Config.REQUEST_ID);
    } 
  }
}
