package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.s2c.mask.AbstractMaskPacket;
import l2.gameserver.network.l2.s2c.mask.IUpdateTypeComponent;

public class PartySmallWindowUpdate
  extends AbstractMaskPacket<PartySmallWindowUpdateType>
{
  private int acr;
  private int class_id;
  private int level;
  private int curCp;
  private int maxCp;
  
  public PartySmallWindowUpdate(Player paramPlayer) { this(paramPlayer, PartySmallWindowUpdateType.VALUES); }
  private int curHp; private int maxHp; private int curMp; private int maxMp; private String ajo;
  private int _flags;
  
  public PartySmallWindowUpdate(Player paramPlayer, PartySmallWindowUpdateType... paramVarArgs) {
    this.acr = paramPlayer.getObjectId();
    this.ajo = paramPlayer.getName();
    this.curCp = (int)paramPlayer.getCurrentCp();
    this.maxCp = paramPlayer.getMaxCp();
    this.curHp = (int)paramPlayer.getCurrentHp();
    this.maxHp = paramPlayer.getMaxHp();
    this.curMp = (int)paramPlayer.getCurrentMp();
    this.maxMp = paramPlayer.getMaxMp();
    this.level = paramPlayer.getLevel();
    
    this.class_id = paramPlayer.getClassId().getId();
    
    addComponentType(paramVarArgs);
  }



  
  protected void addMask(int paramInt) { this._flags |= paramInt; }




  
  public boolean containsMask(PartySmallWindowUpdateType paramPartySmallWindowUpdateType) { return containsMask(this._flags, paramPartySmallWindowUpdateType); }




  
  protected byte[] getMasks() { return new byte[0]; }



  
  protected final void writeImpl() {
    writeC(82);
    writeD(this.acr);
    writeH(this._flags);
    
    if (containsMask(PartySmallWindowUpdateType.CURRENT_CP))
    {
      writeD(this.curCp);
    }
    if (containsMask(PartySmallWindowUpdateType.MAX_CP))
    {
      writeD(this.maxCp);
    }
    if (containsMask(PartySmallWindowUpdateType.CURRENT_HP))
    {
      writeD(this.curHp);
    }
    if (containsMask(PartySmallWindowUpdateType.MAX_HP))
    {
      writeD(this.maxHp);
    }
    if (containsMask(PartySmallWindowUpdateType.CURRENT_MP))
    {
      writeD(this.curMp);
    }
    if (containsMask(PartySmallWindowUpdateType.MAX_MP))
    {
      writeD(this.maxMp);
    }
    if (containsMask(PartySmallWindowUpdateType.LEVEL))
    {
      writeC(this.level);
    }
    if (containsMask(PartySmallWindowUpdateType.CLASS_ID))
    {
      writeH(this.class_id);
    }
    if (containsMask(PartySmallWindowUpdateType.PARTY_SUBSTITUTE))
    {
      writeC(0);
    }
    if (containsMask(PartySmallWindowUpdateType.VITALITY_POINTS))
    {
      writeD(0);
    }
  }
}
