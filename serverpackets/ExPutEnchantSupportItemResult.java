package l2.gameserver.network.l2.s2c;

public class ExPutEnchantSupportItemResult
  extends L2GameServerPacket
{
  public static final L2GameServerPacket FAIL = new ExPutEnchantSupportItemResult(0);
  public static final L2GameServerPacket SUCCESS = new ExPutEnchantSupportItemResult(1);

  
  private int oH;

  
  private ExPutEnchantSupportItemResult(int paramInt) { this.oH = paramInt; }



  
  protected void writeImpl() {
    writeEx(130);
    writeD(this.oH);
  }
}
