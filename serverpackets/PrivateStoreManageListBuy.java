package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.commons.lang.ArrayUtils;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.TradeItem;
import l2.gameserver.model.items.Warehouse;







public class PrivateStoreManageListBuy
  extends AbstractItemListPacket
{
  private final boolean aks;
  private int ZG;
  private long abb;
  private List<TradeItem> akt;
  private List<TradeItem> Go;
  
  public PrivateStoreManageListBuy(boolean paramBoolean, Player paramPlayer) {
    this.aks = paramBoolean;
    this.ZG = paramPlayer.getObjectId();
    this.abb = paramPlayer.getAdena();
    this.akt = paramPlayer.getBuyList();
    this.Go = new ArrayList();
    
    ItemInstance[] arrayOfItemInstance = paramPlayer.getInventory().getItems();
    ArrayUtils.eqSort(arrayOfItemInstance, Warehouse.ItemClassComparator.getInstance());
    
    for (ItemInstance itemInstance : arrayOfItemInstance) {
      if (itemInstance.canBeTraded(paramPlayer) && itemInstance.getItemId() != 57) {
        TradeItem tradeItem;
        this.Go.add(tradeItem = new TradeItem(itemInstance));
        tradeItem.setObjectId(0);
      } 
    } 
  }

  
  protected final void writeImpl() {
    writeC(189);
    writeC(this.aks ? 1 : 2);
    
    if (this.aks) {
      
      writeD(this.ZG);
      writeQ(this.abb);
      
      writeD(0);
      for (TradeItem tradeItem : this.Go) {
        
        writeItemInfo(tradeItem);
        writeQ(tradeItem.getStorePrice());
      } 

      
      writeD(0);
      for (TradeItem tradeItem : this.akt)
      {
        writeItemInfo(tradeItem);
        writeQ(tradeItem.getOwnersPrice());
        writeQ(tradeItem.getStorePrice());
        writeQ(tradeItem.getCount());
      }
    
    } else {
      
      writeD(this.Go.size());
      writeD(this.Go.size());
      for (TradeItem tradeItem : this.Go) {
        
        writeItemInfo(tradeItem);
        writeQ(tradeItem.getStorePrice());
      } 
    } 
  }
}
