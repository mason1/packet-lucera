package l2.gameserver.network.l2.s2c;

public class ExPutItemResultForVariationMake
  extends L2GameServerPacket {
  public static final ExPutItemResultForVariationMake FAIL_PACKET = new ExPutItemResultForVariationMake(0, false);
  
  private int _itemObjId;
  
  private int pF;
  private int oH;
  
  public ExPutItemResultForVariationMake(int paramInt, boolean paramBoolean) {
    this._itemObjId = paramInt;
    this.pF = 0;
    this.oH = paramBoolean ? 1 : 0;
  }


  
  protected void writeImpl() {
    writeEx(84);
    writeD(this._itemObjId);
    writeD(this.pF);
    writeD(this.oH);
  }
}
