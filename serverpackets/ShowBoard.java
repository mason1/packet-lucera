package l2.gameserver.network.l2.s2c;

import java.nio.charset.Charset;
import java.util.List;
import l2.gameserver.model.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShowBoard
  extends L2GameServerPacket
{
  private static final Logger _log = LoggerFactory.getLogger(ShowBoard.class);
  private static final Charset alo = Charset.forName("UTF-16LE");
  private String alp;
  private String alq;
  private List<String> alr;
  private String als = "";

  
  public static void separateAndSend(String paramString, Player paramPlayer) {
    paramString = paramPlayer.getNetConnection().encodeBypasses(paramString, true);
    
    byte[] arrayOfByte = paramString.getBytes(alo);
    if (arrayOfByte.length < 16360) {
      
      paramPlayer.sendPacket(new ShowBoard(paramString, "101", paramPlayer, false));
      paramPlayer.sendPacket(new ShowBoard(null, "102", paramPlayer, false));
      paramPlayer.sendPacket(new ShowBoard(null, "103", paramPlayer, false));
    }
    else if (arrayOfByte.length < 32720) {
      
      paramPlayer.sendPacket(new ShowBoard(new String(arrayOfByte, 0, 16360, alo), "101", paramPlayer, false));
      paramPlayer.sendPacket(new ShowBoard(new String(arrayOfByte, 16360, arrayOfByte.length - 16360, alo), "102", paramPlayer, false));
      paramPlayer.sendPacket(new ShowBoard(null, "103", paramPlayer, false));
    }
    else if (paramString.length() < 49080) {
      
      paramPlayer.sendPacket(new ShowBoard(new String(arrayOfByte, 0, 16360, alo), "101", paramPlayer, false));
      paramPlayer.sendPacket(new ShowBoard(new String(arrayOfByte, 16360, arrayOfByte.length - 16360, alo), "102", paramPlayer, false));
      paramPlayer.sendPacket(new ShowBoard(new String(arrayOfByte, 32720, arrayOfByte.length - 32720, alo), "103", paramPlayer, false));
    } 
  }





















  
  private ShowBoard(String paramString1, String paramString2, Player paramPlayer, boolean paramBoolean) {
    if (paramString1 != null && paramString1.length() > 16384) {
      
      _log.warn("Html '" + paramString1 + "' is too long! this will crash the client!");
      this.alp = "<html><body>Html was too long</body></html>";
      return;
    } 
    this.alq = paramString2;
    
    if (paramPlayer.getSessionVar("add_fav") != null)
    {
      this.als = "bypass _bbsaddfav_List";
    }
    
    if (paramString1 != null) {
      
      if (paramBoolean)
      {
        this.alp = paramPlayer.getNetConnection().encodeBypasses(paramString1, true);
      }
      else
      {
        this.alp = paramString1;
      }
    
    } else {
      
      this.alp = null;
    } 
  }


  
  public ShowBoard(String paramString1, String paramString2, Player paramPlayer) { this(paramString1, paramString2, paramPlayer, true); }


  
  public ShowBoard(List<String> paramList) {
    this.alq = "1002";
    this.alp = null;
    this.alr = paramList;
  }


  
  protected final void writeImpl() {
    writeC(123);
    writeC(1);
    writeS("bypass _bbshome");
    writeS("bypass _bbsgetfav");
    writeS("bypass _bbsloc");
    writeS("bypass _bbsclan");
    writeS("bypass _bbsmemo");
    writeS("bypass _maillist_0_1_0_");
    writeS("bypass _friendlist_0_");
    writeS(this.als);
    String str = this.alq + "\b";
    if (!this.alq.equals("1002")) {
      
      if (this.alp != null)
      {
        str = str + this.alp;
      }
    }
    else {
      
      for (String str1 : this.alr)
      {
        str = str + str1 + " \b";
      }
    } 
    writeS(str);
  }
}
