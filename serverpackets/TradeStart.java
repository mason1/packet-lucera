package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.ItemInstance;

public class TradeStart extends AbstractItemListPacket {
  private final boolean aeE;
  
  public TradeStart(boolean paramBoolean, Player paramPlayer1, Player paramPlayer2) {
    this.amn = new ArrayList();





    
    this.aeE = paramBoolean;
    this.amo = paramPlayer2.getObjectId();
    
    ItemInstance[] arrayOfItemInstance = paramPlayer1.getInventory().getItems();
    for (ItemInstance itemInstance : arrayOfItemInstance) {
      
      if (itemInstance.canBeTraded(paramPlayer1))
      {
        this.amn.add(new ItemInfo(itemInstance));
      }
    } 
    
    if (paramPlayer1.getFriendList().getList().containsKey(Integer.valueOf(paramPlayer2.getObjectId())))
    {
      this.mask |= 0x1;
    }
    
    if (paramPlayer1.getClanId() > 0 && paramPlayer1.getClanId() == paramPlayer2.getClanId())
    {
      this.mask |= 0x2;
    }


    
    if (paramPlayer1.getAllyId() > 0 && paramPlayer1.getAllyId() == paramPlayer2.getAllyId())
    {
      this.mask = 8;
    }
    
    if (paramPlayer2.isGM())
    {
      this.mask |= 0x10; } 
  }
  private List<ItemInfo> amn;
  private int amo;
  private int mask;
  
  protected final void writeImpl() {
    writeC(20);
    writeC(this.aeE ? 1 : 2);
    if (this.aeE) {
      
      writeD(this.amo);
      writeH(this.mask);
      writeD(this.amn.size());
    }
    else {
      
      writeD(this.amn.size());
      writeD(this.amn.size());
      for (ItemInfo itemInfo : this.amn)
      {
        writeItemInfo(itemInfo);
      }
    } 
  }
}
