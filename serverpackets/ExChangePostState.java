package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.mail.Mail;

public class ExChangePostState
  extends L2GameServerPacket
{
  private boolean adJ;
  private Mail[] adK;
  private int adL;
  
  public ExChangePostState(boolean paramBoolean, int paramInt, Mail... paramVarArgs) {
    this.adJ = paramBoolean;
    this.adK = paramVarArgs;
    this.adL = paramInt;
  }


  
  protected void writeImpl() {
    writeEx(180);
    writeD(this.adJ ? 1 : 0);
    writeD(this.adK.length);
    for (Mail mail : this.adK) {
      
      writeD(mail.getMessageId());
      writeD(this.adL);
    } 
  }
}
