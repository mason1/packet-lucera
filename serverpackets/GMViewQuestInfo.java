package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.quest.Quest;
import l2.gameserver.model.quest.QuestState;


public class GMViewQuestInfo
  extends L2GameServerPacket
{
  private final Player aiv;
  
  public GMViewQuestInfo(Player paramPlayer) { this.aiv = paramPlayer; }



  
  protected final void writeImpl() {
    writeC(153);
    writeS(this.aiv.getName());
    
    Quest[] arrayOfQuest = this.aiv.getAllActiveQuests();
    
    if (arrayOfQuest.length == 0) {
      
      writeH(0);
      writeH(0);
      
      return;
    } 
    writeH(arrayOfQuest.length);
    for (Quest quest : arrayOfQuest) {
      
      writeD(quest.getQuestIntId());
      QuestState questState = this.aiv.getQuestState(quest.getName());
      writeD((questState == null) ? 0 : questState.getInt("cond"));
    } 
    
    writeH(0);
  }
}
