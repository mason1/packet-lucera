package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import l2.commons.lang.ArrayUtils;
import l2.gameserver.instancemanager.MatchingRoomManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.matching.MatchingRoom;

public class ExPartyRoomMember
  extends L2GameServerPacket
{
  private int _type;
  private List<PartyRoomMemberInfo> EQ;
  
  public ExPartyRoomMember(MatchingRoom paramMatchingRoom, Player paramPlayer) {
    this.EQ = Collections.emptyList();


    
    this._type = paramMatchingRoom.getMemberType(paramPlayer);
    this.EQ = new ArrayList(paramMatchingRoom.getPlayers().size());
    for (Player player : paramMatchingRoom.getPlayers()) {
      this.EQ.add(new PartyRoomMemberInfo(player, paramMatchingRoom.getMemberType(player)));
    }
  }

  
  protected final void writeImpl() {
    writeEx(8);
    writeD(this._type);
    writeD(this.EQ.size());
    for (PartyRoomMemberInfo partyRoomMemberInfo : this.EQ) {
      
      writeD(partyRoomMemberInfo.objectId);
      writeS(partyRoomMemberInfo.name);
      writeD(partyRoomMemberInfo.classId);
      writeD(partyRoomMemberInfo.level);
      writeD(partyRoomMemberInfo.location);
      writeD(partyRoomMemberInfo.memberType);
      writeD(partyRoomMemberInfo.instanceReuses.length);
      for (int i : partyRoomMemberInfo.instanceReuses) {
        writeD(i);
      }
    } 
  }
  
  static class PartyRoomMemberInfo
  {
    public final int objectId;
    public final int classId;
    public final int level;
    
    public PartyRoomMemberInfo(Player param1Player, int param1Int) {
      this.objectId = param1Player.getObjectId();
      this.name = param1Player.getName();
      this.classId = param1Player.getClassId().ordinal();
      this.level = param1Player.getLevel();
      this.location = MatchingRoomManager.getInstance().getLocation(param1Player);
      this.memberType = param1Int;
      this.instanceReuses = ArrayUtils.toArray(param1Player.getInstanceReuses().keySet());
    }
    
    public final int location;
    public final int memberType;
    public final String name;
    public final int[] instanceReuses;
  }
}
