package l2.gameserver.network.l2.s2c;





public class ExAttributeEnchantResult
  extends L2GameServerPacket
{
  private int oH;
  
  public ExAttributeEnchantResult(int paramInt) { this.oH = paramInt; }



  
  protected final void writeImpl() {
    writeEx(97);
    writeD(this.oH);
  }
}
