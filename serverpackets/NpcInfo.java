package l2.gameserver.network.l2.s2c;

import l2.gameserver.Config;
import l2.gameserver.model.Creature;
import l2.gameserver.model.Player;
import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.model.pledge.Clan;







public class NpcInfo
  extends AbstractNpcPacket
{
  public NpcInfo(NpcInstance paramNpcInstance, Creature paramCreature) {
    this._npcId = (paramNpcInstance.getDisplayId() != 0) ? paramNpcInstance.getDisplayId() : (paramNpcInstance.getTemplate()).npcId;
    this._isAttackable = (paramCreature != null && paramNpcInstance.isAutoAttackable(paramCreature));
    this._rhand = paramNpcInstance.getRightHandItem();
    this._lhand = paramNpcInstance.getLeftHandItem();
    this._enchantEffect = 0;
    if (Config.SERVER_SIDE_NPC_NAME || (paramNpcInstance.getTemplate()).displayId != 0 || paramNpcInstance.getName() != (paramNpcInstance.getTemplate()).name)
    {
      this._name = paramNpcInstance.getName();
    }
    if (Config.SERVER_SIDE_NPC_TITLE || (paramNpcInstance.getTemplate()).displayId != 0 || paramNpcInstance.getTitle() != (paramNpcInstance.getTemplate()).title)
    {
      this._title = paramNpcInstance.getTitle();
    }
    if (Config.SERVER_SIDE_MONSTER_LEVEL_TITLE && paramNpcInstance instanceof l2.gameserver.model.instances.MonsterInstance)
    {
      this._title = "Lv " + paramNpcInstance.getLevel() + ((paramNpcInstance.getAggroRange() > 0) ? "*" : "") + " " + paramNpcInstance.getTitle();
    }
    
    this._showSpawnAnimation = paramNpcInstance.getSpawnAnimation();
    this._showName = paramNpcInstance.isShowName();
    this._state = paramNpcInstance.getNpcState();
    this._isPet = false;
    
    setValues(paramNpcInstance, NpcInfoType.VALUES);
  }

  
  public NpcInfo(Player paramPlayer) {
    if (paramPlayer.isInvisible()) {
      return;
    }

    
    this._npcId = paramPlayer.getPolyId();
    
    this._isAttackable = false;
    this._enchantEffect = 0;
    this._showName = true;
    this._name = paramPlayer.getName();
    this._title = paramPlayer.getTitle();
    this._showSpawnAnimation = 0;

    
    Clan clan = paramPlayer.getClan();
    Alliance alliance = (clan == null) ? null : clan.getAlliance();
    
    this.clan_id = (clan == null) ? 0 : clan.getClanId();
    this.clan_crest_id = (clan == null) ? 0 : clan.getCrestId();
    this.clan_large_crest_id = (clan == null) ? 0 : clan.getCrestLargeId();
    
    this.ally_id = (alliance == null) ? 0 : alliance.getAllyId();
    this.ally_crest_id = (alliance == null) ? 0 : alliance.getAllyCrestId();

    
    this._rhand = paramPlayer.getInventory().getPaperdollItemId(5);
    this._lhand = paramPlayer.getInventory().getPaperdollItemId(7);
    
    this._npcObjId = paramPlayer.getObjectId();
    this._loc = paramPlayer.getLoc();
    this._mAtkSpd = paramPlayer.getMAtkSpd();
    
    this.karma = paramPlayer.getKarma();
    this.pvp_flag = paramPlayer.getPvpFlag();
    this._pAtkSpd = paramPlayer.getPAtkSpd();
    this.running = paramPlayer.isRunning() ? 1 : 0;
    this.incombat = paramPlayer.isInCombat() ? 1 : 0;
    this.dead = paramPlayer.isAlikeDead() ? 1 : 0;
    this.isFlying = paramPlayer.isFlying();
    this._team = paramPlayer.getTeam();
    this._formId = paramPlayer.getFormId();
    this._isNameAbove = paramPlayer.isNameAbove();
    this._isPet = false;
    this.abnormalEffects = paramPlayer.getAbnormalEffects();
    
    this.moveAnimMod = paramPlayer.getRunSpeed() * 1.0D / (paramPlayer.getTemplate()).baseRunSpd;
    this.atkSpeed = paramPlayer.getAttackSpeedMultiplier();
    
    addComponentType(NpcInfoType.VALUES);
    
    this.can_writeImpl = true;
  }


  
  public NpcInfo update() { return (NpcInfo)update(true); }



  
  protected final void writeImpl() {
    if (!this.can_writeImpl) {
      return;
    }

    
    writeC(12);
    writeData();
  }
}
