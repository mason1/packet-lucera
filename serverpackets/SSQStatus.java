package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.entity.SevenSigns;
import l2.gameserver.model.entity.SevenSignsFestival.SevenSignsFestival;
import l2.gameserver.templates.StatsSet;













public class SSQStatus
  extends L2GameServerPacket
{
  private Player _player;
  private int _page;
  private int akS;
  
  public SSQStatus(Player paramPlayer, int paramInt) {
    this._player = paramPlayer;
    this._page = paramInt;
    this.akS = SevenSigns.getInstance().getCurrentPeriod();
  }
  protected final void writeImpl() {
    byte b;
    int n, m, k, j, i;
    long l12, l11, l10, l9, l8, l7, l6, l5, l4, l3, l2, l1;
    writeC(251);
    
    writeC(this._page);
    writeC(this.akS);
    
    switch (this._page) {

      
      case 1:
        writeD(SevenSigns.getInstance().getCurrentCycle());
        
        switch (this.akS) {
          
          case 0:
            writeD(1183);
            break;
          case 1:
            writeD(1176);
            break;
          case 2:
            writeD(1184);
            break;
          case 3:
            writeD(1177);
            break;
        } 
        
        switch (this.akS) {
          
          case 0:
          case 2:
            writeD(1287);
            break;
          case 1:
          case 3:
            writeD(1286);
            break;
        } 
        
        writeC(SevenSigns.getInstance().getPlayerCabal(this._player));
        writeC(SevenSigns.getInstance().getPlayerSeal(this._player));
        
        writeQ(SevenSigns.getInstance().getPlayerStoneContrib(this._player));
        writeQ(SevenSigns.getInstance().getPlayerAdenaCollect(this._player));
        
        l1 = SevenSigns.getInstance().getCurrentStoneScore(2);
        l2 = SevenSigns.getInstance().getCurrentFestivalScore(2);
        l3 = SevenSigns.getInstance().getCurrentScore(2);
        
        l4 = SevenSigns.getInstance().getCurrentStoneScore(1);
        l5 = SevenSigns.getInstance().getCurrentFestivalScore(1);
        l6 = SevenSigns.getInstance().getCurrentScore(1);
        
        l7 = l4 + l1;
        l7 = (l7 == 0L) ? 1L : l7;




        
        l8 = Math.round(l4 * 500.0D / l7);
        l9 = Math.round(l1 * 500.0D / l7);
        
        l10 = l6 + l3;
        l10 = (l10 == 0L) ? 1L : l10;
        
        l11 = Math.round(l3 * 110.0D / l10);
        l12 = Math.round(l6 * 110.0D / l10);

        
        writeQ(l8);
        writeQ(l5);
        writeQ(l6);
        
        writeC((int)l12);

        
        writeQ(l9);
        writeQ(l2);
        writeQ(l3);
        
        writeC((int)l11);
        break;
      
      case 2:
        writeH(1);
        writeC(5);
        
        for (i = 0; i < 5; i++) {
          
          writeC(i + true);
          writeD(SevenSignsFestival.FESTIVAL_LEVEL_SCORES[i]);
          
          long l13 = SevenSignsFestival.getInstance().getHighestScore(1, i);
          long l14 = SevenSignsFestival.getInstance().getHighestScore(2, i);

          
          writeQ(l13);
          
          StatsSet statsSet = SevenSignsFestival.getInstance().getHighestScoreData(1, i);
          
          if (l13 > 0L) {
            
            String[] arrayOfString = statsSet.getString("names").split(",");
            writeC(arrayOfString.length);
            for (String str : arrayOfString) {
              writeS(str);
            }
          } else {
            writeC(0);
          } 
          
          writeQ(l14);
          
          statsSet = SevenSignsFestival.getInstance().getHighestScoreData(2, i);
          
          if (l14 > 0L) {
            
            String[] arrayOfString = statsSet.getString("names").split(",");
            writeC(arrayOfString.length);
            for (String str : arrayOfString) {
              writeS(str);
            }
          } else {
            writeC(0);
          } 
        } 
        break;
      case 3:
        writeC(10);
        writeC(35);
        writeC(3);
        
        i = 1;
        j = 1;
        
        for (k = 1; k <= 3; k++) {
          
          i += SevenSigns.getInstance().getSealProportion(k, 2);
          j += SevenSigns.getInstance().getSealProportion(k, 1);
        } 

        
        i = Math.max(1, i);
        j = Math.max(1, j);
        
        for (k = 1; k <= 3; k++) {
          
          int i1 = SevenSigns.getInstance().getSealProportion(k, 2);
          int i2 = SevenSigns.getInstance().getSealProportion(k, 1);
          
          writeC(k);
          writeC(SevenSigns.getInstance().getSealOwner(k));
          writeC(i2 * 100 / j);
          writeC(i1 * 100 / i);
        } 
        break;

      
      case 4:
        k = SevenSigns.getInstance().getCabalHighestScore();
        writeC(k);
        writeC(3);
        
        m = SevenSigns.getInstance().getTotalMembers(2);
        n = SevenSigns.getInstance().getTotalMembers(1);
        
        for (b = 1; b < 4; b++) {
          
          writeC(b);
          
          int i1 = SevenSigns.getInstance().getSealProportion(b, 2);
          int i2 = SevenSigns.getInstance().getSealProportion(b, 1);
          int i3 = (m > 0) ? (i1 * 100 / m) : 0;
          int i4 = (n > 0) ? (i2 * 100 / n) : 0;
          int i5 = SevenSigns.getInstance().getSealOwner(b);
          
          if (Math.max(i3, i4) < 10) {
            
            writeC(0);
            if (i5 == 0) {
              writeD(1292);
            } else {
              
              writeD(1291);
            } 
          } else if (Math.max(i3, i4) < 35) {
            
            writeC(i5);
            if (i5 == 0) {
              writeD(1292);
            } else {
              
              writeD(1289);
            } 
          } else if (i3 == i4) {
            
            writeC(0);
            writeD(1293);
          
          }
          else {
            
            byte b1 = (i3 > i4) ? 2 : 1;
            writeC(b1);
            if (b1 == i5) {
              writeD(1289);
            } else {
              
              writeD(1290);
            } 
          } 
        } 
        break;
    } 
    this._player = null;
  }
}
