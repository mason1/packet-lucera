package l2.gameserver.network.l2.s2c;











public class ExNpcQuestHtmlMessage
  extends NpcHtmlMessage
{
  private int _questId;
  
  public ExNpcQuestHtmlMessage(int paramInt1, int paramInt2) {
    super(paramInt1);
    this._questId = paramInt2;
  }


  
  protected void writeImpl() {
    if (this._html != null) {
      
      writeEx(142);
      writeD(this._npcObjId);
      writeS(this._html);
      writeD(this._questId);
    } 
  }
}
