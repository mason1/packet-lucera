package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.items.ItemInstance;




public class ExPutItemResultForVariationCancel
  extends L2GameServerPacket
{
  private int Yl;
  private int _itemId;
  private int afJ;
  private int afK;
  private long _price;
  private boolean afL;
  
  public ExPutItemResultForVariationCancel(ItemInstance paramItemInstance, long paramLong, boolean paramBoolean) {
    this.Yl = paramItemInstance.getObjectId();
    this._itemId = paramItemInstance.getItemId();
    this.afJ = paramItemInstance.getVariationStat1();
    this.afK = paramItemInstance.getVariationStat2();
    this._price = paramLong;
    this.afL = paramBoolean;
  }


  
  protected void writeImpl() {
    writeEx(88);
    writeD(this.Yl);
    writeD(this._itemId);
    writeD(this.afJ);
    writeD(this.afK);
    writeQ(this._price);
    writeD(this.afL ? 1 : 0);
  }
}
