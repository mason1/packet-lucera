package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;










public class ExCubeGameExtendedChangePoints
  extends L2GameServerPacket
{
  private int _timeLeft;
  private int _bluePoints;
  private int _redPoints;
  private boolean _isRedTeam;
  private int Bq;
  private int adX;
  
  public ExCubeGameExtendedChangePoints(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean, Player paramPlayer, int paramInt4) {
    this._timeLeft = paramInt1;
    this._bluePoints = paramInt2;
    this._redPoints = paramInt3;
    this._isRedTeam = paramBoolean;
    this.Bq = paramPlayer.getObjectId();
    this.adX = paramInt4;
  }


  
  protected void writeImpl() {
    writeEx(152);
    writeD(0);
    
    writeD(this._timeLeft);
    writeD(this._bluePoints);
    writeD(this._redPoints);
    
    writeD(this._isRedTeam ? 1 : 0);
    writeD(this.Bq);
    writeD(this.adX);
  }
}
