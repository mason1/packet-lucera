package l2.gameserver.network.l2.s2c;




















public class TutorialShowHtml
  extends L2GameServerPacket
{
  public static final int NORMAL_WINDOW = 1;
  public static final int LARGE_WINDOW = 2;
  private String _html;
  private final int _type;
  
  public TutorialShowHtml(String paramString) {
    this._html = paramString;
    this._type = 1;
  }


  
  protected final void writeImpl() {
    writeC(166);
    writeD(this._type);
    writeS(this._html);
  }
}
