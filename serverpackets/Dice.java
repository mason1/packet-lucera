package l2.gameserver.network.l2.s2c;





public class Dice
  extends L2GameServerPacket
{
  private int acn;
  private int _itemId;
  private int _number;
  private int _x;
  private int _y;
  private int _z;
  
  public Dice(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    this.acn = paramInt1;
    this._itemId = paramInt2;
    this._number = paramInt3;
    this._x = paramInt4;
    this._y = paramInt5;
    this._z = paramInt6;
  }


  
  protected final void writeImpl() {
    writeC(218);
    writeD(this.acn);
    writeD(this._itemId);
    writeD(this._number);
    writeD(this._x);
    writeD(this._y);
    writeD(this._z);
  }
}
