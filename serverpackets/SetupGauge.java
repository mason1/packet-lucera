package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;


public class SetupGauge
  extends L2GameServerPacket
{
  public static final int BLUE = 0;
  public static final int RED = 1;
  public static final int CYAN = 2;
  private int Br;
  private int ale;
  private int _time;
  
  public SetupGauge(Creature paramCreature, int paramInt1, int paramInt2) {
    this.Br = paramCreature.getObjectId();
    this.ale = paramInt1;
    this._time = paramInt2;
  }


  
  protected final void writeImpl() {
    writeC(107);
    writeD(this.Br);
    writeD(this.ale);
    writeD(this._time);
    
    writeD(this._time);
  }
}
