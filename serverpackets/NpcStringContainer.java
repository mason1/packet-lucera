package l2.gameserver.network.l2.s2c;

import l2.gameserver.network.l2.components.NpcString;

public abstract class NpcStringContainer
  extends L2GameServerPacket
{
  private final NpcString ajg;
  private final String[] ajh;
  
  protected NpcStringContainer(NpcString paramNpcString, String... paramVarArgs) {
    this.ajh = new String[5];


    
    this.ajg = paramNpcString;
    System.arraycopy(paramVarArgs, 0, this.ajh, 0, paramVarArgs.length);
  }

  
  protected void writeElements() {
    writeD(this.ajg.getId());
    for (String str : this.ajh)
      writeS(str); 
  }
}
