package l2.gameserver.network.l2.s2c;

import gnu.trove.TIntHashSet;
import java.util.Map;
import l2.gameserver.instancemanager.RaidBossSpawnManager;







public class ExRaidBossSpawnInfo
  extends L2GameServerPacket
{
  private int[] afO;
  
  public ExRaidBossSpawnInfo() {
    RaidBossSpawnManager raidBossSpawnManager = RaidBossSpawnManager.getInstance();
    Map map = raidBossSpawnManager.getSpawnTable();
    TIntHashSet tIntHashSet = new TIntHashSet();
    for (Integer integer : map.keySet()) {
      
      RaidBossSpawnManager.Status status = raidBossSpawnManager.getRaidBossStatusId(integer.intValue());
      if (status == RaidBossSpawnManager.Status.ALIVE)
      {
        tIntHashSet.add(integer.intValue());
      }
    } 
    
    this.afO = tIntHashSet.toArray();
  }


  
  protected void writeImpl() {
    writeEx(441);
    writeDD(this.afO, true);
  }
}
