package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Summon;

public class ExPartyPetWindowAdd
  extends L2GameServerPacket {
  private final int TD;
  private final int npcId;
  private final int type;
  private final int curHp;
  
  public ExPartyPetWindowAdd(Summon paramSummon) {
    this.afy = paramSummon.getObjectId();
    this.TD = paramSummon.getPlayer().getObjectId();
    this.npcId = (paramSummon.getTemplate()).npcId + 1000000;
    this.type = paramSummon.getSummonType();
    this.name = paramSummon.getName();
    this.curHp = (int)paramSummon.getCurrentHp();
    this.maxHp = paramSummon.getMaxHp();
    this.curMp = (int)paramSummon.getCurrentMp();
    this.maxMp = paramSummon.getMaxMp();
  }
  private final int maxHp; private final int curMp; private final int maxMp; private final int afy;
  private final String name;
  
  protected final void writeImpl() {
    writeEx(24);
    writeD(this.afy);
    writeD(this.npcId);
    writeC(this.type);
    writeD(this.TD);
    writeS(this.name);
    writeD(this.curHp);
    writeD(this.maxHp);
    writeD(this.curMp);
    writeD(this.maxMp);
  }
}
