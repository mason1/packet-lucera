package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Player;
import l2.gameserver.model.quest.QuestState;



































public class QuestList
  extends L2GameServerPacket
{
  private List<int[]> akw;
  private static byte[] akx = new byte[128];

  
  public QuestList(Player paramPlayer) {
    QuestState[] arrayOfQuestState = paramPlayer.getAllQuestsStates();
    this.akw = new ArrayList(arrayOfQuestState.length);
    for (QuestState questState : arrayOfQuestState) {
      if (questState.getQuest().isVisible() && questState.isStarted()) {
        this.akw.add(new int[] { questState.getQuest().getQuestIntId(), questState.getInt("cond") });
      }
    } 
  }
  
  protected final void writeImpl() {
    writeC(134);
    writeH(this.akw.size());
    for (int[] arrayOfInt : this.akw) {
      
      writeD(arrayOfInt[0]);
      writeD(arrayOfInt[1]);
    } 
    writeB(akx);
  }
}
