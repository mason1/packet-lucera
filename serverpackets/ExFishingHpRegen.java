package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;





public class ExFishingHpRegen
  extends L2GameServerPacket
{
  private int _time;
  private int aem;
  private int aen;
  private int aeo;
  private int aep;
  private int aeq;
  private int aer;
  private int char_obj_id;
  
  public ExFishingHpRegen(Creature paramCreature, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7) {
    this.char_obj_id = paramCreature.getObjectId();
    this._time = paramInt1;
    this.aem = paramInt2;
    this.aen = paramInt3;
    this.aep = paramInt4;
    this.aeo = paramInt5;
    this.aeq = paramInt6;
    this.aer = paramInt7;
  }


  
  protected final void writeImpl() {
    writeEx(40);
    writeD(this.char_obj_id);
    writeD(this._time);
    writeD(this.aem);
    writeC(this.aen);
    writeC(this.aep);
    writeC(this.aeo);
    writeD(this.aeq);
    writeC(this.aer);
  }
}
