package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.ItemInstance;




public class ExReplyPostItemList
  extends AbstractItemListPacket
{
  private final boolean aeE;
  private List<ItemInfo> afX;
  
  public ExReplyPostItemList(boolean paramBoolean, Player paramPlayer) {
    this.afX = new ArrayList();


    
    this.aeE = paramBoolean;
    ItemInstance[] arrayOfItemInstance = paramPlayer.getInventory().getItems();
    for (ItemInstance itemInstance : arrayOfItemInstance) {
      
      if (itemInstance.canBeTraded(paramPlayer))
      {
        this.afX.add(new ItemInfo(itemInstance));
      }
    } 
  }


  
  protected void writeImpl() {
    writeEx(179);
    writeC(this.aeE ? 1 : 2);
    if (this.aeE) {
      
      writeD(this.afX.size());
    }
    else {
      
      writeD(this.afX.size());
      writeD(this.afX.size());
      for (ItemInfo itemInfo : this.afX)
      {
        writeItemInfo(itemInfo);
      }
    } 
  }
}
