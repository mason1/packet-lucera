package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import l2.gameserver.instancemanager.MatchingRoomManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.matching.MatchingRoom;

public class ExMpccRoomMember
  extends L2GameServerPacket
{
  private int _type;
  private List<MpccRoomMemberInfo> EQ;
  
  public ExMpccRoomMember(MatchingRoom paramMatchingRoom, Player paramPlayer) {
    this.EQ = Collections.emptyList();


    
    this._type = paramMatchingRoom.getMemberType(paramPlayer);
    this.EQ = new ArrayList(paramMatchingRoom.getPlayers().size());
    
    for (Player player : paramMatchingRoom.getPlayers()) {
      this.EQ.add(new MpccRoomMemberInfo(player, paramMatchingRoom.getMemberType(player)));
    }
  }

  
  public void writeImpl() {
    writeEx(160);
    writeD(this._type);
    writeD(this.EQ.size());
    for (MpccRoomMemberInfo mpccRoomMemberInfo : this.EQ) {
      
      writeD(mpccRoomMemberInfo.objectId);
      writeS(mpccRoomMemberInfo.name);
      writeD(mpccRoomMemberInfo.level);
      writeD(mpccRoomMemberInfo.classId);
      writeD(mpccRoomMemberInfo.location);
      writeD(mpccRoomMemberInfo.memberType);
    } 
  }

  
  static class MpccRoomMemberInfo
  {
    public final int objectId;
    public final int classId;
    public final int level;
    public final int location;
    public final int memberType;
    public final String name;
    
    public MpccRoomMemberInfo(Player param1Player, int param1Int) {
      this.objectId = param1Player.getObjectId();
      this.name = param1Player.getName();
      this.classId = param1Player.getClassId().ordinal();
      this.level = param1Player.getLevel();
      this.location = MatchingRoomManager.getInstance().getLocation(param1Player);
      this.memberType = param1Int;
    }
  }
}
