package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.items.ItemInstance;




public class ExChooseInventoryAttributeItem
  extends L2GameServerPacket
{
  private int _itemId;
  private boolean adM;
  private boolean adN;
  private boolean adO;
  private boolean adP;
  private boolean adQ;
  private boolean adR;
  private int adS;
  
  public ExChooseInventoryAttributeItem(ItemInstance paramItemInstance) { this._itemId = paramItemInstance.getItemId(); }



  
  protected final void writeImpl() {
    writeEx(98);
    writeD(this._itemId);
  }
}
