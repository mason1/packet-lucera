package l2.gameserver.network.l2.s2c;

import java.util.Collection;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;
import l2.gameserver.tables.SkillTable;


public class GMViewSkillInfo
  extends L2GameServerPacket
{
  private String adj;
  private Collection<Skill> aiw;
  private Player aix;
  
  public GMViewSkillInfo(Player paramPlayer) {
    this.adj = paramPlayer.getName();
    this.aiw = paramPlayer.getAllSkills();
    this.aix = paramPlayer;
  }


  
  protected final void writeImpl() {
    writeC(151);
    writeS(this.adj);
    writeD(this.aiw.size());
    for (Skill skill : this.aiw) {
      
      writeD(skill.isPassive() ? 1 : 0);
      writeH(skill.getDisplayLevel());
      writeH(0);
      writeD(skill.getId());
      writeD(0);
      writeC(this.aix.isUnActiveSkill(skill.getId()) ? 1 : 0);
      writeC((SkillTable.getInstance().getMaxLevel(skill.getId()) > 100) ? 1 : 0);
    } 
  }
}
