package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.utils.Location;

public class ExValidateLocationInAirShip
  extends L2GameServerPacket {
  private int aeK;
  private int YW;
  private Location _loc;
  
  public ExValidateLocationInAirShip(Player paramPlayer) {
    this.aeK = paramPlayer.getObjectId();
    this.YW = paramPlayer.getBoat().getObjectId();
    this._loc = paramPlayer.getInBoatPosition();
  }


  
  protected final void writeImpl() {
    writeEx(111);
    
    writeD(this.aeK);
    writeD(this.YW);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
    writeD(this._loc.h);
  }
}
