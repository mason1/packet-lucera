package l2.gameserver.network.l2.s2c;

import l2.gameserver.instancemanager.CursedWeaponsManager;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;
import l2.gameserver.utils.Location;



public class ExCursedWeaponList
  extends L2GameServerPacket
{
  private int[] adY = CursedWeaponsManager.getInstance().getCursedWeaponsIds();



  
  protected final void writeImpl() {
    writeEx(72);
    writeD(this.adY.length);
    for (int i : this.adY) {
      
      Player player = ((GameClient)getClient()).getActiveChar();
      
      writeD(i);
      writeD(1);
      Location location = player.getLoc();
      writeD(location.x);
      writeD(location.y);
      writeD(location.z);
      writeQ(5000L);
      writeQ(0L);
    } 
  }
}
