package l2.gameserver.network.l2.s2c;

import l2.gameserver.utils.Location;




public class Earthquake
  extends L2GameServerPacket
{
  private Location _loc;
  private int acC;
  private int aaw;
  
  public Earthquake(Location paramLocation, int paramInt1, int paramInt2) {
    this._loc = paramLocation;
    this.acC = paramInt1;
    this.aaw = paramInt2;
  }


  
  protected final void writeImpl() {
    writeC(211);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
    writeD(this.acC);
    writeD(this.aaw);
    writeD(0);
  }
}
