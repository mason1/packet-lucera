package l2.gameserver.network.l2.s2c;

import l2.gameserver.Config;
import l2.gameserver.model.GameObject;
import l2.gameserver.utils.Location;








public class TeleportToLocation
  extends L2GameServerPacket
{
  private int _targetId;
  private Location _loc;
  
  public TeleportToLocation(GameObject paramGameObject, Location paramLocation) {
    this._targetId = paramGameObject.getObjectId();
    this._loc = paramLocation;
  }

  
  public TeleportToLocation(GameObject paramGameObject, int paramInt1, int paramInt2, int paramInt3) {
    this._targetId = paramGameObject.getObjectId();
    this._loc = new Location(paramInt1, paramInt2, paramInt3, paramGameObject.getHeading());
  }


  
  protected final void writeImpl() {
    writeC(34);
    writeD(this._targetId);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z + Config.CLIENT_Z_SHIFT);
    writeD(0);
    writeD(this._loc.h);
  }
}
