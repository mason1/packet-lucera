package l2.gameserver.network.l2.s2c;

public class ExAskModifyPartyLooting
  extends L2GameServerPacket
{
  private String _requestor;
  private int _mode;
  
  public ExAskModifyPartyLooting(String paramString, int paramInt) {
    this._requestor = paramString;
    this._mode = paramInt;
  }


  
  protected void writeImpl() {
    writeEx(192);
    writeS(this._requestor);
    writeD(this._mode);
  }
}
