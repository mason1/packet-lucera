package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Party;
import l2.gameserver.model.Player;
import l2.gameserver.model.Summon;

public class PartySmallWindowAll
  extends L2GameServerPacket {
  private int ajn;
  private int aek;
  private List<PartySmallWindowMemberInfo> members;
  
  public PartySmallWindowAll(Party paramParty, Player paramPlayer) {
    this.members = new ArrayList();


    
    this.ajn = paramParty.getPartyLeader().getObjectId();
    this.aek = paramParty.getLootDistribution();
    
    for (Player player : paramParty.getPartyMembers()) {
      if (player != paramPlayer) {
        this.members.add(new PartySmallWindowMemberInfo(player));
      }
    } 
  }
  
  protected final void writeImpl() {
    writeC(78);
    writeD(this.ajn);
    writeC(this.aek);
    writeC(this.members.size());
    for (PartySmallWindowMemberInfo partySmallWindowMemberInfo : this.members) {
      
      writeD(partySmallWindowMemberInfo._id);
      writeS(partySmallWindowMemberInfo._name);
      writeD(partySmallWindowMemberInfo.curCp);
      writeD(partySmallWindowMemberInfo.maxCp);
      writeD(partySmallWindowMemberInfo.curHp);
      writeD(partySmallWindowMemberInfo.maxHp);
      writeD(partySmallWindowMemberInfo.curMp);
      writeD(partySmallWindowMemberInfo.maxMp);
      writeD(0);
      writeC(partySmallWindowMemberInfo.level);
      writeH(partySmallWindowMemberInfo.class_id);
      writeC(0);
      writeH(partySmallWindowMemberInfo.race_id);
      
      writeD((partySmallWindowMemberInfo.pet_id != 0) ? 1 : 0);
      if (partySmallWindowMemberInfo.pet_id != 0) {
        
        writeD(partySmallWindowMemberInfo.pet_id);
        writeD(partySmallWindowMemberInfo.pet_NpcId);
        writeC(partySmallWindowMemberInfo.pet_type);
        writeS(partySmallWindowMemberInfo.pet_Name);
        writeD(partySmallWindowMemberInfo.pet_curHp);
        writeD(partySmallWindowMemberInfo.pet_maxHp);
        writeD(partySmallWindowMemberInfo.pet_curMp);
        writeD(partySmallWindowMemberInfo.pet_maxMp);
        writeC(partySmallWindowMemberInfo.pet_level);
      } 
    } 
  }
  
  public static class PartySmallWindowMemberInfo {
    public String _name;
    public String pet_Name;
    public int _id;
    public int curCp;
    public int maxCp;
    
    public PartySmallWindowMemberInfo(Player param1Player) {
      this._name = param1Player.getName();
      this._id = param1Player.getObjectId();
      this.curCp = (int)param1Player.getCurrentCp();
      this.maxCp = param1Player.getMaxCp();
      this.curHp = (int)param1Player.getCurrentHp();
      this.maxHp = param1Player.getMaxHp();
      this.curMp = (int)param1Player.getCurrentMp();
      this.maxMp = param1Player.getMaxMp();
      this.level = param1Player.getLevel();
      this.class_id = param1Player.getClassId().getId();
      this.race_id = param1Player.getRace().ordinal();
      
      Summon summon = param1Player.getPet();
      if (summon != null) {
        
        this.pet_id = summon.getObjectId();
        this.pet_type = summon.getSummonType();
        this.pet_NpcId = summon.getNpcId() + 1000000;
        this.pet_Name = summon.getName();
        this.pet_curHp = (int)summon.getCurrentHp();
        this.pet_maxHp = summon.getMaxHp();
        this.pet_curMp = (int)summon.getCurrentMp();
        this.pet_maxMp = summon.getMaxMp();
        this.pet_level = summon.getLevel();
      } else {
        
        this.pet_id = 0;
      } 
    }
    
    public int curHp;
    public int maxHp;
    public int curMp;
    public int maxMp;
    public int level;
    public int class_id;
    public int race_id;
    public int pet_id;
    public int pet_type;
    public int pet_NpcId;
    public int pet_curHp;
    public int pet_maxHp;
    public int pet_curMp;
    public int pet_maxMp;
    public int pet_level;
  }
}
