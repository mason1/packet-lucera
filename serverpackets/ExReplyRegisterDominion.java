package l2.gameserver.network.l2.s2c;

public class ExReplyRegisterDominion
  extends L2GameServerPacket
{
  private int YL;
  private int afY;
  private int _playerCount;
  private boolean afZ;
  private boolean aga;
  private boolean agb;
  
  protected void writeImpl() {
    writeEx(145);
    writeD(this.YL);
    writeD(this.agb);
    writeD(this.aga);
    writeD(this.afZ);
    writeD(this.afY);
    writeD(this._playerCount);
  }
}
