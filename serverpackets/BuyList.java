package l2.gameserver.network.l2.s2c;

import java.util.Collections;
import java.util.List;
import l2.gameserver.data.xml.holder.BuyListHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.TradeItem;





@Deprecated
public class BuyList
  extends L2GameServerPacket
{
  private final int vU;
  private final List<TradeItem> Go;
  private final long abb;
  private final double abc;
  
  public BuyList(BuyListHolder.NpcTradeList paramNpcTradeList, Player paramPlayer, double paramDouble) {
    this.abb = paramPlayer.getAdena();
    this.abc = paramDouble;
    
    if (paramNpcTradeList != null) {
      
      this.vU = paramNpcTradeList.getListId();
      this.Go = paramNpcTradeList.getItems();
      paramPlayer.setBuyListId(this.vU);
    }
    else {
      
      this.vU = 0;
      this.Go = Collections.emptyList();
      paramPlayer.setBuyListId(0);
    } 
  }



  
  protected void writeImpl() {
    writeC(17);
    
    writeD((int)this.abb);
    writeD(this.vU);
    writeH(this.Go.size());
    for (TradeItem tradeItem : this.Go) {
      
      writeH(tradeItem.getType1());
      writeD(tradeItem.getObjectId());
      writeD(tradeItem.getItemId());
      writeD((int)tradeItem.getCurrentValue());
      writeH(tradeItem.getType2());
      writeH(tradeItem.getCustomType1());
      writeD(tradeItem.getBodyPart());
      writeH(tradeItem.getEnchantLevel());
      writeD(0);
      writeD((int)(tradeItem.getOwnersPrice() * (1.0D + this.abc)));
    } 
  }
}
