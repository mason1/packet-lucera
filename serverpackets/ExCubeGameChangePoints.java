package l2.gameserver.network.l2.s2c;







public class ExCubeGameChangePoints
  extends L2GameServerPacket
{
  int _timeLeft;
  int _bluePoints;
  int _redPoints;
  
  public ExCubeGameChangePoints(int paramInt1, int paramInt2, int paramInt3) {
    this._timeLeft = paramInt1;
    this._bluePoints = paramInt2;
    this._redPoints = paramInt3;
  }


  
  protected void writeImpl() {
    writeEx(152);
    writeD(2);
    
    writeD(this._timeLeft);
    writeD(this._bluePoints);
    writeD(this._redPoints);
  }
}
