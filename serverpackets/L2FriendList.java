package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import l2.gameserver.model.Player;
import l2.gameserver.model.actor.instances.player.Friend;

public class L2FriendList
  extends L2GameServerPacket
{
  private List<FriendInfo> abd = Collections.emptyList();

  
  public L2FriendList(Player paramPlayer) {
    Map map = paramPlayer.getFriendList().getList();
    this.abd = new ArrayList(map.size());
    for (Map.Entry entry : map.entrySet()) {
      
      FriendInfo friendInfo = new FriendInfo(null);
      friendInfo.Bq = ((Integer)entry.getKey()).intValue();
      friendInfo._name = ((Friend)entry.getValue()).getName();
      friendInfo.pa = ((Friend)entry.getValue()).isOnline();
      friendInfo.classId = ((Friend)entry.getValue()).getClassId();
      friendInfo.level = ((Friend)entry.getValue()).getLevel();
      this.abd.add(friendInfo);
    } 
  }


  
  protected final void writeImpl() {
    writeC(117);
    writeD(this.abd.size());
    for (FriendInfo friendInfo : this.abd) {
      
      writeD(friendInfo.Bq);
      writeS(friendInfo._name);
      writeD(friendInfo.pa ? 1 : 0);
      writeD(friendInfo.pa ? friendInfo.Bq : 0);
      writeD(friendInfo.level);
      writeD(friendInfo.classId);
      writeH(0);
    } 
  }
  
  private static class FriendInfo {
    private int Bq;
    private String _name;
    private boolean pa;
    private int level;
    private int classId;
    
    private FriendInfo() {}
  }
}
