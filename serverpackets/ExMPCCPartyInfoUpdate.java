package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Party;
import l2.gameserver.model.Player;







public class ExMPCCPartyInfoUpdate
  extends L2GameServerPacket
{
  private Party _party;
  Player _leader;
  private int _mode;
  private int _count;
  
  public ExMPCCPartyInfoUpdate(Party paramParty, int paramInt) {
    this._party = paramParty;
    this._mode = paramInt;
    this._count = this._party.getMemberCount();
    this._leader = this._party.getPartyLeader();
  }


  
  protected void writeImpl() {
    writeEx(91);
    writeS(this._leader.getName());
    writeD(this._leader.getObjectId());
    writeD(this._count);
    writeD(this._mode);
  }
}
