package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.model.entity.residence.ClanHall;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.tables.ClanTable;




public class ExShowAgitInfo
  extends L2GameServerPacket
{
  private List<AgitInfo> ags = Collections.emptyList();

  
  public ExShowAgitInfo() {
    List list = ResidenceHolder.getInstance().getResidenceList(ClanHall.class);
    this.ags = new ArrayList(list.size());
    
    for (ClanHall clanHall : list) {
      byte b;
      int i = clanHall.getId();
      
      if (clanHall.getSiegeEvent().getClass() == l2.gameserver.model.entity.events.impl.ClanHallAuctionEvent.class) {
        b = 0;
      } else if (clanHall.getSiegeEvent().getClass() == l2.gameserver.model.entity.events.impl.ClanHallMiniGameEvent.class) {
        b = 2;
      } else {
        b = 1;
      } 
      Clan clan = ClanTable.getInstance().getClan(clanHall.getOwnerId());
      String str1 = (clanHall.getOwnerId() == 0 || clan == null) ? "" : clan.getName();
      String str2 = (clanHall.getOwnerId() == 0 || clan == null) ? "" : clan.getLeaderName();
      this.ags.add(new AgitInfo(str1, str2, i, b));
    } 
  }


  
  protected final void writeImpl() {
    writeEx(22);
    writeD(this.ags.size());
    for (AgitInfo agitInfo : this.ags) {
      
      writeD(agitInfo.ch_id);
      writeS(agitInfo.clan_name);
      writeS(agitInfo.leader_name);
      writeD(agitInfo.getType);
    } 
  }
  
  static class AgitInfo {
    public String clan_name;
    public String leader_name;
    public int ch_id;
    public int getType;
    
    public AgitInfo(String param1String1, String param1String2, int param1Int1, int param1Int2) {
      this.clan_name = param1String1;
      this.leader_name = param1String2;
      this.ch_id = param1Int1;
      this.getType = param1Int2;
    }
  }
}
