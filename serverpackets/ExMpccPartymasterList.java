package l2.gameserver.network.l2.s2c;

import java.util.Collections;
import java.util.Set;

public class ExMpccPartymasterList
  extends L2GameServerPacket
{
  private Set<String> _members;
  
  public ExMpccPartymasterList(Set<String> paramSet) {
    this._members = Collections.emptySet();


    
    this._members = paramSet;
  }


  
  protected void writeImpl() {
    writeEx(163);
    writeD(this._members.size());
    for (String str : this._members)
      writeS(str); 
  }
}
