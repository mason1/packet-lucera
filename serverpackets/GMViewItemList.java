package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.ItemInstance;

public class GMViewItemList
  extends AbstractItemListPacket
{
  private final boolean aeE;
  private int _size;
  private ItemInstance[] aeF;
  private int aeG;
  private String _name;
  
  public GMViewItemList(boolean paramBoolean, Player paramPlayer, ItemInstance[] paramArrayOfItemInstance, int paramInt) {
    this.aeE = paramBoolean;
    this._size = paramInt;
    this.aeF = paramArrayOfItemInstance;
    this._name = paramPlayer.getName();
    this.aeG = paramPlayer.getInventoryLimit();
  }


  
  protected final void writeImpl() {
    writeC(154);
    writeC(this.aeE ? 1 : 2);
    if (this.aeE) {
      
      writeS(this._name);
      writeD(this.aeG);
    }
    else {
      
      writeD(this.aeF.length);
    } 
    
    writeD(this._size);
    for (ItemInstance itemInstance : this.aeF) {
      
      if (!itemInstance.getTemplate().isQuest())
      {
        writeItemInfo(new ItemInfo(itemInstance));
      }
    } 
  }
}
