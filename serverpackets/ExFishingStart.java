package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;
import l2.gameserver.utils.Location;




public class ExFishingStart
  extends L2GameServerPacket
{
  private int Xg;
  private Location _loc;
  private int aes;
  private boolean aet;
  
  public ExFishingStart(Creature paramCreature, int paramInt, Location paramLocation, boolean paramBoolean) {
    this.Xg = paramCreature.getObjectId();
    this.aes = paramInt;
    this._loc = paramLocation;
    this.aet = paramBoolean;
  }


  
  protected final void writeImpl() {
    writeEx(30);
    writeD(this.Xg);
    writeD(this.aes);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
    writeC(this.aet ? 1 : 0);
    writeC(1);
  }
}
