package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.entity.boat.Boat;
import l2.gameserver.utils.Location;

public class ExMoveToLocationAirShip
  extends L2GameServerPacket {
  private int Bq;
  private Location aeR;
  private Location _destination;
  
  public ExMoveToLocationAirShip(Boat paramBoat) {
    this.Bq = paramBoat.getObjectId();
    this.aeR = paramBoat.getLoc();
    this._destination = paramBoat.getDestination();
  }


  
  protected final void writeImpl() {
    writeEx(101);
    writeD(this.Bq);
    
    writeD(this._destination.x);
    writeD(this._destination.y);
    writeD(this._destination.z);
    writeD(this.aeR.x);
    writeD(this.aeR.y);
    writeD(this.aeR.z);
  }
}
