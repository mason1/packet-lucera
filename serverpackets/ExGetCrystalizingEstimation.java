package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.items.ItemInstance;




public class ExGetCrystalizingEstimation
  extends L2GameServerPacket
{
  private final List<ItemInfo> aG;
  
  private static class ItemInfo
  {
    int itemId;
    long count;
    float chance;
    
    private ItemInfo() {}
  }
  
  public ExGetCrystalizingEstimation(ItemInstance paramItemInstance) {
    int i = paramItemInstance.getTemplate().getCrystalCount();
    int j = (paramItemInstance.getTemplate().getCrystalType()).cry;
    
    this.aG = new ArrayList();
    
    ItemInfo itemInfo = new ItemInfo(null);
    itemInfo.itemId = j;
    itemInfo.count = i;
    itemInfo.chance = 100.0F;
    
    this.aG.add(itemInfo);
  }


  
  protected void writeImpl() {
    writeEx(225);
    writeD(this.aG.size());
    for (ItemInfo itemInfo : this.aG) {
      
      writeD(itemInfo.itemId);
      writeQ(itemInfo.count);
      writeF(itemInfo.chance);
    } 
  }
}
