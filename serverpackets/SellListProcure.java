package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.templates.manor.CropProcure;

public class SellListProcure extends L2GameServerPacket {
  private long abe;
  
  public SellListProcure(Player paramPlayer, int paramInt) {
    this.akT = new HashMap();
    this.ZH = new ArrayList();



    
    this.abe = paramPlayer.getAdena();
    this.akU = paramInt;
    this.ZH = ((Castle)ResidenceHolder.getInstance().getResidence(Castle.class, this.akU)).getCropProcure(0);
    for (CropProcure cropProcure : this.ZH) {
      
      ItemInstance itemInstance = paramPlayer.getInventory().getItemByItemId(cropProcure.getId());
      if (itemInstance != null && cropProcure.getAmount() > 0L)
        this.akT.put(itemInstance, Long.valueOf(cropProcure.getAmount())); 
    } 
  }
  private Map<ItemInstance, Long> akT; private List<CropProcure> ZH;
  private int akU;
  
  protected final void writeImpl() {
    writeC(239);
    writeQ(this.abe);
    writeD(0);
    writeH(this.akT.size());
    
    for (ItemInstance itemInstance : this.akT.keySet()) {
      
      writeH(0);
      writeD(itemInstance.getObjectId());
      writeD(itemInstance.getItemId());
      writeQ(((Long)this.akT.get(itemInstance)).longValue());
      writeH(itemInstance.getTemplate().getType2ForPackets());
      writeH(0);
      writeQ(0L);
    } 
  }
}
