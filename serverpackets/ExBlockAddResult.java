package l2.gameserver.network.l2.s2c;





public class ExBlockAddResult
  extends L2GameServerPacket
{
  private boolean adA;
  private String _name;
  
  public ExBlockAddResult(boolean paramBoolean, String paramString) {
    this.adA = paramBoolean;
    this._name = paramString;
  }


  
  protected void writeImpl() {
    writeEx(237);
    writeD(this.adA);
    writeS(this._name);
  }
}
