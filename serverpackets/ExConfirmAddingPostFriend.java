package l2.gameserver.network.l2.s2c;




public class ExConfirmAddingPostFriend
  extends L2GameServerPacket
{
  public static int NAME_IS_NOT_EXISTS = 0;
  public static int SUCCESS = 1;
  public static int PREVIOS_NAME_IS_BEEN_REGISTERED = -1;
  public static int NAME_IS_NOT_EXISTS2 = -2;
  public static int LIST_IS_FULL = -3;
  public static int ALREADY_ADDED = -4;
  public static int NAME_IS_NOT_REGISTERED = -4;
  
  private String _name;
  
  private int oH;
  
  public ExConfirmAddingPostFriend(String paramString, int paramInt) {
    this._name = paramString;
    this.oH = paramInt;
  }


  
  public void writeImpl() {
    writeEx(210);
    writeS(this._name);
    writeD(this.oH);
  }
}
