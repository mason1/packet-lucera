package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.entity.boat.Boat;
import l2.gameserver.utils.Location;

public class VehicleCheckLocation
  extends L2GameServerPacket
{
  private int YW;
  private Location _loc;
  
  public VehicleCheckLocation(Boat paramBoat) {
    this.YW = paramBoat.getObjectId();
    this._loc = paramBoat.getLoc();
  }


  
  protected final void writeImpl() {
    writeC(109);
    writeD(this.YW);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
    writeD(this._loc.h);
  }
}
