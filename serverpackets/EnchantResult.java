package l2.gameserver.network.l2.s2c;

public class EnchantResult
  extends L2GameServerPacket
{
  private final int acD;
  private final int acE;
  private final long Be;
  private final int acF;
  public static final EnchantResult CANCEL = new EnchantResult(2, 0, 0L);
  public static final EnchantResult BLESSED_FAILED = new EnchantResult(3, 0, 0L);
  public static final EnchantResult FAILED_NO_CRYSTALS = new EnchantResult(4, 0, 0L);
  public static final EnchantResult ANCIENT_FAILED = new EnchantResult(5, 0, 0L);

  
  public EnchantResult(int paramInt1, int paramInt2, long paramLong) {
    this.acD = paramInt1;
    this.acE = paramInt2;
    this.Be = paramLong;
    this.acF = 0;
  }

  
  public EnchantResult(int paramInt) {
    this.acD = 0;
    this.acE = 0;
    this.Be = 0L;
    this.acF = paramInt;
  }


  
  protected final void writeImpl() {
    writeC(135);
    writeD(this.acD);
    writeD(this.acE);
    writeQ(this.Be);
    
    writeD(0);
    writeD(0);
    writeD(0);
    
    writeD(this.acF);
  }
}
