package l2.gameserver.network.l2.s2c;


public class ExShowQuestMark
  extends L2GameServerPacket
{
  private int _questId;
  
  public ExShowQuestMark(int paramInt) { this._questId = paramInt; }



  
  protected void writeImpl() {
    writeEx(33);
    writeD(this._questId);
  }
}
