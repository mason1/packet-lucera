package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.GameObject;
import l2.gameserver.model.GameObjectsStorage;
import l2.gameserver.model.Player;
import l2.gameserver.network.l2.GameClient;









public class DeleteObject
  extends L2GameServerPacket
{
  private int Bq;
  
  public DeleteObject(GameObject paramGameObject) { this.Bq = paramGameObject.getObjectId(); }



  
  protected final void writeImpl() {
    Player player = ((GameClient)getClient()).getActiveChar();
    if (player == null || player.getObjectId() == this.Bq) {
      return;
    }
    writeC(8);
    writeD(this.Bq);
    writeD(1);
  }



  
  public String getType() { return super.getType() + " " + GameObjectsStorage.findObject(this.Bq) + " (" + this.Bq + ")"; }
}
