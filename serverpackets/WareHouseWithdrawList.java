package l2.gameserver.network.l2.s2c;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import l2.commons.lang.ArrayUtils;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.Warehouse;
import l2.gameserver.templates.item.ItemTemplate;




public class WareHouseWithdrawList
  extends AbstractItemListPacket
{
  private final long abb;
  private final List<ItemInfo> aji;
  private final int _type;
  private final boolean aks;
  private final int amI;
  
  public WareHouseWithdrawList(boolean paramBoolean, Player paramPlayer, Warehouse.WarehouseType paramWarehouseType, ItemTemplate.ItemClass paramItemClass) {
    this.aks = paramBoolean;
    this.abb = paramPlayer.getAdena();
    this._type = paramWarehouseType.ordinal();
    this.amI = paramPlayer.getInventory().getSize();

    
    switch (paramWarehouseType) {
      
      case PRIVATE:
        arrayOfItemInstance = paramPlayer.getWarehouse().getItems(paramItemClass);
        break;
      case FREIGHT:
        arrayOfItemInstance = paramPlayer.getFreight().getItems(paramItemClass);
        break;
      case CLAN:
      case CASTLE:
        arrayOfItemInstance = paramPlayer.getClan().getWarehouse().getItems(paramItemClass);
        break;
      default:
        arrayOfItemInstance = new ItemInstance[0];
        break;
    } 
    
    ArrayUtils.eqSort(arrayOfItemInstance, Warehouse.ItemClassComparator.getInstance());
    this.aji = (List)Arrays.stream(arrayOfItemInstance).map(ItemInfo::new).collect(Collectors.toList());
  }


  
  protected final void writeImpl() {
    writeC(66);
    writeC(this.aks ? 1 : 2);
    if (this.aks) {
      
      writeH(this._type);
      writeQ(this.abb);
      writeD(this.amI);
      writeD(this.aji.size());
    }
    else {
      
      writeH(0);

      
      writeD(this.aji.size());
      writeD(this.aji.size());
      for (ItemInfo itemInfo : this.aji) {
        
        writeItemInfo(itemInfo);
        writeD(itemInfo.getObjectId());
        writeD(0);
        writeD(0);
      } 
    } 
  }
}
