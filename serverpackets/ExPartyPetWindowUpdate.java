package l2.gameserver.network.l2.s2c;

public class ExPartyPetWindowUpdate extends L2GameServerPacket {
  private int afB;
  private int afC;
  
  public ExPartyPetWindowUpdate(Summon paramSummon) {
    this.acr = 0;



    
    this.acr = paramSummon.getObjectId();
    this.afB = paramSummon.getPlayer().getObjectId();
    this.afC = (paramSummon.getTemplate()).npcId + 1000000;
    this._type = paramSummon.getSummonType();
    this._name = paramSummon.getName();
    this.curHp = (int)paramSummon.getCurrentHp();
    this.maxHp = paramSummon.getMaxHp();
    this.curMp = (int)paramSummon.getCurrentMp();
    this.maxMp = paramSummon.getMaxMp();
  }
  private int _type; private int curHp; private int maxHp; private int curMp; private int maxMp; private int acr;
  private String _name;
  
  protected final void writeImpl() {
    writeEx(25);
    writeD(this.acr);
    writeD(this.afC);
    writeC(this._type);
    writeD(this.afB);
    writeS(this._name);
    writeD(this.curHp);
    writeD(this.maxHp);
    writeD(this.curMp);
    writeD(this.maxMp);
  }
}
