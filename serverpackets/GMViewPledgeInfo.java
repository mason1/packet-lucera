package l2.gameserver.network.l2.s2c;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.model.pledge.UnitMember;

public class GMViewPledgeInfo extends L2GameServerPacket {
  private String _playerName;
  private String clan_name;
  private String leader_name;
  private String ally_name;
  private int clan_id;
  private int clan_crest_id;
  private int aio;
  private int rank;
  private int aip;
  
  public GMViewPledgeInfo(String paramString, Clan paramClan, SubUnit paramSubUnit) {
    this.EQ = Collections.emptyList();


    
    this.EQ = new ArrayList(paramSubUnit.getUnitMembers().size());
    
    this.GA = paramSubUnit.getType();
    this._playerName = paramString;
    this.clan_id = paramClan.getClanId();
    this.clan_name = paramClan.getName();
    this.leader_name = paramClan.getLeaderName();
    this.clan_crest_id = paramClan.getCrestId();
    this.aio = paramClan.getLevel();
    this.aiq = paramClan.getCastle();
    this.air = paramClan.getHasHideout();
    
    this.rank = paramClan.getRank();
    this.aip = paramClan.getReputationScore();
    
    Alliance alliance = paramClan.getAlliance();
    this.ally_id = (alliance == null) ? 0 : alliance.getAllyId();
    this.ally_name = (alliance == null) ? "" : alliance.getAllyName();
    this.ally_crest_id = (alliance == null) ? 0 : alliance.getAllyCrestId();
    this.ait = paramClan.isAtWar();
    
    for (UnitMember unitMember : paramSubUnit.getUnitMembers())
      this.EQ.add(new PledgeMemberInfo(unitMember.getName(), unitMember.getLevel(), unitMember.getClassId(), unitMember.isOnline() ? unitMember.getObjectId() : 0, unitMember.getSex(), 1, (unitMember.getSponsor() != 0) ? 1 : 0)); 
  }
  private int ally_id; private int ally_crest_id; private int aiq;
  private int air;
  
  protected final void writeImpl() {
    writeC(150);
    writeD((this.GA == 0));
    writeS(this._playerName);
    writeD(this.clan_id);
    writeD(this.GA);
    writeS(this.clan_name);
    writeS(this.leader_name);
    writeD(this.clan_crest_id);
    writeD(this.aio);
    writeD(this.aiq);
    writeD(this.air);
    writeD(0);
    writeD(this.rank);
    writeD(this.aip);
    writeD(0);
    writeD(0);
    writeD(0);
    writeD(this.ally_id);
    writeS(this.ally_name);
    writeD(this.ally_crest_id);
    writeD(this.ait);
    writeD(0);
    writeD(this.EQ.size());
    for (PledgeMemberInfo pledgeMemberInfo : this.EQ) {
      
      writeS(pledgeMemberInfo._name);
      writeD(pledgeMemberInfo.level);
      writeD(pledgeMemberInfo.class_id);
      writeD(pledgeMemberInfo.sex);
      writeD(pledgeMemberInfo.race);
      writeD(pledgeMemberInfo.online);
      writeD(pledgeMemberInfo.sponsor);
    } 
  }
  private int ais; private int ait; private int aiu; private int GA; private List<PledgeMemberInfo> EQ;
  static class PledgeMemberInfo { public String _name; public int level; public int class_id;
    public int online;
    public int sex;
    public int race;
    public int sponsor;
    
    public PledgeMemberInfo(String param1String, int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6) {
      this._name = param1String;
      this.level = param1Int1;
      this.class_id = param1Int2;
      this.online = param1Int3;
      this.sex = param1Int4;
      this.race = param1Int5;
      this.sponsor = param1Int6;
    } }

}
