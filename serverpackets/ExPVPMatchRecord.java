package l2.gameserver.network.l2.s2c;public class ExPVPMatchRecord extends L2GameServerPacket {
  private PVPMatchAction afo;
  private LinkedList<PVPMatchRecord> afp;
  private LinkedList<PVPMatchRecord> afq;
  private int afr;
  private int afs;
  private TeamType _winner;
  private int aft;
  private int afu;
  
  public enum PVPMatchAction {
    INIT(0),
    UPDATE(1),
    DONE(2);
    
    private final int acZ;

    
    PVPMatchAction(int param1Int1) { this.acZ = param1Int1; }



    
    public int getVal() { return this.acZ; }
  }













  
  public ExPVPMatchRecord(PVPMatchAction paramPVPMatchAction, TeamType paramTeamType, int paramInt1, int paramInt2) {
    this.afo = paramPVPMatchAction;
    this.afp = new LinkedList();
    this.afq = new LinkedList();
    this._winner = paramTeamType;
    this.aft = paramInt1;
    this.afu = paramInt2;
  }

  
  public void addRecord(Player paramPlayer, int paramInt1, int paramInt2) {
    if (paramPlayer.getTeam() == TeamType.RED) {
      
      this.afp.add(new PVPMatchRecord(paramPlayer.getName(), paramInt1, paramInt2));
      this.afs++;
    }
    else if (paramPlayer.getTeam() == TeamType.BLUE) {
      
      this.afq.add(new PVPMatchRecord(paramPlayer.getName(), paramInt1, paramInt2));
      this.afr++;
    } 
  }



  
  protected void writeImpl() {
    writeEx(126);
    
    writeD(this.afo.getVal());
    if (this._winner == TeamType.RED) {
      
      writeD(2);
      writeD(1);
    }
    else if (this._winner == TeamType.BLUE) {
      
      writeD(1);
      writeD(2);
    }
    else {
      
      writeD(0);
      writeD(0);
    } 
    
    writeD(this.aft);
    writeD(this.afu);
    
    if (this.afr > 0) {
      
      writeD(this.afq.size());
      for (PVPMatchRecord pVPMatchRecord : this.afq) {
        
        writeS(pVPMatchRecord.name);
        writeD(pVPMatchRecord.kill);
        writeD(pVPMatchRecord.die);
      } 
    } else {
      
      writeD(0);
    } 
    if (this.afs > 0) {
      
      writeD(this.afp.size());
      for (PVPMatchRecord pVPMatchRecord : this.afp) {
        
        writeS(pVPMatchRecord.name);
        writeD(pVPMatchRecord.kill);
        writeD(pVPMatchRecord.die);
      } 
    } else {
      
      writeD(0);
    } 
  }
  
  private class PVPMatchRecord {
    public final String name;
    public final int kill;
    public final int die;
    
    public PVPMatchRecord(String param1String, int param1Int1, int param1Int2) {
      this.name = param1String;
      this.kill = param1Int1;
      this.die = param1Int2;
    }
  }
}
