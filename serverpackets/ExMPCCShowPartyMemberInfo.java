package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Party;
import l2.gameserver.model.Player;









public class ExMPCCShowPartyMemberInfo
  extends L2GameServerPacket
{
  private List<PartyMemberInfo> members = new ArrayList(); public ExMPCCShowPartyMemberInfo(Party paramParty) {
    for (Player player : paramParty.getPartyMembers()) {
      this.members.add(new PartyMemberInfo(player.getName(), player.getObjectId(), player.getClassId().getId()));
    }
  }

  
  protected final void writeImpl() {
    writeEx(76);
    writeD(this.members.size());
    
    for (PartyMemberInfo partyMemberInfo : this.members) {
      
      writeS(partyMemberInfo.name);
      writeD(partyMemberInfo.object_id);
      writeD(partyMemberInfo.class_id);
    } 
    
    this.members.clear();
  }
  
  static class PartyMemberInfo
  {
    public String name;
    public int object_id;
    public int class_id;
    
    public PartyMemberInfo(String param1String, int param1Int1, int param1Int2) {
      this.name = param1String;
      this.object_id = param1Int1;
      this.class_id = param1Int2;
    }
  }
}
