package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;
import l2.gameserver.model.GameObject;

public class Attack
  extends L2GameServerPacket {
  public final int _attackerId;
  public final boolean _soulshot;
  private final int Qx;
  private final int _x;
  private final int _y;
  private final int _z;
  private final int aaW;
  private final int aaX;
  private final int aaY;
  private Hit[] aaZ;
  
  private class Hit {
    int _targetId;
    
    Hit(GameObject param1GameObject, int param1Int, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3) {
      this._targetId = param1GameObject.getObjectId();
      this._damage = param1Int;
      if (Attack.this._soulshot)
        this._flags |= 0x8; 
      if (param1Boolean2)
        this._flags |= 0x4; 
      if (param1Boolean3)
        this._flags |= 0x2; 
      if (param1Boolean1) {
        this._flags |= 0x1;
      }
    }

    
    int _damage;
    
    int _flags;
  }

  
  public Attack(Creature paramCreature1, Creature paramCreature2, boolean paramBoolean, int paramInt) {
    this._attackerId = paramCreature1.getObjectId();
    this._soulshot = paramBoolean;
    this.Qx = paramInt;
    this._x = paramCreature1.getX();
    this._y = paramCreature1.getY();
    this._z = paramCreature1.getZ();
    this.aaW = paramCreature2.getX();
    this.aaX = paramCreature2.getY();
    this.aaY = paramCreature2.getZ();
    this.aaZ = new Hit[0];
  }





  
  public void addHit(GameObject paramGameObject, int paramInt, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    int i = this.aaZ.length;

    
    Hit[] arrayOfHit = new Hit[i + 1];

    
    System.arraycopy(this.aaZ, 0, arrayOfHit, 0, this.aaZ.length);
    arrayOfHit[i] = new Hit(paramGameObject, paramInt, paramBoolean1, paramBoolean2, paramBoolean3);
    this.aaZ = arrayOfHit;
  }





  
  public boolean hasHits() { return (this.aaZ.length > 0); }



  
  protected final void writeImpl() {
    writeC(51);
    
    writeD(this._attackerId);
    writeD((this.aaZ[0])._targetId);
    writeD(0);
    writeD((this.aaZ[0])._damage);
    writeD((this.aaZ[0])._flags);
    writeD(this._soulshot ? this.Qx : 0);
    writeD(this._x);
    writeD(this._y);
    writeD(this._z);
    writeH(this.aaZ.length - 1);
    for (byte b = 1; b < this.aaZ.length; b++) {
      
      writeD((this.aaZ[b])._targetId);
      writeD((this.aaZ[b])._damage);
      writeD((this.aaZ[b])._flags);
      writeD(this._soulshot ? this.Qx : 0);
    } 
    writeD(this.aaW);
    writeD(this.aaX);
    writeD(this.aaY);
  }
}
