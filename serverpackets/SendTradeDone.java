package l2.gameserver.network.l2.s2c;

public class SendTradeDone
  extends L2GameServerPacket {
  public static final L2GameServerPacket SUCCESS = new SendTradeDone(1);
  public static final L2GameServerPacket FAIL = new SendTradeDone(0);

  
  private int pI;

  
  private SendTradeDone(int paramInt) { this.pI = paramInt; }



  
  protected final void writeImpl() {
    writeC(28);
    writeD(this.pI);
  }
}
