package l2.gameserver.network.l2.s2c;public class ExShowScreenMessage extends NpcStringContainer { public static final int SYSMSG_TYPE = 0; public static final int STRING_TYPE = 1;
  private int _type;
  private int agP;
  private boolean agQ;
  private boolean agR;
  private ScreenMessageAlign agS;
  private int _time;
  
  public enum ScreenMessageAlign { TOP_LEFT,
    TOP_CENTER,
    TOP_RIGHT,
    MIDDLE_LEFT,
    MIDDLE_CENTER,
    MIDDLE_RIGHT,
    BOTTOM_CENTER,
    BOTTOM_RIGHT; }











  
  @Deprecated
  public ExShowScreenMessage(String paramString, int paramInt, ScreenMessageAlign paramScreenMessageAlign, boolean paramBoolean) { this(paramString, paramInt, paramScreenMessageAlign, paramBoolean, 1, -1, false); }


  
  @Deprecated
  public ExShowScreenMessage(String paramString, int paramInt1, ScreenMessageAlign paramScreenMessageAlign, boolean paramBoolean1, int paramInt2, int paramInt3, boolean paramBoolean2) {
    super(NpcString.NONE, new String[] { paramString });
    this._type = paramInt2;
    this.agP = paramInt3;
    this._time = paramInt1;
    this.agS = paramScreenMessageAlign;
    this.agQ = paramBoolean1;
    this.agR = paramBoolean2;
  }


  
  public ExShowScreenMessage(NpcString paramNpcString, int paramInt, ScreenMessageAlign paramScreenMessageAlign, String... paramVarArgs) { this(paramNpcString, paramInt, paramScreenMessageAlign, true, 1, -1, false, paramVarArgs); }



  
  public ExShowScreenMessage(NpcString paramNpcString, int paramInt, ScreenMessageAlign paramScreenMessageAlign, boolean paramBoolean, String... paramVarArgs) { this(paramNpcString, paramInt, paramScreenMessageAlign, paramBoolean, 1, -1, false, paramVarArgs); }



  
  public ExShowScreenMessage(NpcString paramNpcString, int paramInt, ScreenMessageAlign paramScreenMessageAlign, boolean paramBoolean1, boolean paramBoolean2, String... paramVarArgs) { this(paramNpcString, paramInt, paramScreenMessageAlign, paramBoolean1, 1, -1, paramBoolean2, paramVarArgs); }


  
  public ExShowScreenMessage(NpcString paramNpcString, int paramInt1, ScreenMessageAlign paramScreenMessageAlign, boolean paramBoolean1, int paramInt2, int paramInt3, boolean paramBoolean2, String... paramVarArgs) {
    super(paramNpcString, paramVarArgs);
    this._type = paramInt2;
    this.agP = paramInt3;
    this._time = paramInt1;
    this.agS = paramScreenMessageAlign;
    this.agQ = paramBoolean1;
    this.agR = paramBoolean2;
  }


  
  protected final void writeImpl() {
    writeEx(57);
    writeD(this._type);
    writeD(this.agP);
    writeD(this.agS.ordinal() + 1);
    writeD(0);
    writeD(this.agQ ? 0 : 1);
    writeD(0);
    writeD(0);
    writeD(this.agR ? 1 : 0);
    writeD(this._time);
    writeD(1);
    writeElements();
  } }
