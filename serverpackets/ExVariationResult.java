package l2.gameserver.network.l2.s2c;

public class ExVariationResult
  extends L2GameServerPacket {
  public static final ExVariationResult FAIL_PACKET = new ExVariationResult(0, 0, 0);
  
  private int ahr;
  private int ahs;
  private int oH;
  
  public ExVariationResult(int paramInt1, int paramInt2, int paramInt3) {
    this.ahr = paramInt1;
    this.ahs = paramInt2;
    this.oH = paramInt3;
  }


  
  protected void writeImpl() {
    writeEx(87);
    writeD(this.ahr);
    writeD(this.ahs);
    writeD(this.oH);
  }
}
