package l2.gameserver.network.l2.s2c;




public class ExBlockDetailInfo
  extends L2GameServerPacket
{
  private String Yc;
  private String adE;
  
  public ExBlockDetailInfo(String paramString1, String paramString2) {
    this.Yc = paramString1;
    this.adE = paramString2;
  }


  
  protected void writeImpl() {
    writeEx(239);
    writeS(this.Yc);
    writeS(this.adE);
  }
}
