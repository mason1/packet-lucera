package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import l2.gameserver.model.entity.events.objects.SiegeClanObject;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.model.pledge.Clan;































public class CastleSiegeDefenderList
  extends L2GameServerPacket
{
  public static int OWNER = 1;
  public static int WAITING = 2;
  public static int ACCEPTED = 3; private int _id;
  public static int REFUSE = 4;
  
  public CastleSiegeDefenderList(Castle paramCastle) {
    this.abh = Collections.emptyList();


    
    this._id = paramCastle.getId();
    this.abf = (!paramCastle.getSiegeEvent().isRegistrationOver() && paramCastle.getOwner() != null) ? 1 : 0;
    
    List list1 = paramCastle.getSiegeEvent().getObjects("defenders");
    List list2 = paramCastle.getSiegeEvent().getObjects("defenders_waiting");
    List list3 = paramCastle.getSiegeEvent().getObjects("defenders_refused");
    this.abh = new ArrayList(list1.size() + list2.size() + list3.size());
    if (paramCastle.getOwner() != null)
      this.abh.add(new DefenderClan(paramCastle.getOwner(), OWNER, 0)); 
    for (SiegeClanObject siegeClanObject : list1)
      this.abh.add(new DefenderClan(siegeClanObject.getClan(), ACCEPTED, (int)(siegeClanObject.getDate() / 1000L))); 
    for (SiegeClanObject siegeClanObject : list2)
      this.abh.add(new DefenderClan(siegeClanObject.getClan(), WAITING, (int)(siegeClanObject.getDate() / 1000L))); 
    for (SiegeClanObject siegeClanObject : list3)
      this.abh.add(new DefenderClan(siegeClanObject.getClan(), REFUSE, (int)(siegeClanObject.getDate() / 1000L))); 
  }
  private int abf;
  private List<DefenderClan> abh;
  
  protected final void writeImpl() {
    writeC(203);
    writeD(this._id);
    writeD(0);
    writeD(this.abf);
    writeD(0);
    
    writeD(this.abh.size());
    writeD(this.abh.size());
    for (DefenderClan defenderClan : this.abh) {
      
      Clan clan = defenderClan.Gy;
      
      writeD(clan.getClanId());
      writeS(clan.getName());
      writeS(clan.getLeaderName());
      writeD(clan.getCrestId());
      writeD(defenderClan._time);
      writeD(defenderClan._type);
      writeD(clan.getAllyId());
      Alliance alliance = clan.getAlliance();
      if (alliance != null) {
        
        writeS(alliance.getAllyName());
        writeS(alliance.getAllyLeaderName());
        writeD(alliance.getAllyCrestId());
        
        continue;
      } 
      writeS("");
      writeS("");
      writeD(0);
    } 
  }

  
  private static class DefenderClan
  {
    private Clan Gy;
    
    private int _type;
    private int _time;
    
    public DefenderClan(Clan param1Clan, int param1Int1, int param1Int2) {
      this.Gy = param1Clan;
      this._type = param1Int1;
      this._time = param1Int2;
    }
  }
}
