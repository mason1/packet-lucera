package l2.gameserver.network.l2.s2c;

import l2.gameserver.network.l2.components.ChatType;
import l2.gameserver.network.l2.components.NpcString;
import l2.gameserver.network.l2.components.SysString;
import l2.gameserver.network.l2.components.SystemMsg;


public class Say2
  extends NpcStringContainer
{
  private ChatType aal;
  private SysString NB;
  private SystemMsg NC;
  private int Bq;
  private String adj;
  
  public Say2(int paramInt, ChatType paramChatType, SysString paramSysString, SystemMsg paramSystemMsg) {
    super(NpcString.NONE, new String[0]);
    this.Bq = paramInt;
    this.aal = paramChatType;
    this.NB = paramSysString;
    this.NC = paramSystemMsg;
  }


  
  public Say2(int paramInt, ChatType paramChatType, String paramString1, String paramString2) { this(paramInt, paramChatType, paramString1, NpcString.NONE, new String[] { paramString2 }); }


  
  public Say2(int paramInt, ChatType paramChatType, String paramString, NpcString paramNpcString, String... paramVarArgs) {
    super(paramNpcString, paramVarArgs);
    this.Bq = paramInt;
    this.aal = paramChatType;
    this.adj = paramString;
  }


  
  protected final void writeImpl() {
    writeC(74);
    writeD(this.Bq);
    writeD(this.aal.ordinal());
    switch (this.aal) {
      
      case SYSTEM_MESSAGE:
        writeD(this.NB.getId());
        writeD(this.NC.getId());
        return;
    } 
    writeS(this.adj);
    writeElements();
  }
}
