package l2.gameserver.network.l2.s2c;

public class TutorialEnableClientEvent extends L2GameServerPacket {
  public TutorialEnableClientEvent(int paramInt) {
    this.amp = 0;


    
    this.amp = paramInt;
  }
  
  private int amp;
  
  protected final void writeImpl() {
    writeC(168);
    writeD(this.amp);
  }
}
