package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.instances.NpcInstance;
import l2.gameserver.network.l2.components.ChatType;
import l2.gameserver.network.l2.components.NpcString;


public class NpcSay
  extends NpcStringContainer
{
  private int abx;
  private int _type;
  private int _id;
  
  public NpcSay(NpcInstance paramNpcInstance, ChatType paramChatType, String paramString) { this(paramNpcInstance, paramChatType, NpcString.NONE, new String[] { paramString }); }


  
  public NpcSay(NpcInstance paramNpcInstance, ChatType paramChatType, NpcString paramNpcString, String... paramVarArgs) {
    super(paramNpcString, paramVarArgs);
    this.abx = paramNpcInstance.getObjectId();
    this._id = paramNpcInstance.getNpcId();
    this._type = paramChatType.ordinal();
  }


  
  protected final void writeImpl() {
    writeC(48);
    writeD(this.abx);
    writeD(this._type);
    writeD(1000000 + this._id);
    writeElements();
  }
}
