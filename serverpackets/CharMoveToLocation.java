package l2.gameserver.network.l2.s2c;

import l2.gameserver.Config;
import l2.gameserver.model.Creature;
import l2.gameserver.utils.Location;
import l2.gameserver.utils.Log;

public class CharMoveToLocation
  extends L2GameServerPacket
{
  private int Bq;
  private int acg;
  private Location ach;
  private Location _destination;
  
  public CharMoveToLocation(Creature paramCreature) { this(paramCreature, paramCreature.getLoc(), paramCreature.getDestination()); }


  
  public CharMoveToLocation(Creature paramCreature, Location paramLocation1, Location paramLocation2) {
    this.Bq = paramCreature.getObjectId();
    this.ach = paramLocation1;
    this._destination = paramLocation2;
    if (!paramCreature.isFlying())
      this.acg = Config.CLIENT_Z_SHIFT; 
    if (paramCreature.isInWater()) {
      this.acg += Config.CLIENT_Z_SHIFT;
    }
    if (this._destination == null) {
      
      Log.debug("CharMoveToLocation: desc is null, but moving. L2Character: " + paramCreature.getObjectId() + ":" + paramCreature.getName() + "; Loc: " + this.ach);
      this._destination = this.ach;
    } 
  }

  
  public CharMoveToLocation(int paramInt, Location paramLocation1, Location paramLocation2) {
    this.Bq = paramInt;
    this.ach = paramLocation1;
    this._destination = paramLocation2;
  }


  
  protected final void writeImpl() {
    writeC(47);
    
    writeD(this.Bq);
    
    writeD(this._destination.x);
    writeD(this._destination.y);
    writeD(this._destination.z + this.acg);
    
    writeD(this.ach.x);
    writeD(this.ach.y);
    writeD(this.ach.z + this.acg);
  }
}
