package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.entity.boat.Boat;
import l2.gameserver.utils.Location;

public class VehicleDeparture
  extends L2GameServerPacket {
  private int Nk;
  private int Nl;
  private int amF;
  private Location _loc;
  
  public VehicleDeparture(Boat paramBoat) {
    this.amF = paramBoat.getObjectId();
    this.Nk = paramBoat.getMoveSpeed();
    this.Nl = paramBoat.getRotationSpeed();
    this._loc = paramBoat.getDestination();
    if (this._loc == null)
    {
      this._loc = paramBoat.getReturnLoc();
    }
  }

  
  public VehicleDeparture(Boat paramBoat, Location paramLocation) {
    this.amF = paramBoat.getObjectId();
    this.Nk = paramBoat.getMoveSpeed();
    this.Nl = paramBoat.getRotationSpeed();
    this._loc = paramLocation;
  }


  
  protected final void writeImpl() {
    writeC(108);
    writeD(this.amF);
    writeD(this.Nk);
    writeD(this.Nl);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
  }
}
