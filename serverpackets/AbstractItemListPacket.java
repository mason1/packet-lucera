package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.items.ItemInfo;






public abstract class AbstractItemListPacket
  extends L2GameServerPacket
{
  public static final int AUGMENTATION = 1;
  public static final int ATTRIBUTES = 2;
  public static final int ENCHANT_OPTIONS = 4;
  public static final int VISUAL = 8;
  public static final int SOUL_CRYSTAL = 16;
  
  protected void writeItemInfo(ItemInfo paramItemInfo) { writeItemInfo(paramItemInfo, paramItemInfo.getCount()); }


  
  protected void writeItemInfo(ItemInfo paramItemInfo, long paramLong) {
    int i = a(paramItemInfo);
    
    writeC(i);
    
    writeD(paramItemInfo.getObjectId());
    writeD(paramItemInfo.getItemId());
    writeC(paramItemInfo.getEquipSlot());
    writeQ(paramLong);
    writeC(paramItemInfo.getItem().getType2ForPackets());
    writeC(paramItemInfo.getCustomType1());
    writeH(paramItemInfo.isEquipped() ? 1 : 0);
    writeQ(paramItemInfo.getItem().getBodyPart());
    writeC(paramItemInfo.getEnchantLevel());
    writeC(1);
    writeD(paramItemInfo.getShadowLifeTime());
    writeD(paramItemInfo.getTemporalLifeTime());
    writeC(1);
    
    writeC(0);
    writeC(0);
    
    if (j(i, 1)) {
      
      writeD(paramItemInfo.getVariationStat1());
      writeD(paramItemInfo.getVariationStat2());
    } 
    
    if (j(i, 2)) {
      
      writeH(paramItemInfo.getAttackElement());
      writeH(paramItemInfo.getAttackElementValue());
      writeH(paramItemInfo.getDefenceFire());
      writeH(paramItemInfo.getDefenceWater());
      writeH(paramItemInfo.getDefenceWind());
      writeH(paramItemInfo.getDefenceEarth());
      writeH(paramItemInfo.getDefenceHoly());
      writeH(paramItemInfo.getDefenceUnholy());
    } 
    
    if (j(i, 4)) {
      
      writeD(paramItemInfo.getEnchantOptions()[0]);
      writeD(paramItemInfo.getEnchantOptions()[1]);
      writeD(paramItemInfo.getEnchantOptions()[2]);
    } 
    
    if (j(i, 8))
    {
      writeD(0);
    }
    
    if (j(i, 16)) {
      
      writeC(0);
      
      writeC(0);
    } 
  }



  
  private boolean j(int paramInt1, int paramInt2) { return ((paramInt1 & paramInt2) == paramInt2); }


  
  private int a(ItemInfo paramItemInfo) {
    null = 0;

    
    null |= true;
    null |= 0x2;
    return 4;
  }
}
