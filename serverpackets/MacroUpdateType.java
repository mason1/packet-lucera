package l2.gameserver.network.l2.s2c;

public static enum MacroUpdateType
{
  ADD(1),
  LIST(1),
  MODIFY(2),
  DELETE(0);

  
  private final int _id;

  
  MacroUpdateType(int paramInt1) { this._id = paramInt1; }



  
  public int getId() { return this._id; }
}
