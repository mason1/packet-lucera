package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.entity.events.impl.DuelEvent;


public class ExDuelStart
  extends L2GameServerPacket
{
  private int YA;
  
  public ExDuelStart(DuelEvent paramDuelEvent) { this.YA = paramDuelEvent.getDuelType(); }



  
  protected final void writeImpl() {
    writeEx(79);
    writeD(this.YA);
  }
}
