package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.ProductItem;
import l2.gameserver.model.ProductItemComponent;


public class ExBR_ProductInfo
  extends L2GameServerPacket
{
  private ProductItem adz;
  
  protected void writeImpl() {
    if (this.adz == null) {
      return;
    }
    writeEx(215);
    
    writeD(this.adz.getProductId());
    writeD(this.adz.getPoints());
    writeD(this.adz.getComponents().size());
    
    for (ProductItemComponent productItemComponent : this.adz.getComponents()) {
      
      writeD(productItemComponent.getItemId());
      writeD(productItemComponent.getCount());
      writeD(productItemComponent.getWeight());
      writeD(productItemComponent.isDropable() ? 1 : 0);
    } 
  }
}
