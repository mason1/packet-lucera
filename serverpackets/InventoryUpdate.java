package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.ItemInstance;


public class InventoryUpdate
  extends AbstractItemListPacket
{
  public static final int UNCHANGED = 0;
  public static final int ADDED = 1;
  public static final int MODIFIED = 2;
  public static final int REMOVED = 3;
  private final List<ItemInfo> _items = new ArrayList(1);

  
  public InventoryUpdate addNewItem(ItemInstance paramItemInstance) {
    a(paramItemInstance).setLastChange(1);
    return this;
  }

  
  public InventoryUpdate addModifiedItem(ItemInstance paramItemInstance) {
    a(paramItemInstance).setLastChange(2);
    return this;
  }

  
  public InventoryUpdate addRemovedItem(ItemInstance paramItemInstance) {
    a(paramItemInstance).setLastChange(3);
    return this;
  }

  
  private ItemInfo a(ItemInstance paramItemInstance) {
    ItemInfo itemInfo;
    this._items.add(itemInfo = new ItemInfo(paramItemInstance));
    return itemInfo;
  }


  
  protected final void writeImpl() {
    writeC(33);
    writeC(0);
    
    writeD(this._items.size());
    writeD(this._items.size());
    
    for (ItemInfo itemInfo : this._items) {
      
      writeH(itemInfo.getLastChange());
      writeItemInfo(itemInfo);
    } 
  }
}
