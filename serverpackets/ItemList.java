package l2.gameserver.network.l2.s2c;

import java.util.List;
import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.LockType;





public class ItemList
  extends AbstractItemListPacket
{
  private final boolean afM;
  private final List<ItemInfo> _items;
  private final boolean aiI;
  private LockType UG;
  private int[] UH;
  
  public ItemList(boolean paramBoolean1, List<ItemInfo> paramList, boolean paramBoolean2, LockType paramLockType, int... paramVarArgs) {
    this.afM = paramBoolean1;
    this._items = paramList;
    this.aiI = paramBoolean2;
    this.UG = paramLockType;
    this.UH = paramVarArgs;
  }


  
  protected final void writeImpl() {
    writeC(17);
    
    writeC(this.afM ? 1 : 2);
    if (this.afM) {
      
      writeD(this.aiI);
      writeD(this._items.size());
    }
    else {
      
      writeD(this._items.size());
      writeD(this._items.size());
      for (ItemInfo itemInfo : this._items)
      {
        writeItemInfo(itemInfo);
      }
    } 
    
    writeH(this.UH.length);
    if (this.UH.length > 0) {
      
      writeC(this.UG.ordinal());
      for (int i : this.UH)
        writeD(i); 
    } 
  }
}
