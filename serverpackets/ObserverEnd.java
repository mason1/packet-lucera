package l2.gameserver.network.l2.s2c;

import l2.gameserver.utils.Location;



public class ObserverEnd
  extends L2GameServerPacket
{
  private Location _loc;
  
  public ObserverEnd(Location paramLocation) { this._loc = paramLocation; }



  
  protected final void writeImpl() {
    writeC(236);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
  }
}
