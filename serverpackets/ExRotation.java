package l2.gameserver.network.l2.s2c;

public class ExRotation
  extends L2GameServerPacket {
  private int Xg;
  private int Xz;
  
  public ExRotation(int paramInt1, int paramInt2) {
    this.Xg = paramInt1;
    this.Xz = paramInt2;
  }


  
  protected void writeImpl() {
    writeEx(194);
    writeD(this.Xg);
    writeD(this.Xz);
  }
}
