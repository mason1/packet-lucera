package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.TradeItem;






public class PrivateStoreManageListSell
  extends AbstractItemListPacket
{
  private final boolean aks;
  private int ZE;
  private long abb;
  private boolean aaq;
  private List<TradeItem> Gl;
  private List<TradeItem> aku;
  
  public PrivateStoreManageListSell(boolean paramBoolean1, Player paramPlayer, boolean paramBoolean2) {
    this.aks = paramBoolean1;
    this.ZE = paramPlayer.getObjectId();
    this.abb = paramPlayer.getAdena();
    this.aaq = paramBoolean2;
    this.aku = paramPlayer.getSellList(this.aaq);
    this.Gl = new ArrayList();

    
    for (TradeItem tradeItem : this.aku) {
      
      if (tradeItem.getCount() <= 0L) {
        
        this.aku.remove(tradeItem);
        
        continue;
      } 
      ItemInstance itemInstance = paramPlayer.getInventory().getItemByObjectId(tradeItem.getObjectId());
      if (itemInstance == null)
      {
        itemInstance = paramPlayer.getInventory().getItemByItemId(tradeItem.getItemId());
      }
      if (itemInstance == null || !itemInstance.canBeTraded(paramPlayer) || itemInstance.getItemId() == 57) {
        
        this.aku.remove(tradeItem);
        
        continue;
      } 
      
      tradeItem.setCount(Math.min(itemInstance.getCount(), tradeItem.getCount()));
    } 
    
    ItemInstance[] arrayOfItemInstance1 = paramPlayer.getInventory().getItems();
    
    ItemInstance[] arrayOfItemInstance2 = arrayOfItemInstance1; int i = arrayOfItemInstance2.length; byte b = 0; while (true) { if (b < i) { ItemInstance itemInstance = arrayOfItemInstance2[b];
        if (itemInstance.canBeTraded(paramPlayer) && itemInstance.getItemId() != 57) {
          
          Iterator iterator = this.aku.iterator(); for (;; b++) { if (iterator.hasNext()) { TradeItem tradeItem = (TradeItem)iterator.next();
              if (tradeItem.getObjectId() == itemInstance.getObjectId())
              
              { if (tradeItem.getCount() == itemInstance.getCount()) {
                  break;
                }
                TradeItem tradeItem1 = new TradeItem(itemInstance);
                tradeItem1.setCount(itemInstance.getCount() - tradeItem.getCount());
                this.Gl.add(tradeItem1); }
              else { continue; }
               }
            else { this.Gl.add(new TradeItem(itemInstance)); break; }
             }
        
        }  }
      else
      { break; }
       b++; }
     } protected final void writeImpl() { writeC(160);
    writeC(this.aks ? 1 : 2);
    
    if (this.aks) {
      
      writeD(this.ZE);
      writeD(this.aaq ? 1 : 0);
      writeQ(this.abb);
      writeD(this.aku.size());
      for (TradeItem tradeItem : this.aku) {
        
        writeItemInfo(tradeItem);
        writeQ(tradeItem.getOwnersPrice());
        writeQ(tradeItem.getStorePrice());
      } 
      writeD(this.Gl.size());
    }
    else {
      
      writeD(this.Gl.size());
      writeD(this.Gl.size());
      for (TradeItem tradeItem : this.Gl) {
        
        writeItemInfo(tradeItem);
        writeQ(tradeItem.getStorePrice());
      } 
    }  }

}
