package l2.gameserver.network.l2.s2c;

import java.util.List;
import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.model.Manor;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.templates.manor.SeedProduction;









public class ExShowSeedSetting
  extends L2GameServerPacket
{
  private int EJ;
  private int _count;
  private long[] agW;
  
  public ExShowSeedSetting(int paramInt) {
    this.EJ = paramInt;
    Castle castle = (Castle)ResidenceHolder.getInstance().getResidence(Castle.class, this.EJ);
    List list = Manor.getInstance().getSeedsForCastle(this.EJ);
    this._count = list.size();
    this.agW = new long[this._count * 12];
    byte b = 0;
    for (Manor.SeedData seedData : list) {
      
      this.agW[b * 12 + 0] = seedData.getId();
      this.agW[b * 12 + 1] = seedData.getLevel();
      this.agW[b * 12 + 2] = seedData.getReward(1);
      this.agW[b * 12 + 3] = seedData.getReward(2);
      this.agW[b * 12 + 4] = seedData.getSeedLimit();
      this.agW[b * 12 + 5] = Manor.getInstance().getSeedBuyPrice(seedData.getId());
      int i = Manor.getInstance().getSeedBasicPrice(seedData.getId());
      this.agW[b * 12 + 6] = (i * 60 / 100);
      this.agW[b * 12 + 7] = (i * 10);
      SeedProduction seedProduction = castle.getSeed(seedData.getId(), 0);
      if (seedProduction != null) {
        
        this.agW[b * 12 + 8] = seedProduction.getStartProduce();
        this.agW[b * 12 + 9] = seedProduction.getPrice();
      }
      else {
        
        this.agW[b * 12 + 8] = 0L;
        this.agW[b * 12 + 9] = 0L;
      } 
      seedProduction = castle.getSeed(seedData.getId(), 1);
      if (seedProduction != null) {
        
        this.agW[b * 12 + 10] = seedProduction.getStartProduce();
        this.agW[b * 12 + 11] = seedProduction.getPrice();
      }
      else {
        
        this.agW[b * 12 + 10] = 0L;
        this.agW[b * 12 + 11] = 0L;
      } 
      b++;
    } 
  }


  
  public void writeImpl() {
    writeEx(38);
    
    writeD(this.EJ);
    writeD(this._count);
    
    for (byte b = 0; b < this._count; b++) {
      
      writeD((int)this.agW[b * 12 + 0]);
      writeD((int)this.agW[b * 12 + 1]);
      
      writeC(1);
      writeD((int)this.agW[b * 12 + 2]);
      
      writeC(1);
      writeD((int)this.agW[b * 12 + 3]);
      
      writeD((int)this.agW[b * 12 + 4]);
      writeD((int)this.agW[b * 12 + 5]);
      writeD((int)this.agW[b * 12 + 6]);
      writeD((int)this.agW[b * 12 + 7]);
      
      writeQ(this.agW[b * 12 + 8]);
      writeQ(this.agW[b * 12 + 9]);
      writeQ(this.agW[b * 12 + 10]);
      writeQ(this.agW[b * 12 + 11]);
    } 
  }
}
