package l2.gameserver.network.l2.s2c;





public class ExResponseShowContents
  extends L2GameServerPacket
{
  private final String agd;
  
  public ExResponseShowContents(String paramString) { this.agd = paramString; }



  
  protected void writeImpl() {
    writeEx(176);
    writeS(this.agd);
  }
}
