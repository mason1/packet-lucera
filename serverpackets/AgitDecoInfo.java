package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.entity.residence.ClanHall;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AgitDecoInfo
  extends L2GameServerPacket
{
  private static final Logger _log = LoggerFactory.getLogger(AgitDecoInfo.class); private static int[] aaJ = { 
      0, 1, 1, 1, 2, 2, 2, 2, 2, 0, 0, 1, 1, 1, 2, 2, 2, 2, 2 };
  private static int[] aaK = { 0, 1, 2, 2 };
  
  private int _id;
  
  private int aaL;
  
  private int aaM;
  
  private int aaN;
  
  private int aaO;
  
  private int aaP;
  
  private int aaQ;
  
  private int aaR;
  private int aaS;
  
  public AgitDecoInfo(ClanHall paramClanHall) {
    this._id = paramClanHall.getId();
    
    this.aaL = S(paramClanHall.isFunctionActive(3) ? paramClanHall.getFunction(3).getLevel() : 0);
    this.aaM = T(paramClanHall.isFunctionActive(4) ? paramClanHall.getFunction(4).getLevel() : 0);
    this.aaN = U(paramClanHall.isFunctionActive(5) ? paramClanHall.getFunction(5).getLevel() : 0);
    this.aaO = paramClanHall.isFunctionActive(1) ? paramClanHall.getFunction(1).getLevel() : 0;
    this.aaP = paramClanHall.isFunctionActive(7) ? paramClanHall.getFunction(7).getLevel() : 0;
    this.aaQ = paramClanHall.isFunctionActive(2) ? aaK[paramClanHall.getFunction(2).getLevel()] : 0;
    this.aaR = paramClanHall.isFunctionActive(6) ? aaJ[paramClanHall.getFunction(6).getLevel()] : 0;
    this.aaS = paramClanHall.isFunctionActive(8) ? paramClanHall.getFunction(8).getLevel() : 0;
  }
























  
  protected final void writeImpl() {
    writeC(253);
    writeD(this._id);
    writeC(this.aaL);
    writeC(this.aaM);
    writeC(this.aaM);
    writeC(this.aaN);
    writeC(this.aaO);
    writeC(0);
    writeC(this.aaP);
    writeC(this.aaQ);
    writeC(this.aaR);
    writeC(this.aaR);
    writeC(this.aaS);
    writeC(this.aaQ);
    writeD(0);
    writeD(0);
    writeD(0);
    writeD(0);
    writeD(0);
  }

  
  private static int S(int paramInt) {
    switch (paramInt) {
      
      case 0:
        return 0;
      case 20:
      case 40:
      case 80:
      case 120:
      case 140:
        return 1;
      case 160:
      case 180:
      case 200:
      case 220:
      case 240:
      case 260:
      case 280:
      case 300:
        return 2;
    } 
    _log.warn("Unsupported percent " + paramInt + " in hp recovery");
    return 0;
  }


  
  private static int T(int paramInt) {
    switch (paramInt) {
      
      case 0:
        return 0;
      case 5:
      case 10:
      case 15:
      case 20:
        return 1;
      case 25:
      case 30:
      case 35:
      case 40:
      case 45:
      case 50:
        return 2;
    } 
    _log.warn("Unsupported percent " + paramInt + " in mp recovery");
    return 0;
  }


  
  private static int U(int paramInt) {
    switch (paramInt) {
      
      case 0:
        return 0;
      case 5:
      case 10:
      case 15:
      case 20:
        return 1;
      case 25:
      case 30:
      case 35:
      case 40:
      case 45:
      case 50:
        return 2;
    } 
    _log.warn("Unsupported percent " + paramInt + " in exp recovery");
    return 0;
  }
}
