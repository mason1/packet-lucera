package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;


public class FriendStatus
  extends L2GameServerPacket
{
  public static final int MODE_OFFLINE = 0;
  public static final int MODE_ONLINE = 1;
  public static final int MODE_LEVEL = 2;
  public static final int MODE_CLASS = 3;
  private String adj;
  private int type;
  private int value;
  
  public FriendStatus(Player paramPlayer, int paramInt1, int paramInt2) {
    this.adj = paramPlayer.getName();
    this.type = paramInt1;
    this.value = paramInt2;
  }


  
  protected final void writeImpl() {
    writeC(89);
    writeD(this.type);
    writeS(this.adj);
    switch (this.type) {
      
      case 0:
      case 2:
      case 3:
        writeD(this.value);
        break;
    } 
  }
}
