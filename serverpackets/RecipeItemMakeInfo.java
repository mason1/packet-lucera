package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.Recipe;




public class RecipeItemMakeInfo
  extends L2GameServerPacket
{
  private int _id;
  private int akC;
  private int _status;
  private int akD;
  private int akE;
  
  public RecipeItemMakeInfo(Player paramPlayer, Recipe paramRecipe, int paramInt) {
    this._id = paramRecipe.getId();
    this.akC = paramRecipe.getType().ordinal();
    this._status = paramInt;
    this.akD = (int)paramPlayer.getCurrentMp();
    this.akE = paramPlayer.getMaxMp();
  }



  
  protected final void writeImpl() {
    writeC(221);
    writeD(this._id);
    writeD(this.akC);
    writeD(this.akD);
    writeD(this.akE);
    writeD(this._status);
  }
}
