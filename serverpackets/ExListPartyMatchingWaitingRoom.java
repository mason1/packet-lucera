package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import l2.commons.lang.ArrayUtils;
import l2.gameserver.instancemanager.MatchingRoomManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.Reflection;




public class ExListPartyMatchingWaitingRoom
  extends L2GameServerPacket
{
  private List<PartyMatchingWaitingInfo> aeP = Collections.emptyList();
  
  private final int aeO;
  
  public ExListPartyMatchingWaitingRoom(Player paramPlayer, int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfInt) {
    int i = (paramInt3 - 1) * 64;
    int j = paramInt3 * 64;
    byte b = 0;
    
    List list = MatchingRoomManager.getInstance().getWaitingList(paramInt1, paramInt2, paramArrayOfInt);
    this.aeO = list.size();
    
    this.aeP = new ArrayList(this.aeO);
    for (Player player : list) {
      
      if (b < i || b >= j)
        continue; 
      this.aeP.add(new PartyMatchingWaitingInfo(player));
      b++;
    } 
  }


  
  protected void writeImpl() {
    writeEx(54);
    
    writeD(this.aeO);
    writeD(this.aeP.size());
    for (PartyMatchingWaitingInfo partyMatchingWaitingInfo : this.aeP) {
      
      writeS(partyMatchingWaitingInfo.name);
      writeD(partyMatchingWaitingInfo.classId);
      writeD(partyMatchingWaitingInfo.level);
      writeD(partyMatchingWaitingInfo.currentInstance);
      writeD(partyMatchingWaitingInfo.instanceReuses.length);
      for (int i : partyMatchingWaitingInfo.instanceReuses)
        writeD(i); 
    } 
  }
  
  static class PartyMatchingWaitingInfo {
    public final int classId;
    public final int level;
    public final int currentInstance;
    public final String name;
    public final int[] instanceReuses;
    
    public PartyMatchingWaitingInfo(Player param1Player) {
      this.name = param1Player.getName();
      this.classId = param1Player.getClassId().getId();
      this.level = param1Player.getLevel();
      Reflection reflection = param1Player.getReflection();
      this.currentInstance = (reflection == null) ? 0 : reflection.getInstancedZoneId();
      this.instanceReuses = ArrayUtils.toArray(param1Player.getInstanceReuses().keySet());
    }
  }
}
