package l2.gameserver.network.l2.s2c;

import java.util.Iterator;
import java.util.List;
import l2.gameserver.model.Manor;


















public class ExShowManorDefaultInfo
  extends L2GameServerPacket
{
  private List<Integer> agu;
  
  public ExShowManorDefaultInfo() {
    this.agu = null;


    
    this.agu = Manor.getInstance().getAllCrops();
  }


  
  protected void writeImpl() {
    writeEx(37);
    writeC(0);
    writeD(this.agu.size());
    for (Iterator iterator = this.agu.iterator(); iterator.hasNext(); ) { int i = ((Integer)iterator.next()).intValue();
      
      writeD(i);
      writeD(Manor.getInstance().getSeedLevelByCrop(i));
      writeD(Manor.getInstance().getSeedBasicPriceByCrop(i));
      writeD(Manor.getInstance().getCropBasicPrice(i));
      writeC(1);
      writeD(Manor.getInstance().getRewardItem(i, 1));
      writeC(1);
      writeD(Manor.getInstance().getRewardItem(i, 2)); }
  
  }
}
