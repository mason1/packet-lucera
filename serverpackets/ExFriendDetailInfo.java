package l2.gameserver.network.l2.s2c;

import java.util.Calendar;
import l2.gameserver.model.actor.instances.player.Friend;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.model.pledge.Clan;

public class ExFriendDetailInfo extends L2GameServerPacket {
  private String name;
  private boolean aeu;
  private int objectId;
  private int level;
  private int classId;
  private int aev;
  private int aew;
  private int aex;
  private int aey;
  private int aez;
  private String aeA;
  private String aeB;
  private int nx;
  private String adE;
  private int aeC;
  private int aeD;
  
  public ExFriendDetailInfo(int paramInt, Friend paramFriend) {
    this.aev = paramInt;
    this.name = paramFriend.getName();
    this.aeu = paramFriend.isOnline();
    this.objectId = paramFriend.getObjectId();
    this.level = paramFriend.getLevel();
    this.classId = paramFriend.getClassId();
    Clan clan = paramFriend.getClan();
    if (clan != null) {
      
      this.aew = clan.getClanId();
      this.aex = clan.getCrestId();
      this.aeA = clan.getName();
      
      Alliance alliance = clan.getAlliance();
      if (alliance != null) {
        
        this.aey = alliance.getAllyId();
        this.aeB = alliance.getAllyName();
        this.aez = alliance.getAllyCrestId();
      } 
    } 
    
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(paramFriend.getCreateTime());
    
    this.aeC = calendar.get(5);
    this.aeD = calendar.get(2) + 1;
    
    this.nx = (int)(paramFriend.isOnline() ? 0L : ((System.currentTimeMillis() - paramFriend.getLastAccess()) / 1000L));
  }


  
  protected void writeImpl() {
    writeEx(236);
    writeD(this.objectId);
    writeS(this.name);
    writeD(this.aeu);
    writeD(this.objectId);
    writeH(this.level);
    writeH(this.classId);
    writeD(this.aew);
    writeD(this.aex);
    writeS(this.aeA);
    writeD(this.aey);
    writeD(this.aez);
    writeS(this.aeB);
    writeC(this.aeD);
    writeC(this.aeC);
    writeD(this.aeu ? -1 : this.nx);
    writeS(this.adE);
  }
}
