package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Player;
import l2.gameserver.model.Summon;

public class ExEventMatchTeamInfo
  extends L2GameServerPacket {
  private int aej;
  
  public ExEventMatchTeamInfo(List<Player> paramList, Player paramPlayer) {
    this.members = new ArrayList();


    
    this.aej = ((Player)paramList.get(0)).getObjectId();
    this.aek = ((Player)paramList.get(0)).getParty().getLootDistribution();
    
    for (Player player : paramList) {
      if (!player.equals(paramPlayer))
        this.members.add(new EventMatchTeamInfo(player)); 
    } 
  }
  private int aek;
  private List<EventMatchTeamInfo> members;
  
  protected void writeImpl() { writeEx(28); }

  
  public static class EventMatchTeamInfo
  {
    public String _name;
    
    public String pet_Name;
    public int _id;
    public int curCp;
    
    public EventMatchTeamInfo(Player param1Player) {
      this._name = param1Player.getName();
      this._id = param1Player.getObjectId();
      this.curCp = (int)param1Player.getCurrentCp();
      this.maxCp = param1Player.getMaxCp();
      this.curHp = (int)param1Player.getCurrentHp();
      this.maxHp = param1Player.getMaxHp();
      this.curMp = (int)param1Player.getCurrentMp();
      this.maxMp = param1Player.getMaxMp();
      this.level = param1Player.getLevel();
      this.class_id = param1Player.getClassId().getId();
      this.race_id = param1Player.getRace().ordinal();
      
      Summon summon = param1Player.getPet();
      if (summon != null) {
        
        this.pet_id = summon.getObjectId();
        this.pet_NpcId = summon.getNpcId() + 1000000;
        this.pet_Name = summon.getName();
        this.pet_curHp = (int)summon.getCurrentHp();
        this.pet_maxHp = summon.getMaxHp();
        this.pet_curMp = (int)summon.getCurrentMp();
        this.pet_maxMp = summon.getMaxMp();
        this.pet_level = summon.getLevel();
      } else {
        
        this.pet_id = 0;
      } 
    }
    
    public int maxCp;
    public int curHp;
    public int maxHp;
    public int curMp;
    public int maxMp;
    public int level;
    public int class_id;
    public int race_id;
    public int pet_id;
    public int pet_NpcId;
    public int pet_curHp;
    public int pet_maxHp;
    public int pet_curMp;
    public int pet_maxMp;
    public int pet_level;
  }
}
