package l2.gameserver.network.l2.s2c;






public class ExCubeGameEnd
  extends L2GameServerPacket
{
  boolean _isRedTeamWin;
  
  public ExCubeGameEnd(boolean paramBoolean) { this._isRedTeamWin = paramBoolean; }



  
  protected void writeImpl() {
    writeEx(152);
    writeD(1);
    
    writeD(this._isRedTeamWin ? 1 : 0);
  }
}
