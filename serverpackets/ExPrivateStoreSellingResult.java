package l2.gameserver.network.l2.s2c;





public class ExPrivateStoreSellingResult
  extends L2GameServerPacket
{
  private int afD;
  private long count;
  private String afE;
  
  public ExPrivateStoreSellingResult(int paramInt, long paramLong, String paramString) {
    this.afD = paramInt;
    this.count = paramLong;
    this.afE = paramString;
  }


  
  protected void writeImpl() {
    writeEx(454);
    writeD(this.afD);
    writeQ(this.count);
    writeS(this.afE);
  }
}
