package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Summon;

public class SetSummonRemainTime
  extends L2GameServerPacket
{
  private final int ald;
  private final int Sx;
  
  public SetSummonRemainTime(Summon paramSummon) {
    this.Sx = paramSummon.getCurrentFed();
    this.ald = paramSummon.getMaxFed();
  }


  
  protected final void writeImpl() {
    writeC(209);
    writeD(this.ald);
    writeD(this.Sx);
  }
}
