package l2.gameserver.network.l2.s2c;

public class ExDuelAskStart
  extends L2GameServerPacket
{
  String _requestor;
  int _isPartyDuel;
  
  public ExDuelAskStart(String paramString, int paramInt) {
    this._requestor = paramString;
    this._isPartyDuel = paramInt;
  }


  
  protected final void writeImpl() {
    writeEx(77);
    writeS(this._requestor);
    writeD(this._isPartyDuel);
  }
}
