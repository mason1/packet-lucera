package l2.gameserver.network.l2.s2c;


public class JoinPledge
  extends L2GameServerPacket
{
  private int aiJ;
  
  public JoinPledge(int paramInt) { this.aiJ = paramInt; }



  
  protected final void writeImpl() {
    writeC(45);
    
    writeD(this.aiJ);
  }
}
