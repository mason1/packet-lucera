package l2.gameserver.network.l2.s2c;

import java.util.List;
import l2.commons.collections.CollectionUtils;
import l2.gameserver.dao.MailDAO;
import l2.gameserver.model.Player;
import l2.gameserver.model.mail.Mail;












public class ExShowSentPostList
  extends L2GameServerPacket
{
  private final List<Mail> agM;
  
  public ExShowSentPostList(Player paramPlayer) {
    this.agM = MailDAO.getInstance().getSentMailByOwnerId(paramPlayer.getObjectId());
    CollectionUtils.shellSort(this.agM);
  }



  
  protected void writeImpl() {
    writeEx(173);
    writeD((int)(System.currentTimeMillis() / 1000L));
    writeD(this.agM.size());
    for (Mail mail : this.agM) {
      
      writeD(mail.getMessageId());
      writeS(mail.getTopic());
      writeS(mail.getReceiverName());
      writeD(mail.isPayOnDelivery() ? 1 : 0);
      writeD(mail.getExpireTime());
      writeD(mail.isUnread() ? 1 : 0);
      writeD((mail.getType() == Mail.SenderType.NORMAL) ? 0 : 1);
      writeD(mail.getAttachments().isEmpty() ? 0 : 1);
      writeD(0);
    } 
  }
}
