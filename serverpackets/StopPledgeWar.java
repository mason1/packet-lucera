package l2.gameserver.network.l2.s2c;

public class StopPledgeWar
  extends L2GameServerPacket
{
  private String aaa;
  private String alH;
  
  public StopPledgeWar(String paramString1, String paramString2) {
    this.aaa = paramString1;
    this.alH = paramString2;
  }


  
  protected final void writeImpl() {
    writeC(101);
    writeS(this.aaa);
    writeS(this.alH);
  }
}
