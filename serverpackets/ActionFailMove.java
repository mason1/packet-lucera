package l2.gameserver.network.l2.s2c;

public class ActionFailMove
  extends L2GameServerPacket {
  public static final L2GameServerPacket STATIC = new ActionFailMove();

  
  protected final void writeImpl() {
    writeC(31);
    writeD(1);
  }
}
