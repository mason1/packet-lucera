package l2.gameserver.network.l2.s2c;

import l2.gameserver.Config;
import l2.gameserver.data.xml.holder.NpcHolder;
import l2.gameserver.instancemanager.CursedWeaponsManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.base.ClassId;
import l2.gameserver.model.base.Element;
import l2.gameserver.model.base.Experience;
import l2.gameserver.model.base.TeamType;
import l2.gameserver.model.entity.events.GlobalEvent;
import l2.gameserver.model.matching.MatchingRoom;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.network.l2.s2c.mask.AbstractMaskPacket;
import l2.gameserver.network.l2.s2c.mask.IUpdateTypeComponent;
import l2.gameserver.skills.effects.EffectCubic;
import l2.gameserver.utils.Location;


public class UserInfo
  extends AbstractMaskPacket<UserInfoType>
{
  private boolean can_writeImpl = false;
  private boolean amq;
  private int abq;
  private int abr;
  private int amr;
  private int ams;
  private int abt;
  private int abu;
  private int abv;
  private int abw;
  private int abA;
  private double aig;
  private double aih;
  private double abC;
  private double abD;
  private Location _loc;
  private Location DQ;
  private int acr;
  private int amt;
  private int Bt;
  private int sex;
  private int aby;
  private int level;
  private int curCp;
  private int maxCp;
  private final byte[] aaE = { 0, 0, 0 }; private int abN; private int abO; private int amu; private long _exp; private int curHp; private int maxHp; private int curMp; private int maxMp; private int aho; private int ahp; private int ahN; private int abz; private int ahG; private int ahH; private int ahI; private int ahJ; private int ahK; private int ahL; private int _sp; private int amv; private int amw; private int ahO; private int ahP; private int ahQ; private int ahR; private int ahS; private int ahT; private int ahU; private int ahV; private int ahW; private int pvp_flag; private int karma; private int abE; private int abF; private int abG; private int ahX; private int aij; private int aik; private int clan_id; private int clan_crest_id; private int ally_id; private int ally_crest_id; private int abH; private int abM; private int amx; private int aic;
  private int aid;
  private int class_id;
  private int amy;
  private int amz;
  private int ahZ;
  private int aia;
  private int aaF = 5; private int abX; private int abW; private int aib; private int running; private int aif; private int abU; private int ahY; private int amA; private int TR; private int TS; private int TT;
  private int TU;
  private int TV;
  
  public UserInfo(Player paramPlayer) { this(paramPlayer, UserInfoType.VALUES); }
  private int TW; private int abS; private String _name; private String title; private EffectCubic[] acc; private Element aii; private int TQ; private boolean amB; private boolean amC; private int ail; private double ain; private TeamType _team;
  private int amD;
  
  public UserInfo(Player paramPlayer, UserInfoType... paramVarArgs) {
    if (paramPlayer.getTransformationName() != null) {
      
      this._name = paramPlayer.getTransformationName();
      this.title = "";
      this.clan_crest_id = 0;
      this.ally_crest_id = 0;
      this.abH = 0;
      this.abW = CursedWeaponsManager.getInstance().getLevel(paramPlayer.getCursedWeaponEquippedId());
    }
    else {
      
      this._name = paramPlayer.getName();
      
      Clan clan = paramPlayer.getClan();
      Alliance alliance = (clan == null) ? null : clan.getAlliance();
      
      this.clan_id = (clan == null) ? 0 : clan.getClanId();
      this.clan_crest_id = (clan == null) ? 0 : clan.getCrestId();
      this.abH = (clan == null) ? 0 : clan.getCrestLargeId();
      
      this.ally_id = (alliance == null) ? 0 : alliance.getAllyId();
      this.ally_crest_id = (alliance == null) ? 0 : alliance.getAllyCrestId();
      
      this.abW = 0;
      this.title = paramPlayer.getTitle();
    } 
    
    if ((paramPlayer.getPlayerAccess()).GodMode && paramPlayer.isInvisible())
    {
      this.title += "(Invisible)";
    }
    if (paramPlayer.isPolymorphed())
    {
      if (NpcHolder.getInstance().getTemplate(paramPlayer.getPolyId()) != null) {
        
        this.title += " - " + (NpcHolder.getInstance().getTemplate(paramPlayer.getPolyId())).name;
      }
      else {
        
        this.title += " - Polymorphed";
      } 
    }
    
    if (paramPlayer.isMounted()) {
      
      this.abN = 0;
      this.abO = 0;
      this.abX = paramPlayer.getMountNpcId() + 1000000;
      this.abS = paramPlayer.getMountType();
    }
    else {
      
      this.abN = paramPlayer.getWeaponEnchantEffect();
      this.abO = paramPlayer.getArmorSetEnchantLevel();
      this.abX = 0;
      this.abS = 0;
    } 
    
    this.amu = paramPlayer.getPhysicalAttackRange();
    
    this.aig = paramPlayer.getMovementSpeedMultiplier();
    this.abq = (int)(paramPlayer.getRunSpeed() / this.aig);
    this.abr = (int)(paramPlayer.getWalkSpeed() / this.aig);
    
    this.abt = 0;
    this.abu = 0;
    
    if (paramPlayer.isFlying()) {
      
      this.abv = this.abq;
      this.abw = this.abr;
    }
    else {
      
      this.abv = 0;
      this.abw = 0;
    } 
    
    this.amr = paramPlayer.getSwimSpeed();
    this.ams = paramPlayer.getSwimSpeed();
    
    if (paramPlayer.getClan() != null) {
      
      this.abA |= 0x20;
      if (paramPlayer.isClanLeader()) {
        
        this.amC = true;
        this.abA |= 0x40;
      } 
    } 
    
    for (GlobalEvent globalEvent : paramPlayer.getEvents())
    {
      this.abA = globalEvent.getUserRelation(paramPlayer, this.abA);
    }
    
    this._loc = paramPlayer.getLoc();
    this.acr = paramPlayer.getObjectId();
    this.amt = paramPlayer.isInBoat() ? paramPlayer.getBoat().getObjectId() : 0;
    this.Bt = paramPlayer.getRace().ordinal();
    this.sex = paramPlayer.getSex();
    this.aby = paramPlayer.getBaseClassId();
    this.level = paramPlayer.getLevel();
    this._exp = paramPlayer.getExp();
    this.ain = Experience.getExpPercent(paramPlayer.getLevel(), paramPlayer.getExp());
    this.ahG = paramPlayer.getSTR();
    this.ahI = paramPlayer.getDEX();
    this.ahH = paramPlayer.getCON();
    this.ahJ = paramPlayer.getINT();
    this.ahK = paramPlayer.getWIT();
    this.ahL = paramPlayer.getMEN();
    this.curHp = (int)paramPlayer.getCurrentHp();
    this.maxHp = paramPlayer.getMaxHp();
    this.curMp = (int)paramPlayer.getCurrentMp();
    this.maxMp = paramPlayer.getMaxMp();
    this.aho = paramPlayer.getCurrentLoad();
    this.ahp = paramPlayer.getMaxLoad();
    this._sp = paramPlayer.getIntSp();
    this.ahO = paramPlayer.getPAtk(null);
    this.ahP = paramPlayer.getPAtkSpd();
    this.ahQ = paramPlayer.getPDef(null);
    this.ahR = paramPlayer.getEvasionRate(null);
    this.ahS = paramPlayer.getAccuracy();
    this.ahT = paramPlayer.getCriticalHit(null, null);
    this.ahU = paramPlayer.getMAtk(null, null);
    this.ahV = paramPlayer.getMAtkSpd();
    this.ahW = paramPlayer.getMDef(null, null);
    this.pvp_flag = paramPlayer.getPvpFlag();
    this.karma = -Math.min(paramPlayer.getKarma(), 999999);
    this.aih = paramPlayer.getAttackSpeedMultiplier();
    this.abC = paramPlayer.getColRadius();
    this.abD = paramPlayer.getColHeight();
    this.abE = paramPlayer.getHairStyle();
    this.abF = paramPlayer.getHairColor();
    this.abG = paramPlayer.getFace();
    this.ahX = (paramPlayer.isGM() || (paramPlayer.getPlayerAccess()).CanUseGMCommand) ? 1 : 0;
    
    this.clan_id = paramPlayer.getClanId();
    this.ally_id = paramPlayer.getAllyId();
    this.abM = paramPlayer.getPrivateStoreType();
    this.amx = (paramPlayer.getSkillLevel(Integer.valueOf(248)) > 0) ? 1 : 0;
    this.aic = paramPlayer.getPkKills();
    this.aid = paramPlayer.getPvpKills();
    this.acc = (EffectCubic[])paramPlayer.getCubics().toArray(new EffectCubic[paramPlayer.getCubics().size()]);
    this.amv = paramPlayer.getClanPrivileges();
    this.ahN = paramPlayer.getGivableRec();
    this.abz = paramPlayer.getReceivedRec();
    this.amw = paramPlayer.getInventoryLimit();
    this.class_id = paramPlayer.getClassId().getId();
    this.amy = ClassId.getClassById(this.class_id).getRootClassId().getId();
    this.maxCp = paramPlayer.getMaxCp();
    this.curCp = (int)paramPlayer.getCurrentCp();
    this._team = paramPlayer.getTeam();
    this.ahZ = (paramPlayer.isNoble() || (paramPlayer.isGM() && Config.GM_HERO_AURA)) ? 2 : 0;
    this.aia = (paramPlayer.isHero() || (paramPlayer.isGM() && Config.GM_HERO_AURA)) ? 2 : 0;
    
    this.DQ = paramPlayer.getFishLoc();
    this.aib = paramPlayer.getNameColor();
    this.running = paramPlayer.isRunning() ? 1 : 0;
    this.aif = paramPlayer.getPledgeClass();
    this.abU = paramPlayer.getPledgeType();
    this.ahY = paramPlayer.getTitleColor();
    this.amA = paramPlayer.getTransformation();
    this.aii = paramPlayer.getAttackElement();
    this.TQ = paramPlayer.getAttack(this.aii);
    this.TR = paramPlayer.getDefence(Element.FIRE);
    this.TS = paramPlayer.getDefence(Element.WATER);
    this.TT = paramPlayer.getDefence(Element.WIND);
    this.TU = paramPlayer.getDefence(Element.EARTH);
    this.TV = paramPlayer.getDefence(Element.HOLY);
    this.TW = paramPlayer.getDefence(Element.UNHOLY);
    this.amz = paramPlayer.getAgathionId();

    
    this.amq = (paramPlayer.getMatchingRoom() != null && paramPlayer.getMatchingRoom().getType() == MatchingRoom.PARTY_MATCHING && paramPlayer.getMatchingRoom().getLeader() == paramPlayer);
    this.amD = paramPlayer.isInFlyingTransform() ? 2 : (paramPlayer.isInWater() ? 1 : 0);
    this.ail = paramPlayer.getTalismanCount();


    
    for (UserInfoType userInfoType : paramVarArgs) {
      
      addComponentType(new UserInfoType[] { userInfoType });
    } 
    
    this.can_writeImpl = true;
  }



  
  protected byte[] getMasks() { return this.aaE; }




  
  protected void onNewMaskAdded(UserInfoType paramUserInfoType) { a(paramUserInfoType); }


  
  private void a(UserInfoType paramUserInfoType) {
    switch (paramUserInfoType) {

      
      case BASIC_INFO:
        this.aaF += paramUserInfoType.getBlockLength() + this._name.length() * 2;
        return;

      
      case CLAN:
        this.aaF += paramUserInfoType.getBlockLength() + this.title.length() * 2;
        return;
    } 

    
    this.aaF += paramUserInfoType.getBlockLength();
  }





  
  protected final void writeImpl() {
    if (!this.can_writeImpl) {
      return;
    }

    
    writeC(50);
    
    writeD(this.acr);
    writeD(this.aaF);
    writeH(24);
    writeB(this.aaE);
    
    if (containsMask(UserInfoType.RELATION))
    {
      writeD(this.abA);
    }
    
    if (containsMask(UserInfoType.BASIC_INFO)) {
      
      writeH(16 + this._name.length() * 2);
      writeStringWithSize(this._name);
      writeC(this.ahX);
      writeC(this.Bt);
      writeC(this.sex);
      writeD(this.aby);
      writeD(this.class_id);
      writeC(this.level);
    } 
    
    if (containsMask(UserInfoType.BASE_STATS)) {
      
      writeH(18);
      writeH(this.ahG);
      writeH(this.ahI);
      writeH(this.ahH);
      writeH(this.ahJ);
      writeH(this.ahK);
      writeH(this.ahL);
      writeH(0);
      writeH(0);
    } 
    
    if (containsMask(UserInfoType.MAX_HPCPMP)) {
      
      writeH(14);
      writeD(this.maxHp);
      writeD(this.maxMp);
      writeD(this.maxCp);
    } 
    
    if (containsMask(UserInfoType.CURRENT_HPMPCP_EXP_SP)) {
      
      writeH(38);
      writeD(this.curHp);
      writeD(this.curMp);
      writeD(this.curCp);
      writeQ(this._sp);
      writeQ(this._exp);
      writeF(this.ain);
    } 
    
    if (containsMask(UserInfoType.ENCHANTLEVEL)) {
      
      writeH(4);
      writeC(this.abN);
      writeC(this.abO);
    } 
    
    if (containsMask(UserInfoType.APPAREANCE)) {
      
      writeH(15);
      writeD(this.abE);
      writeD(this.abF);
      writeD(this.abG);
      writeC(1);
    } 
    
    if (containsMask(UserInfoType.STATUS)) {
      
      writeH(6);
      writeC(this.abS);
      writeC(this.abM);
      writeC(this.amx);
      writeC(0);
    } 
    
    if (containsMask(UserInfoType.STATS)) {
      
      writeH(56);
      writeH(this.amu);
      writeD(this.ahO);
      writeD(this.ahP);
      writeD(this.ahQ);
      writeD(this.ahR);
      writeD(this.ahS);
      writeD(this.ahT);
      writeD(this.ahU);
      writeD(this.ahV);
      writeD(this.ahP);
      writeD(0);
      writeD(this.ahW);
      writeD(0);
      writeD(0);
    } 
    
    if (containsMask(UserInfoType.ELEMENTALS)) {
      
      writeH(14);
      writeH(this.TR);
      writeH(this.TS);
      writeH(this.TT);
      writeH(this.TU);
      writeH(this.TV);
      writeH(this.TW);
    } 
    
    if (containsMask(UserInfoType.POSITION)) {
      
      writeH(18);
      writeD(this._loc.x);
      writeD(this._loc.y);
      writeD(this._loc.z + Config.CLIENT_Z_SHIFT);
      writeD(this.amt);
    } 
    
    if (containsMask(UserInfoType.SPEED)) {
      
      writeH(18);
      writeH(this.abq);
      writeH(this.abr);
      writeH(this.amr);
      writeH(this.ams);
      writeH(this.abt);
      writeH(this.abu);
      writeH(this.abv);
      writeH(this.abw);
    } 
    
    if (containsMask(UserInfoType.MULTIPLIER)) {
      
      writeH(18);
      writeF(this.aig);
      writeF(this.aih);
    } 
    
    if (containsMask(UserInfoType.COL_RADIUS_HEIGHT)) {
      
      writeH(18);
      writeF(this.abC);
      writeF(this.abD);
    } 
    
    if (containsMask(UserInfoType.ATK_ELEMENTAL)) {
      
      writeH(5);
      writeC(this.aii.getId());
      writeH(this.TQ);
    } 
    
    if (containsMask(UserInfoType.CLAN)) {
      
      writeH(32 + this.title.length() * 2);
      writeStringWithSize(this.title);
      writeH(this.abU);
      writeD(this.clan_id);
      writeD(this.abH);
      writeD(this.clan_crest_id);
      writeD(this.amv);
      writeC(this.amC ? 1 : 0);
      writeD(this.ally_id);
      writeD(this.ally_crest_id);
      writeC(this.amq ? 1 : 0);
    } 
    
    if (containsMask(UserInfoType.SOCIAL)) {
      
      writeH(22);
      writeC(this.pvp_flag);
      writeD(this.karma);
      writeC(this.ahZ);
      writeC(this.aia);
      writeC(this.aif);
      writeD(this.aic);
      writeD(this.aid);
      writeH(this.ahN);
      writeH(this.abz);
    } 
    
    if (containsMask(UserInfoType.VITA_FAME)) {
      
      writeH(15);
      writeD(0);
      writeC(0);
      writeD(0);
      writeD(0);
    } 
    
    if (containsMask(UserInfoType.SLOTS)) {
      
      writeH(12);
      writeC(this.ail);
      writeC(0);
      writeC(this._team.ordinal());
      writeD(0);
      writeC(0);
      writeC(0);
      writeC(0);
    } 
    
    if (containsMask(UserInfoType.MOVEMENTS)) {
      
      writeH(4);
      writeC(this.amD);
      writeC(this.running);
    } 
    
    if (containsMask(UserInfoType.COLOR)) {
      
      writeH(10);
      writeD(this.aib);
      writeD(this.ahY);
    } 
    
    if (containsMask(UserInfoType.INVENTORY_LIMIT)) {
      
      writeH(9);
      writeH(0);
      writeH(0);
      writeH(this.amw);
      writeC(this.abW);
    } 
    
    if (containsMask(UserInfoType.TRUE_HERO)) {
      
      writeH(9);
      writeD(0);
      writeH(0);
      writeC(0);
    } 
    
    if (containsMask(UserInfoType.ATT_SPIRITS)) {
      
      writeH(26);
      writeD(0);
      writeD(0);
      writeD(0);
      writeD(0);
      writeD(0);
      writeD(0);
    } 
  }
}
