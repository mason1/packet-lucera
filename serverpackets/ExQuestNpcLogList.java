package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import l2.gameserver.model.quest.QuestNpcLogInfo;
import l2.gameserver.model.quest.QuestState;


public class ExQuestNpcLogList
  extends L2GameServerPacket
{
  private int _questId;
  private List<int[]> afN;
  
  public ExQuestNpcLogList(QuestState paramQuestState) {
    this.afN = Collections.emptyList();


    
    this._questId = paramQuestState.getQuest().getQuestIntId();
    int i = paramQuestState.getCond();
    List list = paramQuestState.getQuest().getNpcLogList(i);
    if (list == null) {
      return;
    }
    this.afN = new ArrayList(list.size());
    for (QuestNpcLogInfo questNpcLogInfo : list) {
      
      int[] arrayOfInt = new int[2];
      arrayOfInt[0] = questNpcLogInfo.getNpcIds()[0] + 1000000;
      arrayOfInt[1] = paramQuestState.getInt(questNpcLogInfo.getVarName());
      this.afN.add(arrayOfInt);
    } 
  }


  
  protected void writeImpl() {
    writeEx(197);
    writeD(this._questId);
    writeC(this.afN.size());
    for (byte b = 0; b < this.afN.size(); b++) {
      
      int[] arrayOfInt = (int[])this.afN.get(b);
      writeD(arrayOfInt[0]);
      writeC(0);
      writeD(arrayOfInt[1]);
    } 
  }
}
