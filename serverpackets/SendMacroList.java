package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.actor.instances.player.Macro;



























public class SendMacroList
  extends L2GameServerPacket
{
  private final MacroUpdateType akW;
  private final int _count;
  private final Macro Zo;
  
  public SendMacroList(MacroUpdateType paramMacroUpdateType, int paramInt, Macro paramMacro) {
    this.akW = paramMacroUpdateType;
    this._count = paramInt;
    this.Zo = paramMacro;
  }


  
  protected final void writeImpl() {
    writeC(232);
    
    writeC(this.akW.getId());
    writeD((this.akW != MacroUpdateType.LIST) ? this.Zo.id : 0);
    writeC(this._count);
    writeC((this.Zo != null) ? 1 : 0);
    
    if (this.Zo != null && this.akW != MacroUpdateType.DELETE) {
      
      writeD(this.Zo.id);
      writeS(this.Zo.name);
      writeS(this.Zo.descr);
      writeS(this.Zo.acronym);
      writeD(this.Zo.icon);
      
      writeC(this.Zo.commands.length);
      
      for (byte b = 0; b < this.Zo.commands.length; b++) {
        
        Macro.L2MacroCmd l2MacroCmd = this.Zo.commands[b];
        writeC(b + true);
        writeC(l2MacroCmd.type);
        writeD(l2MacroCmd.d1);
        writeC(l2MacroCmd.d2);
        writeS(l2MacroCmd.cmd);
      } 
    } 
  }
}
