package l2.gameserver.network.l2.s2c;

import l2.gameserver.utils.Location;

public class ExJumpToLocation
  extends L2GameServerPacket
{
  private int Bq;
  private Location ach;
  private Location _destination;
  
  public ExJumpToLocation(int paramInt, Location paramLocation1, Location paramLocation2) {
    this.Bq = paramInt;
    this.ach = paramLocation1;
    this._destination = paramLocation2;
  }


  
  protected final void writeImpl() {
    writeEx(136);
    
    writeD(this.Bq);
    
    writeD(this._destination.x);
    writeD(this._destination.y);
    writeD(this._destination.z);
    
    writeD(this.ach.x);
    writeD(this.ach.y);
    writeD(this.ach.z);
  }
}
