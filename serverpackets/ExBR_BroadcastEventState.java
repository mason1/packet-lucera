package l2.gameserver.network.l2.s2c;


public class ExBR_BroadcastEventState
  extends L2GameServerPacket
{
  private int adm;
  private int adn;
  private int ado;
  private int YT;
  private int YU;
  private int adp;
  private int adq;
  private String adr;
  private String ads;
  public static final int APRIL_FOOLS = 20090401;
  public static final int EVAS_INFERNO = 20090801;
  public static final int HALLOWEEN_EVENT = 20091031;
  public static final int RAISING_RUDOLPH = 20091225;
  public static final int LOVERS_JUBILEE = 20100214;
  public static final int APRIL_FOOLS_10 = 20100401;
  
  public ExBR_BroadcastEventState(int paramInt1, int paramInt2) {
    this.adm = paramInt1;
    this.adn = paramInt2;
  }

  
  public ExBR_BroadcastEventState(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, String paramString1, String paramString2) {
    this.adm = paramInt1;
    this.adn = paramInt2;
    this.ado = paramInt3;
    this.YT = paramInt4;
    this.YU = paramInt5;
    this.adp = paramInt6;
    this.adq = paramInt7;
    this.adr = paramString1;
    this.ads = paramString2;
  }


  
  protected void writeImpl() {
    writeEx(188);
    writeD(this.adm);
    writeD(this.adn);
    writeD(this.ado);
    writeD(this.YT);
    writeD(this.YU);
    writeD(this.adp);
    writeD(this.adq);
    writeS(this.adr);
    writeS(this.ads);
  }
}
