package l2.gameserver.network.l2.s2c;

public class ExPutEnchantTargetItemResult
  extends L2GameServerPacket {
  public static final L2GameServerPacket FAIL = new ExPutEnchantTargetItemResult(0);
  public static final L2GameServerPacket SUCCESS = new ExPutEnchantTargetItemResult(1);

  
  private int oH;

  
  public ExPutEnchantTargetItemResult(int paramInt) { this.oH = paramInt; }



  
  protected void writeImpl() {
    writeEx(130);
    writeD(this.oH);
  }
}
