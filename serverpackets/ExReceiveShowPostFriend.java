package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import org.napile.primitive.maps.IntObjectMap;






public class ExReceiveShowPostFriend
  extends L2GameServerPacket
{
  private IntObjectMap<String> afU;
  
  public ExReceiveShowPostFriend(Player paramPlayer) { this.afU = paramPlayer.getPostFriends(); }



  
  public void writeImpl() {
    writeEx(212);
    writeD(this.afU.size());
    for (String str : this.afU.values())
      writeS(str); 
  }
}
