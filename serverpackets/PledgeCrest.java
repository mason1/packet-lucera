package l2.gameserver.network.l2.s2c;

import l2.gameserver.Config;

public class PledgeCrest
  extends L2GameServerPacket
{
  private int Vv;
  private int ajR;
  private byte[] _data;
  
  public PledgeCrest(int paramInt, byte[] paramArrayOfByte) {
    this.Vv = paramInt;
    this._data = paramArrayOfByte;
    this.ajR = this._data.length;
  }


  
  protected final void writeImpl() {
    writeC(106);
    writeD(Config.REQUEST_ID);
    writeD(this.Vv);
    writeD(this.ajR);
    writeB(this._data);
  }
}
