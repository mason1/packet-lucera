package l2.gameserver.network.l2.s2c.mask;

import l2.gameserver.network.l2.s2c.L2GameServerPacket;




public abstract class AbstractMaskPacket<T extends IUpdateTypeComponent>
  extends L2GameServerPacket
{
  protected static final byte[] DEFAULT_FLAG_ARRAY = { Byte.MIN_VALUE, 64, 32, 16, 8, 4, 2, 1 };





  
  protected abstract byte[] getMasks();





  
  protected void onNewMaskAdded(T paramT) {}




  
  @SafeVarargs
  public final void addComponentType(T... paramVarArgs) {
    for (T t : paramVarArgs) {
      
      if (!containsMask(t)) {
        
        addMask(t.getMask());
        onNewMaskAdded(t);
      } 
    } 
  }


  
  protected void addMask(int paramInt) { getMasks()[paramInt >> 3] = (byte)(getMasks()[paramInt >> 3] | DEFAULT_FLAG_ARRAY[paramInt & 0x7]); }



  
  public boolean containsMask(T paramT) { return containsMask(paramT.getMask()); }



  
  public boolean containsMask(int paramInt) { return ((getMasks()[paramInt >> 3] & DEFAULT_FLAG_ARRAY[paramInt & 0x7]) != 0); }








  
  public boolean containsMask(int paramInt, T paramT) { return ((paramInt & paramT.getMask()) == paramT.getMask()); }
}
