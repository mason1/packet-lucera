package l2.gameserver.network.l2.s2c.mask;

public interface IUpdateTypeComponent {
  int getMask();
}
