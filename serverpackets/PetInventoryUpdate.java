package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.ItemInstance;


public class PetInventoryUpdate
  extends AbstractItemListPacket
{
  public static final int UNCHANGED = 0;
  public static final int ADDED = 1;
  public static final int MODIFIED = 2;
  public static final int REMOVED = 3;
  private final List<ItemInfo> _items = new ArrayList(1);





  
  public PetInventoryUpdate addNewItem(ItemInstance paramItemInstance) {
    a(paramItemInstance).setLastChange(1);
    return this;
  }

  
  public PetInventoryUpdate addModifiedItem(ItemInstance paramItemInstance) {
    a(paramItemInstance).setLastChange(2);
    return this;
  }

  
  public PetInventoryUpdate addRemovedItem(ItemInstance paramItemInstance) {
    a(paramItemInstance).setLastChange(3);
    return this;
  }

  
  private ItemInfo a(ItemInstance paramItemInstance) {
    ItemInfo itemInfo;
    this._items.add(itemInfo = new ItemInfo(paramItemInstance));
    return itemInfo;
  }


  
  protected final void writeImpl() {
    writeC(180);
    writeH(this._items.size());
    for (ItemInfo itemInfo : this._items) {
      
      writeH(itemInfo.getLastChange());
      writeItemInfo(itemInfo);
    } 
  }
}
