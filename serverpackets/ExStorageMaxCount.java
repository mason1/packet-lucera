package l2.gameserver.network.l2.s2c;

import l2.gameserver.Config;
import l2.gameserver.model.Player;

public class ExStorageMaxCount
  extends L2GameServerPacket
{
  private int KP;
  private int KQ;
  private int KR;
  private int ahb;
  private int ahc;
  private int KT;
  private int KU;
  private int ahd;
  private int ahe;
  
  public ExStorageMaxCount(Player paramPlayer) {
    this.KP = paramPlayer.getInventoryLimit();
    this.KQ = paramPlayer.getWarehouseLimit();
    this.KR = Config.WAREHOUSE_SLOTS_CLAN;
    this.ahc = this.ahb = paramPlayer.getTradeLimit();
    this.KT = paramPlayer.getDwarvenRecipeLimit();
    this.KU = paramPlayer.getCommonRecipeLimit();
    this.ahd = paramPlayer.getBeltInventoryIncrease();
    this.ahe = Config.QUEST_INVENTORY_MAXIMUM;
  }


  
  protected final void writeImpl() {
    writeEx(47);
    
    writeD(this.KP);
    writeD(this.KQ);
    writeD(this.KR);
    writeD(this.ahb);
    writeD(this.ahc);
    writeD(this.KT);
    writeD(this.KU);
    writeD(this.ahd);
    writeD(this.ahe);
    writeD(40);
    writeD(40);
    writeD(100);
  }
}
