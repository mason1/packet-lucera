package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.entity.events.impl.DuelEvent;


public class ExDuelEnd
  extends L2GameServerPacket
{
  private int YA;
  
  public ExDuelEnd(DuelEvent paramDuelEvent) { this.YA = paramDuelEvent.getDuelType(); }



  
  protected final void writeImpl() {
    writeEx(80);
    writeD(this.YA);
  }
}
