package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.templates.PlayerTemplate;


public class NewCharacterSuccess
  extends L2GameServerPacket
{
  private List<PlayerTemplate> ajd = new ArrayList();


  
  public void addChar(PlayerTemplate paramPlayerTemplate) { this.ajd.add(paramPlayerTemplate); }



  
  protected final void writeImpl() {
    writeC(13);
    writeD(this.ajd.size());
    
    for (PlayerTemplate playerTemplate : this.ajd) {
      
      writeD(playerTemplate.race.ordinal());
      writeD(playerTemplate.classId.getId());
      writeD(70);
      writeD(playerTemplate.baseSTR);
      writeD(10);
      writeD(70);
      writeD(playerTemplate.baseDEX);
      writeD(10);
      writeD(70);
      writeD(playerTemplate.baseCON);
      writeD(10);
      writeD(70);
      writeD(playerTemplate.baseINT);
      writeD(10);
      writeD(70);
      writeD(playerTemplate.baseWIT);
      writeD(10);
      writeD(70);
      writeD(playerTemplate.baseMEN);
      writeD(10);
    } 
  }
}
