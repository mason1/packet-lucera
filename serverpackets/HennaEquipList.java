package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.data.xml.holder.HennaHolder;
import l2.gameserver.model.Player;
import l2.gameserver.templates.Henna;

public class HennaEquipList
  extends L2GameServerPacket {
  private int aiz;
  
  public HennaEquipList(Player paramPlayer) {
    this.aiA = new ArrayList();


    
    this.abb = paramPlayer.getAdena();
    this.aiz = paramPlayer.getHennaEmptySlots();
    
    List list = HennaHolder.getInstance().generateList(paramPlayer);
    for (Henna henna : list) {
      if (paramPlayer.getInventory().getItemByItemId(henna.getDyeId()) != null)
        this.aiA.add(henna); 
    } 
  }
  private long abb; private List<Henna> aiA;
  
  protected final void writeImpl() {
    writeC(238);
    
    writeQ(this.abb);
    writeD(this.aiz);
    if (this.aiA.size() != 0) {
      
      writeD(this.aiA.size());
      for (Henna henna : this.aiA)
      {
        writeD(henna.getSymbolId());
        writeD(henna.getDyeId());
        writeQ(henna.getDrawCount());
        writeQ(henna.getPrice());
        writeD(1);
      }
    
    } else {
      
      writeD(1);
      writeD(0);
      writeD(0);
      writeQ(0L);
      writeQ(0L);
      writeD(0);
    } 
  }
}
