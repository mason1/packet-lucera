package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import l2.gameserver.instancemanager.MatchingRoomManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.matching.MatchingRoom;





public class ListPartyWaiting
  extends L2GameServerPacket
{
  private Collection<MatchingRoom> aiO;
  private int aeO;
  
  public ListPartyWaiting(int paramInt1, boolean paramBoolean, int paramInt2, Player paramPlayer) {
    int i = (paramInt2 - 1) * 64;
    int j = paramInt2 * 64;
    this.aiO = new ArrayList();
    
    byte b = 0;
    List list = MatchingRoomManager.getInstance().getMatchingRooms(MatchingRoom.PARTY_MATCHING, paramInt1, paramBoolean, paramPlayer);
    this.aeO = list.size();
    for (MatchingRoom matchingRoom : list) {
      
      if (b < i || b >= j)
        continue; 
      this.aiO.add(matchingRoom);
      b++;
    } 
  }


  
  protected final void writeImpl() {
    writeC(156);
    writeD(this.aeO);
    writeD(this.aiO.size());
    
    for (MatchingRoom matchingRoom : this.aiO) {
      
      writeD(matchingRoom.getId());
      writeS((matchingRoom.getLeader() == null) ? "None" : matchingRoom.getLeader().getName());
      writeD(matchingRoom.getLocationId());
      writeD(matchingRoom.getMinLevel());
      writeD(matchingRoom.getMaxLevel());
      writeD(matchingRoom.getMaxMembersSize());
      writeS(matchingRoom.getTopic());
      
      Collection collection = matchingRoom.getPlayers();
      writeD(collection.size());
      for (Player player : collection) {
        
        writeD(player.getClassId().getId());
        writeS(player.getName());
      } 
    } 
  }
}
