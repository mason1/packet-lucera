package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Summon;

public class PetStatusShow
  extends L2GameServerPacket
{
  private int afA;
  private int afz;
  
  public PetStatusShow(Summon paramSummon) {
    this.afA = paramSummon.getSummonType();
    this.afz = paramSummon.getObjectId();
  }


  
  protected final void writeImpl() {
    writeC(177);
    writeD(this.afA);
    writeD(this.afz);
  }
}
