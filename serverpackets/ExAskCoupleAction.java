package l2.gameserver.network.l2.s2c;

public class ExAskCoupleAction
  extends L2GameServerPacket {
  private int Bq;
  private int adi;
  
  public ExAskCoupleAction(int paramInt1, int paramInt2) {
    this.Bq = paramInt1;
    this.adi = paramInt2;
  }


  
  protected void writeImpl() {
    writeEx(188);
    writeD(this.adi);
    writeD(this.Bq);
  }
}
