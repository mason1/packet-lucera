package l2.gameserver.network.l2.s2c;


public class RequestNetPing
  extends L2GameServerPacket
{
  private final int akP;
  
  public RequestNetPing(int paramInt) { this.akP = paramInt; }



  
  protected void writeImpl() {
    writeC(217);
    writeD(this.akP);
  }
}
