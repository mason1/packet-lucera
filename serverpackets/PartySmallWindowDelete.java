package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;

public class PartySmallWindowDelete
  extends L2GameServerPacket
{
  private final int abx;
  private final String _name;
  
  public PartySmallWindowDelete(Player paramPlayer) {
    this.abx = paramPlayer.getObjectId();
    this._name = paramPlayer.getName();
  }


  
  protected final void writeImpl() {
    writeC(81);
    writeD(this.abx);
    writeS(this._name);
  }
}
