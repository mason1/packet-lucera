package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;














public class AbnormalStatusUpdate
  extends L2GameServerPacket
{
  public static final int INFINITIVE_EFFECT = -1;
  
  public static class Effect
  {
    int skillId;
    int skillLevel;
    int duration;
    int clientAbnormalId;
    int effectorObjectId;
    
    public Effect(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5) {
      this.skillId = param1Int1;
      this.skillLevel = param1Int2;
      this.duration = param1Int3;
      this.clientAbnormalId = param1Int4;
      this.effectorObjectId = param1Int5;
    }
  }




  
  private List<Effect> DF = new ArrayList();




  
  public void addEffect(int paramInt1, int paramInt2, int paramInt3) { this.DF.add(new Effect(paramInt1, paramInt2, paramInt3, 0, 0)); }



  
  protected final void writeImpl() {
    writeC(133);
    
    writeH(this.DF.size());
    
    for (Effect effect : this.DF) {
      
      writeD(effect.skillId);
      writeH(effect.skillLevel);
      writeD(effect.clientAbnormalId);
      writeOptionalD(effect.duration);
    } 
  }
}
