package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.network.l2.components.NpcString;


public class ExSendUIEvent
  extends NpcStringContainer
{
  private int Bq;
  private boolean agg;
  private boolean agh;
  private int agi;
  private int agj;
  
  public ExSendUIEvent(Player paramPlayer, boolean paramBoolean1, boolean paramBoolean2, int paramInt1, int paramInt2, String... paramVarArgs) { this(paramPlayer, paramBoolean1, paramBoolean2, paramInt1, paramInt2, NpcString.NONE, paramVarArgs); }


  
  public ExSendUIEvent(Player paramPlayer, boolean paramBoolean1, boolean paramBoolean2, int paramInt1, int paramInt2, NpcString paramNpcString, String... paramVarArgs) {
    super(paramNpcString, paramVarArgs);
    this.Bq = paramPlayer.getObjectId();
    this.agg = paramBoolean1;
    this.agh = paramBoolean2;
    this.agi = paramInt1;
    this.agj = paramInt2;
  }


  
  protected void writeImpl() {
    writeEx(143);
    writeD(this.Bq);
    writeD(this.agg ? 1 : 0);
    writeD(0);
    writeD(0);
    writeS(this.agh ? "1" : "0");
    writeS(String.valueOf(this.agi / 60));
    writeS(String.valueOf(this.agi % 60));
    writeS(String.valueOf(this.agj / 60));
    writeS(String.valueOf(this.agj % 60));
    writeElements();
  }
}
