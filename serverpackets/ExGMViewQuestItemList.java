package l2.gameserver.network.l2.s2c;

import l2.gameserver.Config;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.ItemInstance;






public class ExGMViewQuestItemList
  extends AbstractItemListPacket
{
  private int _size;
  private final boolean aeE;
  private ItemInstance[] aeF;
  private int aeG;
  private String _name;
  
  public ExGMViewQuestItemList(boolean paramBoolean, Player paramPlayer, ItemInstance[] paramArrayOfItemInstance, int paramInt) {
    this.aeE = paramBoolean;
    this.aeF = paramArrayOfItemInstance;
    this._size = paramInt;
    this._name = paramPlayer.getName();
    this.aeG = Config.QUEST_INVENTORY_MAXIMUM;
  }


  
  protected final void writeImpl() {
    writeEx(200);
    if (this.aeE) {
      
      writeS(this._name);
      writeD(this.aeG);
    }
    else {
      
      writeD(this._size);
    } 
    
    writeD(this._size);
    for (ItemInstance itemInstance : this.aeF) {
      if (itemInstance.getTemplate().isQuest())
        writeItemInfo(new ItemInfo(itemInstance)); 
    } 
  }
}
