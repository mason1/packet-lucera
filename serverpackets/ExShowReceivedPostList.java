package l2.gameserver.network.l2.s2c;

import java.util.List;
import l2.commons.collections.CollectionUtils;
import l2.gameserver.dao.MailDAO;
import l2.gameserver.model.Player;
import l2.gameserver.model.mail.Mail;















public class ExShowReceivedPostList
  extends L2GameServerPacket
{
  private final List<Mail> agM;
  private static final int agN = 100;
  private static final int agO = 1000;
  
  public ExShowReceivedPostList(Player paramPlayer) {
    this.agM = MailDAO.getInstance().getReceivedMailByOwnerId(paramPlayer.getObjectId());
    CollectionUtils.shellSort(this.agM);
  }



  
  protected void writeImpl() {
    writeEx(171);
    writeD((int)(System.currentTimeMillis() / 1000L));
    writeD(this.agM.size());
    for (Mail mail : this.agM) {
      
      writeD(mail.getType().ordinal());
      writeD(mail.getMessageId());
      writeS(mail.getTopic());
      writeS(mail.getSenderName());
      
      writeD(mail.isPayOnDelivery() ? 1 : 0);
      writeD(mail.getExpireTime());
      writeD(mail.isUnread() ? 1 : 0);
      writeD((mail.getType() == Mail.SenderType.NORMAL) ? 0 : 1);
      writeD(mail.getAttachments().isEmpty() ? 0 : 1);
      writeD(0);
      writeD(0);
    } 
    
    writeD(100);
    writeD(1000);
  }
}
