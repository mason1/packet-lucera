package l2.gameserver.network.l2.s2c;

import java.util.List;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ManufactureItem;

public class RecipeShopSellList
  extends L2GameServerPacket {
  private int akN;
  private int curMp;
  private int maxMp;
  private long adc;
  private List<ManufactureItem> akI;
  
  public RecipeShopSellList(Player paramPlayer1, Player paramPlayer2) {
    this.akN = paramPlayer2.getObjectId();
    this.curMp = (int)paramPlayer2.getCurrentMp();
    this.maxMp = paramPlayer2.getMaxMp();
    this.adc = paramPlayer1.getAdena();
    this.akI = paramPlayer2.getCreateList();
  }


  
  protected final void writeImpl() {
    writeC(223);
    writeD(this.akN);
    writeD(this.curMp);
    writeD(this.maxMp);
    writeQ(this.adc);
    writeD(this.akI.size());
    for (ManufactureItem manufactureItem : this.akI) {
      
      writeD(manufactureItem.getRecipeId());
      writeD(0);
      writeQ(manufactureItem.getCost());
      
      writeQ(0L);
      writeQ(0L);
      writeC(0);
    } 
  }
}
