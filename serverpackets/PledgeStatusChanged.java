package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.pledge.Clan;






public class PledgeStatusChanged
  extends L2GameServerPacket
{
  private int aej;
  private int clan_id;
  private int level;
  
  public PledgeStatusChanged(Clan paramClan) {
    this.aej = paramClan.getLeaderId();
    this.clan_id = paramClan.getClanId();
    this.level = paramClan.getLevel();
  }


  
  protected final void writeImpl() {
    writeC(205);
    writeD(this.aej);
    writeD(this.clan_id);
    writeD(0);
    writeD(this.level);
    writeD(0);
    writeD(0);
    writeD(0);
  }
}
