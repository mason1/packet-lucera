package l2.gameserver.network.l2.s2c;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.model.pledge.Clan;

public class PledgeShowInfoUpdate extends L2GameServerPacket {
  private int clan_id;
  private int aio;
  private int ajW;
  
  public PledgeShowInfoUpdate(Clan paramClan) {
    this.ally_name = "";




    
    this.clan_id = paramClan.getClanId();
    this.aio = paramClan.getLevel();
    this.ajZ = paramClan.getCastle();
    this.aka = paramClan.getHasHideout();
    
    this.ajW = paramClan.getRank();
    this.ajX = paramClan.getReputationScore();
    this.crest_id = paramClan.getCrestId();
    this.ally_id = paramClan.getAllyId();
    this.ajY = paramClan.isAtWar();
    
    this.akc = paramClan.isPlacedForDisband();
    Alliance alliance = paramClan.getAlliance();
    if (alliance != null) {
      
      this.ally_name = alliance.getAllyName();
      this.ally_crest = alliance.getAllyCrestId();
    } 
  }
  private int ajX; private int crest_id; private int ally_id; private int ally_crest; private int ajY; private int aiu; private String ally_name; private int ajZ; private int aka; private int akb;
  private boolean akc;
  
  protected final void writeImpl() {
    writeC(142);
    
    writeD(this.clan_id);
    writeD(Config.REQUEST_ID);
    writeD(this.crest_id);
    writeD(this.aio);
    writeD(this.ajZ);
    writeD(0);
    writeD(this.aka);
    writeD(0);
    writeD(this.ajW);
    writeD(this.ajX);
    writeD(this.akc ? 3 : 0);
    writeD(0);
    writeD(this.ally_id);
    writeS(this.ally_name);
    writeD(this.ally_crest);
    writeD(this.ajY);
    
    writeD(0);
    writeD(0);
  }
}
