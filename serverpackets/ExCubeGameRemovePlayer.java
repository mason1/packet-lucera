package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;







public class ExCubeGameRemovePlayer
  extends L2GameServerPacket
{
  private int Bq;
  private boolean _isRedTeam;
  
  public ExCubeGameRemovePlayer(Player paramPlayer, boolean paramBoolean) {
    this.Bq = paramPlayer.getObjectId();
    this._isRedTeam = paramBoolean;
  }


  
  protected void writeImpl() {
    writeEx(151);
    writeD(2);
    
    writeD(-1);
    
    writeD(this._isRedTeam ? 1 : 0);
    writeD(this.Bq);
  }
}
