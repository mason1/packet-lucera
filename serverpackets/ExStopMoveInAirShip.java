package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.utils.Location;

public class ExStopMoveInAirShip extends L2GameServerPacket {
  private int char_id;
  private int aeS;
  private int aha;
  private Location _loc;
  
  public ExStopMoveInAirShip(Player paramPlayer) {
    this.char_id = paramPlayer.getObjectId();
    this.aeS = paramPlayer.getBoat().getObjectId();
    this._loc = paramPlayer.getInBoatPosition();
    this.aha = paramPlayer.getHeading();
  }


  
  protected final void writeImpl() {
    writeEx(110);
    
    writeD(this.char_id);
    writeD(this.aeS);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
    writeD(this.aha);
  }
}
