package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;


public class EtcStatusUpdate
  extends L2GameServerPacket
{
  private static final int acH = 1;
  private static final int acI = 2;
  private static final int acJ = 4;
  private int acK;
  private int acL;
  
  public EtcStatusUpdate(Player paramPlayer) {
    this.acK = paramPlayer.getIncreasedForce();
    this.acL = paramPlayer.getWeightPenalty();
    this.acM = paramPlayer.getArmorsExpertisePenalty();
    this.acN = paramPlayer.getWeaponsExpertisePenalty();
    this.acO = (paramPlayer.getDeathPenalty() == null) ? 0 : paramPlayer.getDeathPenalty().getLevel();
    this.acP = paramPlayer.getConsumedSouls();
    
    boolean bool = (paramPlayer.getMessageRefusal() || paramPlayer.getNoChannel() > 0L || paramPlayer.isBlockAll()) ? 1 : 0;
    boolean bool1 = paramPlayer.isCharmOfCourage();
    boolean bool2 = paramPlayer.isInDangerArea();
    
    if (bool)
    {
      this.mask |= 0x1;
    }
    
    if (bool2)
    {
      this.mask |= 0x2;
    }
    
    if (bool1)
    {
      this.mask |= 0x4; } 
  }
  private int acM; private int acN; private int acO;
  private int acP;
  private int mask;
  
  protected final void writeImpl() {
    writeC(249);
    writeC(this.acK);
    writeD(this.acL);
    writeC(this.acN);
    writeC(this.acM);
    writeC(this.acO);
    writeC(this.acP);
    writeC(this.mask);
  }
}
