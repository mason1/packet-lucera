package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import l2.gameserver.model.Player;





public class BlockList
  extends L2GameServerPacket
{
  private List<String[]> aba = new ArrayList();

  
  public BlockList(Player paramPlayer) {
    Collection collection = paramPlayer.getBlockList();
    for (String str : collection) {

      
      this.aba.add(new String[] { str, "" });
    } 
  }


  
  protected void writeImpl() {
    writeC(213);
    writeD(this.aba.size());
    for (String[] arrayOfString : this.aba) {
      
      writeS(arrayOfString[0]);
      writeS(arrayOfString[1]);
    } 
  }
}
