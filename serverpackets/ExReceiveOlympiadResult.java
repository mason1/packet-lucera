package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import l2.gameserver.model.Player;
import l2.gameserver.model.entity.oly.Participant;




public class ExReceiveOlympiadResult
  extends L2GameServerPacket
{
  private String afQ;
  private int afR;
  private ArrayList<ExReceiveOlympiadResultRecord> afS;
  private ArrayList<ExReceiveOlympiadResultRecord> afT;
  
  public ExReceiveOlympiadResult(int paramInt, String paramString) {
    this.afQ = paramString;
    this.afR = paramInt;
    this.afS = new ArrayList();
    this.afT = new ArrayList();
  }


  
  public void add(int paramInt1, Player paramPlayer, int paramInt2, int paramInt3, int paramInt4) {
    if (paramInt1 == Participant.SIDE_RED)
      this.afS.add(new ExReceiveOlympiadResultRecord(paramPlayer, paramInt2, paramInt3, paramInt4)); 
    if (paramInt1 == Participant.SIDE_BLUE) {
      this.afT.add(new ExReceiveOlympiadResultRecord(paramPlayer, paramInt2, paramInt3, paramInt4));
    }
  }


  
  protected void writeImpl() {
    writeEx(213);
    writeD(1);
    
    if (this.afR != 0) {
      
      writeD(0);
      writeS(this.afQ);
    }
    else {
      
      writeD(1);
      writeS("");
    } 
    
    if (this.afR == Participant.SIDE_RED) {
      
      writeD(1);
      writeD(this.afS.size());
      
      for (ExReceiveOlympiadResultRecord exReceiveOlympiadResultRecord : this.afS) {
        
        writeS(exReceiveOlympiadResultRecord.name);
        writeS(exReceiveOlympiadResultRecord.clan);
        writeD(exReceiveOlympiadResultRecord.crest_id);
        writeD(exReceiveOlympiadResultRecord.class_id);
        writeD(exReceiveOlympiadResultRecord.dmg);
        writeD(exReceiveOlympiadResultRecord.points);
        writeD(exReceiveOlympiadResultRecord.delta);
        writeD(0);
      } 
      
      writeD(0);
      writeD(this.afT.size());
      
      for (ExReceiveOlympiadResultRecord exReceiveOlympiadResultRecord : this.afT)
      {
        writeS(exReceiveOlympiadResultRecord.name);
        writeS(exReceiveOlympiadResultRecord.clan);
        writeD(exReceiveOlympiadResultRecord.crest_id);
        writeD(exReceiveOlympiadResultRecord.class_id);
        writeD(exReceiveOlympiadResultRecord.dmg);
        writeD(exReceiveOlympiadResultRecord.points);
        writeD(exReceiveOlympiadResultRecord.delta);
        writeD(0);
      }
    
    } else {
      
      writeD(0);
      writeD(this.afT.size());
      
      for (ExReceiveOlympiadResultRecord exReceiveOlympiadResultRecord : this.afT) {
        
        writeS(exReceiveOlympiadResultRecord.name);
        writeS(exReceiveOlympiadResultRecord.clan);
        writeD(exReceiveOlympiadResultRecord.crest_id);
        writeD(exReceiveOlympiadResultRecord.class_id);
        writeD(exReceiveOlympiadResultRecord.dmg);
        writeD(exReceiveOlympiadResultRecord.points);
        writeD(exReceiveOlympiadResultRecord.delta);
        writeD(0);
      } 
      
      writeD(1);
      writeD(this.afS.size());
      
      for (ExReceiveOlympiadResultRecord exReceiveOlympiadResultRecord : this.afS) {
        
        writeS(exReceiveOlympiadResultRecord.name);
        writeS(exReceiveOlympiadResultRecord.clan);
        writeD(exReceiveOlympiadResultRecord.crest_id);
        writeD(exReceiveOlympiadResultRecord.class_id);
        writeD(exReceiveOlympiadResultRecord.dmg);
        writeD(exReceiveOlympiadResultRecord.points);
        writeD(exReceiveOlympiadResultRecord.delta);
        writeD(0);
      } 
    } 
  }
  
  private class ExReceiveOlympiadResultRecord
  {
    String name;
    String clan;
    int class_id;
    
    public ExReceiveOlympiadResultRecord(Player param1Player, int param1Int1, int param1Int2, int param1Int3) {
      this.name = param1Player.getName();
      this.class_id = param1Player.getClassId().getId();
      this.clan = (param1Player.getClan() != null) ? param1Player.getClan().getName() : "";
      this.crest_id = param1Player.getClanId();
      this.dmg = param1Int1;
      this.points = param1Int2;
      this.delta = param1Int3;
    }
    
    int crest_id;
    int dmg;
    int points;
    int delta;
  }
}
