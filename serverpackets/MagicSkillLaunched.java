package l2.gameserver.network.l2.s2c;

import java.util.Collection;
import java.util.Collections;
import l2.gameserver.model.Creature;
import l2.gameserver.model.Player;
import l2.gameserver.model.Skill;


public class MagicSkillLaunched
  extends L2GameServerPacket
{
  private final int aiR;
  private final int _skillId;
  private final int _skillLevel;
  private final int aiS;
  private final int aiT;
  private final Collection<Creature> aiU;
  
  public MagicSkillLaunched(Creature paramCreature1, int paramInt1, int paramInt2, Creature paramCreature2) {
    this.aiR = paramCreature1.getObjectId();
    this.aiS = paramCreature1.getX();
    this.aiT = paramCreature1.getY();
    this._skillId = paramInt1;
    this._skillLevel = paramInt2;
    this.aiU = Collections.singletonList(paramCreature2);
  }

  
  public MagicSkillLaunched(Creature paramCreature1, Skill paramSkill, Creature paramCreature2) {
    this.aiR = paramCreature1.getObjectId();
    this.aiS = paramCreature1.getX();
    this.aiT = paramCreature1.getY();
    this._skillId = paramSkill.getDisplayId();
    this._skillLevel = paramSkill.getDisplayLevel();
    this.aiU = Collections.singletonList(paramCreature2);
  }

  
  public MagicSkillLaunched(Creature paramCreature, int paramInt1, int paramInt2, Collection<Creature> paramCollection) {
    this.aiR = paramCreature.getObjectId();
    this.aiS = paramCreature.getX();
    this.aiT = paramCreature.getY();
    this._skillId = paramInt1;
    this._skillLevel = paramInt2;
    this.aiU = paramCollection;
  }

  
  public MagicSkillLaunched(Creature paramCreature, Skill paramSkill, Collection<Creature> paramCollection) {
    this.aiR = paramCreature.getObjectId();
    this.aiS = paramCreature.getX();
    this.aiT = paramCreature.getY();
    this._skillId = paramSkill.getDisplayId();
    this._skillLevel = paramSkill.getDisplayLevel();
    this.aiU = paramCollection;
  }


  
  protected final void writeImpl() {
    writeC(84);
    writeD(0);
    writeD(this.aiR);
    writeD(this._skillId);
    writeD(this._skillLevel);
    writeD(this.aiU.size());
    for (Creature creature : this.aiU) {
      if (creature != null) {
        writeD(creature.getObjectId());
      }
    } 
  }
  
  public L2GameServerPacket packet(Player paramPlayer) {
    if (paramPlayer != null && !paramPlayer.isInObserverMode()) {
      
      if (paramPlayer.buffAnimRange() < 0)
        return null; 
      if (paramPlayer.buffAnimRange() == 0) {
        return (this.aiR == paramPlayer.getObjectId()) ? super.packet(paramPlayer) : null;
      }
      return (paramPlayer.getDistance(this.aiS, this.aiT) < paramPlayer.buffAnimRange()) ? super.packet(paramPlayer) : null;
    } 
    
    return super.packet(paramPlayer);
  }
}
