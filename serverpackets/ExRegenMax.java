package l2.gameserver.network.l2.s2c;

public class ExRegenMax
  extends L2GameServerPacket
{
  private double afV;
  private int _count;
  private int _time;
  
  public ExRegenMax(double paramDouble, int paramInt1, int paramInt2) {
    this.afV = paramDouble * 0.66D;
    this._count = paramInt1;
    this._time = paramInt2;
  }




  
  public static final int POTION_HEALING_GREATER = 16457;



  
  public static final int POTION_HEALING_MEDIUM = 16440;



  
  public static final int POTION_HEALING_LESSER = 16416;




  
  protected void writeImpl() {
    writeEx(1);
    writeD(1);
    writeD(this._count);
    writeD(this._time);
    writeF(this.afV);
  }
}
