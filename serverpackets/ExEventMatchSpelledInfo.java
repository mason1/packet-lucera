package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Player;


public class ExEventMatchSpelledInfo
  extends L2GameServerPacket
{
  private int char_obj_id = 0;

  
  class Effect
  {
    int skillId;
    
    int dat;
    int duration;
    
    public Effect(int param1Int1, int param1Int2, int param1Int3) {
      this.skillId = param1Int1;
      this.dat = param1Int2;
      this.duration = param1Int3;
    }
  }


  
  private List<Effect> DF = new ArrayList();



  
  public void addEffect(int paramInt1, int paramInt2, int paramInt3) { this.DF.add(new Effect(paramInt1, paramInt2, paramInt3)); }


  
  public void addSpellRecivedPlayer(Player paramPlayer) {
    if (paramPlayer != null) {
      this.char_obj_id = paramPlayer.getObjectId();
    }
  }

  
  protected void writeImpl() {
    writeEx(4);
    
    writeD(this.char_obj_id);
    writeD(this.DF.size());
    for (Effect effect : this.DF) {
      
      writeD(effect.skillId);
      writeH(effect.dat);
      writeD(effect.duration);
    } 
  }
}
