package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.model.Player;
































public class RelationChanged
  extends L2GameServerPacket
{
  private static class RelationChangedData
  {
    public int objectId;
    public boolean isAutoAttackable;
    public int relation;
    public int karma;
    public int pvpFlag;
    
    private RelationChangedData() {}
  }
  protected final List<RelationChangedData> _data = new ArrayList(1);
  private byte akO = 1; public static final byte SEND_MULTI = 4; public static final byte SEND_DEFAULT = 1; public static final byte SEND_ONE = 0; public static final int USER_RELATION_IN_DOMINION_WAR = 4096; public static final int USER_RELATION_ATTACKER = 256; public static final int USER_RELATION_IN_SIEGE = 128; public static final int USER_RELATION_CLAN_LEADER = 64; public static final int USER_RELATION_CLAN_MEMBER = 32; public static final int RELATION_IN_DOMINION_WAR = 524288; public static final int RELATION_ALLY_MEMBER = 65536;
  public static final int RELATION_1SIDED_WAR = 32768;
  public static final int RELATION_MUTUAL_WAR = 16384;
  
  public RelationChanged add(Player paramPlayer1, Player paramPlayer2) {
    RelationChangedData relationChangedData = new RelationChangedData(null);
    relationChangedData.objectId = paramPlayer1.getObjectId();
    relationChangedData.karma = -paramPlayer1.getKarma();
    relationChangedData.pvpFlag = paramPlayer1.getPvpFlag();
    relationChangedData.isAutoAttackable = paramPlayer1.isAutoAttackable(paramPlayer2);
    
    relationChangedData.relation = paramPlayer1.getRelation(paramPlayer2);
    
    this._data.add(relationChangedData);
    
    if (this._data.size() == 1) {
      
      this.akO = 0;
    }
    else {
      
      this.akO = 4;
    } 
    return this;
  }
  public static final int RELATION_ENEMY = 4096; public static final int RELATION_ALLY = 2048; public static final int RELATION_ATTACKER = 1024; public static final int RELATION_IN_SIEGE = 512; public static final int RELATION_CLAN_MATE = 256;
  public static final int RELATION_LEADER = 128;
  
  protected void writeImpl() {
    writeC(206);
    writeC(this.akO);
    
    if (this.akO == 4) {
      
      writeH(this._data.size());
      for (RelationChangedData relationChangedData : this._data)
      {
        a(relationChangedData);
      }
    }
    else if (this.akO == 0) {
      
      RelationChangedData relationChangedData = (RelationChangedData)this._data.get(0);
      a(relationChangedData);
    } 
  }
  public static final int RELATION_CLAN_MEMBER = 64; public static final int RELATION_HAS_PARTY = 32; public static final int RELATION_PARTYLEADER = 16; public static final int RELATION_PARTY4 = 8; public static final int RELATION_PARTY3 = 4; public static final int RELATION_PARTY2 = 2; public static final int RELATION_PARTY1 = 1;
  
  private void a(RelationChangedData paramRelationChangedData) {
    writeD(paramRelationChangedData.objectId);
    writeD(paramRelationChangedData.relation);
    writeC(paramRelationChangedData.isAutoAttackable ? 1 : 0);
    writeD(paramRelationChangedData.karma);
    writeC(paramRelationChangedData.pvpFlag);
  }
}
