package l2.gameserver.network.l2.s2c;

import java.util.List;
import l2.gameserver.utils.Location;








public class ExCursedWeaponLocation
  extends L2GameServerPacket
{
  private List<CursedWeaponInfo> adZ;
  
  public ExCursedWeaponLocation(List<CursedWeaponInfo> paramList) { this.adZ = paramList; }



  
  protected final void writeImpl() {
    writeEx(72);
    
    if (this.adZ.isEmpty()) {
      writeD(0);
    } else {
      
      writeD(this.adZ.size());
      for (CursedWeaponInfo cursedWeaponInfo : this.adZ) {
        
        writeD(cursedWeaponInfo._id);
        writeD(cursedWeaponInfo._status);
        
        writeD(cursedWeaponInfo._pos.x);
        writeD(cursedWeaponInfo._pos.y);
        writeD(cursedWeaponInfo._pos.z);
      } 
    } 
  }

  
  public static class CursedWeaponInfo
  {
    public Location _pos;
    public int _id;
    public int _status;
    
    public CursedWeaponInfo(Location param1Location, int param1Int1, int param1Int2) {
      this._pos = param1Location;
      this._id = param1Int1;
      this._status = param1Int2;
    }
  }
}
