package l2.gameserver.network.l2.s2c;




public class ExAskJoinPartyRoom
  extends L2GameServerPacket
{
  private String adj;
  private String adk;
  
  public ExAskJoinPartyRoom(String paramString1, String paramString2) {
    this.adj = paramString1;
    this.adk = paramString2;
  }


  
  protected final void writeImpl() {
    writeEx(53);
    writeS(this.adj);
    writeS(this.adk);
  }
}
