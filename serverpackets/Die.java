package l2.gameserver.network.l2.s2c;

import java.util.HashMap;
import java.util.Map;
import l2.gameserver.Config;
import l2.gameserver.model.Creature;
import l2.gameserver.model.Player;
import l2.gameserver.model.Zone;
import l2.gameserver.model.base.RestartType;
import l2.gameserver.model.entity.events.GlobalEvent;
import l2.gameserver.model.instances.MonsterInstance;
import l2.gameserver.model.pledge.Clan;

public class Die extends L2GameServerPacket {
  private int Bq;
  private boolean aco;
  private boolean acp;
  private Map<RestartType, Boolean> acq;
  
  public Die(Creature paramCreature) {
    this.acq = new HashMap(RestartType.VALUES.length);


    
    this.Bq = paramCreature.getObjectId();
    this.aco = !paramCreature.isDead();
    
    if (paramCreature.isMonster()) {
      this.acp = ((MonsterInstance)paramCreature).isSweepActive();
    } else if (paramCreature.isPlayer()) {
      
      Player player = (Player)paramCreature;
      if (!player.isOlyCompetitionStarted() && !player.isResurectProhibited()) {
        
        a(RestartType.FIXED, (Config.ALT_REVIVE_WINDOW_TO_TOWN || (player.getPlayerAccess()).ResurectFixed || (Config.SERVICE_FEATHER_REVIVE_ENABLE && (!Config.SERVICE_DISABLE_FEATHER_ON_SIEGES_AND_EPIC || (!player.isOnSiegeField() && !player.isInZone(Zone.ZoneType.epic))) && player.getInventory().getCountOf(Config.SERVICE_FEATHER_ITEM_ID) > 0L && !player.getInventory().isLockedItem(Config.SERVICE_FEATHER_ITEM_ID))));
        a(RestartType.TO_VILLAGE, true);
        
        Clan clan = null;
        if (a(RestartType.TO_VILLAGE))
          clan = player.getClan(); 
        if (clan != null) {
          
          a(RestartType.TO_CLANHALL, (clan.getHasHideout() > 0));
          a(RestartType.TO_CASTLE, (clan.getCastle() > 0));
        } 
        
        for (GlobalEvent globalEvent : paramCreature.getEvents()) {
          globalEvent.checkRestartLocs(player, this.acq);
        }
      } 
    } 
  }

  
  protected final void writeImpl() {
    if (this.aco) {
      return;
    }
    writeC(0);
    writeD(this.Bq);
    writeD(a(RestartType.TO_VILLAGE));
    writeD(a(RestartType.TO_CLANHALL));
    writeD(a(RestartType.TO_CASTLE));
    writeD(a(RestartType.TO_FLAG));
    writeD(this.acp ? 1 : 0);
    writeD(a(RestartType.FIXED));
    writeD(a(RestartType.TO_FORTRESS));
    writeD(0);
    writeD(0);
    writeC(0);
    writeD(a(RestartType.AGATHION));
    writeD(0);
  }


  
  private void a(RestartType paramRestartType, boolean paramBoolean) { this.acq.put(paramRestartType, Boolean.valueOf(paramBoolean)); }


  
  private boolean a(RestartType paramRestartType) {
    Boolean bool = (Boolean)this.acq.get(paramRestartType);
    return (bool != null && bool.booleanValue());
  }
}
