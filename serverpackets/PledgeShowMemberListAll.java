package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.Config;
import l2.gameserver.model.pledge.Alliance;
import l2.gameserver.model.pledge.Clan;
import l2.gameserver.model.pledge.SubUnit;
import l2.gameserver.model.pledge.UnitMember;

public class PledgeShowMemberListAll
  extends L2GameServerPacket {
  private int akf;
  private int akg;
  private int _level;
  private int VM;
  private int VG;
  private int akh;
  private int aki;
  private int Vt;
  
  public PledgeShowMemberListAll(Clan paramClan, SubUnit paramSubUnit) {
    this.GA = paramSubUnit.getType();
    this.akf = paramClan.getClanId();
    this.akl = paramSubUnit.getName();
    this.abl = paramSubUnit.getLeaderName();
    this.akg = paramClan.getCrestId();
    this._level = paramClan.getLevel();
    this.Vt = paramClan.getCastle();
    this.akj = paramClan.getHasHideout();
    
    this.VM = paramClan.getRank();
    this.VG = paramClan.getReputationScore();
    this.akk = paramClan.isAtWarOrUnderAttack();
    
    this.akc = paramClan.isPlacedForDisband();
    
    Alliance alliance = paramClan.getAlliance();
    
    if (alliance != null) {
      
      this.akh = alliance.getAllyId();
      this.akm = alliance.getAllyName();
      this.aki = alliance.getAllyCrestId();
    } 
    
    this.EQ = new ArrayList(paramSubUnit.size());
    
    for (UnitMember unitMember : paramSubUnit.getUnitMembers())
      this.EQ.add(new PledgePacketMember(unitMember)); 
  }
  private int akj; private int akk; private String akl; private String abl; private String akm; private int GA; private int aiu; private boolean akc;
  private List<PledgePacketMember> EQ;
  
  protected final void writeImpl() {
    writeC(90);
    
    writeD((this.GA == 0));
    writeD(this.akf);
    writeD(Config.REQUEST_ID);
    writeD(this.GA);
    writeS(this.akl);
    writeS(this.abl);
    writeD(this.akg);
    writeD(this._level);
    writeD(this.Vt);
    writeD(0);
    writeD(this.akj);
    writeD(0);
    writeD(this.VM);
    writeD(this.VG);
    writeD(this.akc ? 3 : 0);
    writeD(0);
    writeD(this.akh);
    writeS(this.akm);
    writeD(this.aki);
    writeD(this.akk);
    writeD(0);
    
    writeD(this.EQ.size());
    for (PledgePacketMember pledgePacketMember : this.EQ) {
      
      writeS(pledgePacketMember._name);
      writeD(pledgePacketMember._level);
      writeD(pledgePacketMember.Bu);
      writeD(pledgePacketMember.BB);
      writeD(pledgePacketMember.Bt);
      writeD(pledgePacketMember.ake);
      writeD(pledgePacketMember.akn ? 1 : 0);
      writeC(0);
    } 
  }

  
  private class PledgePacketMember
  {
    private String _name;
    private int _level;
    private int Bu;
    private int BB;
    private int Bt;
    private int ake;
    private boolean akn;
    
    public PledgePacketMember(UnitMember param1UnitMember) {
      this._name = param1UnitMember.getName();
      this._level = param1UnitMember.getLevel();
      this.Bu = param1UnitMember.getClassId();
      this.BB = param1UnitMember.getSex();
      this.Bt = 0;
      this.ake = param1UnitMember.isOnline() ? param1UnitMember.getObjectId() : 0;
      this.akn = (param1UnitMember.getSponsor() != 0);
    }
  }
}
