package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.instances.DoorInstance;











public class DoorInfo
  extends L2GameServerPacket
{
  private int acr;
  private int acs;
  private int act;
  
  @Deprecated
  public DoorInfo(DoorInstance paramDoorInstance) {
    this.acr = paramDoorInstance.getObjectId();
    this.acs = paramDoorInstance.getDoorId();
    this.act = paramDoorInstance.isHPVisible() ? 1 : 0;
  }


  
  protected final void writeImpl() {
    writeC(76);
    writeD(this.acr);
    writeD(this.acs);
    writeD(this.act);
  }
}
