package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;







public class ExCubeGameChangeTeam
  extends L2GameServerPacket
{
  private int Bq;
  private boolean adW;
  
  public ExCubeGameChangeTeam(Player paramPlayer, boolean paramBoolean) {
    this.Bq = paramPlayer.getObjectId();
    this.adW = paramBoolean;
  }


  
  protected void writeImpl() {
    writeEx(151);
    writeD(5);
    
    writeD(this.Bq);
    writeD(this.adW ? 1 : 0);
    writeD(this.adW ? 0 : 1);
  }
}
