package l2.gameserver.network.l2.s2c;

public class StartPledgeWar
  extends L2GameServerPacket
{
  private String aaa;
  private String alH;
  
  public StartPledgeWar(String paramString1, String paramString2) {
    this.aaa = paramString1;
    this.alH = paramString2;
  }


  
  protected final void writeImpl() {
    writeC(99);
    writeS(this.alH);
    writeS(this.aaa);
  }
}
