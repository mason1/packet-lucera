package l2.gameserver.network.l2.s2c;

import l2.gameserver.utils.Location;




public class TargetSelected
  extends L2GameServerPacket
{
  private int Bq;
  private int _targetId;
  private Location _loc;
  
  public TargetSelected(int paramInt1, int paramInt2, Location paramLocation) {
    this.Bq = paramInt1;
    this._targetId = paramInt2;
    this._loc = paramLocation;
  }


  
  protected final void writeImpl() {
    writeC(35);
    writeD(this.Bq);
    writeD(this._targetId);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
    writeD(0);
  }
}
