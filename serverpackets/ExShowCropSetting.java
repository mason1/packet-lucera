package l2.gameserver.network.l2.s2c;

import java.util.List;
import l2.gameserver.data.xml.holder.ResidenceHolder;
import l2.gameserver.model.Manor;
import l2.gameserver.model.entity.residence.Castle;
import l2.gameserver.templates.manor.CropProcure;









public class ExShowCropSetting
  extends L2GameServerPacket
{
  private int EJ;
  private int _count;
  private long[] agv;
  
  public ExShowCropSetting(int paramInt) {
    this.EJ = paramInt;
    Castle castle = (Castle)ResidenceHolder.getInstance().getResidence(Castle.class, this.EJ);
    List list = Manor.getInstance().getCropsForCastle(this.EJ);
    this._count = list.size();
    this.agv = new long[this._count * 14];
    byte b = 0;
    for (Manor.SeedData seedData : list) {
      
      this.agv[b * 14 + 0] = seedData.getCrop();
      this.agv[b * 14 + 1] = seedData.getLevel();
      this.agv[b * 14 + 2] = seedData.getReward(1);
      this.agv[b * 14 + 3] = seedData.getReward(2);
      this.agv[b * 14 + 4] = seedData.getCropLimit();
      this.agv[b * 14 + 5] = 0L;
      int i = Manor.getInstance().getCropBasicPrice(seedData.getCrop());
      this.agv[b * 14 + 6] = (i * 60 / 100);
      this.agv[b * 14 + 7] = (i * 10);
      CropProcure cropProcure = castle.getCrop(seedData.getCrop(), 0);
      if (cropProcure != null) {
        
        this.agv[b * 14 + 8] = cropProcure.getStartAmount();
        this.agv[b * 14 + 9] = cropProcure.getPrice();
        this.agv[b * 14 + 10] = cropProcure.getReward();
      }
      else {
        
        this.agv[b * 14 + 8] = 0L;
        this.agv[b * 14 + 9] = 0L;
        this.agv[b * 14 + 10] = 0L;
      } 
      cropProcure = castle.getCrop(seedData.getCrop(), 1);
      if (cropProcure != null) {
        
        this.agv[b * 14 + 11] = cropProcure.getStartAmount();
        this.agv[b * 14 + 12] = cropProcure.getPrice();
        this.agv[b * 14 + 13] = cropProcure.getReward();
      }
      else {
        
        this.agv[b * 14 + 11] = 0L;
        this.agv[b * 14 + 12] = 0L;
        this.agv[b * 14 + 13] = 0L;
      } 
      b++;
    } 
  }


  
  public void writeImpl() {
    writeEx(43);
    
    writeD(this.EJ);
    writeD(this._count);
    
    for (byte b = 0; b < this._count; b++) {
      
      writeD((int)this.agv[b * 14 + 0]);
      writeD((int)this.agv[b * 14 + 1]);
      
      writeC(1);
      writeD((int)this.agv[b * 14 + 2]);
      
      writeC(1);
      writeD((int)this.agv[b * 14 + 3]);
      
      writeD((int)this.agv[b * 14 + 4]);
      writeD((int)this.agv[b * 14 + 5]);
      writeD((int)this.agv[b * 14 + 6]);
      writeD((int)this.agv[b * 14 + 7]);
      
      writeQ(this.agv[b * 14 + 8]);
      writeQ(this.agv[b * 14 + 9]);
      writeC((int)this.agv[b * 14 + 10]);
      writeQ(this.agv[b * 14 + 11]);
      writeQ(this.agv[b * 14 + 12]);
      
      writeC((int)this.agv[b * 14 + 13]);
    } 
  }
}
