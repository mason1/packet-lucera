package l2.gameserver.network.l2.s2c;

import l2.gameserver.utils.Location;

public class PlaySound
  extends L2GameServerPacket {
  public static final L2GameServerPacket SIEGE_VICTORY = new PlaySound("Siege_Victory");
  public static final L2GameServerPacket B04_S01 = new PlaySound("B04_S01"); private Type Nz; private String ajO; private int ajP; private int Bq;
  public static final L2GameServerPacket HB01 = new PlaySound(Type.MUSIC, "HB01", 0, 0, 0, 0, 0); private int _x;
  private int _y;
  private int _z;
  
  public enum Type { SOUND,
    MUSIC,
    VOICE; }









  
  public PlaySound(String paramString) { this(Type.SOUND, paramString, 0, 0, 0, 0, 0); }



  
  public PlaySound(Type paramType, String paramString, int paramInt1, int paramInt2, Location paramLocation) { this(paramType, paramString, paramInt1, paramInt2, (paramLocation == null) ? 0 : paramLocation.x, (paramLocation == null) ? 0 : paramLocation.y, (paramLocation == null) ? 0 : paramLocation.z); }


  
  public PlaySound(Type paramType, String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.Nz = paramType;
    this.ajO = paramString;
    this.ajP = paramInt1;
    this.Bq = paramInt2;
    this._x = paramInt3;
    this._y = paramInt4;
    this._z = paramInt5;
  }


  
  protected final void writeImpl() {
    writeC(158);
    
    writeD(this.Nz.ordinal());
    writeS(this.ajO);
    writeD(this.ajP);
    writeD(this.Bq);
    writeD(this._x);
    writeD(this._y);
    writeD(this._z);
  }
}
