package l2.gameserver.network.l2.s2c;






public class ExOlympiadMode
  extends L2GameServerPacket
{
  private int _mode;
  
  public ExOlympiadMode(int paramInt) { this._mode = paramInt; }



  
  protected final void writeImpl() {
    writeEx(125);
    
    writeC(this._mode);
  }
}
