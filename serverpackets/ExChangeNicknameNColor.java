package l2.gameserver.network.l2.s2c;


public class ExChangeNicknameNColor
  extends L2GameServerPacket
{
  private int _itemObjId;
  
  public ExChangeNicknameNColor(int paramInt) { this._itemObjId = paramInt; }



  
  protected void writeImpl() {
    writeEx(131);
    writeD(this._itemObjId);
  }
}
