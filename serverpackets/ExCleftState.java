package l2.gameserver.network.l2.s2c;

public class ExCleftState
  extends L2GameServerPacket
{
  public static final int CleftState_Total = 0;
  public static final int CleftState_TowerDestroy = 1;
  public static final int CleftState_CatUpdate = 2;
  public static final int CleftState_Result = 3;
  public static final int CleftState_PvPKill = 4;
  private int adU = 0;


  
  protected void writeImpl() {
    writeEx(149);
    writeD(this.adU);
    switch (this.adU) {
    
    } 
  }
}
