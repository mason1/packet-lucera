package l2.gameserver.network.l2.s2c;

public class ExDominionChannelSet
  extends L2GameServerPacket {
  public static final L2GameServerPacket ACTIVE = new ExDominionChannelSet(1);
  public static final L2GameServerPacket DEACTIVE = new ExDominionChannelSet(0);

  
  private int aec;

  
  public ExDominionChannelSet(int paramInt) { this.aec = paramInt; }



  
  protected void writeImpl() {
    writeEx(150);
    writeD(this.aec);
  }
}
