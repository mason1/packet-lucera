package l2.gameserver.network.l2.s2c;


public class PledgeReceiveUpdatePower
  extends L2GameServerPacket
{
  private int VN;
  
  public PledgeReceiveUpdatePower(int paramInt) { this.VN = paramInt; }



  
  protected final void writeImpl() {
    writeEx(67);
    writeD(this.VN);
  }
}
