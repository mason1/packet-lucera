package l2.gameserver.network.l2.s2c;

import java.util.Collections;
import java.util.Map;
import l2.gameserver.model.Player;


public class PackageToList
  extends L2GameServerPacket
{
  private Map<Integer, String> ajj;
  
  public PackageToList(Player paramPlayer) {
    this.ajj = Collections.emptyMap();


    
    this.ajj = paramPlayer.getAccountChars();
  }


  
  protected void writeImpl() {
    writeC(200);
    writeD(this.ajj.size());
    for (Map.Entry entry : this.ajj.entrySet()) {
      
      writeD(((Integer)entry.getKey()).intValue());
      writeS((CharSequence)entry.getValue());
    } 
  }
}
