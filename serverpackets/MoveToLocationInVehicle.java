package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.entity.boat.Boat;
import l2.gameserver.utils.Location;

public class MoveToLocationInVehicle extends L2GameServerPacket {
  private int aeK;
  private int YW;
  private Location aeR;
  private Location _destination;
  
  public MoveToLocationInVehicle(Player paramPlayer, Boat paramBoat, Location paramLocation1, Location paramLocation2) {
    this.aeK = paramPlayer.getObjectId();
    this.YW = paramBoat.getObjectId();
    this.aeR = paramLocation1;
    this._destination = paramLocation2;
  }


  
  protected final void writeImpl() {
    writeC(126);
    writeD(this.aeK);
    writeD(this.YW);
    writeD(this._destination.x);
    writeD(this._destination.y);
    writeD(this._destination.z);
    writeD(this.aeR.x);
    writeD(this.aeR.y);
    writeD(this.aeR.z);
  }
}
