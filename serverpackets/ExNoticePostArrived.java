package l2.gameserver.network.l2.s2c;





public class ExNoticePostArrived
  extends L2GameServerPacket
{
  public static final L2GameServerPacket STATIC_TRUE = new ExNoticePostArrived(1);
  public static final L2GameServerPacket STATIC_FALSE = new ExNoticePostArrived(0);

  
  private int DJ;

  
  public ExNoticePostArrived(int paramInt) { this.DJ = paramInt; }



  
  protected void writeImpl() {
    writeEx(170);
    
    writeD(this.DJ);
  }
}
