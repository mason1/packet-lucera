package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;

public class ExBR_GamePoint
  extends L2GameServerPacket
{
  private int Bq;
  private long adv;
  
  public ExBR_GamePoint(Player paramPlayer) {
    this.Bq = paramPlayer.getObjectId();
    this.adv = paramPlayer.getPremiumPoints();
  }


  
  protected void writeImpl() {
    writeEx(213);
    writeD(this.Bq);
    writeQ(this.adv);
    writeD(0);
  }
}
