package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.entity.boat.Boat;
import l2.gameserver.utils.Location;

public class ExGetOffAirShip
  extends L2GameServerPacket {
  private int aeK;
  private int YW;
  private Location _loc;
  
  public ExGetOffAirShip(Player paramPlayer, Boat paramBoat, Location paramLocation) {
    this.aeK = paramPlayer.getObjectId();
    this.YW = paramBoat.getObjectId();
    this._loc = paramLocation;
  }


  
  protected final void writeImpl() {
    writeEx(100);
    writeD(this.aeK);
    writeD(this.YW);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
  }
}
