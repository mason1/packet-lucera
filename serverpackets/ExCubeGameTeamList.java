package l2.gameserver.network.l2.s2c;

import java.util.List;
import l2.gameserver.model.Player;

















public class ExCubeGameTeamList
  extends L2GameServerPacket
{
  List<Player> _bluePlayers;
  List<Player> _redPlayers;
  int _roomNumber;
  
  public ExCubeGameTeamList(List<Player> paramList1, List<Player> paramList2, int paramInt) {
    this._redPlayers = paramList1;
    this._bluePlayers = paramList2;
    this._roomNumber = paramInt - 1;
  }


  
  protected void writeImpl() {
    writeEx(151);
    writeD(0);
    
    writeD(this._roomNumber);
    writeD(-1);
    
    writeD(this._bluePlayers.size());
    for (Player player : this._bluePlayers) {
      
      writeD(player.getObjectId());
      writeS(player.getName());
    } 
    writeD(this._redPlayers.size());
    for (Player player : this._redPlayers) {
      
      writeD(player.getObjectId());
      writeS(player.getName());
    } 
  }
}
