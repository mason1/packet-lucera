package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;













public class ExSetCompassZoneCode
  extends L2GameServerPacket
{
  public static final int ZONE_ALTERED = 8;
  public static final int ZONE_ALTERED2 = 9;
  public static final int ZONE_REMINDER = 10;
  public static final int ZONE_SIEGE = 11;
  public static final int ZONE_PEACE = 12;
  public static final int ZONE_SSQ = 13;
  public static final int ZONE_PVP = 14;
  public static final int ZONE_GENERAL_FIELD = 15;
  public static final int ZONE_PVP_FLAG = 16384;
  public static final int ZONE_ALTERED_FLAG = 256;
  public static final int ZONE_SIEGE_FLAG = 2048;
  public static final int ZONE_PEACE_FLAG = 4096;
  public static final int ZONE_SSQ_FLAG = 8192;
  private final int agr;
  
  public ExSetCompassZoneCode(Player paramPlayer) { this(paramPlayer.getZoneMask()); }


  
  public ExSetCompassZoneCode(int paramInt) {
    if ((paramInt & 0x100) == 256) {
      this.agr = 8;
    } else if ((paramInt & 0x800) == 2048) {
      this.agr = 11;
    } else if ((paramInt & 0x4000) == 16384) {
      this.agr = 14;
    } else if ((paramInt & 0x1000) == 4096) {
      this.agr = 12;
    } else if ((paramInt & 0x2000) == 8192) {
      this.agr = 13;
    } else {
      this.agr = 15;
    } 
  }

  
  protected final void writeImpl() {
    writeEx(51);
    writeD(this.agr);
  }
}
