package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.pledge.SubUnit;

public class PledgeReceiveSubPledgeCreated
  extends L2GameServerPacket {
  private int type;
  private String _name;
  private String leader_name;
  
  public PledgeReceiveSubPledgeCreated(SubUnit paramSubUnit) {
    this.type = paramSubUnit.getType();
    this._name = paramSubUnit.getName();
    this.leader_name = paramSubUnit.getLeaderName();
  }


  
  protected final void writeImpl() {
    writeEx(65);
    
    writeD(1);
    writeD(this.type);
    writeS(this._name);
    writeS(this.leader_name);
  }
}
