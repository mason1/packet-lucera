package l2.gameserver.network.l2.s2c;

import java.util.List;
import l2.gameserver.Config;
import l2.gameserver.data.xml.holder.ItemHolder;
import l2.gameserver.data.xml.holder.MultiSellHolder;
import l2.gameserver.model.base.Element;
import l2.gameserver.model.base.MultiSellEntry;
import l2.gameserver.model.base.MultiSellIngredient;
import l2.gameserver.templates.item.ItemTemplate;



public class MultiSellList
  extends L2GameServerPacket
{
  private final int _page;
  private final int ajc;
  private final int vU;
  private final List<MultiSellEntry> abd;
  
  public MultiSellList(MultiSellHolder.MultiSellListContainer paramMultiSellListContainer, int paramInt1, int paramInt2) {
    this.abd = paramMultiSellListContainer.getEntries();
    this.vU = paramMultiSellListContainer.getListId();
    this._page = paramInt1;
    this.ajc = paramInt2;
  }


  
  protected final void writeImpl() {
    writeC(208);
    writeC(0);
    writeD(this.vU);
    writeC(0);
    writeD(this._page);
    writeD(this.ajc);
    writeD(Config.MULTISELL_SIZE);
    writeD(this.abd.size());
    writeC(0);
    writeC(0);
    writeD(32);

    
    for (MultiSellEntry multiSellEntry : this.abd) {
      
      List list = multiSellEntry.getIngredients();
      
      writeD(multiSellEntry.getEntryId());
      writeC((!multiSellEntry.getProduction().isEmpty() && ((MultiSellIngredient)multiSellEntry.getProduction().get(0)).isStackable()) ? 1 : 0);
      writeH(0);
      writeD(0);
      writeD(0);
      
      writeItemElements();

      
      writeC(0);
      writeC(0);
      
      writeH(multiSellEntry.getProduction().size());
      writeH(list.size());
      
      for (MultiSellIngredient multiSellIngredient : multiSellEntry.getProduction()) {
        
        int i = multiSellIngredient.getItemId();
        ItemTemplate itemTemplate = (i > 0) ? ItemHolder.getInstance().getTemplate(multiSellIngredient.getItemId()) : null;
        writeD(i);
        writeQ((i > 0) ? itemTemplate.getBodyPart() : 0L);
        writeH((i > 0) ? itemTemplate.getType2ForPackets() : 0);
        writeQ(multiSellIngredient.getItemCount());
        writeH(multiSellIngredient.getItemEnchant());
        writeD(100);
        writeD(0);
        writeD(0);
        writeItemElements(multiSellIngredient);
        
        writeC(0);
        writeC(0);
      } 
      
      for (MultiSellIngredient multiSellIngredient : list) {
        
        int i = multiSellIngredient.getItemId();
        ItemTemplate itemTemplate = (i > 0) ? ItemHolder.getInstance().getTemplate(multiSellIngredient.getItemId()) : null;
        writeD(i);
        writeH((i > 0) ? itemTemplate.getType2() : 65535);
        writeQ(multiSellIngredient.getItemCount());
        writeH(multiSellIngredient.getItemEnchant());
        writeD(0);
        writeD(0);
        writeItemElements(multiSellIngredient);
        
        writeC(0);
        writeC(0);
      } 
    } 
  }

  
  protected void writeItemElements(MultiSellIngredient paramMultiSellIngredient) {
    if (paramMultiSellIngredient.getItemId() <= 0) {
      
      writeItemElements();
      return;
    } 
    ItemTemplate itemTemplate = ItemHolder.getInstance().getTemplate(paramMultiSellIngredient.getItemId());
    if (paramMultiSellIngredient.getItemAttributes().getValue() > 0) {
      
      if (itemTemplate.isWeapon())
      {
        Element element = paramMultiSellIngredient.getItemAttributes().getElement();
        writeH(element.getId());
        writeH(paramMultiSellIngredient.getItemAttributes().getValue(element) + itemTemplate.getBaseAttributeValue(element));
        writeH(0);
        writeH(0);
        writeH(0);
        writeH(0);
        writeH(0);
        writeH(0);
      }
      else if (itemTemplate.isArmor())
      {
        writeH(-1);
        writeH(0);
        for (Element element : Element.VALUES)
        {
          writeH(paramMultiSellIngredient.getItemAttributes().getValue(element) + itemTemplate.getBaseAttributeValue(element));
        }
      }
      else
      {
        writeItemElements();
      }
    
    } else {
      
      writeItemElements();
    } 
  }

  
  protected void writeItemElements() {
    writeH(-1);
    writeH(0);
    writeH(0);
    writeH(0);
    writeH(0);
    writeH(0);
    writeH(0);
    writeH(0);
  }
}
