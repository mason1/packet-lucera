package l2.gameserver.network.l2.s2c;

public class ExDivideAdenaDone
  extends L2GameServerPacket
{
  private final int aea;
  private final long Be;
  private final long aeb;
  private final String _name;
  
  public ExDivideAdenaDone(int paramInt, long paramLong1, long paramLong2, String paramString) {
    this.aea = paramInt;
    this.Be = paramLong1;
    this.aeb = paramLong2;
    this._name = paramString;
  }


  
  protected final void writeImpl() {
    writeEx(349);
    writeC(1);
    writeC(0);
    writeD(this.aea);
    writeQ(this.aeb);
    writeQ(this.Be);
    writeS(this._name);
  }
}
