package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Effect;






















public class ShortBuffStatusUpdate
  extends L2GameServerPacket
{
  int _skillId;
  int _skillLevel;
  int _skillDuration;
  
  public ShortBuffStatusUpdate(Effect paramEffect) {
    this._skillId = paramEffect.getSkill().getDisplayId();
    this._skillLevel = paramEffect.getSkill().getDisplayLevel();
    this._skillDuration = paramEffect.getTimeLeft();
  }




  
  public ShortBuffStatusUpdate() {
    this._skillId = 0;
    this._skillLevel = 0;
    this._skillDuration = 0;
  }


  
  protected final void writeImpl() {
    writeC(250);
    writeD(this._skillId);
    writeD(this._skillLevel);
    writeD(this._skillDuration);
  }
}
