package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.actor.instances.player.ShortCut;


public class ShortCutRegister
  extends ShortCutPacket
{
  private ShortCutPacket.ShortcutInfo aln;
  
  public ShortCutRegister(Player paramPlayer, ShortCut paramShortCut) { this.aln = convert(paramPlayer, paramShortCut); }



  
  protected final void writeImpl() {
    writeC(68);
    
    this.aln.write(this);
  }
}
