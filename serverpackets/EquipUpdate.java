package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.ItemInstance;


























@Deprecated
public class EquipUpdate
  extends L2GameServerPacket
{
  private ItemInfo acG;
  
  public EquipUpdate(ItemInstance paramItemInstance, int paramInt) {
    this.acG = new ItemInfo(paramItemInstance);
    this.acG.setLastChange(paramInt);
  }


  
  protected final void writeImpl() {
    writeC(75);
    writeD(this.acG.getLastChange());
    writeD(this.acG.getObjectId());
    writeD(this.acG.getEquipSlot());
  }
}
