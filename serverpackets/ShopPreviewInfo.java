package l2.gameserver.network.l2.s2c;

import java.util.Map;
import l2.gameserver.model.items.Inventory;



public class ShopPreviewInfo
  extends L2GameServerPacket
{
  private Map<Integer, Integer> alf;
  
  public ShopPreviewInfo(Map<Integer, Integer> paramMap) { this.alf = paramMap; }



  
  protected void writeImpl() {
    writeC(246);
    writeD(59);

    
    for (int i : Inventory.PAPERDOLL_ORDER) {
      writeD(W(i));
    }
  }

  
  private int W(int paramInt) { return (this.alf.get(Integer.valueOf(paramInt)) != null) ? ((Integer)this.alf.get(Integer.valueOf(paramInt))).intValue() : 0; }
}
