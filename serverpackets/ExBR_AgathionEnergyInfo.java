package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.items.ItemInstance;

public class ExBR_AgathionEnergyInfo
  extends L2GameServerPacket {
  private int _size;
  private ItemInstance[] adl;
  
  public ExBR_AgathionEnergyInfo(int paramInt, ItemInstance... paramVarArgs) {
    this.adl = null;


    
    this.adl = paramVarArgs;
    this._size = paramInt;
  }


  
  protected void writeImpl() {
    writeEx(222);
    writeD(this._size);
    for (ItemInstance itemInstance : this.adl) {


      
      writeD(itemInstance.getObjectId());
      writeD(itemInstance.getItemId());
      writeD(2097152);
      writeD(0);
      writeD(0);
    } 
  }
}
