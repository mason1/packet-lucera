package l2.gameserver.network.l2.s2c;









public class L2FriendSay
  extends L2GameServerPacket
{
  private String IX;
  private String aiN;
  private String _message;
  
  public L2FriendSay(String paramString1, String paramString2, String paramString3) {
    this.IX = paramString1;
    this.aiN = paramString2;
    this._message = paramString3;
  }


  
  protected final void writeImpl() {
    writeC(120);
    writeD(0);
    writeS(this.aiN);
    writeS(this.IX);
    writeS(this._message);
  }
}
