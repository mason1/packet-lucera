package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import l2.gameserver.model.entity.boat.Boat;
import l2.gameserver.utils.Location;

public class ExMoveToLocationInAirShip extends L2GameServerPacket {
  private int char_id;
  private int aeS;
  private Location aeR;
  private Location _destination;
  
  public ExMoveToLocationInAirShip(Player paramPlayer, Boat paramBoat, Location paramLocation1, Location paramLocation2) {
    this.char_id = paramPlayer.getObjectId();
    this.aeS = paramBoat.getObjectId();
    this.aeR = paramLocation1;
    this._destination = paramLocation2;
  }


  
  protected final void writeImpl() {
    writeEx(109);
    writeD(this.char_id);
    writeD(this.aeS);
    
    writeD(this._destination.x);
    writeD(this._destination.y);
    writeD(this._destination.z);
    writeD(this.aeR.x);
    writeD(this.aeR.y);
    writeD(this.aeR.z);
  }
}
