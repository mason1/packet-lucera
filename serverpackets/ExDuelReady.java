package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.entity.events.impl.DuelEvent;


public class ExDuelReady
  extends L2GameServerPacket
{
  private int YA;
  
  public ExDuelReady(DuelEvent paramDuelEvent) { this.YA = paramDuelEvent.getDuelType(); }



  
  protected final void writeImpl() {
    writeEx(78);
    writeD(this.YA);
  }
}
