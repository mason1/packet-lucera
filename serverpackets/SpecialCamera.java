package l2.gameserver.network.l2.s2c;

public class SpecialCamera
  extends L2GameServerPacket
{
  private int _id;
  private int aeU;
  private int alC;
  private int alD;
  private int _time;
  private int aaw;
  private final int alE;
  private final int alF;
  private final int alG;
  private final int XA;
  
  public SpecialCamera(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    this._id = paramInt1;
    this.aeU = paramInt2;
    this.alC = paramInt3;
    this.alD = paramInt4;
    this._time = paramInt5;
    this.aaw = paramInt6;
    this.alE = 0;
    this.alF = 0;
    this.alG = 0;
    this.XA = 0;
  }

  
  public SpecialCamera(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10) {
    this._id = paramInt1;
    this.aeU = paramInt2;
    this.alC = paramInt3;
    this.alD = paramInt4;
    this._time = paramInt5;
    this.aaw = paramInt6;
    this.alE = paramInt7;
    this.alF = paramInt8;
    this.alG = paramInt9;
    this.XA = paramInt10;
  }


  
  protected final void writeImpl() {
    writeC(214);
    
    writeD(this._id);
    writeD(this.aeU);
    writeD(this.alC);
    writeD(this.alD);
    writeD(this._time);
    writeD(this.aaw);
    writeD(this.alE);
    writeD(this.alF);
    writeD(this.alG);
    writeD(this.XA);
  }
}
