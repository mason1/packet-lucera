package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.instances.NpcInstance;







public class MonRaceInfo
  extends L2GameServerPacket
{
  private int aiX;
  private int aiY;
  private NpcInstance[] aiZ;
  private int[][] aja;
  
  public MonRaceInfo(int paramInt1, int paramInt2, NpcInstance[] paramArrayOfNpcInstance, int[][] paramArrayOfInt) {
    this.aiX = paramInt1;
    this.aiY = paramInt2;
    this.aiZ = paramArrayOfNpcInstance;
    this.aja = paramArrayOfInt;
  }


  
  protected final void writeImpl() {
    writeC(227);
    
    writeD(this.aiX);
    writeD(this.aiY);
    writeD(8);
    
    for (byte b = 0; b < 8; b++) {

      
      writeD(this.aiZ[b].getObjectId());
      writeD((this.aiZ[b].getTemplate()).npcId + 1000000);
      writeD(14107);
      writeD(181875 + 58 * (7 - b));
      writeD(-3566);
      writeD(12080);
      writeD(181875 + 58 * (7 - b));
      writeD(-3566);
      writeF(this.aiZ[b].getColHeight());
      writeF(this.aiZ[b].getColRadius());
      writeD(120);
      for (byte b1 = 0; b1 < 20; b1++)
        writeC((this.aiX == 0) ? this.aja[b][b1] : 0); 
    } 
  }
}
