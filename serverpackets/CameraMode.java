package l2.gameserver.network.l2.s2c;








public class CameraMode
  extends L2GameServerPacket
{
  int _mode;
  
  public CameraMode(int paramInt) { this._mode = paramInt; }



  
  protected final void writeImpl() {
    writeC(247);
    writeD(this._mode);
  }
}
