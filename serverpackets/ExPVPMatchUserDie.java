package l2.gameserver.network.l2.s2c;

public class ExPVPMatchUserDie
  extends L2GameServerPacket {
  private int afw;
  private int afx;
  
  public ExPVPMatchUserDie(int paramInt1, int paramInt2) {
    this.afw = paramInt1;
    this.afx = paramInt2;
  }


  
  protected final void writeImpl() {
    writeEx(127);
    writeD(this.afw);
    writeD(this.afx);
  }
}
