package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import l2.gameserver.Config;
import l2.gameserver.data.xml.holder.BuyListHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInstance;
import l2.gameserver.model.items.TradeItem;

public abstract class ExBuySellList
  extends AbstractItemListPacket {
  protected int _type;
  protected int _slots;
  
  public static class BuyList extends ExBuySellList {
    private final int vU;
    private final List<TradeItem> Go;
    private final long abb;
    private final double abc;
    
    public BuyList(BuyListHolder.NpcTradeList param1NpcTradeList, Player param1Player, double param1Double) {
      super(0, param1Player);
      this.abb = param1Player.getAdena();
      this.abc = param1Double;
      
      if (param1NpcTradeList != null) {
        
        this.vU = param1NpcTradeList.getListId();
        this.Go = param1NpcTradeList.getItems();
        param1Player.setBuyListId(this.vU);
      }
      else {
        
        this.vU = 0;
        this.Go = Collections.emptyList();
        param1Player.setBuyListId(0);
      } 
    }


    
    protected void writeImpl() {
      super.writeImpl();
      writeQ(this.abb);
      writeD(this.vU);
      writeD(this._slots);
      writeH(this.Go.size());
      for (TradeItem tradeItem : this.Go) {
        
        writeItemInfo(tradeItem, tradeItem.getCurrentValue());
        writeQ((long)(tradeItem.getOwnersPrice() * (1.0D + this.abc)));
      } 
    }
  }
  
  public static class SellRefundList
    extends ExBuySellList
  {
    private final List<TradeItem> Gl;
    private final List<TradeItem> adH;
    private int adI;
    
    public SellRefundList(Player param1Player, boolean param1Boolean) {
      super(1, param1Player);
      this.adI = param1Boolean ? 1 : 0;
      if (param1Boolean) {
        
        this.adH = Collections.emptyList();
        this.Gl = Collections.emptyList();
      }
      else {
        
        ItemInstance[] arrayOfItemInstance = param1Player.getRefund().getItems();
        this.adH = new ArrayList(arrayOfItemInstance.length);
        for (ItemInstance itemInstance : arrayOfItemInstance) {
          this.adH.add(new TradeItem(itemInstance));
        }
        arrayOfItemInstance = param1Player.getInventory().getItems();
        this.Gl = new ArrayList(arrayOfItemInstance.length);
        for (ItemInstance itemInstance : arrayOfItemInstance) {
          if (itemInstance.canBeSold(param1Player)) {
            this.Gl.add(new TradeItem(itemInstance));
          }
        } 
      } 
    }
    
    protected void writeImpl() {
      super.writeImpl();
      writeD(this._slots);
      writeH(this.Gl.size());
      for (TradeItem tradeItem : this.Gl) {
        
        writeItemInfo(tradeItem);
        writeQ(Math.max(1L, tradeItem.getReferencePrice() / Config.ALT_SHOP_REFUND_SELL_DIVISOR));
      } 
      writeH(this.adH.size());
      for (TradeItem tradeItem : this.adH) {
        
        writeItemInfo(tradeItem);
        writeD(tradeItem.getObjectId());
        writeQ(tradeItem.getCount() * tradeItem.getReferencePrice() / 2L);
      } 
      writeC(this.adI);
    }
  }



  
  public ExBuySellList(int paramInt, Player paramPlayer) {
    this._type = paramInt;
    this._slots = paramPlayer.getInventory().getSize();
  }


  
  protected void writeImpl() {
    writeEx(184);
    writeD(this._type);
  }
}
