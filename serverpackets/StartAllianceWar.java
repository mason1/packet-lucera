package l2.gameserver.network.l2.s2c;

public class StartAllianceWar
  extends L2GameServerPacket
{
  private String akm;
  private String alH;
  
  public StartAllianceWar(String paramString1, String paramString2) {
    this.akm = paramString1;
    this.alH = paramString2;
  }


  
  protected final void writeImpl() {
    writeC(194);
    writeS(this.alH);
    writeS(this.akm);
  }
}
