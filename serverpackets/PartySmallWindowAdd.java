package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Party;
import l2.gameserver.model.Player;

public class PartySmallWindowAdd
  extends L2GameServerPacket {
  private int Bq;
  private int Vk;
  private final PartySmallWindowAll.PartySmallWindowMemberInfo ajm;
  
  public PartySmallWindowAdd(Party paramParty, Player paramPlayer) {
    this.Bq = paramParty.getPartyLeader().getObjectId();
    this.Vk = paramParty.getLootDistribution();
    this.ajm = new PartySmallWindowAll.PartySmallWindowMemberInfo(paramPlayer);
  }


  
  protected final void writeImpl() {
    writeC(79);
    writeD(this.Bq);
    writeD(this.Vk);
    writeD(this.ajm._id);
    writeS(this.ajm._name);
    
    writeD(this.ajm.curCp);
    writeD(this.ajm.maxCp);
    writeD(this.ajm.curHp);
    writeD(this.ajm.maxHp);
    writeD(this.ajm.curMp);
    writeD(this.ajm.maxMp);
    writeD(0);
    writeC(this.ajm.level);
    writeD(this.ajm.class_id);
    writeC(0);
    writeH(this.ajm.race_id);
  }
}
