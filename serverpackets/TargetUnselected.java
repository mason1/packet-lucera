package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.GameObject;
import l2.gameserver.utils.Location;




public class TargetUnselected
  extends L2GameServerPacket
{
  private int _targetId;
  private Location _loc;
  
  public TargetUnselected(GameObject paramGameObject) {
    this._targetId = paramGameObject.getObjectId();
    this._loc = paramGameObject.getLoc();
  }


  
  protected final void writeImpl() {
    writeC(36);
    writeD(this._targetId);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
    writeD(0);
  }
}
