package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Creature;






public class ChangeWaitType
  extends L2GameServerPacket
{
  private int Bq;
  private int YS;
  private int _x;
  private int _y;
  private int _z;
  public static final int WT_SITTING = 0;
  public static final int WT_STANDING = 1;
  public static final int WT_START_FAKEDEATH = 2;
  public static final int WT_STOP_FAKEDEATH = 3;
  
  public ChangeWaitType(Creature paramCreature, int paramInt) {
    this.Bq = paramCreature.getObjectId();
    this.YS = paramInt;
    this._x = paramCreature.getX();
    this._y = paramCreature.getY();
    this._z = paramCreature.getZ();
  }


  
  protected final void writeImpl() {
    writeC(41);
    writeD(this.Bq);
    writeD(this.YS);
    writeD(this._x);
    writeD(this._y);
    writeD(this._z);
  }
}
