package l2.gameserver.network.l2.s2c;


public class GMHide
  extends L2GameServerPacket
{
  private final int acr;
  
  public GMHide(int paramInt) { this.acr = paramInt; }



  
  protected void writeImpl() {
    writeC(147);
    writeD(this.acr);
  }
}
