package l2.gameserver.network.l2.s2c;

import l2.gameserver.utils.Location;

public class ExAirShipInfo
  extends L2GameServerPacket {
  private int abx;
  private int NV;
  private int NW;
  private int NX;
  
  protected final void writeImpl() {
    writeEx(96);
    
    writeD(this.abx);
    writeD(this._loc.x);
    writeD(this._loc.y);
    writeD(this._loc.z);
    writeD(this._loc.h);
    writeD(this.adf);
    writeD(this.NV);
    writeD(this.NW);
    writeD(this.adg);
    
    if (this.adg != 0) {
      
      writeD(366);
      writeD(0);
      writeD(107);
      writeD(348);
      writeD(0);
      writeD(105);
    }
    else {
      
      writeD(0);
      writeD(0);
      writeD(0);
      writeD(0);
      writeD(0);
      writeD(0);
    } 
    
    writeD(this.NX);
    writeD(this.ade);
  }
  
  private int ade;
  private int adf;
  private int adg;
  private Location _loc;
}
