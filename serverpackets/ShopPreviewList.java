package l2.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;
import l2.gameserver.data.xml.holder.BuyListHolder;
import l2.gameserver.model.Player;
import l2.gameserver.model.items.ItemInfo;
import l2.gameserver.model.items.TradeItem;
import l2.gameserver.templates.item.ItemTemplate;


public class ShopPreviewList
  extends L2GameServerPacket
{
  private int vU;
  private List<ItemInfo> aji;
  private long abe;
  
  public ShopPreviewList(BuyListHolder.NpcTradeList paramNpcTradeList, Player paramPlayer) {
    this.vU = paramNpcTradeList.getListId();
    this.abe = paramPlayer.getAdena();
    List list = paramNpcTradeList.getItems();
    this.aji = new ArrayList(list.size());
    for (TradeItem tradeItem : paramNpcTradeList.getItems()) {
      if (tradeItem.getItem().isEquipable()) {
        this.aji.add(tradeItem);
      }
    } 
  }
  
  protected final void writeImpl() {
    writeC(245);
    writeD(5056);
    writeQ(this.abe);
    writeD(this.vU);
    writeH(this.aji.size());
    
    for (ItemInfo itemInfo : this.aji) {
      if (itemInfo.getItem().isEquipable()) {
        
        writeD(itemInfo.getItemId());
        writeH(itemInfo.getItem().getType2ForPackets());
        writeQ(itemInfo.getItem().isEquipable() ? itemInfo.getItem().getBodyPart() : 0L);
        writeQ(getWearPrice(itemInfo.getItem()));
      } 
    } 
  }
  
  public static int getWearPrice(ItemTemplate paramItemTemplate) {
    switch (paramItemTemplate.getItemGrade()) {
      
      case D:
        return 50;
      case C:
        return 100;
      
      case B:
        return 200;
      case A:
        return 500;
      case S:
        return 1000;
    } 
    return 10;
  }
}
