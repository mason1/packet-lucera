package l2.gameserver.network.l2.s2c;

import l2.gameserver.network.l2.s2c.mask.IUpdateTypeComponent;

public static enum PartySmallWindowUpdateType
  implements IUpdateTypeComponent {
  CURRENT_CP(1),
  MAX_CP(2),
  CURRENT_HP(4),
  MAX_HP(8),
  CURRENT_MP(16),
  MAX_MP(32),
  LEVEL(64),
  CLASS_ID(128),
  PARTY_SUBSTITUTE(256),
  VITALITY_POINTS(512);
  static  {
    VALUES = values();
  }
  
  public static final PartySmallWindowUpdateType[] VALUES;
  private final int VK;
  
  PartySmallWindowUpdateType(int paramInt1) { this.VK = paramInt1; }




  
  public int getMask() { return this.VK; }
}
