package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;
import org.apache.commons.lang3.StringUtils;





public class PrivateStoreMsgSell
  extends L2GameServerPacket
{
  private final int abx;
  private final String _name;
  private boolean akv;
  
  public PrivateStoreMsgSell(Player paramPlayer) {
    this.abx = paramPlayer.getObjectId();
    this.akv = (paramPlayer.getPrivateStoreType() == 8);
    this._name = StringUtils.defaultString(paramPlayer.getSellStoreName());
  }


  
  protected final void writeImpl() {
    if (this.akv) {
      
      writeEx(128);
    } else {
      
      writeC(162);
    }  writeD(this.abx);
    writeS(this._name);
  }
}
