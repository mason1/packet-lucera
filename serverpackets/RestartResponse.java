package l2.gameserver.network.l2.s2c;

public class RestartResponse
  extends L2GameServerPacket {
  public static final RestartResponse OK = new RestartResponse(1); public static final RestartResponse FAIL = new RestartResponse(0);
  
  private String _message;
  private int Mx;
  
  public RestartResponse(int paramInt) {
    this._message = "bye";
    this.Mx = paramInt;
  }


  
  protected final void writeImpl() {
    writeC(113);
    writeD(this.Mx);
    writeS(this._message);
  }
}
