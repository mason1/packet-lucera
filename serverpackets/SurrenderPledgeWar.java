package l2.gameserver.network.l2.s2c;

public class SurrenderPledgeWar
  extends L2GameServerPacket
{
  private String aaa;
  private String alH;
  
  public SurrenderPledgeWar(String paramString1, String paramString2) {
    this.aaa = paramString1;
    this.alH = paramString2;
  }


  
  protected final void writeImpl() {
    writeC(103);
    writeS(this.aaa);
    writeS(this.alH);
  }
}
