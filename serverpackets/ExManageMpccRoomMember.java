package l2.gameserver.network.l2.s2c;

import l2.gameserver.instancemanager.MatchingRoomManager;
import l2.gameserver.model.Player;
import l2.gameserver.model.matching.MatchingRoom;



public class ExManageMpccRoomMember
  extends L2GameServerPacket
{
  public static int ADD_MEMBER = 0;
  public static int UPDATE_MEMBER = 1;
  public static int REMOVE_MEMBER = 2;
  
  private int _type;
  
  private MpccRoomMemberInfo aeQ;
  
  public ExManageMpccRoomMember(int paramInt, MatchingRoom paramMatchingRoom, Player paramPlayer) {
    this._type = paramInt;
    this.aeQ = new MpccRoomMemberInfo(paramPlayer, paramMatchingRoom.getMemberType(paramPlayer));
  }


  
  protected void writeImpl() {
    writeEx(159);
    writeD(this._type);
    writeD(this.aeQ.objectId);
    writeS(this.aeQ.name);
    writeD(this.aeQ.level);
    writeD(this.aeQ.classId);
    writeD(this.aeQ.location);
    writeD(this.aeQ.memberType);
  }

  
  static class MpccRoomMemberInfo
  {
    public final int objectId;
    public final int classId;
    public final int level;
    public final int location;
    public final int memberType;
    public final String name;
    
    public MpccRoomMemberInfo(Player param1Player, int param1Int) {
      this.objectId = param1Player.getObjectId();
      this.name = param1Player.getName();
      this.classId = param1Player.getClassId().ordinal();
      this.level = param1Player.getLevel();
      this.location = MatchingRoomManager.getInstance().getLocation(param1Player);
      this.memberType = param1Int;
    }
  }
}
