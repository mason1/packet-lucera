package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.pledge.UnitMember;


public class PledgeShowMemberListAdd
  extends L2GameServerPacket
{
  private PledgePacketMember akd;
  
  public PledgeShowMemberListAdd(UnitMember paramUnitMember) { this.akd = new PledgePacketMember(paramUnitMember); }



  
  protected final void writeImpl() {
    writeC(92);
    writeS(this.akd._name);
    writeD(this.akd._level);
    writeD(this.akd.Bu);
    writeD(this.akd.BB);
    writeD(this.akd.Bt);
    writeD(this.akd.ake);
    writeD(this.akd.GA);
  }

  
  private class PledgePacketMember
  {
    private String _name;
    private int _level;
    private int Bu;
    private int BB;
    private int Bt;
    private int ake;
    private int GA;
    
    public PledgePacketMember(UnitMember param1UnitMember) {
      this._name = param1UnitMember.getName();
      this._level = param1UnitMember.getLevel();
      this.Bu = param1UnitMember.getClassId();
      this.BB = param1UnitMember.getSex();
      this.Bt = 0;
      this.ake = param1UnitMember.isOnline() ? param1UnitMember.getObjectId() : 0;
      this.GA = param1UnitMember.getPledgeType();
    }
  }
}
