package l2.gameserver.network.l2.s2c;

import l2.gameserver.model.Player;

public class FriendAddRequestResult
  extends L2GameServerPacket
{
  private final int oH;
  private final int Br;
  private final String adj;
  private final int ahA;
  private final int ahB;
  private final int ahC;
  private final int ahD;
  
  public FriendAddRequestResult(Player paramPlayer, int paramInt) {
    this.oH = paramInt;
    this.Br = paramPlayer.getObjectId();
    this.adj = paramPlayer.getName();
    this.ahA = paramPlayer.isOnline() ? 1 : 0;
    this.ahB = paramPlayer.getObjectId();
    this.ahC = paramPlayer.getLevel();
    this.ahD = paramPlayer.getActiveClassId();
  }


  
  protected void writeImpl() {
    writeC(85);
    
    writeD(this.oH);
    writeD(this.Br);
    writeS(this.adj);
    writeD(this.ahA);
    writeD(this.ahB);
    writeD(this.ahC);
    writeD(this.ahD);
    writeH(0);
  }
}
